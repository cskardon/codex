Match all facts that inherit from 'Verb' and bring back the hypernodes
======================================================================

match verbs=(f:Fact)-[:IS_A*]->(:Category)-[]-(cat:Category { Name: "Verb" }) 
match agents=f<-[:AGENT_OF]-()
match entities=f-[]->()
return verbs, agents, entities

Find all agent acts and include images of linked entities
=========================================================

match x=(:Agent { Name: "Leonardo da Vinci" } )-[:AGENT_OF]->(:Act)-[r]->(e)
with e,x
optional match x2=(e)-[r2:HAS_IMAGE]->(image:Image)
where image.Primary
return x,x2

Find all agents who share the same attributes
=============================================

match (a1:Agent)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c1:Category)
with c1, a1
match x=(a2:Agent)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c2:Category)
where c2.Guid = c1.Guid and a2.Name <> a1.Name
return x

match (a1:Agent)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c1:Category)
with c1, a1
match x=(a2:Agent)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c2:Category)
where c2.Guid = c1.Guid and a2.Name <> a1.Name
return a2.Name, a1.Name, collect(distinct(c2.Name)) as categories order by a2.Name

Find all agents who share the same attributes as 'Leonardo da Vinci' and rank them by the number that they share
================================================================================================================
match (a1:Agent {Name:"Leonardo da Vinci"})-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c1:Category)
with c1, a1
match x=(a2:Agent)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c2:Category)
where c2.Guid = c1.Guid and a2.Name <> a1.Name
with a2, a1, collect(distinct(c2.Name)) as categories
return a1.Name, a2.Name, categories, length(categories) as total order by total desc, a2.Name

And only display those who share more than one attribute in common
==================================================================
match (a1:Agent {Name:"Michelangelo"})-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c1:Category)
with c1, a1
match x=(a2:Agent)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c2:Category)
where c2.Guid = c1.Guid and a2.Name <> a1.Name
with a2, a1, collect(distinct(c2.Name)) as categories
where length(categories) > 1
return a1.Name, a2.Name, categories, length(categories) as total order by total desc, a2.Name

With path cost
==============

match (a1:Artefact)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(t)-[:KIND_OF*0..3]->(c1:Category)
with c1, a1, t
match x=(a2:Artefact)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(t1)-[:KIND_OF*0..3]->(c2:Category)
where c2.Guid = c1.Guid and a2.Name <> a1.Name 
with a2, a1, collect(distinct(c2.Name)) as categories2,  collect(distinct(a2.Name)) as names, length(x) as pathCost
where pathCost <= 4
return a1.Name, names,  categories2, pathCost order by pathCost, length(categories2) desc, a2.Name



(Agent)-[:agent_of]->(Act { Name: "Born" })-[:at_place]->(Place)

(Agent)-[:subject_of?]->(Event { Name: "Life" })<-[:part_of???]-(Act { Name: "Born" })
											    <-[:part_of???]-(Act { Name: "Death" })

(leonardo:Agent)-[:agent_of]->(leonardoBirth:Event)-[:subset_of]->(leonardoLife:Event)-[:subset_of]->(history:Event)
												   -[:at_place]->(anchiano:Place)

Logical relations
=================
(Leonardo)-[:maker_of]->(Mona Lisa)
												
Lorenzo de Medici commissioned the Battle of the Centaurs by Michelangelo

(Lorenzo)-[:has]->(Relation)-[:is_a]->(Category { Name: "Prince" })
(Lorenzo)-[:has]->(Relation { Year: "1486" })-[:commissioned]->(Artefact { Name: "Battle of the Centaurs" })<-[:made_by]-(Relation { Type, "MadeBy", Year: "1487" })<-[:has]-(Michelangelo)
										     -[:asserted_by]->(User)
											 -[:cited_in]->(Artefact { Name: "Vasari's Lives"})
											 -[:subset_of]->(Relation { Name: "The Patronage of Lorenzo de Medici" })<-[:has]-(Lorenzo)

(Ludovico)-[:has]-(Relation)-[:sibling_of]->(Michelangelo)  -- NOTE: no year is appropriate here as this is a genetic relationship
(Michelangelo)-[:has]-(Relation { Year: "1488" })-[:worked_on]->(Artefact { Name: "Battle of the Centaurs" })
(Michelangelo)-[:has]-(Relation { Year: "1505" })-[:worked_on]->(Artefact { Name: "Battle of the Centaurs" })
												 -[:at_place]->(Rome)
												 -[:subset_of]->(Relation { Type: "MadeBy", Year: "1487" })-[:subset_of]->(Relation { Type: "Ouevre" })<-[:has]-(Michelangelo)


(Leonardo:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Art:Category)-[:IS_A]->(UniversalInterest:Category)
(Leonardo:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Painting:Category)-[:IS_A]->(UniversalInterest:Category)
(Leonardo:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Invention:Category)
(Michelangelo:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Art:Category)
(Michelangelo:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Painting:Category)
(Michelangelo:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Sculpture:Category)-[:IS_A]->(UniversalInterest:Category)
(Michelangelo:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Architecture:Category)-[:IS_A]->(UniversalInterest:Category)
(Michelangelo:Agent)-[:RECOGNISABILITY { Rank: 3 }]->(Poetry:Category)
(Michelangelo:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(ItalianRenaissancePoetry:Category)-[:IS_A]->(Poetry:Category)
																						-[:QUALIFIER]->(ItalianRenaissance:Category)-[:IS_A]->(ArtPeriod:Category)
(Bernini:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Sculpture:Category)
(Bernini:Agent)-[:RECOGNISABILITY { Rank: 1 }]->(Architecture:Category)
(Bernini:Agent)-[:RECOGNISABILITY { Rank: 4 }]->(Painting:Category)
(Bernini:Agent)-[:RECOGNISABILITY { Rank: 3 }]->(ItalianBaroquePainting:Category)-[:IS_A]->(Painting:Category)
																				 -[:QUALIFIER]->(ItalianBaroque:Category)-[:IS_A]->(ArtPeriod:Category)
(EiffelTower:Artefact)-[:RECOGNISIBILITY { Rank: 1}]->(Landmark:Category)
(TowerOfLondon:Artefact)-[:RECOGNISIBILITY { Rank: 1}]->(Landmark:Category)















