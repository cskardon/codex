﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduledTasks
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = ConfigurationManager.AppSettings["HistoricEventsPath"];
            var lines = File.ReadAllLines(path);
            var query = from line in lines
                        let data = line.Split('\t')
                        select new
                        {
                            Year = int.Parse(data[0]),
                            EventType = int.Parse(data[1]),
                            MonthDayCirca = data[2],
                            EventText = data[3]
                        };
            var acts = query.Select(x => ToAct(x.Year, x.EventType, x.MonthDayCirca, x.EventText)).ToList();
        }
        static Act ToAct(int year, int eventType, string monthDayCirca, string eventText)
        {
            var act = new Act();
            act.Year = Math.Abs(year);
            act.Milennium = year < 0 ? "BC" : "AD";
            if (monthDayCirca.Contains("circa"))
            {
                act.Proximity = "c.";
            }
            var parts = monthDayCirca.Split(' ');
            if (parts.Count() == 2)
            {
                act.Month = FromMonth(parts[0]);
                act.Day = int.Parse(parts[1]);
            }
            act.Guid = Guid.NewGuid().ToString();
            act.Description = eventText;
            return act;
        }
        static string[] Months = new string[] {
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        };
        static int? FromMonth(string month)
        {
            var index = Array.IndexOf(Months, month);
            return index >= 0 ? index + 1 : (int?)null;
        }
    }
}
