﻿using Data;
using Data.Entities;
using Neo4jClient;
using Neo4jClient.Cypher;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IDataSetService : INeo4jClientService
    {
        List<DataSet> GetList();
        Task<DataSet> SaveOrUpdateAsync(DataSet item);
        Task SaveOrUpdateAsync(DataSetHyperNode node);
        Task<DataSetHyperNode> FindDataSetAsync(string guid);
        Task<DataSetPlaceHyperNode> FindDataSetPlacesAsync(string guid);
        Task<DataSetsContainer> FindDataSetRecursiveAsync(string guid);
        Task<DataSetsContainer> FindDataSetByDateRangeAsync(int? start, int? end);
        Task<DataSetsContainer> FindDataSetByPlaceAsync(string guid);
        Task<List<DataSet>> GetListAsync();
        Task<List<DataSetListItem>> GetListItemsAsync();
    }
    public class DataSetListItem
    {
        public DataSet DataSet { get; set; }
        public int? StartYear { get; set; }
        public int? EndYear { get; set; }
        public int Count { get; set; }
    }
    public class DataSetService : IDataSetService
    {
        public User CurrentUser { get; set; }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        private readonly GraphClient Client;
        private readonly ICommonService commonService;

        public DataSetService(
            GraphClient client,
            ICommonService _commonService
            )
        {
            Client = client;
            commonService = _commonService;
        }

        public List<DataSet> GetList()
        {
            var query = Client.Cypher
                .Match("(set:DataSet)");

            var results = query.Return(() => Return.As<DataSet>("set"))
                .OrderBy("set.Name")
                .Results;

            return results != null ? results.ToList() : new List<DataSet>();
        }

        public async Task<List<DataSet>> GetListAsync()
        {
            var query = Client.Cypher
                .Match("(set:DataSet)");

            var results = await query.Return(() => Return.As<DataSet>("set"))
                .OrderBy("set.Name")
                .ResultsAsync;

            return results != null ? results.ToList() : new List<DataSet>();
        }

        public async Task<List<DataSetListItem>> GetListItemsAsync()
        {
            var query = Client.Cypher
                .Match("(parent:DataSet)")
                .OptionalMatch("(set:DataSet)-[:SUBSET_OF*0..]->(parent)")
                .OptionalMatch("(point:DataPoint)-[:PART_OF]->(set)")
                ;

            var results = await query.Return(() => Return.As<DataSetListItem>("{ DataSet: parent, Count: count(point), StartYear: min(point.Year), EndYear: max(point.Year) }"))
                .ResultsAsync;

            return results != null ? results.ToList() : new List<DataSetListItem>();
        }

        public async Task<DataSetsContainer> FindDataSetByDateRangeAsync(int? start, int? end)
        {
            var query = Client.Cypher
                .Match("(datapoints:DataPoint)");

            var parts = new List<string>();
            if (start.HasValue)
            {
                parts.Add("datapoints.Year >= " + start.Value);
            }
            if (end.HasValue)
            {
                parts.Add("datapoints.Year <= " + end.Value);
            }
            if (parts.Any())
            {
                var whereClause = string.Join(" and ", parts);
                query = query.Where(whereClause);
            }

            query = query
                .Match("(datapoints)-[:" + Label.Relationship.PartOf + "]->(set:DataSet)")
                .OptionalMatch("(datapoints)-[:" + Label.Relationship.AtPlace + "]->(at:Place)")
                .OptionalMatch("(datapoints)-[:" + Label.Relationship.HasSource + "]->(act:Act)")
                .OptionalMatch("(set)-[subsetOf:" + Label.Relationship.SubsetOf + "]->(parent:DataSet)")
                .OptionalMatch("(set)-[about:" + Label.Relationship.About + "]->(tag:Tag)")
                .OptionalMatch("(set)-[hasSource:" + Label.Relationship.HasSource + "]->(source:Artefact)")
                .With("datapoints, set, at, act, parent, tag, source, subsetOf, about, hasSource, collect(distinct set) as DataSets")
                .With("{ DataSet: set, Parent: parent, SubsetOfGuid: subsetOf.Guid, DataSetPlaces: collect(distinct { DataPoint: datapoints, At: at, DataSets: DataSets, Act: act }), About: tag, AboutGuid: about.Guid, Source: source, SourceGuid: hasSource.Guid } as HyperNode")
                ;

            var results = await query.Return(() => Return.As<DataSetPlaceHyperNode>("distinct HyperNode"))
                .ResultsAsync;

            var datasets = results.ToList();

            var container = new DataSetsContainer
            {
                Root = datasets.FirstOrDefault(),
                DataSets = datasets
            };

            return container;
        }

        public async Task<DataSetsContainer> FindDataSetByPlaceAsync(string guid)
        {
            var query = Client.Cypher
                .Match("(place:Place { Guid: {guid} })").WithParams(new { guid })
                .Match("(at:Place)-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(datapoints:DataPoint)-[:" + Label.Relationship.AtPlace + "]->(at)")
                .Match("(datapoints)-[:" + Label.Relationship.PartOf + "]->(set:DataSet)")
                .OptionalMatch("(datapoints)-[:" + Label.Relationship.HasSource + "]->(act:Act)")
                .OptionalMatch("(set)-[subsetOf:" + Label.Relationship.SubsetOf + "]->(parent:DataSet)")
                .OptionalMatch("(set)-[about:" + Label.Relationship.About + "]->(tag:Tag)")
                .OptionalMatch("(set)-[hasSource:" + Label.Relationship.HasSource + "]->(source:Artefact)")
                .With("datapoints, set, at, act, parent, tag, source, subsetOf, about, hasSource, collect(distinct set) as DataSets")
                .With("{ DataSet: set, Parent: parent, SubsetOfGuid: subsetOf.Guid, DataSetPlaces: collect(distinct { DataPoint: datapoints, At: at, DataSets: DataSets, Act: act }), About: tag, AboutGuid: about.Guid, Source: source, SourceGuid: hasSource.Guid } as HyperNode")
                ;

            var results = await query.Return(() => Return.As<DataSetPlaceHyperNode>("distinct HyperNode"))
                .ResultsAsync;

            var datasets = results.ToList();

            var container = new DataSetsContainer
            {
                Root = datasets.FirstOrDefault(),
                DataSets = datasets
            };

            return container;
        }

        public async Task<DataSetsContainer> FindDataSetRecursiveAsync(string guid)
        {
            var selector = Client.Cypher
                .Match("(root:DataSet { Guid: {guid} })").WithParams(new { guid })
                .Match("(set:DataSet)-[:" + Label.Relationship.SubsetOf + "*0..]->(root)")
                ;

            var datasets = await FindDataSetRecursiveAsync(selector);

            var container = new DataSetsContainer
            {
                Root = datasets.FirstOrDefault(x => x.DataSet.Guid == guid),
                DataSets = datasets
            };

            return container;
        }

        static async Task<List<DataSetPlaceHyperNode>> FindDataSetRecursiveAsync(ICypherFluentQuery selector)
        {
            var query = selector
                .OptionalMatch("(datapoints:DataPoint)-[:" + Label.Relationship.PartOf + "]->(set)")
                .OptionalMatch("(datapoints:DataPoint)-[:" + Label.Relationship.PartOf + "]->(set2:DataSet)")
                .OptionalMatch("(datapoints)-[:" + Label.Relationship.AtPlace + "]->(at:Place)")
                .OptionalMatch("(datapoints)-[:" + Label.Relationship.HasSource + "]->(act:Act)")
                .OptionalMatch("(set)-[subsetOf:" + Label.Relationship.SubsetOf + "]->(parent:DataSet)")
                .OptionalMatch("(set)-[about:" + Label.Relationship.About + "]->(tag:Tag)")
                .OptionalMatch("(set)-[hasSource:" + Label.Relationship.HasSource + "]->(source:Artefact)")
                .With("datapoints, set, at, act, parent, tag, source, subsetOf, about, hasSource, collect(set2) as DataSets")
                .With("{ DataSet: set, Parent: parent, SubsetOfGuid: subsetOf.Guid, DataSetPlaces: collect({ DataPoint: datapoints, At: at, DataSets: DataSets, Act: act }), About: tag, AboutGuid: about.Guid, Source: source, SourceGuid: hasSource.Guid } as HyperNode")
                ;

            var results = await query.Return(() => Return.As<DataSetPlaceHyperNode>("distinct HyperNode"))
                .ResultsAsync;

            return results.ToList();
        }


        public async Task<DataSetPlaceHyperNode> FindDataSetPlacesAsync(string guid)
        {
            var selector = Client.Cypher
                .Match("(set:DataSet { Guid: {guid} })").WithParams(new { guid })
                ;

            return (await FindDataSetPlacesAsync(selector)).FirstOrDefault();
        }

        static async Task<List<DataSetPlaceHyperNode>> FindDataSetPlacesAsync(ICypherFluentQuery selector)
        {
            var query = selector
                .OptionalMatch("(datapoints:DataPoint)-[:" + Label.Relationship.PartOf + "]->(set)")
                .OptionalMatch("(datapoints)-[:" + Label.Relationship.AtPlace + "]->(at:Place)")
                .OptionalMatch("(datapoints)-[:" + Label.Relationship.HasSource + "]->(act:Act)")
                .OptionalMatch("(set)-[subsetOf:" + Label.Relationship.SubsetOf + "]->(parent:DataSet)")
                .OptionalMatch("(set)-[about:" + Label.Relationship.About + "]->(tag:Tag)")
                .OptionalMatch("(set)-[hasSource:" + Label.Relationship.HasSource + "]->(source:Artefact)")
                .OptionalMatch("(datapoints)-[:" + Label.Relationship.RelatedTo + "]->(relatedDataSets:DataSet)")
                .With("datapoints, set, at, act, parent, tag, source, subsetOf, about, hasSource, collect(distinct relatedDataSets) as DataSets")
                .With("{ DataSet: set, Parent: parent, SubsetOfGuid: subsetOf.Guid, DataSetPlaces: collect(distinct { DataPoint: datapoints, At: at, DataSets: DataSets, Act: act }), About: tag, AboutGuid: about.Guid, Source: source, SourceGuid: hasSource.Guid } as HyperNode")
                ;

            var results = await query.Return(() => Return.As<DataSetPlaceHyperNode>("HyperNode"))
                .ResultsAsync;

            return results.ToList();
        }

        public async Task<DataSetHyperNode> FindDataSetAsync(string guid)
        {
            var selector = Client.Cypher
                .Match("(set:DataSet { Guid: {guid} })").WithParams(new { guid })
                ;

            return (await FindDataSetAsync(selector)).FirstOrDefault();
        }

        static async Task<List<DataSetHyperNode>> FindDataSetAsync(ICypherFluentQuery selector)
        {
            var query = selector
                .OptionalMatch("(datapoints:DataPoint)-[:" + Label.Relationship.PartOf + "]->(set)")
                .OptionalMatch("(set)-[subsetOf:" + Label.Relationship.SubsetOf + "]->(parent:DataSet)")
                .OptionalMatch("(set)-[about:" + Label.Relationship.About + "]->(tag:Tag)")
                .OptionalMatch("(set)-[hasSource:" + Label.Relationship.HasSource + "]->(source:Artefact)")
                ;

            var results = await query.Return(() => new DataSetHyperNode
                {
                    DataSet = Return.As<DataSet>("set"),
                    Parent = Return.As<DataSet>("parent"),
                    SubsetOfGuid = Return.As<string>("subsetOf.Guid"),
                    DataPoints = Return.As<IEnumerable<DataPoint>>("collect(datapoints)"),
                    About = Return.As<Tag>("tag"),
                    AboutGuid = Return.As<string>("about.Guid"),
                    Source = Return.As<Artefact>("source"),
                    SourceGuid = Return.As<string>("hasSource.Guid")
                })
                .ResultsAsync;

            return results.ToList();
        }

        public async Task SaveOrUpdateAsync(DataSetHyperNode node)
        {
            var isNew = node.DataSet.Guid == null;

            await SaveOrUpdateAsync(node.DataSet);

            if (node.Parent != null)
            {
                await commonService.DeleteRelationshipAsync(Label.Relationship.SubsetOf, node.SubsetOfGuid);
                await SubsetOfAsync(node.DataSet, node.Parent, node.SubsetOfGuid);
            }

            if (node.About != null)
            {
                await commonService.DeleteRelationshipAsync(Label.Relationship.About, node.AboutGuid);
                await AboutAsync(node.DataSet, node.About, node.AboutGuid);
            }

            if (node.Source != null)
            {
                await commonService.DeleteRelationshipAsync(Label.Relationship.HasSource, node.SourceGuid);
                await HasSourceAsync(node.DataSet, node.Source, node.SourceGuid);
            }
        }

        async Task HasSourceAsync(IIdentifiable source, IIdentifiable target, string relationshipGuid = null)
        {
            await commonService.CreateRelationshipAsync(
                    Label.Entity.DataSet, source.Guid,
                    Label.Entity.Artefact, target.Guid,
                    Label.Relationship.HasSource,
                    relationshipGuid);
        }

        async Task AboutAsync(IIdentifiable source, IIdentifiable target, string relationshipGuid = null)
        {
            await commonService.CreateRelationshipAsync(
                    Label.Entity.DataSet, source.Guid,
                    Label.Entity.Tag, target.Guid,
                    Label.Relationship.About,
                    relationshipGuid);
        }

        async Task SubsetOfAsync(IIdentifiable source, IIdentifiable target, string relationshipGuid = null)
        {
            await commonService.CreateRelationshipAsync(
                    Label.Entity.DataSet, source.Guid,
                    Label.Entity.DataSet, target.Guid,
                    Label.Relationship.SubsetOf,
                    relationshipGuid);
        }

        public async Task<DataSet> SaveOrUpdateAsync(DataSet item)
        {
            if (false == item.Created.HasValue)
            {
                item.Created = DateTimeOffset.UtcNow;
            }
            await commonService.SaveOrUpdateAsync(item, Label.Entity.DataSet, audit: false);
            return item;
        }

    }
}
