﻿using Data;
using Data.Entities;
using Neo4jClient;
using Neo4jClient.Cypher;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IDataPointService : INeo4jClientService
    {
        Task HasSourceAsync(DataPointHyperNode node);
        Task<DataPoint> SaveOrUpdateAsync(DataPoint item);
        Task SaveOrUpdateAsync(DataPointHyperNode node);
        Task<DataPointHyperNode> FindDataPointAsync(string guid);
        Task AddPartOfAsync(string dataPointGuid, string dataSetGuid);
        Task RemovePartOfAsync(string dataPointGuid, string dataSetGuid);
    }
    public class DataPointService : IDataPointService
    {
        public User CurrentUser { get; set; }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        private readonly GraphClient Client;
        private readonly ICommonService commonService;

        public DataPointService(
            GraphClient client,
            ICommonService _commonService
            )
        {
            Client = client;
            commonService = _commonService;
        }

        public async Task<DataPointHyperNode> FindDataPointAsync(string guid)
        {
            var selector = Client.Cypher
                .Match("(point:DataPoint { Guid: {guid} })").WithParams(new { guid })
                ;

            return (await FindDataPointAsync(selector)).FirstOrDefault();
        }

        static async Task<List<DataPointHyperNode>> FindDataPointAsync(ICypherFluentQuery selector)
        {
            var query = selector
                .OptionalMatch("(point)-[partOf:" + Label.Relationship.PartOf + "]->(datasets:DataSet)")
                .OptionalMatch("(point)-[atPlace:" + Label.Relationship.AtPlace + "]->(place:Place)")
                .OptionalMatch("(point)-[hasSource:" + Label.Relationship.HasSource + "]->(act:Act)")
                ;

            var results = await query.Return(() => new DataPointHyperNode
                {
                    DataPoint = Return.As<DataPoint>("point"),

                    DataSets = Return.As<IEnumerable<DataSet>>("collect(datasets)"),

                    At = Return.As<Place>("place"),
                    AtGuid = Return.As<string>("atPlace.Guid"),

                    Act = Return.As<Act>("act"),
                    HasSourceGuid = Return.As<string>("hasSource.Guid")
                })
                .ResultsAsync;

            return results.ToList();
        }

        public async Task SaveOrUpdateAsync(DataPointHyperNode node)
        {
            var isNew = node.DataPoint.Guid == null;

            await SaveOrUpdateAsync(node.DataPoint);

            //if (node.DataSet != null)
            //{
            //    await RemovePartOfAsync(node.DataPoint.Guid, n)
            //    await DeletePartOfAsync(node);
            //    await PartOfAsync(node);
            //}

            if (node.At != null)
            {
                await DeleteAtPlaceAsync(node);
                await AtPlaceAsync(node);
            }

            if (node.Act != null)
            {
                await DeleteHasSourceAsync(node);
                await HasSourceAsync(node);
            }
        }

        public async Task RemovePartOfAsync(string dataPointGuid, string dataSetGuid)
        {
            await commonService.DeleteRelationshipBetweenAsync(dataPointGuid, dataSetGuid, Label.Relationship.PartOf);
        }

        private async Task DeleteHasSourceAsync(DataPointHyperNode node)
        {
            await commonService.DeleteRelationshipAsync(Label.Relationship.HasSource, node.HasSourceGuid);
        }

        private async Task DeleteAtPlaceAsync(DataPointHyperNode node)
        {
            await commonService.DeleteRelationshipAsync(Label.Relationship.AtPlace, node.AtGuid);
        }

        public async Task AddPartOfAsync(string dataPointGuid, string dataSetGuid)
        {
            await commonService.CreateRelationshipAsync(
                    Label.Entity.DataPoint, dataPointGuid,
                    Label.Entity.DataSet, dataSetGuid,
                    Label.Relationship.PartOf);
        }

        public async Task HasSourceAsync(DataPointHyperNode node)
        {
            await commonService.CreateRelationshipAsync(
                    Label.Entity.DataPoint, node.DataPoint.Guid,
                    Label.Entity.Act, node.Act.Guid,
                    Label.Relationship.HasSource,
                    node.HasSourceGuid);
        }

        async Task AtPlaceAsync(DataPointHyperNode node)
        {
            await commonService.CreateRelationshipAsync(
                    Label.Entity.DataPoint, node.DataPoint.Guid,
                    Label.Entity.Place, node.At.Guid,
                    Label.Relationship.AtPlace,
                    node.AtGuid);
        }

        //async Task PartOfAsync(DataPointHyperNode node)
        //{
        //    await commonService.CreateRelationshipAsync(
        //            Label.Entity.DataPoint, node.DataPoint.Guid,
        //            Label.Entity.DataSet, node.DataSet.Guid,
        //            Label.Relationship.PartOf,
        //            node.PartOfGuid);
        //}

        public async Task<DataPoint> SaveOrUpdateAsync(DataPoint item)
        {
            if (false == item.Created.HasValue)
            {
                item.Created = DateTimeOffset.UtcNow;
            }
            await commonService.SaveOrUpdateAsync(item, Label.Entity.DataPoint, audit: false);
            return item;
        }

    }
}
