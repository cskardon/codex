﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AgentToAgentRelationHyperNode : GenericRelationHyperNode<Agent, Agent>
    {
    }
    public class AgentToCategoryRelationHyperNode : GenericRelationHyperNode<Agent,Category>
    {
    }
    public class AgentToPlaceRelationHyperNode : GenericRelationHyperNode<Agent, Place>
    {
    }
    public class ArtefactToPlaceRelationHyperNode : GenericRelationHyperNode<Artefact, Place>
    {
    }
    public class AgentToArtefactRelationHyperNode : GenericRelationHyperNode<Agent, Artefact>
    {
    }
}
