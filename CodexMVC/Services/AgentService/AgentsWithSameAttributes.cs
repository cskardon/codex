﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AgentsWithSameAttributes
    {
        public Agent Agent1 { get; set; }
        public Agent Agent2 { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public long Total { get; set; }
    }
}
