﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AgentRelatedToCategoryHyperNode
    {
        public Agent Agent { get; set; }
        public Category Category { get; set; }
        public string Relationship { get; set; }
        public string RelationshipGuid { get; set; }
    }
}
