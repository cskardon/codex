﻿using Data;
using Data.Entities;
using Neo4jClient;
using Neo4jClient.ApiModels.Cypher;
using Neo4jClient.Cypher;
using Newtonsoft.Json;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class EntityResultItem
    {
        public Entity Entity { get; set; }
        public string Label { get; set; }
    }
    public class RelatedArtist
    {
        public Agent Artist { get; set; }
        public ArtefactImageTuple Art { get; set; }
        public int SharedMotifs { get; set; }
    }

    public class UniversalSearch
    {
        public IEnumerable<ArtistsWhoHaveArtworks> Artists { get; set; }
        public IEnumerable<Place> Places { get; set; }
        public IEnumerable<ArtefactDetails> Artefacts { get; set; }
    }

    public class EntityJson
    {
        public string Type { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class EntitySearch
    {
        public List<EntityJson> Agents { get; set; }
        public List<EntityJson> Tags { get; set; }
        public List<EntityJson> Places { get; set; }
        public List<EntityJson> Artefacts { get; set; }
        public List<EntityJson> Categories { get; set; }
    }

    public class ArtistsWhoHaveArtworks
    {
        public Agent Artist { get; set; }
        public int ArtworkTotal { get; set; }
        public IEnumerable<Artefact> Artworks { get; set; }
        public IEnumerable<Category> ArtistTypes { get; set; }
        public Place CitizenOf { get; set; }
        public Category RepresentativeOf { get; set; }
        public Relation Birth { get; set; }
        public Relation Death { get; set; }
        public decimal? AverageRating { get; set; }
        public DateTime? GetBirthDate()
        {
            if (Birth == null)
            {
                return null;
            }
            if (Birth.Year.Value == null)
            {
                return null;
            }
            var year = Birth.Year.Value;
            var month = Birth.Month ?? 1;
            var day = Birth.Day ?? 1;
            return new DateTime(year, month, day);
        }
        public DateTime? GetDeathDate()
        {
            if (Death == null)
            {
                return null;
            }
            if (Death.Year.Value == null)
            {
                return null;
            }
            var year = Death.Year.Value;
            var month = Death.Month ?? 1;
            var day = Death.Day ?? 1;
            return new DateTime(year, month, day);
        }
        private string HtmlName
        {
            get
            {
                var prefix = Artist.Article != null ? Artist.Article + " " : "";
                if (false == string.IsNullOrEmpty(Artist.Name))
                {
                    var names = Artist.Name.Split(' ');
                    var styledNames = "<b>" + names.First() + "</b>";
                    if (names.Length > 1)
                    {
                        styledNames += " " + string.Join(" ", names.Skip(1).Take(names.Length - 1).ToArray());
                    }
                    return prefix + styledNames;
                }
                return prefix + string.Format("{0} <b>{1}</b>", Artist.FirstName, Artist.LastName);
            }
        }
        public string ToHtmlDisplay()
        {
            var citizenship = CitizenOf != null ? (CitizenOf.Adjective ?? CitizenOf.Name) + " " : "";
            var represents = RepresentativeOf != null ? (RepresentativeOf.Adjective ?? RepresentativeOf.Name) + " " : "";
            var name = HtmlName;
            if (Birth != null && Death != null)
            {
                var birth = Birth.Year.HasValue ? Birth.Year.Value.ToString() : "?";
                var death = Death.Year.HasValue ? Death.Year.Value.ToString() : "?";
                var dates = string.Format("({0}-{1})", birth, death);
                name += " " + dates;
            }
            return name + " <small class=\"long-list__sub\">" + citizenship + represents + string.Join(", ", ArtistTypes.Select(x => x.Name.ToLower())) + "</small>";
        }
        public string ToUniversalSearchHtmlDisplay()
        {
            var citizenship = CitizenOf != null ? (CitizenOf.Adjective ?? CitizenOf.Name) + " " : "";
            var represents = RepresentativeOf != null ? (RepresentativeOf.Adjective ?? RepresentativeOf.Name) + " " : "";
            var name = "<a href='/Agent/ViewDetail/" + Artist.Guid + "'>" + HtmlName + "</a>";
            if (Birth != null && Death != null)
            {
                var birth = Birth.Year.HasValue ? Birth.Year.Value.ToString() : "?";
                var death = Death.Year.HasValue ? Death.Year.Value.ToString() : "?";
                var dates = string.Format("({0}-{1})", birth, death);
                name += " " + dates;
            }
            return name + " <small>" + citizenship + represents + string.Join(", ", ArtistTypes.Select(x => x.Name.ToLower())) + "</small>";
        }
    }
    public class ArtefactDetailsComparer : EqualityComparer<ArtefactDetails>
    {
        public override bool Equals(ArtefactDetails x, ArtefactDetails y)
        {
            return x.Artefact.Guid == y.Artefact.Guid;
        }

        public override int GetHashCode(ArtefactDetails obj)
        {
            return base.GetHashCode();

        }
    }
    public class ArtefactDetails
    {
        public Artefact Artefact { get; set; }
        public Category ArtefactType { get; set; }
        public Agent Creator { get; set; }
        public Relation Made { get; set; }
        public Image Image { get; set; }
        public Artefact Container { get; set; }
        public Place Location { get; set; }
        public Category Subject { get; set; }
        public Agent RepresentsAgent { get; set; }
        public string ToHtmlDisplay()
        {
            var name = "<a href='/Artefact/ViewDetail/" + Artefact.Guid + "'>" + Artefact.ToDisplay() + "</a>";
            if (ArtefactType != null)
            {
                name += ", " + ArtefactType.Name.ToLower();
            }
            if (Container != null && Container.Guid != Artefact.Guid)
            {
                name += " in <a href='/Artefact/ViewDetail/" + Container.Guid + "'>" + Container.ToDisplay() + "</a>";
            }
            if (Location != null)
            {
                name += " in <a href='/Place/ViewDetail/" + Location.Guid + "'>" + Location.ToDisplay() + "</a>";
            }
            return name;
        }
    }
    public class PersonalDetails
    {
        public Agent Agent { get; set; }
        public Place Citizenship { get; set; }
        public Category RepresentativeOf { get; set; }
        public IEnumerable<Category> Attributes { get; set; }
        public Place BirthPlace { get; set; }
        public Place DeathPlace { get; set; }
        public Relation Birth { get; set; }
        public Relation Death { get; set; }
        public int TotalUsersWhoRated { get; set; }
        public decimal? TotalUserRatings { get; set; }
        public string ToEntityJsonDisplay()
        {
            return "<b>" + Agent.FullName2 + "</b> <small>" + Life() + " " + SubTitle() + "</small>";
        }
        public string Life()
        {
            var birth = Birth != null && Birth.Year.HasValue ? Birth.Year.Value.ToString() : "";
            var death = Death != null && Death.Year.HasValue ? Death.Year.Value.ToString() : "";
            if (string.IsNullOrEmpty(birth) && string.IsNullOrEmpty(death))
            {
                return "";
            }
            return string.Format("({0}-{1})", birth, death);
        }
        public int? Age
        {
            get
            {
                if (Birth == null || Death == null || Birth.Year == null || Death.Year == null)
                {
                    return null;
                }
                return Death.Year.Value - Birth.Year.Value;
            }

        }
        public string FormattedAttributes()
        {
            if (Attributes == null || false == Attributes.Any())
            {
                return string.Empty;
            }
            if (Attributes.Count() == 1)
            {
                return Attributes.First().Name.ToLower();
            }
            var firstPart = Attributes.Take(Attributes.Count() - 1).Select(x => x.Name.ToLower());
            var secondPart = Attributes.Last().Name.ToLower();
            return string.Join(", ", firstPart) + " and " + secondPart;
        }
        public string SubTitle()
        {
            var citizenship = Citizenship != null ? (Citizenship.Adjective ?? Citizenship.Name) + " " : "";
            var represents = RepresentativeOf != null ? string.Format("<a href=\"/Style/ViewDetail/{0}\" target=\"_blank\">{1}</a> ", RepresentativeOf.Guid, (RepresentativeOf.Adjective ?? RepresentativeOf.Name)) : "";
            return citizenship + represents + FormattedAttributes();
        }
    }
    public interface IAgentService : INeo4jClientService
    {
        bool AgentExists(string agentGuid);
        IEnumerable<ArtistWithRepresentativeArtwork> FindOutgoingWithArtworksAsync(string agentGuid);
        Task<IEnumerable<ArtistsWhoHaveArtworks>> AllArtistsWhoHaveArtworks();
        IEnumerable<ArtistsWhoHaveArtworks> AllAgentsWithDetails();
        PersonalDetails FindPersonalDetailsAsync(string agentGuid);
        SocialHyperNode FindSocial(string agentGuid);
        FamilyHyperNode FindFamily(string agentGuid);
        AgentRelatedToOtherAgentHyperNode FindStudentOfHyperNode(string agentGuid, string relationGuid);
        List<Category> FindParentCategories(string categoryGuid);
        Agent SaveOrUpdate(Agent agent);
        List<PlaceAttributeHyperNode> FindAllPlaceAttributesReverse(string placeGuid);
        List<PlaceAttributeHyperNode> FindAllPlaceAttributes(string placeGuid);
        List<List<Category>> FindCategoryHierarchy(string entityGuid);
        List<AgentAttributeHyperNode> FindOutgoingRelationships(string agentGuid);
        List<AgentAttributeHyperNode> FindIncomingRelationships(string agentGuid);

        List<Category> AllCategories();
        Category FindCategoryByGuid(string guid);
        List<CategoryResult> AllSubcategoriesOf(string ancestorName, int maxDepth = 4);
        Agent FindAgentByGuid(string guid);
        List<Agent> AllPeople();
        List<AgentWithAttribute> AllWithAttributes();
        List<ArtefactWithAttribute> AllArtefactsByType(string agentType);
        List<AgentWithAttribute> AllAgentsByType(string agentType);
        bool DeleteAttribute(string relationshipGuid);
        bool DeleteAct(string actGuid);
        List<AgentsWithSameAttributes> FindAgentsWithSameAttributes(string agentGuid);
        void AddAlias(string agentGuid, string alias);
        GenericRelationHyperNode<Agent, Agent> FindKnows(string agentGuid, string relationshipGuid);
        GenericRelationHyperNode<Agent, Agent> FindTeacherOf(string agentGuid, string relationshipGuid);
        GenericRelationHyperNode<Agent, Agent> FindFriendOf(string agentGuid, string relationshipGuid);
        AgentToAgentRelationHyperNode FindAdversaryOf(string agentGuid, string relationshipGuid);
        AgentToAgentRelationHyperNode FindBenefactorOf(string agentGuid, string hasRelationGuid);
        GenericRelationHyperNode<Agent, Agent> FindSymbolOf(string agentGuid, string hasRelationGuid);
        GenericRelationHyperNode<Agent, Place> FindSymbolOfPlace(string agentGuid, string hasRelationGuid);
        GenericRelationHyperNode<Agent, Place> FindBornAt(string agentGuid, string hasRelationGuid = null);
        GenericRelationHyperNode<Agent, Place> FindDiedAt(string agentGuid, string hasRelationGuid = null);
        AgentToAgentRelationHyperNode FindInfluenceOn(string agentGuid, string hasRelationGuid);
        AgentToAgentRelationHyperNode FindChildOf(string agentGuid, string hasRelationGuid);
        AgentToAgentRelationHyperNode FindChildOfReverse(string agentGuid, string hasRelationGuid);
        AgentToAgentRelationHyperNode FindSpouseOf(string agentGuid, string hasRelationGuid);
        GenericRelationNode<Agent, Agent> FindAspectOf(string agentGuid, string hasRelationGuid);
        GenericRelationHyperNode<Agent, Agent> FindPartnerOf(string agentGuid, string hasRelationGuid);
        AgentToAgentRelationHyperNode FindWorkedFor(string agentGuid, string hasRelationGuid);
        GenericRelationHyperNode<Agent, Agent> FindStudentOf(string agentGuid, string hasRelationGuid);

        void IsA(Agent entity, params Category[] categories);
        void AspectOf(GenericRelationNode<Agent, Agent> node);
        void Knows(GenericRelationHyperNode<Agent, Agent> node);
        void FriendOf(GenericRelationHyperNode<Agent, Agent> node);
        void StudentOf(GenericRelationHyperNode<Agent, Agent> node);
        void TeacherOf(GenericRelationHyperNode<Agent, Agent> node);
        void AdversaryOf(AgentToAgentRelationHyperNode node);
        void BenefactorOf(GenericRelationHyperNode<Agent, Agent> node);
        void SymbolOf(GenericRelationHyperNode<Agent, Agent> node);
        void SymbolOfPlace(GenericRelationHyperNode<Agent, Place> node);
        void InfluenceOn(GenericRelationHyperNode<Agent, Agent> node);
        void BornAt(GenericRelationHyperNode<Agent, Place> node);
        void DiedAt(GenericRelationHyperNode<Agent, Place> node);
        void ChildOf(GenericRelationHyperNode<Agent, Agent> node);
        void ParentOf(AgentToAgentRelationHyperNode node);
        void SpouseOf(GenericRelationHyperNode<Agent, Agent> node);
        void PartnerOf(GenericRelationHyperNode<Agent, Agent> node);
        void WorkedFor(GenericRelationHyperNode<Agent, Agent> node);

        List<AgentWithAttribute> AllWithAttributesWithBookmarks(string userGuid);

        GenericRelationHyperNode<Agent, Category> FindIsA(string agentGuid, string hasRelationGuid);
        void IsA(GenericRelationHyperNode<Agent, Category> node);

        GenericRelationHyperNode<Agent, Agent> FindSimilarTo(string agentGuid, string hasRelationGuid);
        void SimilarTo(GenericRelationHyperNode<Agent, Agent> node);

        GenericRelationHyperNode<Agent, Category> FindAssociatedWith(string agentGuid, string hasRelationGuid);
        void AssociatedWith(GenericRelationHyperNode<Agent, Category> node);

        GenericRelationHyperNode<Agent, Category> FindRepresentativeOf(string agentGuid, string hasRelationGuid);
        void RepresentativeOf(GenericRelationHyperNode<Agent, Category> node);

        //GenericRelationHyperNode<Agent, Category> FindIsA(string agentGuid, string hasRelationGuid);
        //void IsA(GenericRelationHyperNode<Agent, Category> node);

        GenericRelationHyperNode<Agent, Place> FindCitizenOf(string agentGuid, string hasRelationGuid);
        void CitizenOf(GenericRelationHyperNode<Agent, Place> node);

        GenericRelationHyperNode<Agent, Category> FindHasMotif(string agentGuid, string hasRelationGuid);
        void HasMotif(GenericRelationHyperNode<Agent, Category> node);

        GenericRelationHyperNode<Agent, Place> FindLocatedAt(string agentGuid, string hasRelationGuid);
        void LocatedAt(GenericRelationHyperNode<Agent, Place> node);

        AgentToArtefactRelationHyperNode FindMade(string agentGuid, string hasRelationGuid);
        void MakerOf(AgentToArtefactRelationHyperNode node);

        GenericRelationHyperNode<Agent, Artefact> FindCommissioned(string agentGuid, string hasRelationGuid);
        void Commissioned(GenericRelationHyperNode<Agent, Artefact> node);

        GenericRelationHyperNode<Agent, Artefact> FindExhibitingIn(string agentGuid, string hasRelationGuid);
        void ExhibitingIn(GenericRelationHyperNode<Agent, Artefact> node);

        GenericRelationHyperNode<Agent, Artefact> FindWorkedOn(string agentGuid, string hasRelationGuid);
        void WorkedOn(GenericRelationHyperNode<Agent, Artefact> node);

        GenericRelationHyperNode<Agent, Artefact> FindRepresentedBy(string agentGuid, string hasRelationGuid);
        void RepresentedBy(GenericRelationHyperNode<Agent, Artefact> node);

        AgentToAgentRelationHyperNode FindParentOf(string agentGuid, string hasRelationGuid);

        GenericRelationHyperNode<Agent, Agent> FindStudentAt(string agentGuid, string hasRelationGuid);
        void StudentAt(GenericRelationHyperNode<Agent, Agent> node);

        GenericRelationHyperNode<Agent, Agent> FindTeacherAt(string agentGuid, string hasRelationGuid);
        void TeacherAt(GenericRelationHyperNode<Agent, Agent> node);

        GenericRelationHyperNode<Agent, Agent> FindPartOf(string agentGuid, string hasRelationGuid);
        void PartOf(GenericRelationHyperNode<Agent, Agent> node);

        GenericRelationHyperNode<Agent, Agent> FindMemberOf(string agentGuid, string hasRelationGuid);
        void MemberOf(GenericRelationHyperNode<Agent, Agent> node);

        /**
         * Agent to Agent
         */
        GenericRelationHyperNode<Agent, Agent> FindCollaboratedWith(string agentGuid, string hasRelationGuid);
        void CollaboratedWith(GenericRelationHyperNode<Agent, Agent> node);

        Stats AllStats();

        IEnumerable<ArtistWithTaxonomy> GetArtistsWithTaxonomy(string categoryName, bool showOnlyWithArtworks = false);

        IEnumerable<Category> Disciplines();

        IEnumerable<Agent> ArtistsWithArtefacts();

        IEnumerable<Place> CitiesRegionsCountries();

        IEnumerable<Category> MovementsWithArtefacts();

        IEnumerable<Category> GenresWithArtefacts();

        IEnumerable<Category> MotifsWithArtefacts();

        IEnumerable<Category> SubjectsWithArtefacts();

        IEnumerable<Category> AestheticConventionsWithArtefacts();

        IEnumerable<Agent> AgentsRepresentedWithArtefacts();

        IEnumerable<Category> AgentTypesRepresentedWithArtefacts();

        IEnumerable<Category> ObjectsRepresentedWithArtefacts();

        Task<UniversalSearch> UniversalSearch(string search);

        EntitySearch EntitySearch(string search);

        IEnumerable<RelatedArtist> FindRelatedArtists(string guid);

        Task<IEnumerable<ExhibitionDetails>> FindExhibitionsOfAsync(string guid);
        Task<IEnumerable<TourDetails>> FindToursCovering(string guid);

        IEnumerable<PersonalDetails> CheckForDuplicates(string name, string firstname, string lastname);

        Agent FindAgentByUrlFriendlyName(string name);
    }
    public class ArtistWithTaxonomy
    {
        public Agent Agent { get; set; }
        public Dictionary<Category, IEnumerable<Category>> Taxonomy { get; set; }
        public Category RepresentativeOf { get; set; }
        public Place Citizenship { get; set; }
        public IEnumerable<Category> Disciplines
        {
            get
            {
                return Taxonomy.Select(x => x.Value.ElementAt(x.Value.Count() - 2 >= 0 ? x.Value.Count() - 2 : 0));
            }
        }
        private string HtmlName
        {
            get
            {
                var prefix = Agent.Article != null ? Agent.Article + " " : "";
                if (false == string.IsNullOrEmpty(Agent.Name))
                {
                    var names = Agent.Name.Split(' ');
                    var styledNames = "<b>" + names.First() + "</b>";
                    if (names.Length > 1)
                    {
                        styledNames += " " + string.Join(" ", names.Skip(1).Take(names.Length - 1).ToArray());
                    }
                    return prefix + styledNames;
                }
                return prefix + string.Format("{0} <b>{1}</b>", Agent.FirstName, Agent.LastName);
            }
        }
        public string ToHtmlDisplay()
        {
            var citizenship = Citizenship != null ? (Citizenship.Adjective ?? Citizenship.Name) + " " : "";
            var represents = RepresentativeOf != null ? RepresentativeOf.Name + " " : "";
            return HtmlName + " <small>" + citizenship + represents + string.Join(", ", Disciplines.Select(x => x.Name.ToLower())) + "</small>";
        }
    }
    public class Stats
    {
        public int Artefacts { get; set; }
        public int Agents { get; set; }
        public int Places { get; set; }
        public int Acts { get; set; }
        public int Categories { get; set; }
        public int Total
        {
            get
            {
                return Artefacts + Agents + Places + Acts + Categories;
            }
        }
    }
    public class ArtefactMotifImageTuple
    {
        public Artefact Artefact { get; set; }
        public Category Motif { get; set; }
        public Image Image { get; set; }
    }
    public class ArtefactCategoryImageTuple
    {
        public Artefact Artefact { get; set; }
        public Category Category { get; set; }
        public Image Image { get; set; }
    }
    public class ArtefactPlaceImageTuple
    {
        public Artefact Artefact { get; set; }
        public Place Place { get; set; }
        public Image Image { get; set; }
    }
    public class ArtefactsRelatedByUsage
    {
        public Artefact Artefact { get; set; }
        public IEnumerable<Image> Images { get; set; }
    }
    public class AgentService : IAgentService
    {
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly IArtefactService artefactService;
        private readonly IPlaceService placeService;
        private readonly IRelationService relationService;
        private readonly ICommonService commonService;
        public AgentService(
            GraphClient client,
            IArtefactService _artefactService,
            IPlaceService _placeService,
            IRelationService _relationService,
            ICommonService _commonService
            )
        {
            Client = client;
            artefactService = _artefactService;
            placeService = _placeService;
            relationService = _relationService;
            commonService = _commonService;
        }

        private string IgnoreCase(string value)
        {
            return "(?i).*" + value + ".*";
        }

        private string ToStringArray(string value)
        {
            var elements = value.Split(' ').Select(x => "'" + x + "'");
            var csv = string.Join(", ", elements);
            return "[" + csv + "]";
        }

        public IEnumerable<PersonalDetails> CheckForDuplicates(string name, string firstname, string lastname)
        {
            var query = Client.Cypher
                .Match("(agent:Agent)")
                ;

            if (false == string.IsNullOrEmpty(name))
            {
                query = query.Where("(agent.Name =~ '(?i).*" + name + ".*')");
            }
            else
            {
                query = query.Where("(agent.FirstName =~ '(?i).*" + firstname + ".*' and agent.LastName =~ '(?i).*" + lastname + ".*' )");
            }

            var search = false == string.IsNullOrEmpty(name) ? name : firstname + " " + lastname;

            query = query
                .OrWhere("(any(alias IN agent.Aliases WHERE alias =~ '" + IgnoreCase(search) + "'))")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:" + Label.Relationship.IsA + "]->(attribute:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(birth:Relation)-[:" + Label.Relationship.BornAt + "]->(birthPlace:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(death:Relation)-[:" + Label.Relationship.DiedAt + "]->(deathPlace:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:" + Label.Relationship.CitizenOf + "]->(citizenOf:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:" + Label.Relationship.RepresentativeOf + "]->(representativeOf:Category)")
                .With("{ Agent: agent, Birth: birth, Death: death, BirthPlace: birthPlace, DeathPlace: deathPlace, Citizenship: citizenOf, RepresentativeOf: representativeOf, Attributes: collect(distinct(attribute)) } as PersonalDetails")
                ;

            var results = query
                .Return(() => Return.As<PersonalDetails>("distinct(PersonalDetails)"))
                .Results
                .OrderBy(x => x.Agent.FullName)
                .ToList();

            return results;
        }

        public Agent FindAgentByUrlFriendlyName(string name)
        {
            var query = Client.Cypher
                .Match("(agent:Agent { UrlFriendlyName: {name} })").WithParams(new { name });

            var result = query
                .Return(() => Return.As<Agent>("agent"))
                .Results
                .FirstOrDefault();

            return result;
        }

        public async Task<IEnumerable<TourDetails>> FindToursCovering(string artistGuid)
        {

            var query = Client.Cypher
               .Match("(artist:Agent { Guid: {artistGuid} })").WithParams(new { artistGuid })
               .Match("(tour:Artefact)-[]->()-[:" + Label.Relationship.Covers + "]->(artist)")
               .Match("(tour)-[:HAS_DETAIL]->(detail:Detail)")
               .OptionalMatch("(tour)-[:STOPS_IN]->(collection:Artefact)")
               .OptionalMatch("(tour)-[:STOPS_AT]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
               .OptionalMatch("(tour)-[:HAS_RELATION]->()-[:" + Label.Relationship.TourFeatures + "]->(artefact:Artefact)")
               .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
               .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, tour, detail, collection, location, inside")
               ;

            var results = await query
                .Return(() => new TourDetails
                {
                    Tour = Return.As<Artefact>("tour"),
                    Detail = Return.As<TourDetail>("detail"),
                    Location = Return.As<Place>("location"),
                    StopsIn = Return.As<IEnumerable<Artefact>>("collect(distinct(collection))"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .ResultsAsync;

            return results;

        }

        public async Task<IEnumerable<ExhibitionDetails>> FindExhibitionsOfAsync(string artistGuid)
        {

            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {artistGuid} })").WithParams(new { artistGuid })
                .Match("(artist)-[:HAS_RELATION]->()-[:EXHIBITING_IN]->(exhibition:Artefact)")
                .Match("(exhibition)-[:HAS_DETAIL]->(detail:EventDetail)")
                .Match("(exhibition)-[:TAKES_PLACE_IN]->(collection:Artefact)")
                .Match("(collection)-[:LOCATED_AT]->(location:Place)-[:INSIDE_OF*1..]->(inside:Place)")
                .OptionalMatch("(artefact:Artefact)-[:HAS_RELATION]->()-[:EXHIBITED_IN]->(exhibition)")
                .OptionalMatch("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct { Artefact: artefact, Image: image }) } as Artworks, exhibition, detail, collection, location, inside")
                ;

            var results = await query
                .Return(() => new ExhibitionDetails
                {
                    Exhibition = Return.As<Artefact>("exhibition"),
                    Detail = Return.As<EventDetail>("detail"),
                    Collection = Return.As<Artefact>("collection"),
                    Location = Return.As<Place>("location"),
                    Inside = Return.As<IEnumerable<Place>>("collect(distinct(inside))"),
                    Artworks = Return.As<IEnumerable<ArtistWithArtefactImage>>("collect(distinct(Artworks))")
                })
                .ResultsAsync;

            return results;

        }

        public IEnumerable<RelatedArtist> FindRelatedArtists(string agentGuid)
        {
            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artist)-[:HAS_RELATION]->(:Relation)-[:HAS_MOTIF]->(motif:Category)")
                .Match("(motifs:Category)-[:KIND_OF*0..]->(motif)")
                .Match("(otherArtist:Agent)-[:HAS_RELATION]->(:Relation)-[:HAS_MOTIF]->(motifs)")
                .Where("otherArtist.Guid <> artist.Guid")
                .Match("(otherArtist)-[:HAS_RELATION]->()-[:REPRESENTED_BY]->(artefact:Artefact)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("distinct { SharedMotifs: count(distinct(motifs)), Artist: otherArtist, Art: { Artefact: artefact, Image: image } } as Test")
                ;
            var results = query
                .Return(() => Return.As<RelatedArtist>("Test"))
                .Results
                .ToList();
            return results;
        }

        public EntitySearch EntitySearch(string search)
        {
            search = search.Trim().Replace("'", "");
            string place = null;
            if (search.Contains(" in "))
            {
                var split = search.Split(new string[] { " in " }, StringSplitOptions.RemoveEmptyEntries);
                search = split[0];
                place = "(?i).*" + split[1] + ".*";
            }
            search = "(?i).*" + search + ".*";
            var results = new EntitySearch();

            var agentQuery = Client.Cypher
                .Match("(agent:Agent)")
                .Where("agent.FullName =~ '" + search + "' or any(alias IN agent.Aliases WHERE alias =~ '" + search + "')")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:IS_A]->(attributes:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenOf:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(birthPlace:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(deathPlace:Place)")
                ;
            var agents = agentQuery
                .Return(() => Return.As<PersonalDetails>("distinct { Agent: agent, Citizenship: citizenOf, RepresentativeOf: representativeOf, Attributes: collect(attributes), Birth: birth, Death: death, BirthPlace: birthPlace, DeathPlace: deathPlace }"))
                .Results
                .OrderBy(x => x.Agent.FullName)
                .Select(x => new EntityJson
                {
                    Type = Label.Entity.Agent,
                    Text = x.ToEntityJsonDisplay(),
                    Value = x.Agent.Guid,
                })
                .ToList();
            results.Agents = agents;

            var placeQuery = Client.Cypher
                .Match("(place:Place)")
                .Where("place.Name =~ '" + search + "'")
                .OptionalMatch("(place)-[:HAS_RELATION]->()-[:IS_A]->(placeType:Category)")
                .OptionalMatch("(place)-[:INSIDE_OF]->(container:Place)")
                ;
            var places = placeQuery
                .Return(() => Return.As<Place>("distinct(place)"))
                .Results
                .Select(x => new EntityJson
                {
                    Type = Label.Entity.Place,
                    Text = x.Name,
                    Value = x.Guid,
                })
                .ToList();
            results.Places = places;

            var artefactQuery = Client.Cypher
               .Match("(artefact:Artefact)")
               .Where("artefact.Name =~ '" + search + "' or any(alias IN artefact.Aliases WHERE alias =~ '" + search + "')")
               .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(attributes:Category)")
               .OptionalMatch("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist:Agent)")
               ;
            var artefacts = artefactQuery
                .Return(() => Return.As<ArtistArtefactImageTuple>("distinct { Artefact: artefact, Artist: artist, ArtefactType: head(collect(attributes)) }"))
                .Results
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new EntityJson
                {
                    Type = Label.Entity.Artefact,
                    Text = x.ToEntityJsonDisplay(),
                    Value = x.Artefact.Guid,
                })
                .ToList();
            results.Artefacts = artefacts;

            var categoryQuery = Client.Cypher
               .Match("(category:Category)")
               .Where("category.Name =~ '" + search + "'")
               ;
            var categories = categoryQuery
                .Return(() => Return.As<Category>("distinct(category)"))
                .OrderBy("category.Name")
                .Results
                .Select(x => new EntityJson
                {
                    Type = Label.Entity.Category,
                    Text = x.Name,
                    Value = x.Guid,
                })
                .ToList();
            results.Categories = categories;

            var tagQuery = Client.Cypher
               .Match("(tag:Tag)")
               .Where("tag.Name =~ '" + search + "'")
               ;
            var tags = tagQuery
                .Return(() => Return.As<Tag>("distinct(tag)"))
                .OrderBy("tag.Name")
                .Results
                .Select(x => new EntityJson
                {
                    Type = Label.Entity.Tag,
                    Text = x.Name,
                    Value = x.Guid,
                })
                .ToList();
            results.Tags = tags;

            return results;
        }

        public async Task<UniversalSearch> UniversalSearch(string search)
        {
            search = search.Trim().Replace("'", "");
            string place = null;
            if (search.Contains(" in "))
            {
                var split = search.Split(new string[] { " in " }, StringSplitOptions.RemoveEmptyEntries);
                search = split[0];
                place = "(?i).*" + split[1] + ".*";
            }
            search = "(?i).*" + search + ".*";
            var results = new UniversalSearch();

            var agentQuery = Client.Cypher
                .Match("(artist:Agent)")
                .Where("artist.FullName =~ '" + search + "' or any(alias IN artist.Aliases WHERE alias =~ '" + search + "')")
                .Match("(artist)-[:HAS_RELATION]->()-[:IS_A]->(artistType:Category)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenOf:Place)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Place)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(:Place)")
                ;

            var agents = await agentQuery
                .Return(() => new ArtistsWhoHaveArtworks
                {
                    Artist = Return.As<Agent>("artist"),
                    CitizenOf = Return.As<Place>("citizenOf"),
                    RepresentativeOf = Return.As<Category>("representativeOf"),
                    Birth = Return.As<Relation>("birth"),
                    Death = Return.As<Relation>("death"),
                    ArtistTypes = Return.As<IEnumerable<Category>>("collect(distinct(artistType))")
                })
                .ResultsAsync;
            results.Artists = agents;

            var placeQuery = Client.Cypher
                .Match("(place:Place)")
                .Where("place.Name =~ '" + search + "'")
                .Match("(:Artefact)-[:LOCATED_AT]->(place)")
                .OptionalMatch("(place)-[:HAS_RELATION]->()-[:IS_A]->(placeType:Category)")
                .OptionalMatch("(place)-[:INSIDE_OF]->(container:Place)")
                ;
            var places = await placeQuery
                .Return(() => Return.As<Place>("distinct(place)"))
                .ResultsAsync;
            results.Places = places;

            var artefactQuery = Client.Cypher
               .Match("(artefact:Artefact)")
               .Where("artefact.Name =~ '" + search + "' or any(alias IN artefact.Aliases WHERE alias =~ '" + search + "')")
               .Match("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)");
            if (place != null)
            {
                artefactQuery = artefactQuery
                    .Match("(place:Place)").Where("place.Name =~ '" + place + "'")
                    .Match("(places:Place)-[:INSIDE_OF*0..]->(place)")
                    .Match("(artefact)-[:LOCATED_IN*..]->()-[:LOCATED_AT]->(places)");
            }
            artefactQuery = artefactQuery
               .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(container:Artefact)-[:LOCATED_AT]->(location:Place)")
               .OptionalMatch("hasImage=(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
               .OptionalMatch("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
               .OptionalMatch("subHasImage=(subartefact)-[:HAS_IMAGE]->(:Image { Primary: true })")
               .Where("length(hasImage) > 0 or length(subHasImage) > 0")
               .OptionalMatch("(artefact)-[:HAS_RELATION]->(madeBy:Relation)-[:MADE_BY]->(creator:Agent)")
               ;
            var artefacts = await artefactQuery
                .Return(() => new ArtefactDetails
                {
                    Artefact = Return.As<Artefact>("distinct(artefact)"),
                    ArtefactType = Return.As<Category>("artefactType"),
                    Container = Return.As<Artefact>("container"),
                    Location = Return.As<Place>("location"),
                    Creator = Return.As<Agent>("creator"),
                    Image = Return.As<Image>("image")
                })
                .OrderBy("artefact.Name")
                .ResultsAsync;
            results.Artefacts = artefacts;

            if (place != null)
            {
                var artefactByQuery = Client.Cypher
                   .Match("(artist:Agent)")
                   .Where("artist.FullName =~ '" + search + "' or any(alias IN artist.Aliases WHERE alias =~ '" + search + "')")
                   .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                   .Match("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
                   .Match("(place:Place)").Where("place.Name =~ '" + place + "'")
                    .Match("(places:Place)-[:INSIDE_OF*0..]->(place)")
                    .Match("(artefact)-[:LOCATED_IN*..]->()-[:LOCATED_AT]->(places)")
                     .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(container:Artefact)-[:LOCATED_AT]->(location:Place)")
                   .OptionalMatch("hasImage=(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                   .OptionalMatch("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
                   .OptionalMatch("subHasImage=(subartefact)-[:HAS_IMAGE]->(:Image { Primary: true })")
                   .Where("length(hasImage) > 0 or length(subHasImage) > 0")
                   .OptionalMatch("(artefact)-[:HAS_RELATION]->(madeBy:Relation)-[:MADE_BY]->(creator:Agent)")
                   ;
                var artefactsBy = await artefactByQuery
                    .Return(() => new ArtefactDetails
                    {
                        Artefact = Return.As<Artefact>("distinct(artefact)"),
                        ArtefactType = Return.As<Category>("artefactType"),
                        Container = Return.As<Artefact>("container"),
                        Location = Return.As<Place>("location"),
                        Creator = Return.As<Agent>("creator"),
                        Image = Return.As<Image>("image")
                    })
                    .OrderBy("artefact.Name")
                    .ResultsAsync;
                var artefactsTemp = results.Artefacts.ToList();
                artefactsTemp.AddRange(artefactsBy);
                results.Artefacts = artefactsTemp;
            }

            //var artefactsByGenreQuery = Client.Cypher
            //   .Match("(genre:Category)")
            //   .Where("genre.Name =~ '" + search + "'")
            //   .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.HasGenre + "]->(genre)")
            //   .Match("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)")
            //   .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(container:Artefact)-[:LOCATED_AT]->(location:Place)")
            //   .OptionalMatch("hasImage=(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
            //   .OptionalMatch("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
            //   .OptionalMatch("subHasImage=(subartefact)-[:HAS_IMAGE]->(:Image { Primary: true })")
            //   .Where("length(hasImage) > 0 or length(subHasImage) > 0")
            //   .OptionalMatch("(artefact)-[:HAS_RELATION]->(madeBy:Relation)-[:MADE_BY]->(creator:Agent)")
            //   ;
            //var artefactsByGenre = artefactsByGenreQuery
            //    .Return(() => new ArtefactDetails
            //    {
            //        Artefact = Return.As<Artefact>("distinct(artefact)"),
            //        ArtefactType = Return.As<Category>("artefactType"),
            //        Container = Return.As<Artefact>("container"),
            //        Location = Return.As<Place>("location"),
            //        Creator = Return.As<Agent>("creator"),
            //        Image = Return.As<Image>("image")
            //    })
            //    .OrderBy("artefact.Name")
            //    .Results
            //    .ToList();
            //results.Artefacts.AddRange(artefactsByGenre);

            var artefactsBySubjectQuery = Client.Cypher
               .Match("(subject:Category)-[:KIND_OF*0..]->(:Category { Name: '" + Label.Categories.ArtisticSubject + "' })")
               .Where("subject.Name =~ '" + search + "'")
               .Match("(subjects:Category)-[:KIND_OF*0..]->(subject)")
               .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.HasSubject + "]->(subjects)")
               .Match("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)");
            if (place != null)
            {
                artefactsBySubjectQuery = artefactsBySubjectQuery
                    .Match("(place:Place)").Where("place.Name =~ '" + place + "'")
                    .Match("(places:Place)-[:INSIDE_OF*0..]->(place)")
                    .Match("(artefact)-[:LOCATED_IN*..]->()-[:LOCATED_AT]->(places)");
            }
            artefactsBySubjectQuery = artefactsBySubjectQuery
               .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(container:Artefact)-[:LOCATED_AT]->(location:Place)")
               .OptionalMatch("hasImage=(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
               .OptionalMatch("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
               .OptionalMatch("subHasImage=(subartefact)-[:HAS_IMAGE]->(:Image { Primary: true })")
               .Where("length(hasImage) > 0 or length(subHasImage) > 0")
               .OptionalMatch("(artefact)-[:HAS_RELATION]->(madeBy:Relation)-[:MADE_BY]->(creator:Agent)")
               ;
            var artefactsBySubject = await artefactsBySubjectQuery
                .Return(() => new ArtefactDetails
                {
                    Artefact = Return.As<Artefact>("distinct(artefact)"),
                    ArtefactType = Return.As<Category>("artefactType"),
                    Container = Return.As<Artefact>("container"),
                    Location = Return.As<Place>("location"),
                    Creator = Return.As<Agent>("creator"),
                    Image = Return.As<Image>("image"),
                    Subject = Return.As<Category>("subject")
                })
                .OrderBy("artefact.Name")
                .ResultsAsync;
            var resultsArtefacts = results.Artefacts.ToList();
            resultsArtefacts.AddRange(artefactsBySubject.ToList());
            results.Artefacts = resultsArtefacts;

            var artefactsByRepresentsQuery = Client.Cypher
              .Match("(agent:Agent)")
              .Match("(aspect:Agent)-[:" + Label.Relationship.AspectOf + "*0..]->(agent)")
              .Where("(agent.FullName =~ '" + search + "' or any(alias IN agent.Aliases WHERE alias =~ '" + search + "')) or (aspect.FullName =~ '" + search + "' or any(alias IN aspect.Aliases WHERE alias =~ '" + search + "'))")
              .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.Represents + "]->(aspect)")
              .Match("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(artefactType:Category)");
            if (place != null)
            {
                artefactsByRepresentsQuery = artefactsByRepresentsQuery
                    .Match("(place:Place)").Where("place.Name =~ '" + place + "'")
                    .Match("(places:Place)-[:INSIDE_OF*0..]->(place)")
                    .Match("(artefact)-[:LOCATED_IN*..]->()-[:LOCATED_AT]->(places)");
            }
            artefactsByRepresentsQuery = artefactsByRepresentsQuery
              .OptionalMatch("(artefact)-[:LOCATED_IN*0..]->(container:Artefact)-[:LOCATED_AT]->(location:Place)")
              .OptionalMatch("hasImage=(artefact)-[:HAS_IMAGE]->(image:Image { Primary: true })")
              .OptionalMatch("(subartefact:Artefact)-[:LOCATED_IN*0..]->(artefact)")
              .OptionalMatch("subHasImage=(subartefact)-[:HAS_IMAGE]->(:Image { Primary: true })")
              .Where("length(hasImage) > 0 or length(subHasImage) > 0")
              .OptionalMatch("(artefact)-[:HAS_RELATION]->(madeBy:Relation)-[:MADE_BY]->(creator:Agent)")
              ;
            var artefactsByRepresents = await artefactsByRepresentsQuery
                .Return(() => new ArtefactDetails
                {
                    Artefact = Return.As<Artefact>("distinct(artefact)"),
                    ArtefactType = Return.As<Category>("artefactType"),
                    Container = Return.As<Artefact>("container"),
                    Location = Return.As<Place>("location"),
                    Creator = Return.As<Agent>("creator"),
                    Image = Return.As<Image>("image"),
                    RepresentsAgent = Return.As<Agent>("aspect")
                })
                .ResultsAsync;
            var temp = results.Artefacts.ToList();
            temp.AddRange(artefactsByRepresents.ToList());
            results.Artefacts = temp;

            results.Artefacts = results.Artefacts
                .Distinct(new ArtefactDetailsComparer())
                .OrderBy(x => x.Artefact.Name)
                .ToList();

            return results;
        }

        public PersonalDetails FindPersonalDetailsAsync(string agentGuid)
        {
            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {agentGuid }})").WithParams(new { agentGuid })
                .OptionalMatch("(artist)-[:HAS_RELATION]->()-[:IS_A]->(attribute:Category)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenship:Place)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Category)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(birthPlace:Place)")
                .OptionalMatch("(artist)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(deathPlace:Place)")
                .OptionalMatch("(:" + Label.Entity.User + ")-[allRatings:" + Label.Relationship.Rates + "]->(artist)")
                ;

            var results = query
                .Return(() => new PersonalDetails
                {
                    Agent = Return.As<Agent>("artist"),
                    Birth = Return.As<Relation>("birth"),
                    Death = Return.As<Relation>("death"),
                    BirthPlace = Return.As<Place>("birthPlace"),
                    DeathPlace = Return.As<Place>("deathPlace"),
                    Attributes = Return.As<IEnumerable<Category>>("collect(distinct(attribute))"),
                    Citizenship = Return.As<Place>("citizenship"),
                    RepresentativeOf = Return.As<Category>("representativeOf"),
                    TotalUserRatings = Return.As<decimal?>("sum(allRatings.Rating)"),
                    TotalUsersWhoRated = Return.As<int>("count(allRatings)")
                })
                .Results;

            return results.FirstOrDefault();
        }
        public IEnumerable<Category> Disciplines()
        {
            var query = Client.Cypher
                .Match("(artist:" + Label.Entity.Category + " { Name: {categoryName} })").WithParams(new { categoryName = "Maker" })
                .Match("(discipline:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "]->(artist)");
            var results = query.Return((ICypherResultItem discipline) => discipline.As<Category>())
                .Results
                .ToList();
            return results;
        }
        public IEnumerable<Place> CitiesRegionsCountries()
        {
            var result = Client.Cypher
                .Match("(city:Category { Name: 'City' })")
                .Match("(region:Category { Name: 'Region' })")
                .Match("(country:Category { Name: 'Country' })")
                .Match("(place:Place)-[:IS_A]->(placeType:Category)")
                .Match("(places:Place)-[:INSIDE_OF*0..]->(place)")
                .Match("(:Artefact)-[:LOCATED_AT]->(places)")
                .Where("placeType.Guid IN [city.Guid, region.Guid, country.Guid]")
                .Return(() => Return.As<Place>("distinct(place)"))
                .Results
                .ToList();
            return result;
        }
        public IEnumerable<Category> ObjectsRepresentedWithArtefacts()
        {
            var result = Client.Cypher
                .Match("p=(:Artefact)-[:HAS_RELATION]->()-[:REPRESENTS]->(category:Category)")
                .Where("length(p) > 0")
                .Return(() => Return.As<Category>("distinct(category)"))
                .Results
                .ToList();
            return result;
        }
        public IEnumerable<Category> AgentTypesRepresentedWithArtefacts()
        {
            var result = Client.Cypher
                 .Match("(agent:Agent)-[:HAS_RELATION]->()-[:IS_A]->(agentType:Category)")
                 .Match("p=(:Artefact)-[:HAS_RELATION]->()-[:REPRESENTS]->(agent)")
                //.Match("(agentType)-[:KIND_OF*0..]->(agentTypes:Category)")
                 .Where("length(p) > 0")
                 .Return(() => Return.As<Category>("distinct(agentType)"))
                 .Results
                 .ToList();
            return result;
        }
        public IEnumerable<Agent> AgentsRepresentedWithArtefacts()
        {
            var result = Client.Cypher
                .Match("p=(:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.Represents + "]->(agent:" + Label.Entity.Agent + ")")
                .Where("length(p) > 0")
                .Return(() => Return.As<Agent>("distinct(agent)"))
                .Results
                .ToList();
            return result;
        }
        public IEnumerable<Category> AestheticConventionsWithArtefacts()
        {
            var result = Client.Cypher
                .Match("(quality:" + Label.Entity.Category + " { Name: '" + Label.Categories.AestheticConvention + "' })")
                .Match("(qualities:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(qualities)")
                .Match("p=(:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.Uses + "]->(qualities)")
                .Match("(qualities)-[:" + Label.Relationship.KindOf + "*0..]->(parent:" + Label.Entity.Category + ")")
                .Where("length(p) > 0 and parent.Guid <> quality.Guid")
                .Return(() => Return.As<Category>("distinct(parent)"))
                .Results
                .ToList();
            return result;
        }
        public IEnumerable<Category> SubjectsWithArtefacts()
        {
            return GetSubCategoriesWithArtefacts("Artistic Subject", Label.Relationship.HasSubject);
        }
        public IEnumerable<Category> MotifsWithArtefacts()
        {
            return GetSubCategoriesWithArtefacts("Motif", Label.Relationship.HasMotif);
        }
        private IEnumerable<Category> GetSubCategoriesWithArtefacts(string categoryName, string relationshipType)
        {
            var result = Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Name: {categoryName} })").WithParams(new { categoryName })
                .Match("(categories:" + Label.Entity.Category + ")-[:KIND_OF*0..]->(category)")
                .Match("p=(artefact:Artefact)-[:HAS_RELATION]->()-[:" + relationshipType + "]->(categories)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:" + relationshipType + "]->(type:Category)")
                .Match("(type)-[:KIND_OF*0..]->(archetype:Category)")
                .Where("length(p) > 0 and archetype.Guid <> category.Guid")
                .Return(() => Return.As<Category>("distinct(archetype)"))
                .Results
                .ToList();
            return result;
        }
        public IEnumerable<Category> GenresWithArtefacts()
        {
            return GetSubCategoriesWithArtefacts("Artistic Genre", Label.Relationship.HasGenre);
        }
        public IEnumerable<Category> MovementsWithArtefacts()
        {
            var result = Client.Cypher
                .Match("(movement:Category { Name: 'Art Movement' })")
                .Match("(movements:Category)-[:KIND_OF*0..]->(movement)")
                .Match("(artist:Agent)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(movements)")
                .Match("p=(artist)<-[:MADE_BY|DESIGNED_BY]-()<-[:HAS_RELATION]-(:Artefact)")
                .Where("length(p) > 0")
                .Return(() => Return.As<Category>("distinct(movements)"))
                .Results
                .ToList();
            return result;
        }
        public IEnumerable<Agent> ArtistsWithArtefacts()
        {
            var result = Client.Cypher
                .Match("(artistCategory:Category { Name: 'Artist' })")
                .Match("(artistCategories:Category)-[:KIND_OF*0..]->(artistCategory)")
                .Match("(artist:Agent)-[:HAS_RELATION]->()-[:IS_A]->(artistCategories)")
                .Match("p=(artist)<-[:MADE_BY|DESIGNED_BY]-()<-[:HAS_RELATION]-(:Artefact)")
                .Where("length(p) > 0")
                .Return(() => Return.As<Agent>("distinct(artist)"))
                .Results
                .ToList();
            return result;
        }

        public IEnumerable<ArtistsWhoHaveArtworks> AllAgentsWithDetails()
        {
            var query = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + ")")
                .Match("(agent)-[:HAS_RELATION]->()-[:IS_A]->(artistType:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenOf:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(:Place)")
                    ;
            var results = query
                .Return(() => new ArtistsWhoHaveArtworks
                {
                    Artist = Return.As<Agent>("agent"),
                    ArtistTypes = Return.As<IEnumerable<Category>>("collect(distinct(artistType))"),
                    CitizenOf = Return.As<Place>("citizenOf"),
                    RepresentativeOf = Return.As<Category>("head(collect(representativeOf))"),
                    Birth = Return.As<Relation>("birth"),
                    Death = Return.As<Relation>("death")
                })
                .Results
                .ToList();
            return results;
        }

        public async Task<IEnumerable<ArtistsWhoHaveArtworks>> AllArtistsWhoHaveArtworks()
        {
            var query = Client.Cypher
                .Match("(artist:" + Label.Entity.Category + " { Name: {categoryName} })").WithParams(new { categoryName = Label.Categories.Artist })
                .Match("(artistType:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(artist)")
                .Match("(agent:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artistType)")
                .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(agent)")
                .Match("(artefact)-[:HAS_IMAGE]->(image:Image)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenOf:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(:" + Label.Entity.User + ")-[allRatings:" + Label.Relationship.Rates + "]->(agent)")
                ;

            var results = await query
                .Return(() => new
                {
                    Artist = Return.As<Agent>("agent"),
                    Artworks = Return.As<IEnumerable<Artefact>>("collect(distinct(artefact))"),
                    ArtistTypes = Return.As<IEnumerable<Category>>("collect(distinct(artistType))"),
                    CitizenOf = Return.As<Place>("citizenOf"),
                    RepresentativeOf = Return.As<Category>("head(collect(representativeOf))"),
                    Birth = Return.As<Relation>("birth"),
                    Death = Return.As<Relation>("death"),
                    TotalUsersWhoRated = Return.As<int>("count(allRatings)"),
                    TotalUserRatings = Return.As<decimal?>("sum(allRatings.Rating)")
                })
                .ResultsAsync
                ;

            return results
                .AsParallel()
                .Select(x => new ArtistsWhoHaveArtworks
                {
                    Artist = x.Artist,
                    Artworks = x.Artworks,
                    ArtistTypes = x.ArtistTypes,
                    CitizenOf = x.CitizenOf,
                    RepresentativeOf = x.RepresentativeOf,
                    Birth = x.Birth,
                    Death = x.Death,
                    AverageRating = x.TotalUserRatings / (x.TotalUsersWhoRated == 0 ? 1 : x.TotalUsersWhoRated)
                });

        }
        public IEnumerable<ArtistWithTaxonomy> GetArtistsWithTaxonomy(string categoryName, bool showOnlyWithArtworks = false)
        {
            var query = Client.Cypher
                .Match("(artist:" + Label.Entity.Category + " { Name: {categoryName} })").WithParams(new { categoryName })
                .Match("p=(artistType:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..4]->(artist)")
                .Match("(agent:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(artistType)");
            if (showOnlyWithArtworks)
            {
                //query = query.Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:MADE_BY]->(agent)");
            }
            query = query
                .OptionalMatch("(agent)-[]->()-[:CITIZEN_OF]->(citizenship:Place)")
                .OptionalMatch("(agent)-[]->()-[:REPRESENTATIVE_OF]->(representativeOf:Category)")
                .With("agent, citizenship, representativeOf, p")
                ;
            var temp = query.Return(() => new
            {
                Agent = Return.As<Agent>("agent"),
                Taxonomy = Return.As<IEnumerable<Category>>("nodes(p)")
            })
            .Results
            .ToList();
            var results = new List<ArtistWithTaxonomy>();
            foreach (var item in temp.GroupBy(x => x.Agent.Guid))
            {
                var tax = new ArtistWithTaxonomy
                {
                    Agent = item.First().Agent,
                    Taxonomy = new Dictionary<Category, IEnumerable<Category>>()
                };
                foreach (var row in item)
                {
                    tax.Taxonomy[row.Taxonomy.First()] = row.Taxonomy;
                }
                results.Add(tax);
            }
            results.OrderBy(x => x.Taxonomy.OrderBy(x2 => x2.Value.ElementAt(x2.Value.Count() - 2))).ThenBy(x => x.Agent.FullName2);
            return results;
        }
        public Stats AllStats()
        {
            var result = Client.Cypher
                .Match("(agent:Agent)").With("count(agent) as Agents")
                .Match("(artefact:Artefact)").With("count(artefact) as Artefacts, Agents")
                .Match("(place:Place)").With("count(place) as Places, Artefacts, Agents")
                .Match("(act:Act)").With("count(act) as Acts, Places, Artefacts, Agents")
                .Match("(category:Category)").With("count(category) as Categories, Acts, Places, Artefacts, Agents")
                .Return(() => new Stats
                {
                    Agents = Return.As<int>("Agents"),
                    Artefacts = Return.As<int>("Artefacts"),
                    Places = Return.As<int>("Places"),
                    Acts = Return.As<int>("Acts"),
                    Categories = Return.As<int>("Categories")
                })
                .Results
                .FirstOrDefault();
            return result;
        }

        public bool AgentExists(string agentGuid)
        {

            var query = Client.Cypher
                .Match("(agent:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                ;

            var result = query
                .Return(() => Return.As<Agent>("agent"))
                .Results
                .FirstOrDefault();

            return result != null;

        }

        public IEnumerable<ArtistWithRepresentativeArtwork> FindOutgoingWithArtworksAsync(string agentGuid)
        {
            var relationships = Label.Relationship.StudentOf + "|" + Label.Relationship.TeacherOf + "|" + Label.Relationship.FriendOf + "|" + Label.Relationship.Knows + "|" + Label.Relationship.AdversaryOf + "|" + Label.Relationship.ChildOf + "|" + Label.Relationship.InfluenceOn + "|" + Label.Relationship.WorkedFor;

            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artist)-[:HAS_RELATION]->()-[R:" + relationships + "]->(other:Agent)");

            var resultsTemp = FindOutgoingConnectionWithArtworks(query);

            var results = resultsTemp
                .Select(x => new ArtistWithRepresentativeArtwork
                {
                    Artworks = x.Artworks,
                    Relationship = Relationship(x.Relationship),
                    PersonalDetails = x.PersonalDetails
                })
                .ToList();

            results.AddRange(FindFellowStudents(agentGuid));
            results.AddRange(FindSiblings(agentGuid));
            results.AddRange(FindNephewsAndNieces(agentGuid));
            results.AddRange(FindIncomingConnectionsWithArtworks(agentGuid));

            return results;
        }

        public IEnumerable<ArtistWithRepresentativeArtwork> FindIncomingConnectionsWithArtworks(string agentGuid)
        {
            var relationships = Label.Relationship.InfluenceOn + "|" + Label.Relationship.ChildOf + "|" + Label.Relationship.FriendOf + "|" + Label.Relationship.WorkedFor;

            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(other:Agent)-[:HAS_RELATION]->()-[R:" + relationships + "]->(artist)")
                ;

            var result = FindOutgoingConnectionWithArtworks(query);

            return result
                .Select(x => new ArtistWithRepresentativeArtwork
                {
                    Relationship = ReverseRelationship(x.Relationship),
                    Artworks = x.Artworks,
                    PersonalDetails = x.PersonalDetails
                });
        }

        public IEnumerable<ArtistWithRepresentativeArtwork> FindNephewsAndNieces(string agentGuid)
        {
            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artist)-[:HAS_RELATION]->()-[R:" + Label.Relationship.ChildOf + "]->(parent:Agent)")
                .Match("(sibling:Agent)-[:HAS_RELATION]->()-[:" + Label.Relationship.ChildOf + "]->(parent)")
                .Where("artist.Guid <> sibling.Guid")
                .Match("(other:Agent)-[:HAS_RELATION]->()-[:CHILD_OF]->(sibling)")
                ;

            var result = FindOutgoingConnectionWithArtworks(query);

            return result.Select(x => new ArtistWithRepresentativeArtwork
                {
                    Relationship = "Nephews/Nieces",
                    Artworks = x.Artworks,
                    PersonalDetails = x.PersonalDetails
                });
        }

        public IEnumerable<ArtistWithRepresentativeArtwork> FindSiblings(string agentGuid)
        {
            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artist)-[:HAS_RELATION]->()-[R:" + Label.Relationship.ChildOf + "]->(parent:Agent)")
                .Match("(other:Agent)-[:HAS_RELATION]->()-[:" + Label.Relationship.ChildOf + "]->(parent)")
                .Where("artist.Guid <> other.Guid")
                ;

            var result = FindOutgoingConnectionWithArtworks(query);

            return result
                .Select(x => new ArtistWithRepresentativeArtwork
                {
                    Relationship = "Siblings",
                    Artworks = x.Artworks,
                    PersonalDetails = x.PersonalDetails
                });
        }

        public IEnumerable<ArtistWithRepresentativeArtwork> FindFellowStudents(string agentGuid)
        {
            var query = Client.Cypher
                .Match("(artist:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(artist)-[:HAS_RELATION]->()-[R:" + Label.Relationship.StudentOf + "]->(teacher:Agent)")
                .Match("(other:Agent)-[:HAS_RELATION]->()-[:" + Label.Relationship.StudentOf + "]->(teacher)")
                .Where("artist.Guid <> other.Guid")
                ;

            var result = FindOutgoingConnectionWithArtworks(query);

            return result
                .Select(x => new ArtistWithRepresentativeArtwork
                {
                    Relationship = "Fellow students",
                    Artworks = x.Artworks,
                    PersonalDetails = x.PersonalDetails
                });
        }

        private static IEnumerable<ArtistWithRepresentativeArtwork> FindOutgoingConnectionWithArtworks(ICypherFluentQuery query)
        {
            query = query
                .Match("(other)-[:HAS_RELATION]->()-[:IS_A]->(attributes:Category)")
                .OptionalMatch("(other)-[]->(birth:Relation)-[:BORN_AT]->(birthPlace:Place)")
                .OptionalMatch("(other)-[]->(death:Relation)-[:DIED_AT]->(deathPlace:Place)")
                .OptionalMatch("(other)-[:HAS_RELATION]->()-[:" + Label.Relationship.CitizenOf + "]->(citizenship:Place)")
                .OptionalMatch("(other)-[:HAS_RELATION]->()-[:" + Label.Relationship.RepresentativeOf + "]->(representativeOf:Category)")
                .With("other, type(R) as relationship, { Agent: other, Attributes: collect(distinct(attributes)), Citizenship: citizenship, RepresentativeOf: representativeOf, Birth: birth, Death: death, BirthPlace: birthPlace, DeathPlace: deathPlace } as details")
                .OptionalMatch("(artefact:Artefact)-[:HAS_RELATION]->()-[:" + Label.Relationship.MadeBy + "]->(other)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasImage + "]->(image:Image { Primary: true })")
                .With("details, relationship, collect(distinct({ Artefact: artefact, Image: image }))[0..5] as art")
                ;

            var result = query
                .Return(() => Return.As<ArtistWithRepresentativeArtwork>("distinct({ Relationship: relationship, PersonalDetails: details, Artworks: art })"))
                .Results;

            return result;
        }

        private static string ReverseRelationship(string relationship)
        {
            switch (relationship)
            {
                case Label.Relationship.StudentAt: return "Pupils";
                case Label.Relationship.StudentOf: return "Students";
                case Label.Relationship.TeacherOf: return "Teachers";
                case Label.Relationship.Knows: return "Acquaintances";
                case Label.Relationship.AdversaryOf: return "Rivals";
                case Label.Relationship.FriendOf: return "Friends";
                case Label.Relationship.ChildOf: return "Children";
                case Label.Relationship.InfluenceOn: return "Follower of";
                case Label.Relationship.WorkedFor: return "Employees";
                case Label.Relationship.CollaboratedWith: return "Collaborators";
                default: return relationship;
            }
        }

        private static string Relationship(string relationship)
        {
            switch (relationship)
            {
                case Label.Relationship.StudentAt: return "Studied at";
                case Label.Relationship.StudentOf: return "Teachers";
                case Label.Relationship.TeacherOf: return "Students";
                case Label.Relationship.Knows: return "Acquaintances";
                case Label.Relationship.AdversaryOf: return "Rivals";
                case Label.Relationship.FriendOf: return "Friends";
                case Label.Relationship.ChildOf: return "Parents";
                case Label.Relationship.InfluenceOn: return "Followers";
                case Label.Relationship.WorkedFor: return "Employers";
                case Label.Relationship.CollaboratedWith: return "Collaborators";
                default: return relationship;
            }
        }

        public SocialHyperNode FindSocial(string agentGuid)
        {
            var main = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    agentGuid
                })
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.FriendOf + "]->(friendTo:" + Label.Entity.Agent + ")")
                .OptionalMatch("(friendOf:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.FriendOf + "]->(agent)")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.AdversaryOf + "]->(adversary:" + Label.Entity.Agent + ")")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.Knows + "]->(know:" + Label.Entity.Agent + ")")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.StudentOf + "]->(teacher:" + Label.Entity.Agent + ")")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.TeacherOf + "]->(student:" + Label.Entity.Agent + ")")
                .OptionalMatch("(benefactorOf:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.BenefactorOf + "]->(agent)")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.BenefactorOf + "]->(benefactorTo:" + Label.Entity.Agent + ")")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MemberOf + "]->(memberOf:" + Label.Entity.Agent + ")")
                .OptionalMatch("(hasMember:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MemberOf + "]->(agent)")
                .Return(() => new
                {
                    FriendTo = Return.As<IEnumerable<Agent>>("collect(distinct(friendTo))"),
                    FriendOf = Return.As<IEnumerable<Agent>>("collect(distinct(friendOf))"),
                    Adversaries = Return.As<IEnumerable<Agent>>("collect(distinct(adversary))"),
                    Knows = Return.As<IEnumerable<Agent>>("collect(distinct(know))"),
                    Students = Return.As<IEnumerable<Agent>>("collect(distinct(student))"),
                    Teachers = Return.As<IEnumerable<Agent>>("collect(distinct(teacher))"),
                    BenefactorsOf = Return.As<IEnumerable<Agent>>("collect(distinct(benefactorOf))"),
                    BenefactorsTo = Return.As<IEnumerable<Agent>>("collect(distinct(benefactorTo))"),
                    MemberOf = Return.As<IEnumerable<Agent>>("collect(distinct(memberOf))"),
                    HasMember = Return.As<IEnumerable<Agent>>("collect(distinct(hasMember))")
                })
                .Results
                .FirstOrDefault();
            var teacherOfTeacher = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    agentGuid
                })
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.StudentOf + "]->(teacher:" + Label.Entity.Agent + ")")
                .OptionalMatch("(teacher)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.StudentOf + "]->(teacherOfTeacher:" + Label.Entity.Agent + ")")
                .Return(() => new
                {
                    TeacherOfTeacher = Return.As<IEnumerable<Agent>>("collect(distinct(teacherOfTeacher))")
                })
                .Results
                .FirstOrDefault();
            var fellowBenefactors = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    agentGuid
                })
                .OptionalMatch("(benefactor:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.BenefactorOf + "]->(agent)")
                .OptionalMatch("(benefactor)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.BenefactorOf + "]->(fellowBenefactor:" + Label.Entity.Agent + ")")
                .Where("fellowBenefactor.Guid <> agent.Guid")
                .Return(() => new
                {
                    FellowBenefactors = Return.As<IEnumerable<Agent>>("collect(distinct(fellowBenefactor))")
                })
                .Results
                .FirstOrDefault();
            var fellowStudents = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    agentGuid
                })
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.StudentOf + "]->(teacher:" + Label.Entity.Agent + ")")
                .OptionalMatch("(fellowStudent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.StudentOf + "]->(teacher)")
                .Where("fellowStudent.Guid <> agent.Guid")
                .Return(() => new
                {
                    FellowStudents = Return.As<IEnumerable<Agent>>("collect(distinct(fellowStudent))")
                })
                .Results
                .FirstOrDefault();
            var friendToFriend = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    agentGuid
                })
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.FriendOf + "]->(friendTo:" + Label.Entity.Agent + ")")
                .OptionalMatch("(friendTo)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.FriendOf + "]->(friendToFriend:" + Label.Entity.Agent + ")")
                .Where("friendToFriend.Guid <> agent.Guid")
                .Return(() => new
                {
                    FriendToFriends = Return.As<IEnumerable<Agent>>("collect(distinct(friendToFriend))")
                })
                .Results
                .FirstOrDefault();
            var friendOfFriend = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    agentGuid
                })
                .OptionalMatch("(friendOf:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.FriendOf + "]->(agent)")
                .OptionalMatch("(friendOf)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.FriendOf + "]->(friendOfFriend:" + Label.Entity.Agent + ")")
                .Where("friendOfFriend.Guid <> agent.Guid")
                .Return(() => new
                {
                    FriendOfFriends = Return.As<IEnumerable<Agent>>("collect(distinct(friendOfFriend))")
                })
                .Results
                .FirstOrDefault();
            var result = new SocialHyperNode
            {
                Teachers = main.Teachers,
                BenefactorsOf = main.BenefactorsOf,
                BenefactorsTo = main.BenefactorsTo,
                FellowBenefactorsOf = fellowBenefactors.FellowBenefactors,
                TeacherOfTeachers = teacherOfTeacher.TeacherOfTeacher,
                FellowStudents = fellowStudents.FellowStudents,
                Students = main.Students,
                Adversaries = main.Adversaries,
                Knows = main.Knows,
                Friends = main.FriendOf.Concat(main.FriendTo),
                FriendOfFriends = friendToFriend.FriendToFriends.Concat(friendOfFriend.FriendOfFriends),
                MemberOf = main.MemberOf,
                HasMember = main.HasMember
            };
            return result;
        }
        public FamilyHyperNode FindFamily(string agentGuid)
        {
            var main = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    agentGuid
                })
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(parent:" + Label.Entity.Agent + ")")
                .OptionalMatch("(parent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(grandParent:" + Label.Entity.Agent + ")")
                .OptionalMatch("(child:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(agent)")
                .OptionalMatch("(grandChild:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(child)")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.SpouseOf + "]->(spouseOf:" + Label.Entity.Agent + ")")
                .OptionalMatch("(spouseTo:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.SpouseOf + "]->(agent)")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.PartnerOf + "]->(mistressOf:" + Label.Entity.Agent + ")")
                .OptionalMatch("(mistressTo:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.PartnerOf + "]->(agent)")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.PartOf + "]->(partOf:" + Label.Entity.Agent + ")")
                .OptionalMatch("(contains:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.PartOf + "]->(agent)")
                .Return(() => new
                {
                    Agent = Return.As<Agent>("agent"),
                    Children = Return.As<IEnumerable<Agent>>("collect(distinct(child))"),
                    GrandChildren = Return.As<IEnumerable<Agent>>("collect(distinct(grandChild))"),
                    Parents = Return.As<IEnumerable<Agent>>("collect(distinct(parent))"),
                    GrandParents = Return.As<IEnumerable<Agent>>("collect(distinct(grandParent))"),
                    SpouseOf = Return.As<IEnumerable<Agent>>("collect(distinct(spouseOf))"),
                    SpouseTo = Return.As<IEnumerable<Agent>>("collect(distinct(spouseTo))"),
                    MistressOf = Return.As<IEnumerable<Agent>>("collect(distinct(mistressOf))"),
                    MistressTo = Return.As<IEnumerable<Agent>>("collect(distinct(mistressTo))"),
                    PartOf = Return.As<IEnumerable<Agent>>("collect(distinct(partOf))"),
                    Contains = Return.As<IEnumerable<Agent>>("collect(distinct(contains))")
                })
                .Results
                .FirstOrDefault();
            var auntsUncles = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(parent:" + Label.Entity.Agent + ")")
                .Match("(parent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(grandParent:" + Label.Entity.Agent + ")")
                .Match("(auntsUncles)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(grandParent:" + Label.Entity.Agent + ")")
                .WithParams(new { agentGuid })
                .Where("auntsUncles.Guid <> parent.Guid")
                .Return(() => Return.As<Agent>("auntsUncles"))
                .Results
                .ToList();
            var niecesNephews = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(parent:" + Label.Entity.Agent + ")")
                .Match("(sibling:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(parent)")
                .Match("(niecesNephews)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(sibling:" + Label.Entity.Agent + ")")
                .WithParams(new { agentGuid })
                .Where("sibling.Guid <> agent.Guid")
                .Return(() => Return.As<Agent>("niecesNephews"))
                .Results
                .ToList();
            var siblings = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(parent:" + Label.Entity.Agent + ")")
                .WithParams(new { agentGuid })
                .Match("(sibling:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.ChildOf + "]->(parent)")
                .Where("sibling.Guid <> agent.Guid")
                .Return(() => Return.As<Agent>("sibling"))
                .Results
                .ToList();
            var results = new FamilyHyperNode
            {
                Agent = main.Agent,
                MistressOf = main.MistressOf,
                MistressTo = main.MistressTo,
                SpouseOf = main.SpouseOf,
                SpouseTo = main.SpouseTo,
                Children = main.Children,
                GrandChildren = main.GrandChildren,
                Parents = main.Parents,
                GrandParents = main.GrandParents,
                Siblings = siblings,
                AuntsUncles = auntsUncles,
                NiecesNephews = niecesNephews,
                PartOf = main.PartOf,
                Contains = main.Contains
            };
            return results;
        }
        public void SaveOrUpdate(Relation relation)
        {
            if (relation.Guid == null)
            {
                relation.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(relation:" + Label.Entity.Relation + " { Guid: {guid} })")
                .OnCreate().Set("relation = {relation}")
                .OnMatch().Set("relation = {relation}")
                .WithParams(new
                {
                    guid = relation.Guid,
                    relation
                })
                .ExecuteWithoutResults();
        }
        public void CreateRelationship(string sourceType, string sourceGuid, string destType, string destGuid, string relationshipType, string relationshipGuid = null)
        {
            if (relationshipGuid == null)
            {
                relationshipGuid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Match(
                    "(x:" + sourceType + " { Guid: {sourceGuid} })",
                    "(y:" + destType + " { Guid: {destGuid} })")
                .WithParams(new
                {
                    sourceGuid,
                    destGuid
                })
                .CreateUnique("x-[:" + relationshipType + " { Guid: {relationshipGuid} }]->y")
                .WithParams(new
                {
                    relationshipGuid
                })
                .ExecuteWithoutResults();
        }
        public GenericRelationHyperNode<Agent, Agent> FindKnows(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.Knows, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Agent> FindStudentAt(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.StudentAt, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Agent> FindCollaboratedWith(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.CollaboratedWith, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Agent> FindTeacherAt(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.TeacherAt, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Agent> FindStudentOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.StudentOf, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Place> FindBornAt(string agentGuid, string hasRelationGuid = null)
        {
            return commonService.FindGenericRelation<Agent, Place>(agentGuid, Label.Relationship.BornAt, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Place> FindDiedAt(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Place>(agentGuid, Label.Relationship.DiedAt, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Place> FindSymbolOfPlace(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Place>(agentGuid, Label.Relationship.SymbolOfPlace, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Agent> FindSymbolOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.SymbolOf, hasRelationGuid);
        }
        public AgentToAgentRelationHyperNode FindBenefactorOf(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToAgentRelation(agentGuid, hasRelationGuid, Label.Relationship.BenefactorOf);
        }
        public GenericRelationHyperNode<Agent, Category> FindIsA(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Category>(agentGuid, Label.Relationship.IsA, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Category> FindRepresentativeOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Category>(agentGuid, Label.Relationship.RepresentativeOf, hasRelationGuid);
        }
        public AgentToAgentRelationHyperNode FindInfluenceOn(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToAgentRelation(agentGuid, hasRelationGuid, Label.Relationship.InfluenceOn);
        }
        public AgentToAgentRelationHyperNode FindChildOf(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToAgentRelation(agentGuid, hasRelationGuid, Label.Relationship.ChildOf);
        }
        public AgentToAgentRelationHyperNode FindChildOfReverse(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToAgentRelation(agentGuid, hasRelationGuid, Label.Relationship.ChildOf, true);
        }
        public AgentToAgentRelationHyperNode FindParentOf(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToAgentRelation(agentGuid, hasRelationGuid, Label.Relationship.ChildOf, reverse: true);
        }
        public GenericRelationHyperNode<Agent, Agent> FindPartOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.PartOf, hasRelationGuid, reverse: true);
        }
        public GenericRelationHyperNode<Agent, Agent> FindMemberOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.MemberOf, hasRelationGuid, reverse: true);
        }
        public AgentToAgentRelationHyperNode FindSpouseOf(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToAgentRelation(agentGuid, hasRelationGuid, Label.Relationship.SpouseOf);
        }
        public GenericRelationNode<Agent, Agent> FindAspectOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelationNode<Agent, Agent>(agentGuid, Label.Relationship.AspectOf, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Agent> FindPartnerOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.SpouseOf, hasRelationGuid);
        }
        public AgentToAgentRelationHyperNode FindWorkedFor(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToAgentRelation(agentGuid, hasRelationGuid, Label.Relationship.WorkedFor);
        }
        //public GenericRelationHyperNode<Agent, Category> FindIsA(string agentGuid, string hasRelationGuid)
        //{
        //    return commonService.FindGenericRelation<Agent, Category>(agentGuid, Label.Relationship.IsA, hasRelationGuid);
        //}
        public GenericRelationHyperNode<Agent, Category> FindAssociatedWith(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Category>(agentGuid, Label.Relationship.AssociatedWith, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Agent> FindSimilarTo(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.SimilarTo, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Place> FindLocatedAt(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Place>(agentGuid, Label.Relationship.LocatedAt, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Category> FindHasMotif(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Category>(agentGuid, Label.Relationship.HasMotif, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Place> FindCitizenOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Place>(agentGuid, Label.Relationship.CitizenOf, hasRelationGuid);
        }
        public AgentToArtefactRelationHyperNode FindMade(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToArtefactRelation(agentGuid, hasRelationGuid, Label.Relationship.MadeBy);
        }
        public GenericRelationHyperNode<Agent, Artefact> FindExhibitingIn(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Artefact>(agentGuid, Label.Relationship.ExhibitingIn, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Artefact> FindCommissioned(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Artefact>(agentGuid, Label.Relationship.CommissionedBy, hasRelationGuid, reverse: true);
        }
        public GenericRelationHyperNode<Agent, Artefact> FindRepresentedBy(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Artefact>(agentGuid, Label.Relationship.RepresentedBy, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Artefact> FindWorkedOn(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Artefact>(agentGuid, Label.Relationship.WorkedOn, hasRelationGuid);
        }
        public GenericRelationHyperNode<Agent, Agent> FindFriendOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.FriendOf, hasRelationGuid);
        }
        public AgentToAgentRelationHyperNode FindAdversaryOf(string agentGuid, string hasRelationGuid)
        {
            return FindAgentToAgentRelation(agentGuid, hasRelationGuid, Label.Relationship.AdversaryOf);
        }
        public GenericRelationHyperNode<Agent, Agent> FindTeacherOf(string agentGuid, string hasRelationGuid)
        {
            return commonService.FindGenericRelation<Agent, Agent>(agentGuid, Label.Relationship.StudentOf, hasRelationGuid);
        }
        private AgentToArtefactRelationHyperNode FindAgentToArtefactRelation(string agentGuid, string hasRelationGuid, string relationToDestinationType)
        {
            var result = Client.Cypher
                   .Match("(source:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                   .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Artefact + ")")
                   .WithParams(new
                   {
                       agentGuid,
                       hasRelationGuid
                   })
                   .Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new AgentToArtefactRelationHyperNode
                   {
                       Source = source.As<Agent>(),
                       RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                       Relation = relation.As<Relation>(),
                       Destination = destination.As<Artefact>()
                   })
                   .Results
                   .FirstOrDefault();
            result.HasRelationGuid = hasRelationGuid;
            return result;
        }
        private GenericRelationHyperNode<TSource, TDest> FindGenericRelation<TSource, TDest>(string agentGuid, string hasRelationGuid, string relationToDestinationType, bool reverse = false)
            where TSource : IEntity
            where TDest : IEntity
        {
            var sourceType = typeof(TSource).UnderlyingSystemType.Name;
            var destType = typeof(TDest).UnderlyingSystemType.Name;
            var query = Client.Cypher
                   .Match("(source:" + sourceType + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                   .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + destType + ")");
            if (reverse)
            {
                query = Client.Cypher
                   .Match("(relation:" + Label.Entity.Relation + ")-[toDest:" + relationToDestinationType + "]->(destination:" + sourceType + " { Guid: {agentGuid} })")
                   .Match("(source:" + destType + ")-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation)");
            }
            var result = query
                   .WithParams(new
                   {
                       agentGuid,
                       hasRelationGuid
                   })
                   .Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new GenericRelationHyperNode<TSource, TDest>
                   {
                       Source = source.As<TSource>(),
                       RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                       Relation = relation.As<Relation>(),
                       Destination = destination.As<TDest>()
                   })
                   .Results
                   .FirstOrDefault();
            result.HasRelationGuid = hasRelationGuid;
            return result;
        }
        private AgentToAgentRelationHyperNode FindAgentToAgentRelation(string agentGuid, string hasRelationGuid, string relationToDestinationType, bool reverse = false)
        {
            var query = Client.Cypher
                   .Match("(source:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                   .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Agent + ")");
            if (reverse)
            {
                query = Client.Cypher
                   .Match("(relation:" + Label.Entity.Relation + ")-[toDest:" + relationToDestinationType + "]->(destination:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                   .Match("(source:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation)");
            }
            var result = query
                   .WithParams(new
                   {
                       agentGuid,
                       hasRelationGuid
                   })
                   .Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new AgentToAgentRelationHyperNode
                   {
                       Source = source.As<Agent>(),
                       RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                       Relation = relation.As<Relation>(),
                       Destination = destination.As<Agent>()
                   })
                   .Results
                   .FirstOrDefault();
            result.HasRelationGuid = hasRelationGuid;
            return result;
        }
        public void SaveOrUpdate(AgentToCategoryRelationHyperNode node, string relationToDestinationType)
        {
            SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                CreateRelationship(Label.Entity.Agent, node.Source.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Category, node.Destination.Guid, relationToDestinationType);
                }
            }
            else
            {
                commonService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (node.Destination.Guid != null)
                {
                    CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Category, node.Destination.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
        }
        public void SaveOrUpdate(AgentToArtefactRelationHyperNode node, string relationToDestinationType, bool reverse = false)
        {
            SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    CreateRelationship(Label.Entity.Agent, node.Source.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
                else
                {
                    CreateRelationship(Label.Entity.Artefact, node.Destination.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Artefact, node.Destination.Guid, relationToDestinationType);
                    }
                    else
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Agent, node.Source.Guid, relationToDestinationType);
                    }
                }
            }
            else
            {
                commonService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Artefact, node.Destination.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Agent, node.Source.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
        }
        public void SaveOrUpdate(AgentToPlaceRelationHyperNode node, string relationToDestinationType, bool reverse = false)
        {
            SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    CreateRelationship(Label.Entity.Agent, node.Source.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
                else
                {
                    CreateRelationship(Label.Entity.Agent, node.Destination.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Place, node.Destination.Guid, relationToDestinationType);
                    }
                    else
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Place, node.Source.Guid, relationToDestinationType);
                    }
                }
            }
            else
            {
                commonService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Place, node.Destination.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Place, node.Source.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
        }
        public void SaveOrUpdate(AgentToAgentRelationHyperNode node, string relationToDestinationType, bool reverse = false)
        {
            SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    CreateRelationship(Label.Entity.Agent, node.Source.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
                else
                {
                    CreateRelationship(Label.Entity.Agent, node.Destination.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Agent, node.Destination.Guid, relationToDestinationType);
                    }
                    else
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Agent, node.Source.Guid, relationToDestinationType);
                    }
                }
            }
            else
            {
                commonService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Agent, node.Destination.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    CreateRelationship(Label.Entity.Relation, node.Relation.Guid, Label.Entity.Agent, node.Source.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
        }
        public void Knows(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.Knows);
        }
        public void FriendOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.FriendOf);
        }
        public void StudentOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.StudentOf);
        }
        public void TeacherOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.StudentOf, reverse: true);
        }
        public void AspectOf(GenericRelationNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.AspectOf);
        }
        public void AdversaryOf(AgentToAgentRelationHyperNode node)
        {
            SaveOrUpdate(node, Label.Relationship.AdversaryOf);
        }
        public void BenefactorOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.BenefactorOf);
        }
        public void SymbolOfPlace(GenericRelationHyperNode<Agent, Place> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.SymbolOfPlace);
        }
        public void SymbolOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.SymbolOf);
        }
        public void IsA(GenericRelationHyperNode<Agent, Category> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.IsA);
        }
        public void RepresentativeOf(GenericRelationHyperNode<Agent, Category> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.RepresentativeOf);
        }
        public void InfluenceOn(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.InfluenceOn);
        }
        public void BornAt(GenericRelationHyperNode<Agent, Place> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.BornAt);
        }
        public void DiedAt(GenericRelationHyperNode<Agent, Place> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.DiedAt);
        }
        public void ChildOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.ChildOf);
        }
        public void SpouseOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.SpouseOf);
        }
        public void StudentAt(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.StudentAt);
        }
        public void CollaboratedWith(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation<Agent, Agent>(node, Label.Relationship.CollaboratedWith);
        }
        public void TeacherAt(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation<Agent, Agent>(node, Label.Relationship.TeacherAt);
        }
        public void PartnerOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.PartnerOf);
        }
        public void WorkedFor(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.WorkedFor);
        }
        public void PartOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.PartOf);
        }
        public void MemberOf(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.MemberOf);
        }
        public void CitizenOf(GenericRelationHyperNode<Agent, Place> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.CitizenOf);
        }
        public void HasMotif(GenericRelationHyperNode<Agent, Category> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.HasMotif);
        }
        public void LocatedAt(GenericRelationHyperNode<Agent, Place> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.LocatedAt);
        }
        public void MakerOf(AgentToArtefactRelationHyperNode node)
        {
            SaveOrUpdate(node, Label.Relationship.MadeBy, reverse: true);
        }
        public void ExhibitingIn(GenericRelationHyperNode<Agent, Artefact> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.ExhibitingIn);
        }
        public void Commissioned(GenericRelationHyperNode<Agent, Artefact> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.CommissionedBy, reverse: true);
        }
        public void RepresentedBy(GenericRelationHyperNode<Agent, Artefact> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.RepresentedBy);
        }
        public void WorkedOn(GenericRelationHyperNode<Agent, Artefact> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.WorkedOn);
        }
        public void ParentOf(AgentToAgentRelationHyperNode node)
        {
            SaveOrUpdate(node, Label.Relationship.ChildOf, reverse: true);
        }
        //public void IsA(GenericRelationHyperNode<Agent, Category> node)
        //{
        //    commonService.SaveOrUpdateRelation(node, Label.Relationship.IsA);
        //}
        public void SimilarTo(GenericRelationHyperNode<Agent, Agent> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.SimilarTo);
        }
        public void AssociatedWith(GenericRelationHyperNode<Agent, Category> node)
        {
            commonService.SaveOrUpdateRelation(node, Label.Relationship.AssociatedWith);
        }
        public AgentRelatedToOtherAgentHyperNode FindStudentOfHyperNode(string agentGuid, string relationGuid)
        {
            return FindAgentRelatedToAgent(agentGuid, relationGuid, Label.Relationship.TeacherOf, true);
        }
        private AgentRelatedToOtherAgentHyperNode FindAgentRelatedToAgent(string agentGuid, string relationGuid, string relationship, bool reverse = false)
        {
            var match = false == reverse
                ? "(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + " { Guid: {relationGuid} })-[r:" + relationship + "]->(other:" + Label.Entity.Agent + ")"
                : "(other:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + " { Guid: {relationGuid} })-[r:" + relationship + "]->(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })";
            return Client.Cypher
                .Match(match)
                .WithParams(new
                {
                    agentGuid,
                    relationGuid
                })
                .Return((ICypherResultItem category, ICypherResultItem other, ICypherResultItem r) => new AgentRelatedToOtherAgentHyperNode
                {
                    Agent = category.As<Agent>(),
                    OtherAgent = other.As<Agent>(),
                    RelationshipGuid = Return.As<string>("r.Guid"),
                    Relationship = r.Type()
                })
                .Results
                .FirstOrDefault();
        }
        public void AddAlias(string agentGuid, string alias)
        {
            var agent = FindAgentByGuid(agentGuid);
            var aliases = (agent.Aliases ?? new string[] { }).ToList();
            aliases.Add(alias);
            agent.Aliases = aliases.ToArray();
            SaveOrUpdate(agent);
        }
        /**
         *  match (a1:Agent {Name:"Leonardo da Vinci"})-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c1:Category)
            with c1, a1
            match (a2:Agent)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(c2:Category)
            where c2.Guid = c1.Guid and a2.Name <> a1.Name
            with a2, a1, collect(distinct(c2.Name)) as categories
            return a1.Name, a2.Name, categories, length(categories) as total order by total desc, a2.Name
         */
        public List<AgentsWithSameAttributes> FindAgentsWithSameAttributes(string agentGuid)
        {
            return Client.Cypher
                .Match("(a1:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(c1:" + Label.Entity.Category + ")")
                .WithParams(new
                {
                    agentGuid
                })
                .With("c1, a1")
                .Match("(a2:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[:" + Label.Relationship.IsA + "]->(c2:" + Label.Entity.Category + ")")
                .Where("c2.Guid = c1.Guid and a2.Guid <> a1.Guid")
                .With("a2, a1, c2, count(distinct(c2)) as Total")
                // .Where("Total > 1")
                .Return((ICypherResultItem a1, ICypherResultItem a2, ICypherResultItem c2) => new AgentsWithSameAttributes
                {
                    Agent1 = a1.As<Agent>(),
                    Agent2 = a2.As<Agent>(),
                    Categories = Return.As<IEnumerable<Category>>("collect(distinct(c2))"),
                    Total = c2.CountDistinct()
                })
                .OrderBy("Total desc, a1.Name", "a2.Name")
                .Results
                .ToList();
        }
        public bool DeleteAct(string actGuid)
        {
            try
            {
                Client.Cypher
                    .Match("(:" + Label.Entity.Agent + ")-[r1:" + Label.AgentOf + "]->(act:" + Label.Entity.Act + " { Guid: {actGuid} })-[r2]->()")
                    .WithParams(new
                    {
                        actGuid
                    })
                    .Delete("r1, act, r2")
                    .ExecuteWithoutResults();
                return true;
            }
            catch (Exception ex)
            {
                // Log.Error(ex);
                return false;
            }
        }
        public bool DeleteAttribute(string relationshipGuid)
        {
            try
            {
                Client.Cypher
                    .Match("(e1)-[r1:" + Label.Relationship.HasRelation + "]->(a:" + Label.Entity.Relation + ")-[r2 { Guid: {relationshipGuid} }]->(e2)")
                    .WithParams(new
                    {
                        relationshipGuid = relationshipGuid
                    })
                    .With("a")
                    .Match("(e1)-[r1:" + Label.Relationship.HasRelation + "]->(a)-[r2]->(e2)")
                    .Delete("r1, r2, a")
                    .ExecuteWithoutResults();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public Agent SaveOrUpdate(Agent agent)
        {
            commonService.SaveOrUpdate(agent, Label.Entity.Agent);
            Cache.RemoveFromCache(CacheKey.AgentService.AllWithAttributes);
            return agent;
        }
        public Category Create(Category category)
        {
            if (category.Guid == null)
            {
                category.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(category:" + Label.Entity.Category + " { Guid: {guid} })")
                .OnCreate()
                .Set("category = {category}")
                .WithParams(new
                {
                    guid = category.Guid,
                    category
                })
                .ExecuteWithoutResults();
            return category;
        }
        public void Inside(Place place, Agent agent)
        {
            Client.Cypher
                .Match("(p:" + Label.Place + ")",
                       "(o:" + Label.Entity.Agent + ")")
                .Where((Place p) => p.Guid == place.Guid)
                .AndWhere((Agent o) => o.Guid == agent.Guid)
                .CreateUnique("p-[:" + Label.LocatedAt + "]->o")
                .ExecuteWithoutResults();
        }
        public void MemberOf(Agent person, Agent organisation)
        {
            Client.Cypher
                .Match("(p:" + Label.Agent + ")",
                       "(o:" + Label.Entity.Agent + ")")
                .Where((Agent p) => p.Guid == person.Guid)
                .AndWhere((Agent o) => o.Guid == organisation.Guid)
                .CreateUnique("p-[:" + Label.MemberOf + "]->o")
                .ExecuteWithoutResults();
        }
        public class QueryFact
        {
            public Agent Agent { get; set; }
            public Act Act { get; set; }
            public Place Place { get; set; }
        }
        public List<QueryFact> QueryFacts()
        {
            /**
             * match (p:Agent)
                match p-[:AGENT_OF]-(f:Fact)-[:AT_PLACE]-(e:Place)
                return p.Name as Agent, f.Name
             * */
            return Client.Cypher
                         .Match("(agent:" + Label.Agent + ")-[:" + Label.AgentOf + "]->(fact:" + Label.Entity.Act + ")-[:" + Label.AtPlace + "]->(place:" + Label.Place + ")")
                         .Return((ICypherResultItem agent, ICypherResultItem fact, ICypherResultItem place) => new QueryFact
                         {
                             Agent = agent.As<Agent>(),
                             Act = fact.As<Act>(),
                             Place = place.As<Place>()
                         }).Results.OrderBy(x => x.Act.Year).ToList();
        }

        public void MemberOf(Agent person, Topic _event)
        {
            Client.Cypher
                .Match("(p:" + Label.Agent + ")",
                       "(e:" + Label.Entity.Topic + ")")
                .Where((Agent p) => p.Guid == person.Guid)
                .AndWhere((Topic e) => e.Guid == _event.Guid)
                .CreateUnique("p-[:" + Label.MemberOf + "]->e")
                .ExecuteWithoutResults();
        }
        /// <summary>
        /// Logical relation ...
        /// </summary>
        /// <param name="_child"></param>
        /// <param name="_parent"></param>
        public void ChildOf(Agent _child, Agent _parent)
        {
            Client.Cypher
                .Match("(child:" + Label.Agent + " { Guid: {childGuid} })",
                       "(parent:" + Label.Agent + " { Guid: {parentGuid} })")
                .WithParams(new
                {
                    childGuid = _child.Guid,
                    parentGuid = _parent.Guid
                })
                .CreateUnique("child-[:" + Label.Relationship.ChildOf + "]->parent")
                .ExecuteWithoutResults();
        }
        public void MemberOf(Agent person, Category movementCategory)
        {
            Client.Cypher
                .Match("(p:" + Label.Agent + ")",
                       "(m:" + Label.Entity.Category + ")")
                .Where((Agent p) => p.Guid == person.Guid)
                .AndWhere((Category m) => m.Guid == movementCategory.Guid)
                .CreateUnique("p-[:" + Label.MemberOf + "]->m")
                .ExecuteWithoutResults();
        }
        public void AffiliatedWith(Category movementCategory, Category styleCategory)
        {
            Client.Cypher
                .Match("(m:" + Label.Entity.Category + ")",
                       "(s:" + Label.Entity.Category + ")")
                .Where((Category m) => m.Guid == movementCategory.Guid)
                .AndWhere((Category s) => s.Guid == styleCategory.Guid)
                .CreateUnique("m-[:" + Label.AffiliatedWith + "]->s")
                .ExecuteWithoutResults();
        }
        public void Practised(Agent person, Category style)
        {
            Client.Cypher
                .Match("(p:" + Label.Agent + ")",
                       "(s:" + Label.Entity.Category + ")")
                .Where((Agent p) => p.Guid == person.Guid)
                .AndWhere((Category s) => s.Guid == style.Guid)
                .CreateUnique("p-[:" + Label.Practised + "]->s")
                .ExecuteWithoutResults();
        }
        public void HasNationality(Agent person, Place place)
        {
            Client.Cypher
                .Match("(p:" + Label.Agent + ")",
                       "(p2:" + Label.Place + ")")
                .Where((Agent p) => p.Guid == person.Guid)
                .AndWhere((Place p2) => p2.Guid == place.Guid)
                .CreateUnique("p-[:" + Label.HasNationality + "]->p2")
                .ExecuteWithoutResults();
        }
        public void HasDatabase(Entity entity, Database database)
        {
            return;
            if (database == null)
            {
                return;
            }
            Client.Cypher
                .Match("(e)", "(db:" + Label.Entity.Database + ")")
                .Where((Entity e) => e.Guid == entity.Guid)
                .AndWhere((Database db) => db.Guid == database.Guid)
                .CreateUnique("e-[:" + Label.HasDatabase + "]->db")
                .ExecuteWithoutResults();
        }


        public void Represents(IEntity source, params IEntity[] targets)
        {
            foreach (var target in targets)
            {
                Client.Cypher
                    .Match("(src)", "(trg)")
                    .Where((IEntity src) => src.Guid == source.Guid)
                    .AndWhere((IEntity trg) => trg.Guid == target.Guid)
                    .CreateUnique("src-[:" + Label.Represents + "]->trg")
                    .ExecuteWithoutResults();
            }
        }
        public void HasAlias(Artefact artwork, params Alias[] aliases)
        {
            foreach (var alias in aliases)
            {
                Client.Cypher
                    .Match("(a:" + Label.Agent + ")",
                           "(al:" + Label.Alias + ")")
                    .Where((Artefact a) => a.Guid == artwork.Guid)
                    .AndWhere((Alias al) => al.Guid == alias.Guid)
                    .CreateUnique("a-[:" + Label.HasAlias + "]->al")
                    .ExecuteWithoutResults();
            }
        }
        public void AssociateFactWithTopic(string factGuid, string topicGuid)
        {
            Client.Cypher
                .Match("(f:" + Label.Entity.Act + ")",
                       "(t:" + Label.Entity.Topic + ")")
                .Where((Act f) => f.Guid == factGuid)
                .AndWhere((Topic t) => t.Guid == topicGuid)
                .CreateUnique("f-[:" + Label.HasTopic + "]->t")
                .ExecuteWithoutResults();
        }
        public void ParentOf(Topic child, Topic parent)
        {
            Client.Cypher
                .Match("(c:" + Label.Entity.Topic + ")",
                       "(p:" + Label.Entity.Topic + ")")
                .Where((Topic c) => c.Guid == child.Guid)
                .AndWhere((Topic p) => p.Guid == parent.Guid)
                .CreateUnique("p-[:" + Label.ParentOf + "]->c")
                .ExecuteWithoutResults();
        }
        public void Uses(Artefact artwork, Category formalElement, int? degree)
        {
            Client.Cypher
                .Match("(a:" + Label.Entity.Artefact + ")",
                       "(f:" + Label.Entity.Category + ")")
                .Where((Artefact a) => a.Guid == artwork.Guid)
                .AndWhere((Category f) => f.Guid == formalElement.Guid)
                .CreateUnique("a-[:" + Label.Relationship.Uses + " { degree: {degree} }]-f")
                .WithParams(new { degree = degree })
                .ExecuteWithoutResults();
        }
        public void ProducedBy(Category activity, Category actor)
        {
            Client.Cypher
                .Match("(c:" + Label.Entity.Category + ")",
                       "(p:" + Label.Entity.Category + ")")
                .Where((Category c) => c.Guid == activity.Guid)
                .AndWhere((Category p) => p.Guid == actor.Guid)
                .CreateUnique("c-[:" + Label.ProducedBy + "]->p")
                .ExecuteWithoutResults();
        }
        public void BelongsTo(Place place, Agent organisation)
        {
            Client.Cypher
            .Match("(p:" + Label.Place + ")",
                   "(o:" + Label.Entity.Agent + ")")
            .Where((Place p) => p.Guid == place.Guid)
            .AndWhere((Agent o) => o.Guid == organisation.Guid)
            .CreateUnique("p-[:" + Label.BelongsTo + "]->o")
            .ExecuteWithoutResults();
        }
        public void CapitalOf(Place place, Agent organisation)
        {
            Client.Cypher
            .Match("(p:" + Label.Place + ")", "(o:" + Label.Entity.Agent + ")")
            .Where((Place p) => p.Guid == place.Guid)
            .AndWhere((Agent o) => o.Guid == organisation.Guid)
            .CreateUnique("p-[:" + Label.CapitolOf + "]->o")
            .ExecuteWithoutResults();
        }
        //public List<IGrouping<string, PlaceFactHyperNode>> FindAllPlaceFacts(string placeGuid)
        //{
        //    var hyperNodesGrouping = Client.Cypher
        //        .Match("(place:" + Label.Place + ")<-[:" + Label.AtPlace + "]-(fact:" + Label.Entity.Act + ")<-[relationship]-(agent:" + Label.Agent + ")")
        //        .Where((Place place) => place.Guid == placeGuid)
        //        .Return((ICypherResultItem place, ICypherResultItem fact, ICypherResultItem relationship,
        //            ICypherResultItem agent) => new PlaceFactHyperNode
        //            {
        //                Agent = agent.As<Agent>(),
        //                Act = fact.As<Act>(),
        //                Relationship = relationship.Type(),
        //                Place = place.As<Place>()
        //            }).OrderBy("fact.Guid").Results.ToList()
        //              .GroupBy(x => x.Act.Guid).ToList();
        //    return hyperNodesGrouping;
        //}
        public class FindServedTestResult
        {
            public Agent Agent { get; set; }
            public Act Fact { get; set; }
            public Agent Master { get; set; }
            public Place Place { get; set; }
            public IEnumerable<Node<Place>> Places { get; set; }
            public IEnumerable<Node<Category>> Capacities { get; set; }
        }
        public List<FindServedTestResult> FindServedTest()
        {
            var results = Client.Cypher
                  .Match("(person:" + Label.Agent + ")-[:" + Label.AgentOf + "]->(fact:" + Label.Entity.Act + ")-[:" + Label.Serves + "]->(master:" + Label.Agent + ")")
                  .OptionalMatch("(fact)-[:" + Label.AtPlace + "]->(place:" + Label.Place + ")")
                  .OptionalMatch("(place)-[:" + Label.InsideOf + "*1..3]->(places:" + Label.Place + ")")
                  .OptionalMatch("(fact)-[:" + Label.InCapacity + "]->(capacity:" + Label.Entity.Category + ")")
                  .Return((person, places, fact, master, place, capacity) =>
                  new FindServedTestResult
                   {
                       Agent = person.As<Agent>(),
                       Fact = fact.As<Act>(),
                       Master = master.As<Agent>(),
                       Place = place.As<Place>(),
                       Places = places.CollectAs<Node<Place>>(),
                       Capacities = capacity.CollectAs<Node<Category>>()
                   })
                  .Results.ToList();
            return results;
        }
        public class CategoryGroupItem
        {
            public Category Main { get; set; }

            public IEnumerable<Node<Category>> Ancestors { get; set; }

            public string Name
            {
                get
                {
                    if (Ancestors.Count() == 0)
                    {
                        return Main.Name;
                    }
                    var ancestors = string.Join(" > ", Ancestors.Select(x => x.Data.Name).ToArray());
                    return Main.Name + " > " + ancestors;
                }
            }
        }
        public List<List<Category>> FindCategoryHierarchy(string entityGuid)
        {
            var grouping = Client.Cypher.Match("(entity)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.IsA + "]->(main:" + Label.Entity.Category + ")-[:" + Label.KindOf + "*0..]->(ancestors:" + Label.Entity.Category + ")")
                .Where((Entity entity) => entity.Guid == entityGuid)
                .Return((ICypherResultItem main, ICypherResultItem ancestors) =>
                new CategoryGroupItem
                {
                    Main = main.As<Category>(),
                    Ancestors = ancestors.CollectAs<Node<Category>>()
                }).OrderBy("main.Guid").Results.ToList()
                .GroupBy(x => x.Ancestors.First().Data.Guid).ToList();
            var flattened = grouping.Select(x => x.First().Ancestors.Select(x2 => x2.Data).ToList()).ToList();
            return flattened;
        }
        public void IsA(Agent entity, params Category[] categories)
        {
            /**
             * (entity)-[:HAS_ATTR]->(:Attribute)-[:IS_A]->(category)
             *                                   -[:ASSERTED_BY]->(CurrentUser)
             */
            foreach (var category in categories)
            {
                var attribute = HasRelation(entity, CreateRelation());
                Client.Cypher
                    .Match(
                        "(attr:" + Label.Entity.Relation + " { Guid: {attrGuid} })",
                        "(cat:" + Label.Entity.Category + " { Guid: {catGuid} })")
                    .WithParams(new
                    {
                        attrGuid = attribute.Guid,
                        catGuid = category.Guid
                    })
                    .CreateUnique("attr-[:" + Label.IsA + " { Guid: {guid}, IsActive: {isActive} }]->cat")
                    .WithParams(new
                    {
                        guid = Guid.NewGuid().ToString(),
                        isActive = true
                    })
                    .ExecuteWithoutResults();
            }
            Cache.RemoveFromCache(CacheKey.AgentService.AllWithAttributes);
        }
        public Relation HasRelation(IEntity entity, Data.Entities.Relation attribute)
        {
            /**
             * (entity)-[:HAS_ATTR]->(:Attribute)
             */
            Client.Cypher
                    .Match("(ent)", "(attr:" + Label.Entity.Relation + ")")
                    .Where((IEntity ent) => ent.Guid == entity.Guid)
                    .AndWhere((Relation attr) => attr.Guid == attribute.Guid)
                    .CreateUnique("ent-[:" + Label.Relationship.HasRelation + " { Guid: {guid}, IsActive: {isActive} }]->attr")
                    .WithParams(new
                    {
                        guid = Guid.NewGuid().ToString(),
                        isActive = true
                    })
                    .ExecuteWithoutResults();
            return attribute;
        }
        public Relation CreateRelation()
        {
            /**
             * (:Attribute)-[:ASSERTED_BY]->(CurrentUser)
             */
            var attribute = new Relation { Guid = Guid.NewGuid().ToString() };
            Client.Cypher
                .Merge("(attribute:" + Label.Entity.Relation + " { Guid: {guid} })")
                .OnCreate().Set("attribute = {attribute}")
                .WithParams(new
                {
                    guid = attribute.Guid,
                    attribute = attribute
                })
                .ExecuteWithoutResults();
            return attribute;
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        public Alias Create(Alias alias)
        {
            if (alias.Guid == null)
            {
                alias.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(alias:" + Label.Alias + " { Guid: {guid} })")
                .OnCreate()
                .Set("alias = {alias}")
                .WithParams(new
                {
                    guid = alias.Guid,
                    alias = alias
                })
                .ExecuteWithoutResults();
            return alias;
        }
        public bool RelationshipExists(string label, string guid)
        {
            return Client.Cypher
                .Match("()-[r:" + label + "]-()")
                .Where("r.Guid = '" + guid + "'")
                .Return(r => r.Type()).Results.Any();
        }
        public static class HyperNodeType
        {
            public static string Birth = "Birth";
            public static string Death = "Death";
            public static string Visited = "Visited";
            public static string LivedAt = "LivedAt";
        }
        public Category FindCategoryByGuid(string guid)
        {
            var node = Client.Cypher
                .Match("(p:" + Label.Entity.Category + ")")
                .Where((Place p) => p.Guid == guid)
                .Return(p => p.As<Category>())
                .Results.FirstOrDefault();
            return node;
        }
        public Category FindCategoryByName(string name)
        {
            var node = Client.Cypher
                .Match("(p:" + Label.Entity.Category + ")")
                .Where((Place p) => p.Name == name)
                .Return(p => p.As<Category>())
                .Results.FirstOrDefault();
            return node;
        }
        public List<Topic> FindEvents(string name)
        {
            var place = Client.Cypher
                .Match("(e:" + Label.Entity.Topic + ")")
                .Where("e.Name =~ '(?i)" + name + ".*'")
                .Return(e => e.As<Topic>())
                .Results.ToList();
            return place;
        }

        public void PlaceIs(string placeGuid, string categoryGuid)
        {
            Client.Cypher
                .Match("(p:" + Label.Place + ")",
                       "(category:" + Label.Entity.Category + ")")
                .Where((Place place) => place.Guid == placeGuid)
                .AndWhere((Category category) => category.Guid == categoryGuid)
                .CreateUnique("place-[:" + Label.IsA + "]->category")
                .ExecuteWithoutResults();
        }
        public void AgentIs(string agentGuid, string categoryGuid)
        {
            Client.Cypher
                .Match("(agent:" + Label.Agent + ")",
                       "(category:" + Label.Entity.Category + ")")
                .Where((Agent agent) => agent.Guid == agentGuid)
                .AndWhere((Category category) => category.Guid == categoryGuid)
                .CreateUnique("agent-[:" + Label.IsA + "]->category")
                .ExecuteWithoutResults();
        }
        public List<Category> FindParentCategories(string categoryGuid)
        {
            var parents = Client.Cypher
                .Match("(category:" + Label.Entity.Category + ")-[:" + Label.KindOf + "]->(parent:" + Label.Entity.Category + ")")
                .Where((Category category) => category.Guid == categoryGuid)
                .Return(parent => parent.As<Category>()).Results.ToList();
            return parents;
        }

        public List<CategoryResult> AllSubcategoriesOf(string ancestorName, int maxDepth = 4)
        {
            var result = Client.Cypher
                .Match("(child:" + Label.Entity.Category + ")-[:" + Label.KindOf + "]->(parent:" + Label.Entity.Category + ")-[:" + Label.KindOf + "*0.." + maxDepth + "]->(ancestor:" + Label.Entity.Category + " { Name: {ancestorName} })")
                .WithParams(new { ancestorName })
                .With("child")
                .Match("(child)-[:" + Label.Relationship.KindOf + "]->()-[:" + Label.Relationship.KindOf + "*0.." + maxDepth + "]->(parent)")
                .Return((ICypherResultItem child, ICypherResultItem parent) => new CategoryResult
                {
                    Child = child.As<Category>(),
                    Ancestors = Return.As<IEnumerable<Category>>("collect(parent)")
                }).OrderBy("child.Name").Results.ToList();
            return result;
        }
        public List<PlaceAttributeHyperNode> FindAllPlaceAttributes(string placeGuid)
        {
            var attributes = Client.Cypher
                .Match("(place:" + Label.Place + " { Guid: {placeGuid} })-[r]->y")
                .WithParams(new
                {
                    placeGuid = placeGuid
                })
                .Return((ICypherResultItem place, ICypherResultItem y, ICypherResultItem r) =>
                    new PlaceAttributeHyperNode
                    {
                        Place = place.As<Place>(),
                        Entity = y.As<Entity>(),
                        EntityLabels = y.Labels(),
                        Relationship = r.Type(),
                        RelationshipGuid = Return.As<string>("r.Guid")
                    })
                    .Results
                    .Select(x => new PlaceAttributeHyperNode
                    {
                        Place = x.Place,
                        Entity = x.Entity,
                        EntityLabel = x.EntityLabels.FirstOrDefault(),
                        Relationship = x.Relationship,
                        RelationshipGuid = x.RelationshipGuid
                    })
                    .ToList();
            return attributes;
        }
        public List<PlaceAttributeHyperNode> FindAllPlaceAttributesReverse(string placeGuid)
        {
            var attributes = Client.Cypher
                .Match("(y:" + Label.Place + ")-[r]->(place:" + Label.Place + " { Guid: {placeGuid} })")
                .WithParams(new
                {
                    placeGuid = placeGuid
                })
                .Return((ICypherResultItem place, ICypherResultItem r, ICypherResultItem y) =>
                    new PlaceAttributeHyperNode
                    {
                        Place = place.As<Place>(),
                        Entity = y.As<Entity>(),
                        EntityLabels = y.Labels(),
                        Relationship = r.Type(),
                        RelationshipGuid = Return.As<string>("r.Guid")
                    })
                    .Results
                    .Select(x => new PlaceAttributeHyperNode
                    {
                        Place = x.Place,
                        Entity = x.Entity,
                        EntityLabel = x.EntityLabels.FirstOrDefault(),
                        Relationship = x.Relationship,
                        RelationshipGuid = x.RelationshipGuid
                    })
                    .ToList();
            return attributes;
        }
        public List<AgentAttributeHyperNode> FindIncomingRelationships(string agentGuid)
        {
            var attributes = Client.Cypher
                .Match("(agent:" + Label.Agent + " { Guid: {agentGuid} })<-[r]-(relation:" + Label.Entity.Relation + ")<-[hasRelation:" + Label.Relationship.HasRelation + "]-(other)")
                .WithParams(new
                {
                    agentGuid = agentGuid
                })
                .Where("not (relation-[:ASSERTED_BY|:CITED_IN]->other)")
                .Return((ICypherResultItem agent, ICypherResultItem other, ICypherResultItem r, ICypherResultItem hasRelation) =>
                    new
                    {
                        Agent = agent.As<Agent>(),
                        Entity = other.As<Node<string>>(),
                        EntityType = Return.As<string>("head(labels(other))"),
                        Relationship = r.Type(),
                        RelationshipGuid = Return.As<string>("r.Guid"),
                        HasRelationGuid = Return.As<string>("hasRelation.Guid")
                    })
                    .Results
                    .Select(x => new AgentAttributeHyperNode
                    {
                        Agent = x.Agent,
                        Entity = ToEntity(x.EntityType, x.Entity),
                        EntityType = x.EntityType,
                        Relationship = x.Relationship,
                        RelationshipGuid = x.RelationshipGuid,
                        HasRelationGuid = x.HasRelationGuid
                    })
                    .ToList();
            return attributes;
        }
        public IEntity ToEntity(string entityType, Node<string> node)
        {
            var dynamicNode = JsonConvert.DeserializeObject<dynamic>(node.Data);
            if (Label.Entity.Agent == entityType)
            {
                return ToAgent(dynamicNode);
            }
            if (Label.Entity.Artefact == entityType)
            {
                return ToArtefact(dynamicNode);
            }
            if (Label.Entity.Place == entityType)
            {
                return ToPlace(dynamicNode);
            }
            if (Label.Entity.Category == entityType)
            {
                return ToCategory(dynamicNode);
            }
            if (Label.Entity.Event == entityType)
            {
                return ToEvent(dynamicNode);
            }
            return null;
        }
        public Event ToEvent(dynamic node)
        {
            return new Event
            {
                Milennium = node.Milennium,
                Proximity = node.Proximity,
                Year = node.Year,
                Month = node.Month,
                Day = node.Day,
                Hour = node.Hour,
                Minute = node.Minute,
                MilenniumEnd = node.MilenniumEnd,
                ProximityEnd = node.ProximityEnd,
                YearEnd = node.YearEnd,
                MonthEnd = node.MonthEnd,
                DayEnd = node.DayEnd,
                HourEnd = node.HourEnd,
                MinuteEnd = node.MinuteEnd
            };
        }
        public Category ToCategory(dynamic node)
        {
            return new Category
            {
                Guid = node.Guid,
                Name = node.Name,
                Article = node.Article,
                Aliases = node.Aliases,
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Tagline
            };
        }
        public Artefact ToArtefact(dynamic node)
        {
            return new Artefact
            {
                Guid = node.Guid,
                Name = node.Name,
                Article = node.Article,
                Aliases = node.Aliases != null ? node.Aliases.ToObject<string[]>() : new string[] { },
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Subtitle
            };
        }
        public Agent ToAgent(dynamic node)
        {
            return new Data.Agent
            {
                Guid = node.Guid,
                Name = node.Name,
                Article = node.Article,
                Aliases = node.Aliases != null ? node.Aliases.ToObject<string[]>() : new string[] { },
                FirstName = node.FirstName,
                LastName = node.LastName,
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Subtitle
            };
        }
        public Place ToPlace(dynamic node)
        {
            return new Place
            {
                Guid = node.Guid,
                Name = node.Name,
                Article = node.Article,
                Aliases = node.Aliases != null ? node.Aliases.ToObject<string[]>() : new string[] { },
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Subtitle,
                Latitude = node.Latitude,
                Longitude = node.Longitude
            };
        }
        public List<AgentAttributeHyperNode> FindOutgoingRelationships(string agentGuid)
        {
            var attributes = Client.Cypher
                .Match("(agent:" + Label.Agent + " { Guid: {agentGuid} })-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")-[r]->other")
                .WithParams(new
                {
                    agentGuid = agentGuid
                })
                .Where("not (relation-[:ASSERTED_BY|:CITED_IN]->other)")
                .Return((ICypherResultItem agent, ICypherResultItem other, ICypherResultItem r, ICypherResultItem hasRelation) =>
                    new
                    {
                        Agent = agent.As<Agent>(),
                        Entity = other.As<Node<string>>(),
                        EntityType = Return.As<string>("head(labels(other))"),
                        Relationship = r.Type(),
                        RelationshipGuid = Return.As<string>("r.Guid"),
                        HasRelationGuid = Return.As<string>("hasRelation.Guid")
                    })
                    .Results
                    .Select(x => new AgentAttributeHyperNode
                    {
                        Agent = x.Agent,
                        Entity = ToEntity(x.EntityType, x.Entity),
                        EntityType = x.EntityType,
                        Relationship = x.Relationship,
                        RelationshipGuid = x.RelationshipGuid,
                        HasRelationGuid = x.HasRelationGuid
                    })
                    .ToList();
            return attributes;
        }

        public List<Agent> AllPeople()
        {
            var people = Client.Cypher
                .Match("(p:" + Label.Agent + ")")
                .Return(p => p.As<Agent>())
                .OrderBy("p.Name", "p.LastName", "p.FirstName")
                .Results.ToList();
            return people;
        }

        public List<AgentWithAttribute> AllAgentsByType(string agentTypeName)
        {

            var query = Client.Cypher
                .Match("(type:Category { Name: {agentTypeName} })").WithParams(new { agentTypeName })
                .Match("(types:Category)-[:KIND_OF*0..]->(type)")
                .Match("(agent:Agent)-[:HAS_RELATION]->()-[:IS_A]->(types)")
                .Match("(agent)-[:HAS_RELATION]->(isaRelation:Relation)-[:IS_A]->(category:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(born:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(died:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenOf:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Category)")
                ;

            var results = query
                .Return(() => new AgentWithAttribute
                {
                    Agent = Return.As<Agent>("agent"),
                    Types = Return.As<IEnumerable<CategoryRelationTuple>>("collect(distinct { Category: category, Relation: isaRelation })"),
                    BirthYear = Return.As<int?>("born.Year"),
                    DeathYear = Return.As<int?>("died.Year"),
                    Citizenship = Return.As<Place>("citizenOf"),
                    RepresentativeOf = Return.As<Category>("representativeOf")
                })
                .OrderBy("agent.Name")
                .Results
                .ToList();

            return results;
        }

        public List<ArtefactWithAttribute> AllArtefactsByType(string agentTypeName)
        {
            var query = Client.Cypher
                .Match("(type:Category { Name: {agentTypeName} })").WithParams(new { agentTypeName })
                .Match("(types:Category)-[:KIND_OF*0..]->(type)")
                .Match("(artefact:Artefact)-[:HAS_RELATION]->()-[:IS_A]->(types)")
                .Match("(artefact)-[:HAS_RELATION]->()-[:IS_A]->(category:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact)-[:LOCATED_AT]->(location:Place)")
                ;

            var results = query
                .Return(() => new ArtefactWithAttribute
                {
                    Artefact = Return.As<Artefact>("artefact"),
                    Attributes = Return.As<IEnumerable<Category>>("collect(distinct(category))"),
                    Location = Return.As<Place>("location")
                })
                .OrderBy("artefact.Name")
                .Results
                .ToList();

            return results;
        }

        public List<AgentWithAttribute> AllWithAttributes()
        {
            var query = Client.Cypher
                .Match("(agent:Agent)-[:HAS_RELATION]->(isaRelation:Relation)-[:IS_A]->(category:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(born:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(died:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenOf:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Category)")
                ;

            var results = query
                .Return(() => new AgentWithAttribute
                {
                    Agent = Return.As<Agent>("agent"),
                    Types = Return.As<IEnumerable<CategoryRelationTuple>>("collect(distinct { Category: category, Relation: isaRelation })"),
                    BirthYear = Return.As<int?>("born.Year"),
                    DeathYear = Return.As<int?>("died.Year"),
                    Citizenship = Return.As<Place>("citizenOf"),
                    RepresentativeOf = Return.As<Category>("representativeOf")
                })
                .OrderBy("agent.Name")
                .Results
                .ToList();

            return results;

        }
        public List<AgentWithAttribute> AllWithAttributesWithBookmarks(string userGuid)
        {
            var people = Client.Cypher
                .Match("(agent:" + Label.Agent + ")")
                .OptionalMatch("(user:" + Label.Entity.User + " { Guid: {userGuid} })-[bookmarked:" + Label.Relationship.Bookmarked + "]->(agent)")
                .WithParams(new
                {
                    userGuid
                })
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(category:" + Label.Entity.Category + ")")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.CitizenOf + "]->(place:" + Label.Entity.Place + ")")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.RepresentativeOf + "]->(representative:" + Label.Entity.Category + ")")
                .Return((ICypherResultItem agent, ICypherResultItem category, ICypherResultItem place, ICypherResultItem representative) => new AgentWithAttribute
                {
                    Agent = agent.As<Agent>(),
                    Citizenship = place.As<Place>(),
                    RepresentativeOf = representative.As<Category>(),
                    Categories = Return.As<IEnumerable<string>>("collect(category.Name)"),
                    Bookmarked = Return.As<bool>("count(bookmarked) >= 1")
                })
                .OrderBy("agent.FullName")
                .Results
                .ToList();
            return people;
        }
        public List<Topic> AllTopics()
        {
            var results = Client.Cypher
                .Match("(p:" + Label.Entity.Topic + ")")
                .Return(p => p.As<Topic>())
                .OrderBy("p.Name")
                .Results.ToList();
            return results;
        }
        public List<Category> AllCategories()
        {
            var category = Client.Cypher
                .Match("(c:" + Label.Entity.Category + ")")
                .Return(c => c.As<Category>())
                .OrderBy("c.Name")
                .Results.ToList();
            return category;
        }
        public Agent FindAgentByGuid(string guid)
        {
            var agent = Client.Cypher
                .Match("(p:" + Label.Agent + ")")
                .Where((Agent p) => p.Guid == guid)
                .Return(p => p.As<Agent>())
                .Results.FirstOrDefault();
            return agent;
        }
    }
    public class EntityResult
    {
        public Entity Entity { get; set; }
        public IEnumerable<string> Labels { get; set; }
    }
    public class ArtefactWithAttribute
    {
        public Artefact Artefact { get; set; }
        public IEnumerable<Category> Attributes { get; set; }
        public Place Location { get; set; }
        public string ToDisplay()
        {
            var attributes = Attributes.Any() ? ", " + string.Join(", ", Attributes.Select(x => x.Name.ToLower())) : "";
            var location = Location != null ? " in " + Location.Name : "";
            return Artefact.ToDisplay() + attributes + location;
        }
    }
    public class CategoryRelationTuple
    {
        public Category Category { get; set; }
        public Relation Relation { get; set; }
    }
    public class AgentWithAttribute
    {
        public Agent Agent { get; set; }
        public Place Citizenship { get; set; }
        public Category RepresentativeOf { get; set; }
        public IEnumerable<string> Categories { get; set; }
        public IEnumerable<CategoryRelationTuple> Types { get; set; }
        public int? BirthYear { get; set; }
        public int? DeathYear { get; set; }
        public bool Bookmarked { get; set; }
        private string HtmlName
        {
            get
            {
                var prefix = Agent.Article != null ? Agent.Article + " " : "";
                if (false == string.IsNullOrEmpty(Agent.Name))
                {
                    //if (prefix.Length > 0)
                    //{
                    //    return prefix + string.Format("<b>{0}</b>", Agent.Name);
                    //}
                    var names = Agent.Name.Split(' ');
                    var styledNames = "<b>" + names.First() + "</b>";
                    if (names.Length > 1)
                    {
                        styledNames += " " + string.Join(" ", names.Skip(1).Take(names.Length - 1).ToArray());
                    }
                    return prefix + styledNames;
                }
                return prefix + string.Format("{0} <b>{1}</b>", Agent.FirstName, Agent.LastName);
            }
        }
        private string ListName
        {
            get
            {
                var prefix = Agent.Article != null ? ", " + Agent.Article + " " : "";
                if (false == string.IsNullOrEmpty(Agent.Name))
                {
                    var names = Agent.Name.Split(' ');
                    var styledNames = names.First().ToUpper();
                    if (names.Length > 1)
                    {
                        styledNames += " " + string.Join(" ", names.Skip(1).Take(names.Length - 1).ToArray());
                    }
                    return styledNames + prefix;
                }
                if (string.IsNullOrEmpty(Agent.FirstName) || string.IsNullOrEmpty(Agent.LastName))
                {
                    return "";
                }
                return string.Format("{0}, {1}{2}", Agent.LastName.ToUpper(), Agent.Article != null ? Agent.Article + " " : "", Agent.FirstName);
            }
        }
        public string ToDisplay()
        {
            var brackets = new List<string>();
            if (Citizenship != null)
            {
                brackets.Add(Citizenship.Adjective ?? Citizenship.Name);
            }
            if (RepresentativeOf != null)
            {
                brackets.Add(RepresentativeOf.Adjective ?? RepresentativeOf.Name);
            }
            if (Types != null && Types.Any())
            {
                var types2 = Types.OrderByDescending(x => x.Relation.Primary);
                brackets.Add(string.Join(", ", types2.Select(x => x.Relation.Primary ? x.Category.Name.ToUpper() : x.Category.Name.ToLower())));
            }
            var preBrackets = new List<string>();
            if (Agent.Sex.HasValue)
            {
                if (Agent.Sex.Value != Sex.None && Agent.Sex.Value != Sex.Other)
                {
                    preBrackets.Add(Agent.Sex.Value == Sex.Male ? "m." : "f.");
                }
            }
            var dates = "";
            if (BirthYear.HasValue || DeathYear.HasValue)
            {
                dates = string.Format("b.{0}-d.{1}", BirthYear.HasValue ? BirthYear.Value.ToString() : "?", DeathYear.HasValue ? DeathYear.Value.ToString() : "?");
                preBrackets.Add(dates);
            }
            preBrackets.Add(string.Join(" ", brackets.ToArray()));
            if (brackets.Count == 0)
            {
                return ListName + " " + string.Join("; ", preBrackets) + ")";
            }

            return ListName + " (" + string.Join("; ", preBrackets) + ")";
        }
        public List<string> Attributes()
        {
            var brackets = new List<string>();
            if (Citizenship != null)
            {
                brackets.Add(Citizenship.Adjective ?? Citizenship.Name);
            }
            if (RepresentativeOf != null)
            {
                brackets.Add(RepresentativeOf.Adjective ?? RepresentativeOf.Name);
            }
            if (Categories.Any())
            {
                brackets.Add(string.Join(", ", Categories).ToLower());
            }
            return brackets;
        }
        public string SubTitle()
        {
            var brackets = new List<string>();
            if (Citizenship != null)
            {
                brackets.Add(Citizenship.Adjective ?? Citizenship.Name);
            }
            if (RepresentativeOf != null)
            {
                brackets.Add(RepresentativeOf.Adjective ?? RepresentativeOf.Name);
            }
            if (Categories != null && Categories.Any())
            {
                brackets.Add(string.Join(", ", Categories.Select(x => x.ToLower())));
            }
            return string.Join(" ", brackets.Select(x => x));
        }
        public List<string> SlabText()
        {
            var brackets = new List<string>();
            brackets.Add(Agent.FullName2);
            var prefix = "";
            if (Citizenship != null)
            {
                prefix += (Citizenship.Adjective ?? Citizenship.Name);
            }
            if (RepresentativeOf != null)
            {
                prefix += (RepresentativeOf.Adjective ?? RepresentativeOf.Name);
            }
            if (prefix.Length > 0)
            {
                brackets.Add(prefix);
            }
            if (Categories != null && Categories.Any())
            {
                brackets.Add(string.Join(" ", Categories));
            }
            return brackets;
        }
        public string ToHtmlDisplay()
        {
            var brackets = Attributes();
            if (brackets.Count == 0)
            {
                return HtmlName;
            }
            return HtmlName + " (" + string.Join(" ", brackets.ToArray()) + ")";
        }
    }
}
