﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class FamilyHyperNode
    {
        public Agent Agent { get; set; }
        public IEnumerable<Agent> MistressOf { get; set; }
        public IEnumerable<Agent> MistressTo { get; set; }
        public IEnumerable<Agent> SpouseOf { get; set; }
        public IEnumerable<Agent> SpouseTo { get; set; }
        public IEnumerable<Agent> Children { get; set; }
        public IEnumerable<Agent> GrandChildren { get; set; }
        public IEnumerable<Agent> Parents { get; set; }
        public IEnumerable<Agent> GrandParents { get; set; }
        public IEnumerable<Agent> Siblings { get; set; }
        public IEnumerable<Agent> AuntsUncles { get; set; }
        public IEnumerable<Agent> NiecesNephews { get; set; }
        public IEnumerable<Agent> PartOf { get; set; }
        public IEnumerable<Agent> Contains { get; set; }
    }
}
