﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class RelatedTo
    {
        public Agent Agent { get; set; }
        public Agent Relative { get; set; }
        public Category Relationship { get; set; }
        public string RelatedToGuid { get; set; }
        public string RelatedAsGuid { get; set; }
    }
}
