﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class AgentRelatedToOtherAgentHyperNode
    {
        public Agent Agent { get; set; }
        public Agent OtherAgent { get; set; }
        public string Relationship { get; set; }
        public string RelationshipGuid { get; set; }
    }
}
