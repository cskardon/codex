﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ArtefactsRelatedToSimilarArtefacts
    {
        public Artefact Artefact1 { get; set; }
        public Artefact Artefact2 { get; set; }
        public IEnumerable<Artefact> Artefacts { get; set; }
        public IEnumerable<string> RelationTypes { get; set; }
        public long Total { get; set; }
        public long PathCost { get; set; }
        public decimal Rank
        {
            get
            {
                return (decimal)Artefacts.Count() / PathCost;
            }
        }
    }
}
