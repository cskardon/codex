﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ArtefactAgentImageTuple
    {
        public Artefact Artefact { get; set; }
        public Agent Agent { get; set; }
        public Image Image { get; set; }
    }
}
