﻿using Data.Entities;
using log4net;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface IEventService
    {
        void SetCurrentUser(User user);
        Event Find(string guid);
        List<Event> All();
        Event FindParent(string guid);
        List<Event> FindChildren(string guid);
        Event SaveOrUpdate(Event data);
        EventIsA FindIsA(string relationshipGuid);
        List<EventAttributeHyperNode> FindAllAttributes(string eventGuid);
        void AssociatedWith(string eventGuid, string agentGuid);
        void IsA(string eventGuid, string categoryGuid);
        List<Event> FindEventsThatAre(string categoryName);
    }
    public class EventAttributeHyperNode
    {
        public Event Event { get; set; }
        public string EntityType { get; set; }
        public string Relationship { get; set; }
        public string RelationshipGuid { get; set; }
        public Entity Entity { get; set; }
    }
    public class EventIsA
    {
        public Event Event { get; set; }
        public string IsAGuid { get; set; }
        public Category Category { get; set; }
    }
    public class EventService : IEventService
    {
        private readonly ILog Log = LogManager.GetLogger("DefaultLogger");
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly IRelationService relationService;
        public EventService(
            GraphClient client,
            IRelationService _relationService
        )
        {
            Client = client;
            relationService = _relationService;
        }
        public List<Event> FindEventsThatAre(string categoryName)
        {
            return Client.Cypher
                .Match("(evt:" + Label.Entity.Event + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(category:" + Label.Entity.Category + " { Name: {categoryName} })")
                .WithParams(new
                {
                    categoryName
                })
                .Return(evt => evt.As<Event>())
                .Results
                .ToList();
        }
        public void IsA(string eventGuid, string categoryGuid)
        {
            var evt = Find(eventGuid);
            CreateRelationship(evt, categoryGuid, Label.Entity.Category, Label.Relationship.IsA);
        }
        public void AssociatedWith(string eventGuid, string agentGuid)
        {
            var evt = Find(eventGuid);
            CreateRelationship(evt, agentGuid, Label.Entity.Agent, Label.Relationship.AssociatedWith);
        }
        private void CreateRelationship(IEntity entity, string otherGuid, string otherType, string relationship)
        {
            var relation = relationService.HasRelation(entity, relationService.CreateRelation());
            Client.Cypher
                .Match(
                    "(relation:" + Label.Entity.Relation + " { Guid: {relationGuid} })",
                    "(other:" + otherType + " { Guid: {otherGuid} })")
                .WithParams(new
                {
                    relationGuid = relation.Guid,
                    otherGuid
                })
                .CreateUnique("relation-[:" + relationship + " { Guid: {guid} }]->other")
                .WithParams(new
                {
                    guid = Guid.NewGuid().ToString()
                })
                .ExecuteWithoutResults();
        }
        public List<EventAttributeHyperNode> FindAllAttributes(string eventGuid)
        {
            var result = Client.Cypher
                .Match("(evt:" + Label.Entity.Event + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[r]->(entity)")
                .Where((Event evt) => evt.Guid == eventGuid)
                .Return((ICypherResultItem evt, ICypherResultItem entity, ICypherResultItem r) =>
                    new EventAttributeHyperNode
                    {
                        Event = evt.As<Event>(),
                        Entity = entity.As<Entity>(),
                        EntityType = Return.As<string>("head(labels(entity))"),
                        Relationship = r.Type(),
                        RelationshipGuid = Return.As<string>("r.Guid")
                    })
                    .Results
                    .ToList();
            return result;
        }
        public EventIsA FindIsA(string relationshipGuid)
        {
            return Client.Cypher
                    .Match("(self:" + Label.Entity.Event + ")-[r:" + Label.Relationship.IsA + " { Guid: {relationshipGuid} }]->(category:" + Label.Entity.Category + ")")
                    .WithParams(new { relationshipGuid })
                    .Return((ICypherResultItem self, ICypherResultItem category) => new EventIsA
                    {
                        Event = self.As<Event>(),
                        Category = self.As<Category>(),
                        IsAGuid = Return.As<string>("r.Guid")
                    })
                    .Results
                    .FirstOrDefault();
        }
        public Event SaveOrUpdate(Event _event)
        {
            if (_event.Guid == null)
            {
                _event.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(event:" + Label.Entity.Event + " { Guid: {guid} })")
                .OnCreate().Set("event = {_event}")
                .OnMatch().Set("event = {_event}")
                .WithParams(new
                {
                    guid = _event.Guid,
                    _event
                })
                .ExecuteWithoutResults();
            return _event;

        }
        public Event FindParent(string guid)
        {
            var result = Client.Cypher
                 .Match("(parent:" + Label.Entity.Event + ")-[:" + Label.ParentOf + "]->(child:" + Label.Entity.Event + ")")
                 .Where((Event child) => child.Guid == guid)
                 .Return(parent => parent.As<Event>())
                 .Results.FirstOrDefault();
            return result;
        }
        public List<Event> FindChildren(string guid)
        {
            var result = Client.Cypher
                 .Match("(parent:" + Label.Entity.Event + ")-[:" + Label.ParentOf + "]->(child:" + Label.Entity.Event + ")")
                 .Where((Event parent) => parent.Guid == guid)
                 .Return(child => child.As<Event>())
                 .Results
                 .ToList();
            return result;
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        public List<Event> All()
        {
            return Client.Cypher
                .Match("(e:" + Label.Entity.Event + ")")
                .Return(e => e.As<Event>())
                .Results
                .ToList();
        }
        public Event Find(string guid)
        {
            return Client.Cypher
                .Match("(e:" + Label.Entity.Event + " { Guid: {guid} })")
                .WithParams(new
                {
                    guid
                })
                .Return(e => e.As<Event>())
                .Results
                .FirstOrDefault();
        }
    }
}
