﻿using Data;
using Data.Entities;
using Neo4jClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Services.Helpers
{
    public static class Common
    {
        public static string YearsMonthsDays(double totalDays)
        {
            var result = new List<string>();
            var totalYears = Math.Truncate(totalDays / 365);
            var totalMonths = Math.Truncate((totalDays % 365) / 30);
            var remainingDays = Math.Truncate((totalDays % 365) % 30);
            if (totalYears > 0)
            {
                result.Add(totalYears + " " + (totalYears == 1 ? "year" : "years"));
            }
            if (totalMonths > 0)
            {
                result.Add(totalMonths + " " + (totalMonths == 1 ? "month" : "months"));
            }
            if (remainingDays > 0)
            {
                result.Add(remainingDays + " " + (remainingDays == 1 ? "day" : "days"));
            }
            return string.Join(", ", result);
        }

        public static int? TryParseInt(string value)
        {
            if (value != null)
            {
                if (value.Contains(" to "))
                {
                    try
                    {
                        var index = value.IndexOf(" to ");
                        var first = TryParseInt(value.Substring(0, index));
                        var second = TryParseInt(value.Substring(index + 4));
                        if (first.HasValue && second.HasValue)
                        {
                            var average = (first.Value + second.Value) / 2;
                            value = average.ToString();
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                value = Regex.Replace(value, "[^0-9.]", "");
            }
            int parsed = 0;
            return int.TryParse(value, out parsed) ? parsed : (int?)null;
        }

        public static DateTime? ToDateTime(int? year, int? month, int? day, int? hour, int? minute)
        {
            if (year == null)
            {
                return null;
            }
            var monthTest = month ?? 1;
            if (monthTest > 12)
            {
                monthTest -= 12;
            }
            return new DateTime(year.Value, monthTest, day ?? 1, hour ?? 0, minute ?? 0, 0);
        }

        public static string NameOf(IEntity entity)
        {
            if (entity is Data.Agent)
            {
                return ((Data.Agent)entity).FullName2;
            }
            if (entity is Artefact)
            {
                return ((Artefact)entity).ToDisplay();
            }
            return entity.Name;
        }
        public static string ToHtml(IEntity actOrRelation, IEnumerable<IEntity> entities, bool generateAnchor = false)
        {
            if (actOrRelation == null || actOrRelation.Description == null)
            {
                return null;
            }
            var text = actOrRelation.Description;
            var result = text;
            if (entities == null || entities.All(x => x == null))
            {
                return null;
            }
            foreach (var entity in entities.Where(x => x != null))
            {
                var name = entity is Agent ? ((Agent)entity).FullName2 : entity is Artefact ? ((Artefact)entity).ToDisplay() : entity.Name;
                var type = entity.GetType().Name;
                var guid = entity.Guid;
                var partPattern = @"\[" + type + @"\|" + guid + @"\]";
                var fullPattern = @"\[" + type + @"\|" + guid + @"(\|.*?)\]";
                var anchor = generateAnchor ? "#act-" + actOrRelation.Guid : "";
                if (Regex.IsMatch(result, partPattern))
                {
                    var link = "<a href=\"/Admin/" + type + "/ViewDetail?guid=" + guid + anchor + "\">" + name + "</a>";
                    result = Regex.Replace(result, partPattern, link);
                }
                else
                {
                    var matches = Regex.Matches(result, fullPattern);
                    if (matches.Count > 0)
                    {
                        var element = matches[0].Value;
                        var lastIndex = element.LastIndexOf("|");
                        if (lastIndex > 0)
                        {
                            var customName = element.Substring(lastIndex + 1).Replace("]", "");
                            var link = "<a href=\"/Admin/" + type + "/ViewDetail?guid=" + guid + anchor + "\" title=\"" + name + "\">" + customName + "</a>";
                            result = Regex.Replace(result, fullPattern, link);

                        }
                    }
                }
            }
            return result;
        }
        public static T ConvertTo<T>(dynamic source) where T : class
        {
            return (T)Convert.ChangeType(source, typeof(T));
        }
        public static Artefact ToArtefact(dynamic node)
        {
            return new Artefact
            {
                Guid = node.Guid,
                Name = node.Name,
                Description = node.Description,
                Article = node.Article,
                Height = node.Height,
                Width = node.Width,
                Depth = node.Depth,
                Plural = node.Plural,
                Subtitle = node.Subtitle
            };
        }
        public static Agent ToAgent(dynamic node)
        {
            return new Data.Agent
            {
                Guid = node.Guid,
                Name = node.Name,
                FirstName = node.FirstName,
                LastName = node.LastName,
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Subtitle
            };
        }
        public static Place ToPlace(dynamic node)
        {
            return new Place
            {
                Guid = node.Guid,
                Name = node.Name,
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Subtitle,
                Latitude = node.Latitude,
                Longitude = node.Longitude
            };
        }
        public static Category ToCategory(dynamic node)
        {
            return new Category
            {
                Guid = node.Guid,
                Name = node.Name,
                Description = node.Description,
                Plural = node.Plural,
                Subtitle = node.Tagline
            };
        }

        public static T ToStatic<T>(object expando)
        {
            var entity = Activator.CreateInstance<T>();

            //ExpandoObject implements dictionary
            var properties = expando as IDictionary<string, object>;

            if (properties == null)
                return entity;

            foreach (var entry in properties)
            {
                var propertyInfo = entity.GetType().GetProperty(entry.Key);
                if (propertyInfo != null)
                {
                    var setMethod = propertyInfo.GetSetMethod();
                    if (setMethod != null)
                    {
                        propertyInfo.SetValue(entity, entry.Value, null);
                    }
                }
            }
            return entity;
        }


        public static Video ToVideo(dynamic node)
        {
            return new Video
            {
                Guid = node.Guid,
                Name = node.Name,
                Uri = node.Uri,
                StartTime = node.StartTime,
                EndTime = node.EndTime,
                Description = node.Description,
                Duration = node.Duration,
                QuickAdded = node.QuickAdded
            };
        }
        public static Document ToDocument(dynamic node)
        {
            return new Document
            {
                Guid = node.Guid,
                Name = node.Name,
                Description = node.Description,
                Proximity = node.Proximity,
                Milennium = node.Milennium,
                Year = node.Year,
                Month = node.Month,
                Day = node.Day,
                Hour = node.Hour,
                Minute = node.Minute,
                ProximityEnd = node.ProximityEnd,
                MilenniumEnd = node.MilenniumEnd,
                YearEnd = node.YearEnd,
                MonthEnd = node.MonthEnd,
                DayEnd = node.DayEnd,
                HourEnd = node.HourEnd,
                MinuteEnd = node.MinuteEnd
            };
        }
        public static IEntity ToEntity(string type, Node<string> node)
        {
            if (node == null || node.Data == null)
            {
                return null;
            }
            var dynamicNode = JsonConvert.DeserializeObject<dynamic>(node.Data);
            if (type == Label.Entity.Agent)
            {
                return ToAgent(dynamicNode);
            }
            if (type == Label.Entity.Place)
            {
                return ToPlace(dynamicNode);
            }
            if (type == Label.Entity.Category)
            {
                return ToCategory(dynamicNode);
            }
            if (type == Label.Entity.Artefact)
            {
                return ToArtefact(dynamicNode);
            }
            if (type == Label.Entity.Document)
            {
                return ToDocument(dynamicNode);
            }
            if (type == Label.Entity.Video)
            {
                return ToVideo(dynamicNode);
            }
            if (type == Label.Entity.Image)
            {
                return ToStatic<Image>(JsonConvert.DeserializeObject<ExpandoObject>(node.Data)); ;
            }
            if (type == Label.Entity.Tag)
            {
                return ToStatic<Tag>(JsonConvert.DeserializeObject<ExpandoObject>(node.Data)); ;
            }
            return (IEntity)null;
        }
    }
}
