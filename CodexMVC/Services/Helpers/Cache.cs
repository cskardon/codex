﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;

namespace Services.Helpers
{
    public static class Cache
    {
        public static void AddToCache<T>(string key, T value, int? minutes = null)
        {
            MemoryCache.Default.Add(
                key,
                value,
                minutes == null ? null
                    : new CacheItemPolicy
                    {
                        SlidingExpiration = TimeSpan.FromMinutes(minutes.Value)
                    }
            );
        }
        public static void AddToCache<T>(string key, T value, int minutes)
        {
            MemoryCache.Default.Add(key, value, new CacheItemPolicy { SlidingExpiration = TimeSpan.FromMinutes(minutes) });
        }
        public static T GetFromCache<T>(string key)
        {
            return (T)MemoryCache.Default.Get(key);
        }
        public static void RemoveFromCache(string key)
        {
            MemoryCache.Default.Remove(key);
        }
        public static T GetFromOrAddToCache<T>(string key, Func<T> getter, int? minutes = null)
        {
            if (GetFromCache<T>(key) == null)
            {
                AddToCache(key, getter(), minutes);
            }
            return GetFromCache<T>(key);
        }
        public async static Task<T> GetFromOrAddToCacheAsync<T>(string key, Func<Task<T>> getter, int? minutes = null)
        {
            if (GetFromCache<T>(key) == null)
            {
                var result = await getter();
                AddToCache(key, result, minutes);
            }
            return GetFromCache<T>(key);
        }
    }
}
