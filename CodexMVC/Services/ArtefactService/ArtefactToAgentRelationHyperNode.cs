﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Services
{
    public class ArtefactToCategoryRelationHyperNode : GenericRelationHyperNode<Artefact,Category>
    {
    }
    public class ArtefactToEntityRelationHyperNode : GenericRelationHyperNode<Artefact, Entity>
    {
       
    }
}
