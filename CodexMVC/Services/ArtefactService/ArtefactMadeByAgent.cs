﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ArtefactMadeByAgent
    {
        public Artefact Artefact { get; set; }
        public Relation MadeBy { get; set; }
        public IEnumerable<Agent> Agents { get; set; }
        public IEnumerable<Category> IsA { get; set; }
        public Place Location { get; set; }
        public string ToDisplay()
        {
            var prefix = Artefact.Article != null ? ", " + Artefact.Article + " " : "";
            var agents = Agents != null && Agents.Any() ? " by " + string.Join(", ", Agents.Select(x => x.FullName2.ToUpper()).ToArray()) : "";
            var isA = IsA != null && IsA.Any() ? string.Join(", ", IsA.Select(x => x.Name.ToLower()).ToArray()) + " " : "";
            var location = Location != null ? " in " + Location.Name.ToUpper() : "";
            var startYear = MadeBy != null && MadeBy.Year.HasValue ? MadeBy.Year.Value.ToString() : null;
            var endYear = MadeBy != null && MadeBy.YearEnd.HasValue ? MadeBy.YearEnd.Value.ToString() : null;
            var years = "";
            if (startYear != null)
            {
                years = startYear;
                if (endYear != null)
                {
                    years += "->" + endYear;
                }
                years = " [" + years + "]";
            }
            return Artefact.Name + prefix + location + years + " (" + isA + agents + ")";
        }
    }
}
