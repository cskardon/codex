﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class GenericRelationNode<TSource, TDest>
        where TSource : IEntity
        where TDest : IEntity
    {
        public TSource Source { get; set; }
        public TDest Destination { get; set; }
        public Relation SourceBirth { get; set; }
        public Relation DestinationBirth { get; set; }
        public Relation SourceDeath { get; set; }
        public Relation DestinationDeath { get; set; }
        public string DestinationType { get; set; }
        public string RelationToDestinationType { get; set; }
        public string RelationToDestinationGuid { get; set; }
        public string NameOf(IEntity entity)
        {
            if (entity is Data.Agent)
            {
                return ((Data.Agent)entity).FullName2;
            }
            if (entity is Artefact)
            {
                return ((Artefact)entity).ToDisplay();
            }
            if (entity is Place)
            {
                return ((Place)entity).ToDisplay();
            }
            return entity.Name;
        }
        public bool IsNew
        {
            get
            {
                return RelationToDestinationGuid == null;
            }
        }
        public bool IsSaveable
        {
            get
            {
                return Destination != null && Destination.Guid != null;
            }
        }
        public bool IsExisting
        {
            get
            {
                return RelationToDestinationGuid != null;
            }
        }
    }

    /// <summary>
    /// Represents a relation between one entity and another -- e.g., 'Leonardo' IS_A 'Painter', 'Nocturne in E' MADE_BY 'Chopin'
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TDest"></typeparam>
    public class GenericRelationHyperNode<TSource, TDest> : GenericRelationNode<TSource, TDest>
        where TSource : IEntity
        where TDest : IEntity
    {
        public string Action { get; set; }
        public Relation Relation { get; set; }
        public string HasRelationGuid { get; set; }
        public IEnumerable<IEntity> Entities { get; set; }
        public IEnumerable<EntityImage> EntityImages { get; set; }
        public IEnumerable<Category> SourceAttributes { get; set; }
        public IEnumerable<Category> DestinationAttributes { get; set; }
        public IEnumerable<string[]> References { get; set; }
        public IEnumerable<IEntity> Context()
        {
            if (Entities == null || Entities.All(x => x == null))
            {
                return null;
            }
            return Entities.Where(x => false == Relation.Description.Contains(x.Guid));
        }
        public string ToHtml(bool generateAnchor = false)
        {
            return Helpers.Common.ToHtml(Relation, Entities, generateAnchor);
        }
        public string ToMapHtml(bool reverse = false)
        {
            var sourceName = NameOf(Source);
            var destName = NameOf(Destination);
            var relation = Label.Relationship.ToEnglish(RelationToDestinationType, reverse);
            return string.Format("<a href='/Admin/{3}/ViewDetail?guid={4}'>{0}</a> {1} <a href='/Admin/{5}/ViewDetail?guid={6}'>{2}</a>", sourceName, relation, destName, Source.GetType().Name, Source.Guid, Destination.GetType().Name, Destination.Guid);
        }
    }
    /// <summary>
    /// Represents a relation between an entity and two others -- e.g., 'Harrison Ford' PERFORMED_IN 'Star Wars' AS_CHARACTER 'Han Solo',
    /// 'Vladimir Horowitz' PERFORMED_IN 'Rachmaninov's Third Piano Concerto' ON_INSTRUMENT 'Piano'.
    /// </summary>
    /// <typeparam name="TSource"></typeparam>
    /// <typeparam name="TDest"></typeparam>
    /// <typeparam name="TDest2"></typeparam>
    public class GenericRelationHyperNodeDouble<TSource, TDest, TDest2> : GenericRelationHyperNode<TSource, TDest>
        where TSource : IEntity
        where TDest : IEntity
        where TDest2 : IEntity
    {
        public TDest2 Destination2 { get; set; }
        public string Destination2Type { get; set; }
        public string RelationToDestination2Guid { get; set; }
    }
    public class ArtefactToAgentRelationHyperNode : GenericRelationHyperNode<Artefact, Agent>
    {

    }
}
