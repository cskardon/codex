﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class FindByCategoryAndAgentResult
    {
        public Agent MadeBy { get; set; }
        public Artefact Artefact { get; set; }
        public IEnumerable<Category> Categories { get; set; }
        public Image Image { get; set; }
    }
    public class AgentCategoryTuple
    {
        public Agent Agent { get; set; }
        public Category Category { get; set; }
    }
    public class ArtefactsMadeByResult
    {
        public Artefact Artefact { get; set; }
        public IEnumerable<Category> Is { get; set; }
        public IEnumerable<AgentCategoryTuple> AgentsRepresented { get; set; }
        public IEnumerable<Category> CategoriesRepresented { get; set; }
        public Image Image { get; set; }
        public Artefact Location { get; set; }
        public Agent Owner { get; set; }
        public Place Place { get; set; }
        public bool HasChildren { get; set; }
    }
}
