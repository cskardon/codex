﻿using Data.Entities;
using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ArtefactAttributeDynamic
    {
        public Artefact Artefact { get; set; }
        public string EntityType { get; set; }
        public Node<string> EntityNode { get; set; }
        public dynamic EntityDynamic { get; set; }
        public dynamic RelationshipDynamic { get; set; }
        public string RelationshipType { get; set; }
        public Node<string> RelationshipNode { get; set; }
        public string HasRelationGuid { get; set; }
    }
}
