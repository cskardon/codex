﻿using Data;
using Data.Entities;
using log4net;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;

namespace Services
{
    public enum ResetPasswordFromEmailResult
    {
        Exception,
        Success,
        TokenWindowExpired,
        UserNotFound,
        TokenNotRecognised,
        TokenAlreadyConsumed
    }
    public enum ForgotPasswordResult
    {
        Success,
        UserNotFound,
        EmailAlreadySentWithinWindow,
        UnableToSendEmail
    }
    public class ItineraryDay : IIdentifiable, IOrdinal
    {
        /**
         * IIdentifiable
         */
        public string Guid { get; set; }
        /**
         * IOrdinal
         */
        public int? Ordinal { get; set; }

        public string Notes { get; set; }
    }
    public class DayArtefactsTuple
    {
        public ItineraryDay Day { get; set; }
        public IEnumerable<Artefact> Artefacts { get; set; }
    }
    public class Itinerary : IBaseEntity
    {
        /**
         * IBaseEntity
         */
        public string Guid { get; set; }
        public string Name { get; set; }
        public DateTimeOffset? Created { get; set; }
        public string Notes { get; set; }
        public DateTimeOffset? From { get; set; }
        public DateTimeOffset? To { get; set; }
        public int? Duration { get; set; }

        public string Dates()
        {
            if (false == From.HasValue)
            {
                return "Undated";
            }
            var results = new List<string>();

            results.Add(From.Value.Date.ToShortDateString());
            if (To.HasValue)
            {
                results.Add(To.Value.Date.ToShortDateString());
            }
            return string.Join(" to ", results);
        }
    }
    public class ItineraryWithDaysAndUsers
    {
        public Itinerary Itinerary { get; set; }
        public IEnumerable<DayArtefactsTuple> Days { get; set; }
        public User Owner { get; set; }
        public IEnumerable<User> SharingWith { get; set; }
    }
    public interface IUserService
    {
        IEnumerable<Itinerary> FindItinerariesForUser(string userGuid);
        ItineraryWithDaysAndUsers FindItineraryWithDaysAndUsers(string guid);
        UserNode GetUserWithPermissionsFromOAuth(string username);
        bool AddRoleToUser(string userGuid, string roleGuid);
        bool AddPermissionToRole(string permissionGuid, string roleGuid);
        List<Role> AllRoles();
        List<User> AllUsers();
        List<Permission> AllPermissions();
        User SaveOrUpdate(User user);
        Role SaveOrUpdate(Role role);
        Permission SaveOrUpdate(Permission permission);
        void AddRoleToUser(Role role, User user);
        User FindUser(string name);
        User FindUserByGuid(string guid);
        Role FindRoleByGuid(string guid);
        Permission FindPermission(string permissionGuid);
        List<Role> GetRolesForUser(string userGuid);
        List<Role> FindRolesAssociatedWithPermission(string permissionGuid);
        List<Permission> GetPermissionsForRole(string roleGuid);
        UserNode GetUserWithPermissions(string username, string password);

        void AddBookmark(string userGuid, string entityType, string entityGuid);
        void DeleteBookmark(string userGuid, string entityType, string entityGuid);

        IEnumerable<NewAct> FindBookmarkedActs(string userGuid);
        bool IsAgentBookmarked(string agentGuid, string userGuid);
        List<AgentWithAttribute> FindBookmarkedAgents(string userGuid);
        //IEnumerable<ArtistArtefactImageTuple> FindBookmarkedArtefacts(string userGuid);
        IEnumerable<ItineraryWithDaysAndUsers> FindItineraryItemHyperNodes(string userGuid);

        void Join(string email, string password, string firstname, string lastname, int? age, string profession);

        void ResetPassword(string userGuid, string newPassword);

        ForgotPasswordResult ForgotPassword(string email);

        bool IsResetPasswordAuthorised(string email, string tokenGuid);

        ResetPasswordFromEmailResult ResetPasswordFromEmail(string username, string tokenGuid, string newPassword);

        void AddSeenIt(string userGuid, string artefactGuid);

        void DeleteSeenIt(string p, string artefactGuid);

        void RemovePermissionFromRole(string permissionGuid, string roleGuid);

        void InterestedIn(string userGuid, string styleGuid);

        Itinerary SaveOrUpdate(Itinerary itinerary);
        ItineraryDay SaveOrUpdate(ItineraryDay day);
        void HasDay(string itineraryGuid, string dayGuid);
        void DayRefersTo(string dayGuid, string artefactGuid);
        void HasItinerary(string userGuid, string itineraryGuid);
        void FollowAgent(string userGuid, string agentGuid);
        void UnfollowAgent(string userGuid, string agentGuid);
        UserAgentDetails FindUserAgentDetails(string userGuid, string agentGuid);
        UserArtefactDetails FindUserArtefactDetails(string userGuid, string artefactGuid);

        int FindFollowedArtistsUpdatedTotal(string userGuid);
        IEnumerable<ArtistWithArtefactImage> FindArtworkNotifications(string userGuid, int days);

        void UpdateAgentRating(string userGuid, string agentGuid, decimal? rating);
        void UpdateArtefactRating(string userGuid, string artefactGuid, decimal? rating);
    }
    public class UserAgentDetails
    {
        public bool Following { get; set; }
        public bool Bookmarked { get; set; }
        public decimal? Rating { get; set; }
        public int TotalUsersWhoRated { get; set; }
        public decimal? TotalUserRatings { get; set; }
    }
    public class UserArtefactDetails
    {
        public bool Following { get; set; }
        public bool Bookmarked { get; set; }
        public decimal? Rating { get; set; }
    }
    public class Role : Entity, IEntity
    {
        //public string Description { get; set; }
    }
    public class Permission : Entity, IEntity
    {
        //public string Description { get; set; }
    }
    public class UserService : IUserService
    {
        private readonly ILog Log = LogManager.GetLogger("DefaultLogger");
        private readonly GraphClient Client;
        private readonly ICommonService commonService;
        public UserService(
            GraphClient client,
            ICommonService _commonService
            )
        {
            Client = client;
            commonService = _commonService;
        }

        public void HasItinerary(string userGuid, string itineraryGuid)
        {
            commonService.CreateRelationship(Label.Entity.User, userGuid, Label.Entity.Itinerary, itineraryGuid, Label.Relationship.HasItinerary);
        }

        public IEnumerable<ArtistWithArtefactImage> FindArtworkNotifications(string userGuid, int days)
        {
            days = days * -1;
            var cutoff = DateTimeOffset.Now.AddDays(days);

            var query = Client.Cypher
                .Match("(user:User { Guid: {userGuid }})").WithParams(new { userGuid })
                .Match("(user)-[:FOLLOWS]->(artist:Agent)")
                .Match("(artwork)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Where((Artefact artwork) => artwork.Created > cutoff)
                .Match("(artwork)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .With("{ Artist: artist, ArtefactWithImages: collect(distinct{ Artefact: artwork, Image: image }) } as ArtistWithArtworks")
                ;

            var result = query
                .Return(() => Return.As<ArtistWithArtefactImage>("ArtistWithArtworks"))
                .Results
                .ToList();

            return result;

        }

        public int FindFollowedArtistsUpdatedTotal(string userGuid)
        {
            var yesterday = DateTimeOffset.Now.AddDays(-3);

            var query = Client.Cypher
                .Match("(user:User { Guid: {userGuid }})").WithParams(new { userGuid })
                .Match("(user)-[:FOLLOWS]->(artist:Agent)")
                .Match("(artwork)-[:HAS_RELATION]->()-[:MADE_BY]->(artist)")
                .Where((Artefact artwork) => artwork.Created > yesterday)
                ;

            var artistsUpdated = query
                .Return(() => Return.As<int?>("count(distinct artist)"))
                .Results
                .FirstOrDefault();

            return artistsUpdated ?? 0;
        }

        public UserArtefactDetails FindUserArtefactDetails(string userGuid, string artefactGuid)
        {
            var query = Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })").WithParams(new { userGuid, artefactGuid })
                .OptionalMatch("following=(user)-[:" + Label.Relationship.Follows + "]->(artefact)")
                .OptionalMatch("bookmarked=(user)-[:" + Label.Relationship.Bookmarked + "]->(artefact)")
                .OptionalMatch("(user)-[rates:" + Label.Relationship.Rates + "]->(artefact)")                
                ;

            var result = query
                .Return(() => new UserArtefactDetails
                {
                    Following = Return.As<bool>("length(following) = 1"),
                    Bookmarked = Return.As<bool>("length(bookmarked) = 1"),
                    Rating = Return.As<decimal?>("rates.Rating")
                })
                .Results
                .FirstOrDefault();

            return result;
        }

        public UserAgentDetails FindUserAgentDetails(string userGuid, string agentGuid)
        {
            var query = Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })").WithParams(new { userGuid, agentGuid })
                .OptionalMatch("following=(user)-[:" + Label.Relationship.Follows + "]->(agent)")
                .OptionalMatch("bookmarked=(user)-[:" + Label.Relationship.Bookmarked + "]->(agent)")
                .OptionalMatch("(user)-[rates:" + Label.Relationship.Rates + "]->(agent)")
                .OptionalMatch("(:" + Label.Entity.User + ")-[allRatings:" + Label.Relationship.Rates + "]->(agent)")
                ;

            var result = query
                .Return(() => new UserAgentDetails
                {
                    Following = Return.As<bool>("length(following) = 1"),
                    Bookmarked = Return.As<bool>("length(bookmarked) = 1"),
                    Rating = Return.As<decimal?>("rates.Rating"),
                    TotalUsersWhoRated = Return.As<int>("count(allRatings)"),
                    TotalUserRatings = Return.As<decimal?>("sum(allRatings.Rating)")
                })
                .Results
                .FirstOrDefault();

            return result;
        }

        public void FollowAgent(string userGuid, string agentGuid)
        {
            var relationshipGuid = Guid.NewGuid().ToString();
            var created = DateTimeOffset.Now;
            Client.Cypher
                .Match(
                    "(user:" + Label.Entity.User + " { Guid: {userGuid} })",
                    "(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    userGuid,
                    agentGuid
                })
                .CreateUnique("user-[:" + Label.Relationship.Follows + " { Guid: {relationshipGuid}, Created: {created} }]->agent")
                .WithParams(new
                {
                    relationshipGuid,
                    created
                })
                .ExecuteWithoutResults();
        }

        public void UpdateArtefactRating(string userGuid, string artefactGuid, decimal? rating)
        {
            var match = Client.Cypher
                .Match(
                    "(user:" + Label.Entity.User + " { Guid: {userGuid} })",
                    "(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })")
                .WithParams(new
                {
                    userGuid,
                    artefactGuid
                })
            ;

            match
                .Merge("(user)-[rating:" + Label.Relationship.Rates + "]->(artefact)")
                .Set("rating.Updated = {updated}")
                .Set("rating.Rating = {rating}")
                .WithParams(new
                {
                    updated = DateTimeOffset.Now,
                    rating
                })
                .ExecuteWithoutResults();
        }

        public void UpdateAgentRating(string userGuid, string agentGuid, decimal? rating)
        {
            var match = Client.Cypher
                .Match(
                    "(user:" + Label.Entity.User + " { Guid: {userGuid} })",
                    "(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    userGuid,
                    agentGuid
                })
            ;

            match
                .Merge("(user)-[rating:" + Label.Relationship.Rates + "]->(agent)")
                .Set("rating.Updated = {updated}")
                .Set("rating.Rating = {rating}")
                .WithParams(new
                {
                    updated = DateTimeOffset.Now,
                    rating
                })
                .ExecuteWithoutResults();
        }

        public void UnfollowAgent(string userGuid, string agentGuid)
        {
            Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })-[r:" + Label.Relationship.Follows + "]->(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    userGuid,
                    agentGuid
                })
                .Delete("r")
                .ExecuteWithoutResults();
        }

        public void DayRefersTo(string dayGuid, string artefactGuid)
        {
            commonService.CreateRelationship(Label.Entity.Day, dayGuid, Label.Entity.Artefact, artefactGuid, Label.Relationship.RefersTo);
        }

        public void HasDay(string itineraryGuid, string dayGuid)
        {
            commonService.CreateRelationship(Label.Entity.Itinerary, itineraryGuid, Label.Entity.Day, dayGuid, Label.Relationship.HasDay);
        }

        public ItineraryDay SaveOrUpdate(ItineraryDay day)
        {
            return commonService.SaveOrUpdate<ItineraryDay>(day, Label.Entity.Day);
        }

        public Itinerary SaveOrUpdate(Itinerary itinerary)
        {
            return commonService.SaveOrUpdate<Itinerary>(itinerary, Label.Entity.Itinerary);
        }

        public IEnumerable<Itinerary> FindItinerariesForUser(string userGuid)
        {

            var query = Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })").WithParams(new { userGuid })
                .Match("(user)-[:" + Label.Relationship.HasItinerary + "]->(itinerary:" + Label.Entity.Itinerary + ")")
                ;

            var result = query
                .Return(() => Return.As<Itinerary>("itinerary"))
                .Results
                .ToList();

            return result;

        }

        public ItineraryWithDaysAndUsers FindItineraryWithDaysAndUsers(string itineraryGuid)
        {

            var query = Client.Cypher
                .Match("(itinerary:" + Label.Entity.Itinerary + " { Guid: {itineraryGuid} })").WithParams(new { itineraryGuid })
                .Match("(owner:" + Label.Entity.User + ")-[:" + Label.Relationship.HasItinerary + "]->(itinerary)")
                .OptionalMatch("(itinerary)-[:" + Label.Relationship.SharedWith + "]->(sharingWith:" + Label.Entity.User + ")")
                .OptionalMatch("(itinerary)-[:" + Label.Relationship.HasDay + "]->(day:" + Label.Entity.Day + ")")
                .OptionalMatch("(day)-[:" + Label.Relationship.RefersTo + "]->(artefact:" + Label.Entity.Artefact + ")")
                .With("{ Day: day, Artefacts: collect(distinct(artefact)) } as Days, sharingWith, owner, itinerary")
                ;

            var result = query
                .Return(() => new ItineraryWithDaysAndUsers
                {
                    Itinerary = Return.As<Itinerary>("itinerary"),
                    Owner = Return.As<User>("owner"),
                    SharingWith = Return.As<IEnumerable<User>>("collect(distinct(sharingWith))"),
                    Days = Return.As<IEnumerable<DayArtefactsTuple>>("collect(distinct Days)")
                })
                .Results
                .FirstOrDefault();

            return result;
        }

        public void InterestedIn(string userGuid, string styleGuid)
        {
            commonService.CreateRelationship(Label.Entity.User, userGuid, Label.Entity.Category, styleGuid, Label.Relationship.InterestedIn);
        }
        public void AddSeenIt(string userGuid, string artefactGuid)
        {
            var relationshipGuid = Guid.NewGuid().ToString();
            var date = DateTimeOffset.Now;
            Client.Cypher
                .Match(
                    "(user:" + Label.Entity.User + " { Guid: {userGuid} })",
                    "(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })")
                .WithParams(new
                {
                    userGuid,
                    artefactGuid
                })
                .CreateUnique("user-[:" + Label.Relationship.Saw + " { Guid: {relationshipGuid}, Date: {date} }]->artefact")
                .WithParams(new
                {
                    relationshipGuid,
                    date
                })
                .ExecuteWithoutResults();
        }

        public IEnumerable<ItineraryWithDaysAndUsers> FindItineraryItemHyperNodes(string userGuid)
        {
            var result = Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
                .Match("(user)-[:" + Label.Relationship.HasItinerary + "]->(itinerary:" + Label.Entity.Itinerary + ")")
                .OptionalMatch("(itinerary)-[:" + Label.Relationship.RefersTo + "]->(artefact)")
                .OptionalMatch("(itinerary)-[:" + Label.Relationship.SharedWith + "]->(sharedWith:" + Label.Entity.User + ")")
                .WithParams(new
                {
                    userGuid
                })
                 .Return(() => new
                 {
                     User = Return.As<User>("user"),
                     SharedWith = Return.As<IEnumerable<User>>("collect(distinct(sharedWith))"),
                     ItineraryItem = Return.As<Itinerary>("itineraryItem"),
                     Entity = Return.As<Node<string>>("entity"),
                     EntityType = Return.As<string>("head(labels(entity))")
                 })
                 .Results
                 .Select(x => new ItineraryWithDaysAndUsers
                 {
                     Owner = x.User,
                     SharingWith = x.SharedWith,
                     Itinerary = x.ItineraryItem
                 })
                 .ToList();
            return result;
        }

        public bool IsResetPasswordAuthorised(string email, string tokenGuid)
        {
            var user = FindUser(email);
            if (false == user.PasswordResetStartWindow.HasValue)
            {
                return false;
            }
            var now = DateTimeOffset.Now;
            var elapsed = now - user.PasswordResetStartWindow.Value;
            if (elapsed.TotalMinutes > 15)
            {
                return false;
            }
            if (user.PasswordResetToken != tokenGuid)
            {
                return false;
            }
            return true;
        }

        public bool IsAgentBookmarked(string agentGuid, string userGuid)
        {
            return IsBookmarked(Label.Entity.Agent, agentGuid, userGuid);
        }

        private bool IsBookmarked(string type, string entityGuid, string userGuid)
        {
            return Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })-[bookmarked:" + Label.Relationship.Bookmarked + "]->(entity:" + type + " { Guid: {entityGuid }})")
                .WithParams(new
                {
                    entityGuid,
                    userGuid
                })
                .Return((ICypherResultItem bookmarked) => bookmarked.Count())
                .Results
                .First() > 0;
        }
        public List<AgentWithAttribute> FindBookmarkedAgents(string userGuid)
        {
            var people = Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })-[bookmarked:" + Label.Relationship.Bookmarked + "]->(agent:" + Label.Entity.Agent + ")")
                .WithParams(new
                {
                    userGuid
                })
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(category:" + Label.Entity.Category + ")")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.CitizenOf + "]->(place:" + Label.Entity.Place + ")")
                .OptionalMatch("(agent)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.RepresentativeOf + "]->(representative:" + Label.Entity.Category + ")")
                .Return((ICypherResultItem agent, ICypherResultItem category, ICypherResultItem place, ICypherResultItem representative) => new AgentWithAttribute
                {
                    Agent = agent.As<Agent>(),
                    Citizenship = place.As<Place>(),
                    RepresentativeOf = representative.As<Category>(),
                    Categories = Return.As<IEnumerable<string>>("collect(category.Name)"),
                    Bookmarked = Return.As<bool>("count(bookmarked) >= 1")
                })
                .OrderBy("agent.FullName")
                .Results
                .ToList();
            return people;
        }
        //public IEnumerable<Artefact> FindBookmarkedArtefacts(string userGuid)
        //{
        //    var result = Client.Cypher
        //        .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
        //        .Match("(user)-[:" + Label.Relationship.Bookmarked + "]->(artefact:" + Label.Entity.Artefact + ")")
        //        .WithParams(new { userGuid })
        //        .Return((ICypherResultItem artefact) => artefact.As<Artefact>())
        //        .Results
        //        .ToList();
        //    return result;
        //}
        public IEnumerable<NewAct> FindBookmarkedActs(string userGuid)
        {
            var query = Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
                .WithParams(new
                {
                    userGuid
                })
                .Match("(user)-[:" + Label.Relationship.Bookmarked + "]->(act:" + Label.Entity.Act + ")")
                .OptionalMatch("(act)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .OptionalMatch("(entity)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .OptionalMatch("(entity)-[:HAS_RELATION]->()-[:IS_A]->(type:Category)")
                .With("collect(type) as Attributes, entity, image, act")
                .With("{ Act: act, Entities: collect({ Entity: entity, Type: head(labels(entity)) }), EntityImages: collect({ Entity: entity, Type: head(labels(entity)), Image: image, Attributes: Attributes }) } as ActNode")
                ;
            var results = query
                .Return(() => Return.As<ActEntityImageNode>("ActNode"))
                .Results
                .Select(x => new NewAct
                {
                    Act = x.Act,
                    EntityImages = x.EntityImages.Select(y => new EntityImage { Attributes = y.Attributes, Image = y.Image, Entity = Helpers.Common.ToEntity(y.Type, y.Entity) })
                })
                .ToList();
            return results;
        }

        public void AddBookmark(string userGuid, string entityType, string entityGuid)
        {
            commonService.CreateRelationship(Label.Entity.User, userGuid, entityType, entityGuid, Label.Relationship.Bookmarked);
        }

        public void DeleteBookmark(string userGuid, string entityType, string entityGuid)
        {
            Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })-[r:" + Label.Relationship.Bookmarked + "]->(entity:" + entityType + " { Guid: {entityGuid }})")
                .WithParams(new
                {
                    userGuid,
                    entityGuid
                })
                .Delete("r")
                .ExecuteWithoutResults();
        }

        public void RemovePermissionFromRole(string permissionGuid, string roleGuid)
        {
            Client.Cypher
                .Match("(role:" + Label.Entity.Role + " { Guid: {roleGuid} })-[r:" + Label.Relationship.HasPermission + "]->(permission:" + Label.Entity.Permission + " { Guid: {permissionGuid }})")
                .WithParams(new
                {
                    roleGuid,
                    permissionGuid
                })
                .Delete("r")
                .ExecuteWithoutResults();
        }

        public void DeleteSeenIt(string userGuid, string artefactGuid)
        {
            Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })-[r:" + Label.Relationship.Saw + "]->(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid }})")
                .WithParams(new
                {
                    userGuid,
                    artefactGuid
                })
                .Delete("r")
                .ExecuteWithoutResults();
        }

        public bool AddRoleToUser(string userGuid, string roleGuid)
        {
            try
            {
                var hasRoleGuid = Guid.NewGuid().ToString();
                Client.Cypher
                    .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })",
                           "(role:" + Label.Entity.Role + " { Guid: {roleGuid} })")
                    .WithParams(new
                    {
                        userGuid = userGuid,
                        roleGuid = roleGuid
                    })
                    .CreateUnique("user-[:" + Label.Relationship.HasRole + " { Guid: {guid}, IsActive: {isActive} }]->role")
                    .WithParams(new
                    {
                        guid = hasRoleGuid,
                        isActive = true
                    })
                    .ExecuteWithoutResults();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool AddPermissionToRole(string permissionGuid, string roleGuid)
        {
            try
            {
                var hasPermissionGuid = Guid.NewGuid().ToString();
                Client.Cypher
                    .Match("(permission:" + Label.Entity.Permission + " { Guid: {permissionGuid} })",
                           "(role:" + Label.Entity.Role + " { Guid: {roleGuid} })")
                    .WithParams(new
                    {
                        permissionGuid = permissionGuid,
                        roleGuid = roleGuid
                    })
                    .CreateUnique("role-[:" + Label.Relationship.HasPermission + " { Guid: {guid}, IsActive: {isActive} }]->permission")
                    .WithParams(new
                    {
                        guid = hasPermissionGuid,
                        isActive = true
                    })
                    .ExecuteWithoutResults();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void Join(string email, string password, string firstname, string lastname, int? age, string profession)
        {
            // var hashedPassword = Crypto.HashPassword(password);
            var hashedPassword = password;
            var user = new User
            {
                Name = email,
                Password = hashedPassword,
                FirstName = firstname,
                LastName = lastname,
                Age = age,
                Profession = profession,
                Created = DateTime.Now
            };
            SaveOrUpdate(user);
            var publicUser = FindRoleByName(Label.Roles.PublicUser);
            commonService.CreateRelationship(Label.Entity.User, user.Guid, Label.Entity.Role, publicUser.Guid, Label.Relationship.HasRole);
        }

        public ForgotPasswordResult ForgotPassword(string email)
        {
            var now = DateTimeOffset.Now;
            var user = FindUser(email);
            if (user == null)
            {
                return ForgotPasswordResult.UserNotFound;
            }
            if (user.PasswordResetStartWindow.HasValue)
            {
                var elapsed = now - user.PasswordResetStartWindow.Value;
                if (elapsed.TotalMinutes <= 15)
                {
                    // return ForgotPasswordResult.EmailAlreadySentWithinWindow;
                }
            }
            var tokenGuid = Guid.NewGuid().ToString();
            var sent = SendPasswordResetEmail(email, tokenGuid);
            if (false == sent)
            {
                return ForgotPasswordResult.UnableToSendEmail;
            }
            user.PasswordResetStartWindow = now;
            user.PasswordResetToken = tokenGuid;
            SaveOrUpdate(user);
            return ForgotPasswordResult.Success;
        }

        private readonly string HostName = ConfigurationManager.AppSettings["Codex.HostName"];
        private readonly string HostEmail = ConfigurationManager.AppSettings["Codex.HostEmail"];
        private bool SendPasswordResetEmail(string email, string tokenGuid)
        {
            try
            {
                var message = new MailMessage();

                message.To.Add(email);
                message.Subject = "Forgot your password?";
                message.From = new MailAddress(HostEmail);
                var body = new List<string>();
                body.Add("Hi,");
                body.Add("");
                body.Add("You have received this email because we were told that you want to reset your password at The Codex.");
                body.Add("");
                body.Add("If you don't wish to reset your password at this time just ignore this letter. If you do, click the 'Reset Password' link below within the next few minutes.");
                body.Add("");
                body.Add(string.Format("<b><a href='{0}/Account/ResetPasswordFromEmail?username={1}&tokenGuid={2}'>Reset Password</a></b>", HostName, System.Uri.EscapeDataString(email), System.Uri.EscapeDataString(tokenGuid)));
                body.Add("");
                body.Add("Thank you,");
                body.Add("The Codex team");
                message.IsBodyHtml = true;
                message.Body = string.Join("<br/>", body);
                var smtp = new SmtpClient();
                smtp.Send(message);
                return true;
            }
            catch (Exception ex)
            {
                ex.Data.Add("email", "email");
                ex.Data.Add("tokenGuid", "tokenGuid");
                Log.Error(string.Format("Exception on trying to email the user '{0}' a reset password token email.", email), ex);
                return false;
            }
        }

        public void ResetPassword(string userGuid, string newPassword)
        {
            var user = FindUserByGuid(userGuid);
            user.PasswordResetToken = null;
            user.PasswordResetStartWindow = null;
            // user.Password = Crypto.HashPassword(newPassword);
            SaveOrUpdate(user);
        }

        public ResetPasswordFromEmailResult ResetPasswordFromEmail(string username, string tokenGuid, string newPassword)
        {
            try
            {
                var user = FindUser(username);
                if (user == null)
                {
                    return ResetPasswordFromEmailResult.UserNotFound;
                }
                if (false == user.PasswordResetStartWindow.HasValue)
                {
                    return ResetPasswordFromEmailResult.TokenAlreadyConsumed;
                }
                var now = DateTimeOffset.Now;
                var diff = now - user.PasswordResetStartWindow.Value;
                if (diff.TotalMinutes > 15)
                {
                    return ResetPasswordFromEmailResult.TokenWindowExpired;
                }
                if (user.PasswordResetToken != tokenGuid)
                {
                    return ResetPasswordFromEmailResult.TokenNotRecognised;
                }
                ResetPassword(user.Guid, newPassword);
                return ResetPasswordFromEmailResult.Success;
            }
            catch (Exception ex)
            {
                Log.Error("Exception trying to reset the user's password from email.", ex);
                return ResetPasswordFromEmailResult.Exception;
            }
        }

        public User SaveOrUpdate(User user)
        {
            commonService.SaveOrUpdate(user, Label.Entity.User);
            return user;
        }
        public List<Permission> AllPermissions()
        {
            return Client.Cypher
               .Match("(p:" + Label.Entity.Permission + ")")
               .Return(p => p.As<Permission>())
               .Results
               .ToList();
        }
        public List<Role> AllRoles()
        {
            return Client.Cypher
               .Match("(r:" + Label.Entity.Role + ")")
               .Return(r => r.As<Role>())
               .Results
               .ToList();
        }
        public List<User> AllUsers()
        {
            return Client.Cypher
               .Match("(u:" + Label.Entity.User + ")")
               .Return(u => u.As<User>())
               .Results
               .ToList();
        }
        public List<Role> GetRolesForUser(string userGuid)
        {
            return Client.Cypher
                .Match("(:" + Label.Entity.User + " { Guid: {userGuid} })-[:" + Label.Relationship.HasRole + "]->(role:" + Label.Entity.Role + ")")
                .WithParams(new
                {
                    userGuid = userGuid
                })
                .Return(role => role.As<Role>())
                .Results
                .ToList();
        }
        public List<Role> FindRolesAssociatedWithPermission(string permissionGuid)
        {
            return Client.Cypher
                .Match("(role:" + Label.Entity.Role + ")-[:" + Label.Relationship.HasPermission + "]->(:" + Label.Entity.Permission + " { Guid: {permissionGuid} })")
                .WithParams(new
                {
                    permissionGuid = permissionGuid
                })
                .Return(role => role.As<Role>())
                .Results
                .ToList();
        }
        public List<Permission> GetPermissionsForRole(string roleGuid)
        {
            return Client.Cypher
                .Match("(role:" + Label.Entity.Role + " { Guid: {roleGuid} })-[:" + Label.Relationship.HasPermission + "]->(permission:" + Label.Entity.Permission + ")")
                .WithParams(new
                {
                    roleGuid = roleGuid
                })
                .Return(permission => permission.As<Permission>())
                .Results
                .ToList();
        }
        public Permission FindPermission(string guid)
        {
            return Client.Cypher
                .Match("(p:" + Label.Entity.Permission + " { Guid: {permissionGuid} })")
                .WithParams(new
                {
                    permissionGuid = guid
                })
                .Return(p => p.As<Permission>())
                .Results
                .FirstOrDefault();
        }
        public User FindUser(string name)
        {
            return Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Name: {userName} })")
                .WithParams(new
                {
                    userName = name
                })
                .Return(user => user.As<User>())
                .Results
                .FirstOrDefault();
        }
        public Role FindRoleByName(string name)
        {
            return Client.Cypher
                .Match("(role:" + Label.Entity.Role + " { Name: {name} })")
                .WithParams(new
                {
                    name
                })
                .Return(role => role.As<Role>())
                .Results
                .FirstOrDefault();
        }
        public Role FindRoleByGuid(string guid)
        {
            return Client.Cypher
                .Match("(role:" + Label.Entity.Role + " { Guid: {userGuid} })")
                .WithParams(new
                {
                    userGuid = guid
                })
                .Return(role => role.As<Role>())
                .Results
                .FirstOrDefault();
        }
        public User FindUserByGuid(string guid)
        {
            return Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
                .WithParams(new
                {
                    userGuid = guid
                })
                .Return(user => user.As<User>())
                .Results
                .FirstOrDefault();
        }

        public UserNode GetUserWithPermissionsFromOAuth(string username)
        {
            var query = Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Name: {username} })").WithParams(new { username })
                .OptionalMatch("(user)-[:" + Label.Relationship.HasRole + "]->(role:" + Label.Entity.Role + ")-[:" + Label.Relationship.HasPermission + "]->(permission:" + Label.Entity.Permission + ")")
                ;

            var result = query
                .Return(() =>
                new
                {
                    User = Return.As<User>("user"),
                    Role = Return.As<Role>("role"),
                    Permissions = Return.As<IEnumerable<Permission>>("collect(distinct(permission))")
                })
                .Results
                .FirstOrDefault();

            return new UserNode
            {
                User = result.User,
                Role = result.Role.Name,
                Permissions = result.Permissions.Select(x2 => x2.Name)
            };

        }

        public UserNode GetUserWithPermissions(string username, string password)
        {
            var query = Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Name: {username} })").WithParams(new { username })
                .OptionalMatch("(user)-[:" + Label.Relationship.HasRole + "]->(role:" + Label.Entity.Role + ")-[:" + Label.Relationship.HasPermission + "]->(permission:" + Label.Entity.Permission + ")")
                ;
            var result = query
                .Return(() =>
                new
                {
                    User = Return.As<User>("user"),
                    Role = Return.As<Role>("role"),
                    Permissions = Return.As<IEnumerable<Permission>>("collect(distinct(permission))")
                })
                .Results
                .FirstOrDefault();
            if (result == null)
            {
                return null;
            }
            //var verified = Crypto.VerifyHashedPassword(result.User.Password, password);
            //if (verified)
            //{
                return new UserNode
                {
                    User = result.User,
                    Role = "",
                    Permissions = result.Permissions.Select(x2 => x2.Name)
                };
            //}
            //return null;
        }

        public Role SaveOrUpdate(Role role)
        {
            commonService.SaveOrUpdate(role, Label.Entity.Role);
            return role;
        }

        public Permission SaveOrUpdate(Permission permission)
        {
            commonService.SaveOrUpdate(permission, Label.Entity.Permission);
            return permission;
        }

        public void AddRoleToUser(Role role, User user)
        {
            throw new NotImplementedException();
        }
    }

    public class SaltedPasswordHash
    {
        public string Password { get; set; }
        public string Salt { get; set; }
    }
    public class UserNode
    {
        public User User { get; set; }
        public string Role { get; set; }
        public IEnumerable<string> Permissions { get; set; }
    }
}
