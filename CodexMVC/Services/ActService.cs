﻿using Data;
using Data.Entities;
using Neo4jClient;
using Neo4jClient.Cypher;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Services
{
    public class TagGraphResult
    {
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public Tag Tag { get; set; }
        public TagValue Value { get; set; }
    }

    public class TagNode
    {
        public Tag Tag { get; set; }
        public IEnumerable<EntityNode> Entities { get; set; }
    }

    public class TagHyperNode
    {
        public Tag Tag { get; set; }
        public IEnumerable<IEntity> Entities { get; set; }
        public string ToJsonDisplay()
        {
            var result = Tag.Name;
            var entities = Entities.Where(x => x != null);
            if (entities != null && entities.Any())
            {
                result += " (" + string.Join("; ", entities.Select(x => Helpers.Common.NameOf(x))) + ")";
            }
            return result;
        }
        public string ToDisplay()
        {
            var result = Tag.Name;
            var entities = Entities.Where(x => x != null);
            if (entities != null && entities.Any())
            {
                result += " (subset of " + string.Join("; ", entities.Select(x => Helpers.Common.NameOf(x))) + ")";
            }
            return result;
        }
    }
    public class PlaceCategoryTuple
    {
        public Place Place { get; set; }
        public Category Category { get; set; }
    }
    public class BaseActHyperNode
    {
        public Agent Agent { get; set; }
        public Act Act { get; set; }
        public Place Place { get; set; }
        public IEnumerable<PlaceCategoryTuple> PlaceCategory { get; set; }
    }
    public class EntityImage
    {
        public IEntity Entity { get; set; }
        public Image Image { get; set; }
        public IEnumerable<Category> Attributes { get; set; }
        public Relation Birth { get; set; }
        public Relation Death { get; set; }
    }
    public class RelatedAgents
    {
        public string ActGuid { get; set; }
        public Agent Agent1 { get; set; }
        public Agent Agent2 { get; set; }
        public string Relationship { get; set; }
    }
    public class NewAct
    {
        public List<IEntity> Actors { get; set; }
        public List<RelatedAgents> RelatedAgents { get; set; }
        public Act Act { get; set; }
        public IEnumerable<EntityImage> EntityImages { get; set; }
        public IEnumerable<Category> Attributes { get; set; }
        public IEnumerable<ValueItem> Tags { get; set; }
        public List<TagValueItemNode> TagValues { get; set; }
        public IEnumerable<IEntity> Attachments { get; set; }
        public Artefact Source { get; set; }
        public string HasSourceGuid { get; set; }
        public Agent StatedBy { get; set; }
        public string StatedByGuid { get; set; }
        public bool Bookmarked { get; set; }
        public bool InItinerary { get; set; }
        public GenericRelationNode<Act, Act> SubsetOf { get; set; }
        public IEnumerable<DataSet> DataSets { get; set; }

        public NewAct()
        {
            RelatedAgents = new List<RelatedAgents>();
        }

        public string NameOf(IEntity entity)
        {
            if (entity is Agent)
            {
                return ((Agent)entity).FullName2;
            }
            if (entity is Artefact)
            {
                return ((Artefact)entity).ToDisplay();
            }
            return entity.Name;
        }
        public string ToTimelineHeader(bool useEntitiesForHeaderIfNoVisitors = true, params IEntity[] visitors)
        {
            if (false == string.IsNullOrWhiteSpace(Act.Name))
            {
                return Act.Name;
            }
            if (visitors != null)
            {
                var entities = EntityImages
                    .Where(x =>
                           false == visitors.Any(x2 => x2.Guid == x.Entity.Guid)
                        && x.Entity.GetType().UnderlyingSystemType != typeof(Category))
                    .Select(x => x.Entity)
                    .ToList();
                if (entities.Count() > 0)
                {
                    return And(entities.Select(x => NameOf(x)));
                }
            }
            else if (useEntitiesForHeaderIfNoVisitors)
            {
                var entities = EntityImages
                    .Where(x => x.Entity.GetType().UnderlyingSystemType != typeof(Category))
                    .Select(x => x.Entity)
                    .ToList();
                if (entities.Count() > 0)
                {
                    return And(entities.Select(x => NameOf(x)).Distinct());
                }
            }
            var html = StripHTML(ToHtml(visitors: visitors));
            var words = html.Split(' ');
            return string.Join(" ", words.Take(3)) + " ...";
        }
        public class EntityOrderer : IComparer<IEntity>
        {
            public int Compare(IEntity x, IEntity y)
            {
                var xT = x.GetType().UnderlyingSystemType;
                var yT = y.GetType().UnderlyingSystemType;
                const int HIGHER = 1;
                const int LOWER = -1;
                const int SAME = 0;
                if (xT == typeof(Agent))
                {
                    return HIGHER;
                }
                if (xT == typeof(Artefact))
                {
                    if (yT == typeof(Agent))
                    {
                        return LOWER;
                    }
                    if (yT == typeof(Place))
                    {
                        return HIGHER;
                    }
                }
                if (xT == typeof(Place))
                {
                    if (yT == typeof(Agent))
                    {
                        return LOWER;
                    }
                    if (yT == typeof(Artefact))
                    {
                        return LOWER;
                    }
                }
                return SAME;
            }
        }
        public static string And(IEnumerable<string> value)
        {
            var list = value.ToList();
            if (list == null || list.Count() == 0)
            {
                return "";
            }
            if (list.Count() == 1)
            {
                return list.First();
            }
            if (list.Count() == 2)
            {
                return list[0] + " and " + list[1];
            }
            return string.Join(", ", list.Take(list.Count() - 1)) + " and " + list.Last();
        }
        public static string StripHTML(string HTMLText)
        {
            var reg = new Regex("<[^>]+>", RegexOptions.IgnoreCase);
            var stripped = reg.Replace(HTMLText, "");
            return stripped;
        }
        public string ToHtml(bool generateAnchor = false, bool openInNewTab = false, bool linkToTimeline = false, params IEntity[] visitors)
        {
            if (Act == null || Act.Description == null)
            {
                return null;
            }
            var text = Act.Description;
            var result = text;
            var entities = EntityImages.Where(x => x != null && x.Entity != null);
            var entityClasses = entities
                .Select(x => string.Format("{0}-{1}", x.Entity.GetType().UnderlyingSystemType.Name, x.Entity.Guid));
            var attributes = entities
                .Where(x => x.Attributes != null)
                .SelectMany(x => x.Attributes);
            var attributeClasses = attributes != null ? attributes.Select(x => string.Format("Category-{0}", x.Guid)) : new List<string>();
            var target = openInNewTab ? "target=\"_blank\"" : "";
            foreach (var entityImage in entities)
            {
                var name = entityImage.Entity is Agent ? ((Agent)entityImage.Entity).FullName2 : entityImage.Entity is Artefact ? ((Artefact)entityImage.Entity).ToDisplay() : entityImage.Entity.Name;
                var type = entityImage.Entity.GetType();
                var typeName = type.Name;
                var guid = entityImage.Entity.Guid;
                var partPattern = @"\[" + typeName + @"\|" + guid + @"\]";
                var fullPattern = @"\[" + typeName + @"\|" + guid + @"(\|.*?)\]";
                var anchor = generateAnchor ? "#act-" + Act.Guid : "";
                var ageText = "";
                var linkContent = "";
                //if (entityImage.Image != null)
                //{
                //    linkContent = "data-toggle='popover' data-title='" + NameOf(entityImage.Entity) + "' class='popover' data-image='" + entityImage.Image.VirtualPath().Replace("~", "") + "?mode=fit&height=300&width=300' ";
                //}
                if (visitors == null || visitors.All(x => x.Guid != entityImage.Entity.Guid))
                {
                    if (ShowAge(entityImage)
                    )
                    {
                        var age = Act.Year.Value - entityImage.Birth.Year.Value;
                        if (age > 0)
                        {
                            // ageText = string.Format(" (then aged {0})", age);
                        }
                    }
                }
                if (Regex.IsMatch(result, partPattern))
                {
                    var link = "";
                    if (false == linkToTimeline)
                    {
                        link = "<a href=\"/" + typeName + "/ViewDetail?guid=" + guid + anchor + "\" " + target + linkContent + ">" + name + "</a>";
                    }
                    else
                    {
                        link = "<a href=\"/Time/By" + typeName + "/" + guid + anchor + "\" " + target + linkContent + ">" + name + "</a>";
                    }
                    result = Regex.Replace(result, partPattern, link + ageText);
                }
                else
                {
                    var matches = Regex.Matches(result, fullPattern);
                    if (matches.Count > 0)
                    {
                        var element = matches[0].Value;
                        var lastIndex = element.LastIndexOf("|");
                        if (lastIndex > 0)
                        {
                            var customName = element.Substring(lastIndex + 1).Replace("]", "");
                            var link = "";
                            if (false == linkToTimeline)
                            {
                                link = "<a href=\"/" + typeName + "/ViewDetail?guid=" + guid + anchor + "\" " + target + linkContent + " title=\"" + name + "\">" + customName + "</a>";
                            }
                            else
                            {
                                link = "<a href=\"/Time/By" + typeName + "/" + guid + anchor + "\" " + target + linkContent + " title=\"" + name + "\">" + customName + "</a>";
                            }
                            result = Regex.Replace(result, fullPattern, link + ageText);
                        }
                    }
                }
            }
            return "<span class=\"act " + string.Join(" ", entityClasses) + " " + string.Join(" ", attributeClasses) + "\">" + result + "</span>";
        }

        public string ToHtmlJson(bool generateAnchor = false, bool openInNewTab = false, bool linkToTimeline = false, params IEntity[] visitors)
        {
            if (Act == null || Act.Description == null)
            {
                return null;
            }
            var text = Act.Description;
            var result = text;
            var entities = EntityImages.Where(x => x != null && x.Entity != null);
            var entityClasses = entities
                .Select(x => string.Format("{0}-{1}", x.Entity.GetType().UnderlyingSystemType.Name, x.Entity.Guid));
            var attributes = entities
                .Where(x => x.Attributes != null)
                .SelectMany(x => x.Attributes);
            var attributeClasses = attributes != null ? attributes.Select(x => string.Format("Category-{0}", x.Guid)) : new List<string>();
            var target = openInNewTab ? "target=\"_blank\"" : "";
            foreach (var entityImage in entities)
            {
                var name = entityImage.Entity is Agent ? ((Agent)entityImage.Entity).FullName2 : entityImage.Entity is Artefact ? ((Artefact)entityImage.Entity).ToDisplay() : entityImage.Entity.Name;
                var type = entityImage.Entity.GetType();
                var typeName = type.Name;
                var guid = entityImage.Entity.Guid;
                var partPattern = @"\[" + typeName + @"\|" + guid + @"\]";
                var fullPattern = @"\[" + typeName + @"\|" + guid + @"(\|.*?)\]";
                var anchor = generateAnchor ? "#act-" + Act.Guid : "";
                var ageText = "";
                var linkContent = "";
                //if (entityImage.Image != null)
                //{
                //    linkContent = "data-toggle='popover' data-title='" + NameOf(entityImage.Entity) + "' class='popover' data-image='" + entityImage.Image.VirtualPath().Replace("~", "") + "?mode=fit&height=300&width=300' ";
                //}
                if (visitors == null || visitors.All(x => x.Guid != entityImage.Entity.Guid))
                {
                    if (ShowAge(entityImage)
                    )
                    {
                        var age = Act.Year.Value - entityImage.Birth.Year.Value;
                        if (age > 0)
                        {
                            // ageText = string.Format(" (then aged {0})", age);
                        }
                    }
                }
                if (Regex.IsMatch(result, partPattern))
                {
                    var link = name;
                    result = Regex.Replace(result, partPattern, link + ageText);
                }
                else
                {
                    var matches = Regex.Matches(result, fullPattern);
                    if (matches.Count > 0)
                    {
                        var element = matches[0].Value;
                        var lastIndex = element.LastIndexOf("|");
                        if (lastIndex > 0)
                        {
                            var customName = element.Substring(lastIndex + 1).Replace("]", "");
                            var link = customName;
                            result = Regex.Replace(result, fullPattern, link + ageText);
                        }
                    }
                }
            }
            return "<span class=\"act " + string.Join(" ", entityClasses) + " " + string.Join(" ", attributeClasses) + "\">" + result + "</span>";
        }

        private bool ShowAge(EntityImage entityImage)
        {
            if (false == Act.Year.HasValue)
            {
                return false;
            }
            if (entityImage.Birth == null || entityImage.Death == null)
            {
                return false;
            }
            if (false == entityImage.Birth.Year.HasValue || false == entityImage.Death.Year.HasValue)
            {
                return false;
            }
            if (Act.Year.Value < entityImage.Birth.Year.Value || Act.Year.Value > entityImage.Death.Year.Value)
            {
                return false;
            }
            return true;
        }
        public string ToVerticalTimelineDate()
        {
            return FactExtensions.Start(Act);
        }
        public string ToTimelineStartDate()
        {
            var result = new List<string>();
            if (Act.Year.HasValue)
            {
                result.Add(Act.Year.Value.ToString());
            }
            if (Act.Month.HasValue)
            {
                result.Add(Act.Month.Value.ToString());
            }
            if (Act.Day.HasValue)
            {
                result.Add(Act.Day.Value.ToString());
            }
            return string.Join(",", result.ToArray());
        }
        public string ToTimelineEndDate()
        {
            if (false == Act.YearEnd.HasValue)
            {
                return ToTimelineStartDate();
            }
            var result = new List<string>();
            if (Act.YearEnd.HasValue)
            {
                result.Add(Act.YearEnd.Value.ToString());
            }
            if (Act.MonthEnd.HasValue)
            {
                result.Add(Act.MonthEnd.Value.ToString());
            }
            if (Act.DayEnd.HasValue)
            {
                result.Add(Act.DayEnd.Value.ToString());
            }
            return string.Join(",", result.ToArray());
        }
    }
    public class EntityNode
    {
        public string Type { get; set; }
        public Node<string> Entity { get; set; }
    }
    public class EntityNodeWithImage : EntityNode
    {
        public Image Image { get; set; }
        public IEnumerable<Category> Attributes { get; set; }
        public Relation Birth { get; set; }
        public Relation Death { get; set; }
    }
    public class TagValueItemNode
    {
        public string ActGuid { get; set; }
        public Tag Tag { get; set; }
        public IEnumerable<Tag> Parents { get; set; }
        public ValueItem Value { get; set; }
    }
    public class ActEntityImageNode
    {
        public bool Bookmarked { get; set; }
        public Act Act { get; set; }
        public IEnumerable<DataSet> DataSets { get; set; }
        public IEnumerable<EntityNode> Attachments { get; set; }
        public IEnumerable<ValueItem> Tags { get; set; }
        public IEnumerable<TagValueItemNode> TagValues { get; set; }
        public IEnumerable<EntityNodeWithImage> EntityImages { get; set; }
        public string HasSourceGuid { get; set; }
        public Artefact Source { get; set; }
        public Agent StatedBy { get; set; }
        public string StatedByGuid { get; set; }
        public Act Container { get; set; }
    }
    public class NewActContainer
    {
        public NewAct Act { get; set; }
        public IEnumerable<Act> Children { get; set; }
        public IEnumerable<Act> Parents { get; set; }
    }
    public class EntityItem
    {
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
    }
    public class ValueItem : EntityItem
    {
        public string Value { get; set; }
    }
    public interface IActService
    {
        /// <summary>
        /// Imports <paramref name="take"/> number of historic event entries starting from <paramref name="start"/>, and returns the cursor position (i.e., start + take). If <paramref name="take"/> is NULL it will attempt to import ALL past <paramref name="start"/>.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        int ImportHistoricEvents(int start = 0, int? take = null);
        IEnumerable<NewAct> FindNewActsByAgentAndSource(string agentGuid);
        IEnumerable<TagHyperNode> SearchTags(string search);
        GenericRelationNode<Tag, Category> FindTagRefersToCategory(string actGuid, string toDestGuid);
        GenericRelationNode<Tag, Tag> FindTagSubsetOfTag(string actGuid, string toDestGuid);
        IEnumerable<TagHyperNode> GetTags();
        GenericRelationNode<Act, Tag> FindHasTag(string actGuid, string toDestGuid);
        void HasTag(GenericRelationNode<Act, Tag> node);
        void TagRefersToCategory(GenericRelationNode<Tag, Category> node);
        void TagSubsetOfTag(GenericRelationNode<Tag, Tag> node);
        GenericRelationNode<Act, Artefact> FindHasSubject(string artefactGuid, string hasRelationGuid);
        void StatedBy(GenericRelationNode<Act, Agent> node);
        void HasSource(GenericRelationNode<Act, Artefact> node);
        void HasVideo(GenericRelationNode<Act, Video> node);
        List<NewAct> FindNewActsThatIntersectWithCategory(string agentGuid, string categoryGuid);
        List<NewAct> FindNewActsByDates(string startYear, string endYear);
        NewAct FindAct(string guid);
        Act SaveOrUpdate(Act act);
        BaseActHyperNode FindBornActByAgent(string agentGuid);
        BaseActHyperNode FindDiedActByAgent(string agentGuid);
        BaseActHyperNode FindActByAgent(string agentGuid, string actType);
        void SaveOrUpdate(Act act, IEnumerable<string[]> list);
        void SaveOrUpdateAct(Act act, IEnumerable<string[]> entities, IEnumerable<string[]> attachments, IEnumerable<ValueItem> tags);
        List<NewAct> FindOutgoingAttributesByDates(string startYear, string endYear);
        Task<IEnumerable<NewAct>> FindNewActsByAgentType(string agentTypeGuid, string userGuid = null, Significance significance = Significance.Personal);
        Task<IEnumerable<NewAct>> FindActsByAgent(string agentGuid, string userGuid = null, Significance significance = Significance.Personal);
        Task<IEnumerable<NewAct>> FindNewActsByCategory(string categoryGuid, string userGuid = null, Significance importance = Significance.Personal);
        Task<IEnumerable<NewAct>> FindNewActsByArtefact(string artefactGuid, string userGuid = null, Significance importance = Significance.Personal);
        Task<IEnumerable<NewAct>> FindNewActsByPlaceAsync(string placeGuid, string userGuid = null, Significance importance = Significance.Personal);
        NewAct FindNewAct(string actGuid);
        Result DeleteNewAct(string actGuid);
        DocumentHyperNode FindDocumentHyperNode(string documentGuid);
        void SaveOrUpdate(string agentGuid, Document document, IEnumerable<string[]> list);
        IEnumerable<DocumentHyperNode> FindDocumentHyperNodesByAgent(string agentGuid);
        Task<IEnumerable<DocumentHyperNode>> FindDocumentHyperNodesByReferrerAsync(string referrerGuid);
        IEnumerable<DocumentHyperNode> AllDocumentHyperNodes();
        void DeleteDocument(string documentGuid);
        void AddToActContext(string actGuid, string entityGuid, string entityType);
        void RemoveFromActContext(string actGuid, string entityGuid, string entityType);

        Task<IEnumerable<YearActs>> GetYears();

        void SubsetOf(GenericRelationNode<Act, Act> node);

        IEnumerable<Act> FindAllContainers();

        Task<NewActContainer> FindNewActContainer(string actGuid);

        Tag FindTagByGuid(string tagGuid);

        List<NewAct> FindNewActsByTag(string tagGuid);
        IEnumerable<IGrouping<string, TagGraphResult>> TagCharts();
        List<NewAct> FindNewActsLatest();
        List<NewAct> FindNewActsByTagCategory(string tagCategoryGuid);

        List<Place> GetNewActPlaces();
        List<PersonalDetails> GetNewActAgents();
        List<ArtefactMadeByAgent> GetNewActArtefacts();
        List<Category> GetNewActAgentTypes();
        List<TagHyperNode> GetNewActTags();
        List<TagHyperNode> GetNewActThemes();
        List<CooccurenceItem> CooccurencesData();
    }
    public class CooccurenceItem
    {
        public Agent Agent { get; set; }
        public IEnumerable<Category> Attributes { get; set; }
        public Place BirthPlace { get; set; }
        public int? BirthYear { get; set; }
        public IEnumerable<CooccurenceLink> Links { get; set; }
    }
    public class CooccurenceLink
    {
        public Agent Agent { get; set; }
        public int ActCount { get; set; }
    }
    public class ActService : IActService
    {
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly ICategoryService categoryService;
        private readonly ICommonService commonService;
        private readonly IRelationService relationService;
        public ActService(
            GraphClient client,
            ICategoryService _categoryService,
            ICommonService _commonService,
            IRelationService _relationService
        )
        {
            Client = client;
            categoryService = _categoryService;
            commonService = _commonService;
            relationService = _relationService;
        }

        public int ImportHistoricEvents(int start = 0, int? take = null)
        {
            var path = ConfigurationManager.AppSettings["HistoricEventsPath"];
            var lines = File.ReadAllLines(path);
            var query = from line in lines
                        let data = line.Split('\t')
                        select new
                        {
                            Year = int.Parse(data[0]),
                            EventType = int.Parse(data[1]),
                            MonthDayCirca = data[2],
                            EventText = data[3]
                        };
            var records = query.Skip(start);
            if (take.HasValue)
            {
                records = records.Take(take.Value);
            }
            var acts = records
                .Select(x => ToAct(x.Year, x.EventType, x.MonthDayCirca, x.EventText))
                .ToList();
            int cursor = start;
            try
            {
                foreach (var act in acts)
                {
                    SaveOrUpdate(act);
                    cursor++;
                }
            }
            catch (Exception ex)
            {
                // log
            }
            return start + cursor;
        }

        static Act ToAct(int year, int eventType, string monthDayCirca, string eventText)
        {
            var act = new Act();
            act.Year = Math.Abs(year);
            act.Milennium = year < 0 ? "BC" : "AD";
            if (monthDayCirca.Contains("circa"))
            {
                act.Proximity = "c.";
            }
            var parts = monthDayCirca.Split(' ');
            if (parts.Count() == 2)
            {
                act.Month = FromMonth(parts[0]);
                act.Day = int.Parse(parts[1]);
            }
            act.Description = eventText;
            return act;
        }
        static string[] Months = new string[] {
            "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"
        };
        static int? FromMonth(string month)
        {
            var index = Array.IndexOf(Months, month);
            return index >= 0 ? index + 1 : (int?)null;
        }

        public List<CooccurenceItem> CooccurencesData()
        {
            var query = Client.Cypher
                .Match("(act:Act)-[:REFERS_TO]->(agents1:Agent)")
                .Match("(act:Act)-[:REFERS_TO]->(agents2:Agent)")
                .OptionalMatch("(agents1)-[:HAS_RELATION]->()-[:IS_A]->(attributes:Category)")
                .OptionalMatch("(agents1)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(birthPlace:Place)")
                .Where("agents2 <> agents1")
                .With("agents1, attributes, birth, birthPlace, { Agent: { Guid: agents2.Guid }, ActCount: count(act) } as links")
                ;

            var results = query.Return(() => new CooccurenceItem
            {
                Agent = Return.As<Agent>("agents1"),
                Attributes = Return.As<IEnumerable<Category>>("collect(attributes)"),
                BirthPlace = Return.As<Place>("birthPlace"),
                BirthYear = Return.As<int?>("birth.Year"),
                Links = Return.As<IEnumerable<CooccurenceLink>>("collect(links)")
            }).Results;

            return results.ToList();
        }

        public Tag FindTagByName(string tagName)
        {
            var query = Client.Cypher
                .Match("(tag:Tag)")
                .Where("tag.Name =~ \"" + tagName.Trim() + "\" ")
                ;

            var results = query
                .Return(() => Return.As<Tag>("tag"))
                .Results
                .FirstOrDefault();

            return results;
        }


        public Tag FindTagByGuid(string tagGuid)
        {

            var query = Client.Cypher
                .Match("(tag:Tag { Guid: {tagGuid} })").WithParams(new { tagGuid })
                ;

            var results = query
                .Return(() => Return.As<Tag>("tag"))
                .Results
                .FirstOrDefault();

            return results;

        }

        public void RemoveFromActContext(string actGuid, string entityGuid, string entityType)
        {
            Client.Cypher
                .Match("(:" + Label.Entity.Act + " { Guid: {actGuid} })-[refersTo:" + Label.Relationship.RefersTo + "]->(:" + entityType + " { Guid: {entityGuid} })")
                .WithParams(new { actGuid, entityGuid })
                .Delete("refersTo")
                .ExecuteWithoutResults();
        }
        public void AddToActContext(string actGuid, string entityGuid, string entityType)
        {
            if (entityType != Label.Entity.Agent && entityType != Label.Entity.Artefact && entityType != Label.Entity.Place && entityType != Label.Entity.Category && entityType != Label.Entity.Document)
            {
                return;
            }
            relationService.CreateRelationship(Label.Entity.Act, actGuid, entityType, entityGuid, Label.Relationship.RefersTo);
        }
        public void DeleteDocument(string documentGuid)
        {
            Client.Cypher
                .Match("(document:" + Label.Entity.Document + " { Guid: {documentGuid} })-[r]->()")
                .WithParams(new
                {
                    documentGuid
                })
                .Delete("document, r")
                .ExecuteWithoutResults();
        }

        public GenericRelationNode<Act, Artefact> FindHasSubject(string actGuid, string toDestGuid)
        {
            return commonService.FindGenericRelationNode<Act, Artefact>(actGuid, Label.Relationship.HasSource, toDestGuid);
        }

        public GenericRelationNode<Act, Tag> FindHasTag(string actGuid, string toDestGuid)
        {
            return commonService.FindGenericRelationNode<Act, Tag>(actGuid, Label.Relationship.HasTag, toDestGuid);
        }

        public GenericRelationNode<Tag, Tag> FindTagSubsetOfTag(string actGuid, string toDestGuid)
        {
            return commonService.FindGenericRelationNode<Tag, Tag>(actGuid, Label.Relationship.SubsetOf, toDestGuid);
        }

        public GenericRelationNode<Tag, Category> FindTagRefersToCategory(string actGuid, string toDestGuid)
        {
            return commonService.FindGenericRelationNode<Tag, Category>(actGuid, Label.Relationship.RefersTo, toDestGuid);
        }

        public void StatedBy(GenericRelationNode<Act, Agent> node)
        {
            SaveOrUpdate(node, Label.Relationship.StatedBy);
        }

        public void HasSource(GenericRelationNode<Act, Artefact> node)
        {
            SaveOrUpdate(node, Label.Relationship.HasSource);
        }

        public void HasVideo(GenericRelationNode<Act, Video> node)
        {
            SaveOrUpdate(node, Label.Relationship.HasVideo);
        }

        public void SubsetOf(GenericRelationNode<Act, Act> node)
        {
            SaveOrUpdate(node, Label.Relationship.SubsetOf);
        }

        public void TagSubsetOfTag(GenericRelationNode<Tag, Tag> node)
        {
            SaveOrUpdate(node, Label.Relationship.SubsetOf);
        }

        public void TagRefersToCategory(GenericRelationNode<Tag, Category> node)
        {
            SaveOrUpdate(node, Label.Relationship.RefersTo);
        }

        public void HasTag(GenericRelationNode<Act, Tag> node)
        {
            SaveOrUpdate(node, Label.Relationship.HasTag);
        }

        public IEnumerable<TagHyperNode> SearchTags(string search)
        {
            search = search.Trim().Replace("'", "");
            search = "(?i).*" + search + ".*";

            var query = Client.Cypher
                .Match("(tag:Tag)")
                .Where("tag.Name =~ \"" + search + "\" ")
                .OptionalMatch("(tag)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                ;

            var result = query
                .Return(() => Return.As<TagNode>("{ Tag: tag, Entities: collect({ Entity: entity, Type: head(labels(entity)) }) }"))
                .Results
                .Select(x => new TagHyperNode
                {
                    Tag = x.Tag,
                    Entities = x.Entities.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity))
                })
                .Distinct(new TagHyperNodeComparer())
                .OrderBy(x => x.Tag.Name)
                .ToList();

            return result;
        }

        public IEnumerable<TagHyperNode> GetTags()
        {

            var query = Client.Cypher
                .Match("(tag:Tag)")
                .OptionalMatch("(tag)-[:" + Label.Relationship.SubsetOf + "]->(entity:Tag)")
                ;

            var result = query
                .Return(() => Return.As<TagNode>("{ Tag: tag, Entities: collect({ Entity: entity, Type: head(labels(entity)) }) }"))
                .Results
                .Select(x => new TagHyperNode
                {
                    Tag = x.Tag,
                    Entities = x.Entities.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity))
                })
                .ToList()
                .OrderBy(x => x.Tag.Name);

            return result;

        }

        public IEnumerable<Act> FindAllContainers()
        {

            var query = Client.Cypher
                .Match("(act:Act { Container: true })");

            var result = query
                .Return(() => Return.As<Act>("act"))
                .OrderBy("act.Year")
                .Results
                .ToList();

            return result;
        }

        public void SaveOrUpdate<TSource, TDest>(GenericRelationNode<TSource, TDest> node, string relationToDestinationType, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            var sourceType = GetNodeLabel<TSource>();
            var destType = GetNodeLabel<TDest>();
            var isNew = node.RelationToDestinationGuid == null;
            /**
             * If the destination relation has already been created then delete it before recreating it.
             */
            if (false == isNew)
            {
                relationService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
            }
            /**
             * If there is no destination specified we end here; the user selected an empty list value.
             */
            if (node.Destination.Guid == null)
            {
                return;
            }
            if (false == reverse)
            {
                relationService.CreateRelationship(sourceType, node.Source.Guid, destType, node.Destination.Guid, relationToDestinationType);
            }
            else
            {
                relationService.CreateRelationship(destType, node.Destination.Guid, sourceType, node.Source.Guid, relationToDestinationType);
            }
        }

        /// <summary>
        /// Although the label and the type names are currently equivalent this could be rewritten
        /// to check the type and return the label constant explicitly.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public string GetNodeLabel<TEntity>() where TEntity : class, IEntity
        {
            return typeof(TEntity).UnderlyingSystemType.Name;
        }

        public Result DeleteNewAct(string actGuid)
        {
            try
            {
                Client.Cypher
                    .Match("(act:" + Label.Entity.Act + " { Guid: {actGuid} })").WithParams(new { actGuid })
                    .OptionalMatch("(act)<-[r]->(entity)")
                    .Delete("act, r")
                    .ExecuteWithoutResults();
                return Result.Success();
            }
            catch (Exception ex)
            {
                return Result.Danger(exception: ex);
            }
        }
        public DocumentHyperNode FindDocumentHyperNode(string documentGuid)
        {
            var temp = Client.Cypher
                .Match("(entity)<-[:" + Label.Relationship.RefersTo + "]-(document:" + Label.Entity.Document + " { Guid: {documentGuid} })")
                .OptionalMatch("(document)-[:" + Label.Relationship.MadeBy + "]->(madeBy:" + Label.Entity.Agent + ")")
                .WithParams(new
                {
                    documentGuid
                })
                .Return(() => new
                {
                    MadeBy = Return.As<Agent>("madeBy"),
                    Entities = Return.As<IEnumerable<EntityNode>>("collect(distinct({ Entity: entity, Type: head(labels(entity)) }))"),
                    Document = Return.As<Document>("document")
                })
                .Results
                .FirstOrDefault();
            if (temp == null)
            {
                return null;
            }
            return new DocumentHyperNode
            {
                MadeBy = temp.MadeBy,
                Document = temp.Document,
                Entities = temp.Entities.Select(y => Helpers.Common.ToEntity(y.Type, y.Entity))
            };
        }
        public NewAct FindNewAct(string actGuid)
        {
            var query = Client.Cypher
                .Match("(act:" + Label.Entity.Act + " { Guid: {actGuid} })").WithParams(new { actGuid })
                .OptionalMatch("(entity)<-[:" + Label.Relationship.RefersTo + "]-(act)")
                .OptionalMatch("(act)-[hasSource:HAS_SOURCE]->(source:Artefact)")
                .OptionalMatch("(act)-[statedByRelationship:" + Label.Relationship.StatedBy + "]->(statedBy:Agent)")
                .OptionalMatch("(act)-[subsetOf:SUBSET_OF]->(set:Act)")
                .OptionalMatch("(act)-[:" + Label.Relationship.HasAttachment + "]->(attachment)")
                .OptionalMatch("(act)-[:" + Label.Relationship.HasTag + "]->(tag:Tag)")
                .OptionalMatch("(tag)-[:" + Label.Relationship.HasValue + "]->(value:" + Label.Entity.TagValue + ")-[:" + Label.Relationship.ValueSource + "]->(act)")
                ;
            var temp = query
                .Return(() => new
                {
                    EntityImages = Return.As<IEnumerable<EntityNodeWithImage>>("collect(distinct({ Entity: entity, Type: head(labels(entity)) }))"),
                    Attachments = Return.As<IEnumerable<EntityNode>>("collect(distinct({ Entity: attachment, Type: head(labels(attachment)) }))"),
                    Act = Return.As<Act>("act"),
                    Source = Return.As<Artefact>("source"),
                    HasSourceGuid = Return.As<string>("hasSource.Guid"),
                    StatedBy = Return.As<Agent>("statedBy"),
                    StatedByRelationship = Return.As<string>("statedByRelationship.Guid"),
                    SubsetOfGuid = Return.As<string>("subsetOf.Guid"),
                    Set = Return.As<Act>("set"),
                    Tags = Return.As<IEnumerable<Tag>>("collect(distinct(tag))"),
                    TagValueItems = Return.As<IEnumerable<TagValueItemNode>>("collect(distinct({ Tag: tag, Value: value }))")
                })
                .Results
                .FirstOrDefault();
            if (temp == null)
            {
                return null;
            }
            return new NewAct
            {
                Act = temp.Act,
                EntityImages = temp.EntityImages.Select(y => new EntityImage { Entity = Helpers.Common.ToEntity(y.Type, y.Entity) }),
                Attachments = temp.Attachments.Select(x => Helpers.Common.ToEntity(x.Type, x.Entity)),
                Source = temp.Source,
                HasSourceGuid = temp.HasSourceGuid,
                StatedBy = temp.StatedBy,
                StatedByGuid = temp.StatedByRelationship,
                SubsetOf = new GenericRelationNode<Act, Act>
                {
                    Source = temp.Act,
                    RelationToDestinationGuid = temp.SubsetOfGuid,
                    Destination = temp.Set
                },
                TagValues = temp.TagValueItems.ToList()
            };
        }

        public async Task<IEnumerable<NewAct>> FindNewActsByCategory(string categoryGuid, string userGuid = null, Significance significance = Significance.Personal)
        {
            return await FindNewActs(Label.Entity.Category, categoryGuid, userGuid, significance);
        }

        public async Task<IEnumerable<NewAct>> FindNewActsByPlaceAsync(string placeGuid, string userGuid = null, Significance significance = Significance.Personal)
        {
            var actSelector = Client.Cypher
                .Match("(place:" + Label.Entity.Place + " { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(inside:" + Label.Entity.Place + ")-[:" + Label.Relationship.InsideOf + "*0..]->(place)")
                .Match("(inside)<-[:" + Label.Relationship.RefersTo + "]-(act:" + Label.Entity.Act + ")")
                ;

            /*
             * var actQuery = graph.Match<Place>(placeGuid)
             *                     .Match<InsideOf>(from: "inside", relPath: "*0..")
             *                     .Match<ActRefersToPlace>()
             * 
             */

            var query = GetActQuery(actSelector);
            var acts = GetActResults(query);

            //var query = actSelector
            //    .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(entity)")
            //    .OptionalMatch("(entity)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
            //    .OptionalMatch("(entity)-[:HAS_RELATION]->()-[:IS_A]->(type:Category)")
            //    .With("collect(type) as Attributes, entity, image, act")
            //    .With("{ Act: act, Entities: collect({ Entity: entity, Type: head(labels(entity)) }), EntityImages: collect({ Entity: entity, Type: head(labels(entity)), Image: image, Attributes: Attributes }) } as ActNode")
            //    ;

            //var results = query
            //    .Return(() => Return.As<ActEntityImageNode>("ActNode"))
            //    .Results;

            //var acts = results.AsParallel()
            //    .Select(x => new NewAct
            //    {
            //        Act = x.Act,
            //        EntityImages = x.EntityImages.Select(y => new EntityImage { Attributes = y.Attributes, Image = y.Image, Entity = Helpers.Common.ToEntity(y.Type, y.Entity) })
            //    })
            //    .ToList();

            SetTagsAndOtherEntities(actSelector, acts);

            return acts;
        }
        public async Task<NewActContainer> FindNewActContainer(string actGuid)
        {

            var act = FindNewAct(actGuid);

            var children = Client.Cypher
                .Match("(act:Act { Guid: {actGuid} })").WithParams(new { actGuid })
                .Match("(children:Act)-[:SUBSET_OF*..]->(act)")
                .Return(() => Return.As<Act>("children"))
                .Results;

            var parents = Client.Cypher
                .Match("(act:Act { Guid: {actGuid} })").WithParams(new { actGuid })
                .Match("(act)-[:SUBSET_OF*..]->(parents:Act)")
                .Return(() => Return.As<Act>("parents"))
                .Results;

            var container = new NewActContainer
            {
                Act = act,
                Children = children,
                Parents = parents
            };

            return container;

        }
        public async Task<IEnumerable<NewAct>> FindNewActsByArtefact(string artefactGuid, string userGuid = null, Significance significance = Significance.Personal)
        {
            return await FindNewActs(Label.Entity.Artefact, artefactGuid, userGuid, significance);
        }
        public async Task<IEnumerable<NewAct>> FindNewActsByAgentType(string agentTypeGuid, string userGuid = null, Significance significance = Significance.Personal)
        {
            var actSelector = Client.Cypher
                .Match("(agentType:" + Label.Entity.Category + " { Guid: {agentTypeGuid} })").WithParams(new { agentTypeGuid })
                .Match("(agentTypes:Category)-[:KIND_OF*0..]->(agentType)")
                .Match("(agent:Agent)-[:HAS_RELATION]->()-[:IS_A]->(agentTypes)")
                .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(agent)")
                .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                ;

            var query = GetActQuery(actSelector);
            var acts = GetActResults(query);

            //var query = actSelector
            //    .OptionalMatch("(entity)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
            //    .OptionalMatch("(entity)-[:HAS_RELATION]->()-[:IS_A]->(type:Category)")
            //    .With("collect(type) as Attributes, entity, image, act")
            //    .With("{ Act: act, Entities: collect({ Entity: entity, Type: head(labels(entity)) }), EntityImages: collect({ Entity: entity, Type: head(labels(entity)), Image: image, Attributes: Attributes }) } as ActNode")
            //    ;

            //var results = query
            //    .Return(() => Return.As<ActEntityImageNode>("ActNode"))
            //    .Results;

            //var acts = results
            //    .AsParallel()
            //    .Select(x => new NewAct
            //    {
            //        Act = x.Act,
            //        EntityImages = x.EntityImages.Select(y => new EntityImage { Attributes = y.Attributes, Image = y.Image, Entity = Helpers.Common.ToEntity(y.Type, y.Entity) })
            //    })
            //    .ToList();

            SetTagsAndOtherEntities(actSelector, acts);

            return acts;
        }

        public async Task<IEnumerable<NewAct>> FindActsByAgent(string agentGuid, string userGuid = null, Significance significance = Significance.Personal)
        {
            return await FindNewActs(Label.Entity.Agent, agentGuid, userGuid, significance);
        }

        public async Task<IEnumerable<DocumentHyperNode>> FindDocumentHyperNodesByReferrerAsync(string referrerGuid)
        {
            var query = Client.Cypher
                .Match("(referrer { Guid: {referrerGuid} })<-[:" + Label.Relationship.RefersTo + "]-(document:" + Label.Entity.Document + ")")
                .Match("(document)-[:" + Label.Relationship.MadeBy + "]->(madeBy:" + Label.Entity.Agent + ")")
                .OptionalMatch("(madeBy)-[:HAS_RELATION]->()-[:IS_A]->(madeByTypes:Category)")
                .OptionalMatch("(madeBy)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenship:Place)")
                .OptionalMatch("(madeBy)-[:HAS_RELATION]->(birth)-[:BORN_AT]->(bornAt:Place)")
                .OptionalMatch("(madeBy)-[:HAS_RELATION]->(death)-[:DIED_AT]->(diedAt:Place)")
                .OptionalMatch("(document)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .WithParams(new
                {
                    referrerGuid
                })
                ;

            var results = await query
                .Return(() => new
                {
                    Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })"),
                    Document = Return.As<Document>("document"),
                    MadeBy = Return.As<Agent>("madeBy"),
                    MadeByTypes = Return.As<IEnumerable<Category>>("collect(distinct(madeByTypes))"),
                    Citizenship = Return.As<Place>("citizenship"),
                    Birth = Return.As<int?>("birth.Year"),
                    Death = Return.As<int?>("death.Year")
                })
                .ResultsAsync
                ;

            return results
                .AsParallel()
                .Select(x => new DocumentHyperNode
                {
                    MadeBy = x.MadeBy,
                    Document = x.Document,
                    Entities = x.Entities.Select(y => Helpers.Common.ToEntity(y.Type, y.Entity)).ToList(),
                    MadeByTypes = x.MadeByTypes,
                    Citizenship = x.Citizenship,
                    Birth = x.Birth,
                    Death = x.Death
                });

        }
        public IEnumerable<DocumentHyperNode> FindDocumentHyperNodesByAgent(string agentGuid)
        {
            var results = Client.Cypher
                .Match("(madeBy:" + Label.Entity.Agent + " { Guid: {agentGuid} })<-[:" + Label.Relationship.MadeBy + "]-(document:" + Label.Entity.Document + ")")
                .OptionalMatch("(document)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .WithParams(new { agentGuid })
                .Return(() => new
                {
                    Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })"),
                    Document = Return.As<Document>("document"),
                    MadeBy = Return.As<Agent>("madeBy")
                })
                .Results
                .Select(x => new DocumentHyperNode
                {
                    MadeBy = x.MadeBy,
                    Document = x.Document,
                    Entities = x.Entities.Select(y => Helpers.Common.ToEntity(y.Type, y.Entity)).ToList()
                });
            return results.ToList();
        }
        public IEnumerable<DocumentHyperNode> AllDocumentHyperNodes()
        {
            var results = Client.Cypher
                .Match("(madeBy:" + Label.Entity.Agent + ")<-[:" + Label.Relationship.MadeBy + "]-(document:" + Label.Entity.Document + ")")
                .Return(() => new
                {
                    Document = Return.As<Document>("document"),
                    MadeBy = Return.As<Agent>("madeBy")
                })
                .Results
                .Select(x => new DocumentHyperNode
                {
                    MadeBy = x.MadeBy,
                    Document = x.Document
                });
            return results.ToList();
        }

        public List<NewAct> FindNewActsThatIntersectWithCategory(string agentGuid, string categoryGuid)
        {
            var actSelector = Client.Cypher
                .Match("(agent:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(category:Category { Guid: { categoryGuid} })").WithParams(new { categoryGuid })
                .Match("(categories:Category)-[:KIND_OF*0..]->(category)")
                .Match("(categoryAgents:Agent)-[:HAS_RELATION]->()-[:IS_A]->(categories)")
                .Match("(act:Act)-[:REFERS_TO]->(agent)")
                ;

            var query = actSelector
                .Match("(act)-[:REFERS_TO]->(categoryAgents)")
                .Match("(act)-[:REFERS_TO]->(entity)")
                .Where("categoryAgents.Guid <> agent.Guid")
                .OptionalMatch("(entity)-[:HAS_IMAGE]->(image:Image { Primary: true })")
                .OptionalMatch("(entity)-[:HAS_RELATION]->()-[:IS_A]->(type:Category)")
                .With("collect(type) as Attributes, entity, image, act")
                .With("{ Act: act, Entities: collect({ Entity: entity, Type: head(labels(entity)) }), EntityImages: collect({ Entity: entity, Type: head(labels(entity)), Image: image, Attributes: Attributes }) } as ActNode")
                ;

            var results = query
                .Return(() => Return.As<ActEntityImageNode>("ActNode"))
                .Results
                .Select(x => new NewAct
                {
                    Act = x.Act,
                    EntityImages = x.EntityImages.Select(y => new EntityImage { Attributes = y.Attributes, Image = y.Image, Entity = Helpers.Common.ToEntity(y.Type, y.Entity) })
                })
                .ToList();

            return results.ToList();
        }

        public List<NewAct> FindOutgoingAttributesByDates(string startYear, string endYear)
        {
            var query = Client.Cypher
                .Match("(someone)-[:HAS_RELATION]->(attribute:" + Label.Entity.Relation + ")-[x]->(entity)")
                ;
            if (false == string.IsNullOrEmpty(endYear))
            {
                query = query.Where("attribute.Year >= {startYear}").WithParams(new { startYear = int.Parse(startYear) })
                             .AndWhere("attribute.Year <= {endYear}").WithParams(new { endYear = int.Parse(endYear) });
            }
            else
            {
                query = query.Where("attribute.Year = {startYear}").WithParams(new { startYear = int.Parse(startYear) });
            }
            query = query
                .OptionalMatch("(entity)-[:HAS_RELATION]->()-[:IS_A]->(type:Category)")
                .OptionalMatch("(entity)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .With("collect(type) as Attributes, entity, image, attribute")
                .With("{ Act: attribute, Entities: collect({ Entity: entity, Type: head(labels(entity)) }), EntityImages: collect({ Entity: entity, Type: head(labels(entity)), Image: image, Attributes: Attributes }) } as ActNode")
                ;
            var results = query
                .Return(() => Return.As<ActEntityImageNode>("ActNode"))
                .Results
                .Select(x => new NewAct
                {
                    Act = x.Act,
                    EntityImages = x.EntityImages.Select(y => new EntityImage { Attributes = y.Attributes, Image = y.Image, Entity = Helpers.Common.ToEntity(y.Type, y.Entity) })
                });
            return results.ToList();
        }

        public IEnumerable<int> YearNavigation(int year)
        {

            var query = Client.Cypher
                .OptionalMatch("(act:Act)-[:REFERS_TO]->()")
                .OptionalMatch("()-[:HAS_RELATION]->(relation:Relation)")
                .Where((Act act) => act.Year >= year - 10 && act.Year <= year + 10)
                .OrWhere((Relation relation) => relation.Year >= year - 10 && relation.Year <= year + 10)
                ;

            var results = query
                .Return(() => new
                {
                    ActYears = Return.As<IEnumerable<int?>>("act.Year"),
                    RelationYears = Return.As<IEnumerable<int?>>("relation.Year")
                })
                .Results
                .FirstOrDefault()
                ;

            var result = results.ActYears.Where(x => x.HasValue).Concat(results.RelationYears.Where(x => x.HasValue))
                .Select(x => x.Value)
                .Distinct()
                .ToList()
                ;

            return result;

        }

        public List<TagHyperNode> GetNewActThemes()
        {
            return GetTagsOrThemes(isTheme: true);
        }

        public List<TagHyperNode> GetNewActTags()
        {
            return GetTagsOrThemes();
        }

        List<TagHyperNode> GetTagsOrThemes(bool? isTheme = null)
        {
            var query = Client.Cypher
                .Match("(act)-[:" + Label.Relationship.HasTag + "]->(tag:Tag)")
                .Where((Tag tag) => tag.IsTheme == isTheme)
                .OptionalMatch("(tag)-[:" + Label.Relationship.SubsetOf + "]->(entity:Tag)")
                ;

            var result = query
                .Return(() => Return.As<TagNode>("{ Tag: tag, Entities: collect(distinct { Entity: entity, Type: head(labels(entity)) }) }"))
                .Results
                .Select(x => new TagHyperNode
                {
                    Tag = x.Tag,
                    Entities = x.Entities.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity))
                                         .Where(x2 => x2 != null)
                })
                .Distinct(new TagHyperNodeComparer())
                .OrderBy(x => x.Tag.Name)
                .ToList();

            return result;
        }

        public List<Place> GetNewActPlaces()
        {
            var query = Client.Cypher
                .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(place:Place)")
                ;

            var result = query
                .Return(() => Return.As<Place>("distinct place"))
                .Results
                .Distinct(new PlaceComparer())
                .ToList();

            return result;
        }

        public List<PersonalDetails> GetNewActAgents()
        {
            var query = Client.Cypher
                .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(agent:Agent)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:IS_A]->(attribute:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:CITIZEN_OF]->(citizenship:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->()-[:REPRESENTATIVE_OF]->(representativeOf:Category)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(birthPlace:Place)")
                .OptionalMatch("(agent)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(deathPlace:Place)")
                ;

            var results = query
                .Return(() => new PersonalDetails
                {
                    Agent = Return.As<Agent>("agent"),
                    Birth = Return.As<Relation>("birth"),
                    Death = Return.As<Relation>("death"),
                    BirthPlace = Return.As<Place>("birthPlace"),
                    DeathPlace = Return.As<Place>("deathPlace"),
                    Attributes = Return.As<IEnumerable<Category>>("collect(distinct(attribute))"),
                    Citizenship = Return.As<Place>("citizenship"),
                    RepresentativeOf = Return.As<Category>("representativeOf")
                })
                .Results
                .ToList();

            return results;
        }

        public List<ArtefactMadeByAgent> GetNewActArtefacts()
        {
            var query = Client.Cypher
                .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(artefact:Artefact)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(madeBy:" + Label.Entity.Relation + ")-[:" + Label.Relationship.MadeBy + "]->(agent)")
                .OptionalMatch("(artefact)-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[:" + Label.Relationship.IsA + "]->(category:" + Label.Entity.Category + ")")
                .OptionalMatch("(artefact)-[:LOCATED_AT]->(location:Place)")
                ;

            var result = query
                .Return((ICypherResultItem artefact, ICypherResultItem agent) => new ArtefactMadeByAgent
                {
                    Artefact = artefact.As<Artefact>(),
                    MadeBy = Return.As<Relation>("madeBy"),
                    Agents = Return.As<IEnumerable<Agent>>("collect(agent)"),
                    IsA = Return.As<IEnumerable<Category>>("collect(distinct(category))"),
                    Location = Return.As<Place>("location")
                })
                .Results
                .ToList();

            return result;
        }

        public List<Category> GetNewActAgentTypes()
        {
            var query = Client.Cypher
                .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(agent:Agent)")
                .Match("(agent)-[:" + Label.Relationship.HasRelation + "]->(:Relation)-[:" + Label.Relationship.IsA + "]->(category:Category)")
                ;

            var result = query
                .Return(() => Return.As<Category>("distinct category"))
                .Results
                .ToList();

            return result;
        }

        public List<NewAct> FindNewActsByTagCategory(string tagCategoryGuid)
        {
            var actSelector = Client.Cypher
                .Match("(categories:Category { Guid: {tagCategoryGuid} })").WithParams(new { tagCategoryGuid })
                .Match("(category:Category)-[:" + Label.Relationship.KindOf + "*0..]->(categories)")
                .Match("(startingTag:Tag)-[:" + Label.Relationship.RefersTo + "]->(category)")
                .Match("(act)-[:" + Label.Relationship.HasTag + "]->(startingTag)");

            var query = actSelector
                .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .OptionalMatch("(entity)-[:HAS_RELATION]->()-[:IS_A]->(type:Category)")
                .OptionalMatch("(entity)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(act)-[:" + Label.Relationship.HasAttachment + "]->(attachment)")
                .OptionalMatch("(entity)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(entity)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(act)-[:" + Label.Relationship.SubsetOf + "]->(container:" + Label.Entity.Act + ")")
                .With("collect(type) as Attributes, entity, image, attachment, birth, death, act, container")
                .With("{ Act: act, Attachments: collect({ Entity: attachment, Type: head(labels(attachment)) }), EntityImages: collect({ Entity: entity, Type: head(labels(entity)), Image: image, Attributes: Attributes, Birth: birth, Death: death }), Container: container } as ActNode")
                ;

            var results = query
                .Return(() => Return.As<ActEntityImageNode>("ActNode"))
                .Results;

            var acts = results.AsParallel()
                .Select(x => new NewAct
                {
                    Act = x.Act,
                    EntityImages = x.EntityImages.Select(x2 => new EntityImage
                    {
                        Attributes = x2.Attributes,
                        Image = x2.Image,
                        Entity = Helpers.Common.ToEntity(x2.Type, x2.Entity),
                        Birth = x2.Birth,
                        Death = x2.Death
                    }),
                    SubsetOf = new GenericRelationNode<Act, Act>
                    {
                        Source = x.Act,
                        Destination = x.Container
                    },
                    Attachments = x.Attachments.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity))
                })
                .ToList();

            SetTagsAndOtherEntities(actSelector, acts);

            return acts;
        }

        public IEnumerable<IGrouping<string, TagGraphResult>> TagCharts()
        {
            var query = Client.Cypher
                .Match("(act:Act)-[:HAS_TAG]->(tag:Tag)-[:HAS_VALUE]->(value:TagValue)");

            var results = query
                .Return(() => new TagGraphResult
                {
                    Year = Return.As<int?>("act.Year"),
                    Month = Return.As<int?>("act.Month"),
                    Day = Return.As<int?>("act.Day"),
                    Tag = Return.As<Tag>("tag"),
                    Value = Return.As<TagValue>("value")
                })
                .Results
                .GroupBy(x => x.Tag.Guid)
                ;

            return results;
        }

        public List<NewAct> FindNewActsByTag(string tagGuid)
        {
            var actSelector = Client.Cypher
                .Match("(startingTag:Tag { Guid: {tagGuid} })").WithParams(new { tagGuid })
                .Match("(subsets:Tag)-[:" + Label.Relationship.SubsetOf + "*0..]->(startingTag)")
                .Match("(act)-[:" + Label.Relationship.HasTag + "]->(subsets)")
                ;

            var query = GetActQuery(actSelector);
            var acts = GetActResults(query);

            SetTagsAndOtherEntities(actSelector, acts);

            return acts;
        }

        public List<NewAct> FindNewActsLatest()
        {
            var lastweek = DateTime.Now.AddDays(-14);

            var actSelector = Client.Cypher
                .Match("(act:Act)")
                .Where((Act act) => act.Created >= lastweek)
                ;

            var query = GetActQuery(actSelector);

            //var query = actSelector
            //    .Match("(act)-[:" + Label.Relationship.RefersTo + "]->(entity)")
            //    .OptionalMatch("(entity)-[:HAS_RELATION]->()-[:IS_A]->(type:Category)")
            //    .OptionalMatch("(entity)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
            //    .OptionalMatch("(act)-[:" + Label.Relationship.HasAttachment + "]->(attachment)")
            //    .OptionalMatch("(entity)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
            //    .OptionalMatch("(entity)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(:Place)")
            //    .OptionalMatch("(act)-[:" + Label.Relationship.SubsetOf + "]->(container:" + Label.Entity.Act + ")")
            //    .With("collect(type) as Attributes, entity, image, attachment, birth, death, act, container")
            //    .With("{ Act: act, Attachments: collect({ Entity: attachment, Type: head(labels(attachment)) }), EntityImages: collect({ Entity: entity, Type: head(labels(entity)), Image: image, Attributes: Attributes, Birth: birth, Death: death }), Container: container } as ActNode")
            //    ;

            var acts = GetActResults(query);

            //var results = query
            //    .Return(() => new
            //    {
            //        ActNode = Return.As<IEnumerable<ActEntityImageNode>>("collect(ActNode)")
            //    })
            //    .Results;

            //var acts = results.SelectMany(x => x.ActNode).Select(x => new NewAct
            //{
            //    Act = x.Act,
            //    EntityImages = x.EntityImages.Select(x2 => new EntityImage
            //    {
            //        Attributes = x2.Attributes,
            //        Image = x2.Image,
            //        Entity = Helpers.Common.ToEntity(x2.Type, x2.Entity),
            //        Birth = x2.Birth,
            //        Death = x2.Death
            //    }),
            //    SubsetOf = new GenericRelationNode<Act, Act>
            //    {
            //        Source = x.Act,
            //        Destination = x.Container
            //    },
            //    Attachments = x.Attachments.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity)),
            //}).ToList();

            SetTagsAndOtherEntities(actSelector, acts);

            return acts;

        }

        public NewAct FindAct(string guid)
        {
            var actSelector = Client.Cypher
                .Match("(act:" + Label.Entity.Act + " { Guid: {guid} })").WithParams(new { guid })
                ;

            var query = GetActQuery(actSelector);
            var acts = GetActResults(query);

            SetTagsAndOtherEntities(actSelector, acts);

            return acts.FirstOrDefault();

        }

        public List<NewAct> FindNewActsByDates(string startYear, string endYear)
        {
            var actSelector = Client.Cypher
                .Match("(act:" + Label.Entity.Act + ")")
                ;
            if (false == string.IsNullOrEmpty(endYear))
            {
                actSelector = actSelector.Where("act.Year >= {startYear}").WithParams(new { startYear = int.Parse(startYear) })
                             .AndWhere("act.Year <= {endYear}").WithParams(new { endYear = int.Parse(endYear) });
            }
            else
            {
                actSelector = actSelector.Where("act.Year = {startYear}").WithParams(new { startYear = int.Parse(startYear) });
            }
            if (startYear.Substring(0, 1) == "-")
            {
                actSelector = actSelector.AndWhere("act.Milennium = 'BC'");
            }
            var query = GetActQuery(actSelector);
            var acts = GetActResults(query);

            SetTagsAndOtherEntities(actSelector, acts);

            return acts;

        }

        private static List<NewAct> GetActResults(ICypherFluentQuery query)
        {
            var results = query
                .Return(() => new
                {
                    ActNode = Return.As<IEnumerable<ActEntityImageNode>>("collect(ActNode)")
                })
                .Results;

            var acts = results.SelectMany(x => x.ActNode).Select(x => new NewAct
            {
                Act = x.Act,
                EntityImages = x.EntityImages.Select(x2 => new EntityImage
                {
                    Attributes = x2.Attributes,
                    Entity = Helpers.Common.ToEntity(x2.Type, x2.Entity),
                    Birth = x2.Birth,
                    Death = x2.Death
                }),
                SubsetOf = new GenericRelationNode<Act, Act>
                {
                    Source = x.Act,
                    Destination = x.Container
                },
                Source = x.Source,
                HasSourceGuid = x.HasSourceGuid,
                StatedBy = x.StatedBy,
                StatedByGuid = x.StatedByGuid,
                Attachments = x.Attachments.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity)),
                DataSets = x.DataSets
            }).ToList();

            return acts;
        }

        private static ICypherFluentQuery GetActQuery(ICypherFluentQuery actSelector)
        {
            var query = actSelector
                .OptionalMatch("(act)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .OptionalMatch("(act)-[hasSource:HAS_SOURCE]->(source:Artefact)")
                .OptionalMatch("(act)-[statedByRelationship:" + Label.Relationship.StatedBy + "]->(statedBy:Agent)")
                .OptionalMatch("(entity)-[:HAS_RELATION]->()-[:IS_A]->(type:Category)")
                .OptionalMatch("(act)-[:" + Label.Relationship.HasAttachment + "]->(attachment)")
                .OptionalMatch("(entity)-[:HAS_RELATION]->(birth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(entity)-[:HAS_RELATION]->(death:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(act)-[:" + Label.Relationship.SubsetOf + "]->(container:" + Label.Entity.Act + ")")
                .OptionalMatch("(dataset:DataSet)<-[:" + Label.Relationship.PartOf + "]-(datapoint:DataPoint)-[:" + Label.Relationship.HasSource + "]->(act)")
                .With("collect(type) as Attributes, entity, attachment, birth, death, act, container, source, hasSource, statedByRelationship, statedBy, dataset")
                .With(@"{
                    Act: act,
                    DataSets: collect(distinct dataset),
                    Attachments: collect({ Entity: attachment, Type: head(labels(attachment)) }),
                    EntityImages: collect({ Entity: entity, Type: head(labels(entity)), Attributes: Attributes, Birth: birth, Death: death }),
                    Container: container,
                    Source: source, HasSourceGuid: hasSource.Guid,
                    StatedBy: statedBy, StatedByGuid: statedByRelationship.Guid
                } as ActNode")
                ;
            return query;
        }

        private void SetRelatedEntitiesOnActs(ICypherFluentQuery actSelector, List<NewAct> acts)
        {
            var query = actSelector
                    .Match("(act)-[:REFERS_TO]->(agent1:Agent)")
                    .With("agent1, act")
                    .Match("(act)-[:REFERS_TO]->(agent2:Agent)")
                    .Where("agent1.Guid <> agent2.Guid")
                    .With("agent1, agent2, act")
                    .Match("(agent1)-[]->()-[rel]->(agent2)")
                    ;

            var result = query.Return(() => new
            {
                ActGuid = Return.As<string>("act.Guid"),
                Agent1 = Return.As<Agent>("agent1"),
                Relationship = Return.As<string>("type(rel)"),
                Agent2 = Return.As<Agent>("agent2")
            })
            .Results;

            foreach (var row in result.GroupBy(x => x.ActGuid))
            {
                var act = acts.FirstOrDefault(x => x.Act.Guid == row.Key);
                if (act != null)
                {
                    if (act.RelatedAgents == null)
                    {
                        act.RelatedAgents = new List<RelatedAgents>();
                    }
                    act.RelatedAgents.AddRange(row.Select(x => new RelatedAgents
                    {
                        ActGuid = row.Key,
                        Agent1 = x.Agent1,
                        Agent2 = x.Agent2,
                        Relationship = x.Relationship
                    }));
                }
            }
        }

        private void SetTagsAndOtherEntities(ICypherFluentQuery actSelector, List<NewAct> acts)
        {
            var tags = actSelector
                .Match("(act)-[:" + Label.Relationship.HasTag + "]->(tag:Tag)")
                .OptionalMatch("(tag)-[:" + Label.Relationship.SubsetOf + "*1..]->(parents:Tag)")
                .Return(() => Return.As<TagValueItemNode>("{ ActGuid: act.Guid, Tag: tag, Parents: collect(distinct(parents)) }"))
                .Results
                .ToList();

            var tagValues = actSelector
                .Match("(value:" + Label.Entity.TagValue + ")-[:" + Label.Relationship.ValueSource + "]->(act)")
                .Match("(tag:Tag)-[:" + Label.Relationship.HasValue + "]->(value)")
                .OptionalMatch("(tag)-[:" + Label.Relationship.SubsetOf + "*1..]->(parents:Tag)")
                .Return(() => Return.As<TagValueItemNode>("{ ActGuid: act.Guid, Tag: tag, Value: value, Parents: collect(distinct(parents)) }"))
                .Results
                .ToList();

            foreach (var tagRow in tags.GroupBy(x => x.ActGuid))
            {
                var act = acts.FirstOrDefault(x => x.Act.Guid == tagRow.Key);
                if (act != null)
                {
                    if (act.TagValues == null)
                    {
                        act.TagValues = new List<TagValueItemNode>();
                    }
                    act.TagValues.AddRange(tagRow.Where(x2 => x2.Tag != null)
                        .Distinct(new TagValueItemNodeComparer()));
                }
            }

            foreach (var tagRow in tagValues.GroupBy(x => x.ActGuid))
            {
                var act = acts.FirstOrDefault(x => x.Act.Guid == tagRow.Key);
                if (act != null)
                {
                    if (act.TagValues == null)
                    {
                        act.TagValues = new List<TagValueItemNode>();
                    }
                    var values = tagRow.Where(x2 => x2.Tag != null).Distinct(new TagValueItemNodeComparer());
                    foreach (var value in values)
                    {
                        var match = act.TagValues.FirstOrDefault(x => x.Tag.Guid == value.Tag.Guid);
                        if (match != null)
                        {
                            act.TagValues.Remove(match);
                        }
                        act.TagValues.Add(value);
                    }
                }
            }

            // SetRelatedEntitiesOnActs(actSelector, acts);
        }

        public async Task<IEnumerable<YearActs>> GetYears()
        {
            var query = Client.Cypher
                .Match("(act:" + Label.Entity.Act + ")")
                .With("distinct(act) as Acts")
                .Where("NOT (Acts.Year IS NULL)")
                .With("{ Year: Acts.Year, Milennium: Acts.Milennium, TotalActs: count(Acts) } as Years")
                ;

            var results1 = await query
                .Return(() => Return.As<YearActs>("distinct Years"))
                .OrderBy("Years.Year")
                .ResultsAsync;

            //var query2 = Client.Cypher
            //    .Match("(entity)-[:HAS_RELATION]->(relation:" + Label.Entity.Relation + ")")
            //    .With("distinct(relation) as Relation")
            //     .Where("NOT (Relation.Year IS NULL)")
            //    .With("{ Year: Relation.Year, TotalActs: count(Relation) } as Years")
            //    ;

            //var results2 = await query2
            //    .Return(() => Return.As<YearActs>("distinct Years"))
            //    .OrderBy("Years.Year")
            //    .ResultsAsync;

            var results = results1
                .Select(x => new YearActs
                {
                    TotalActs = x.TotalActs,
                    Year = x.Milennium == "BC" ? -1 * x.Year : x.Year
                })
                .ToList();
            return results.OrderBy(x => x.Year);
        }

        public async Task<IEnumerable<NewAct>> FindNewActs(string entityType, string entityGuid, string userGuid = null, Significance significance = Significance.Personal)
        {
            var rel = entityType == Label.Entity.Agent ? Label.Relationship.AspectOf : Label.Relationship.LocatedIn;

            var actSelector = Client.Cypher
                .Match("(referrer:" + entityType + " { Guid: {entityGuid} })")
                .Match("(part:" + entityType + ")-[:" + rel + "*0..]->(referrer)")
                .Match("(part)<-[:" + Label.Relationship.RefersTo + "]-(act:" + Label.Entity.Act + " { Significance: {significance} })")
                .WithParams(new { entityGuid, significance })
                ;

            return FindActsWithSelector(actSelector);
        }

        public IEnumerable<NewAct> FindNewActsByAgentAndSource(string agentGuid)
        {
            var actSelector = Client.Cypher
                .Match("(author:" + Label.Entity.Agent + " { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(act:" + Label.Entity.Act + ")-[:" + Label.Relationship.StatedBy + "]->(author)")
                ;

            var statedBy = FindActsWithSelector(actSelector);
            var agentOf = FindActsByAgent(agentGuid).Result;
            var combined = statedBy.Concat(agentOf).Distinct(new NewActComparer());

            return combined;
        }

        private IEnumerable<NewAct> FindActsWithSelector(ICypherFluentQuery actSelector)
        {
            var query = GetActQuery(actSelector);
            var acts = GetActResults(query);

            SetTagsAndOtherEntities(actSelector, acts);

            return acts;
        }
        public void SaveOrUpdate(string agentGuid, Document document, IEnumerable<string[]> list)
        {
            var isNew = document.Guid == null;
            var node = SaveOrUpdate(document);
            if (false == isNew)
            {
                DeleteDocumentRefersTo(document.Guid);
            }
            else
            {
                commonService.CreateRelationship(Label.Entity.Document, document.Guid, Label.Entity.Agent, agentGuid, Label.Relationship.MadeBy);
            }
            foreach (var tuple in list)
            {
                var type = tuple[0];
                var guid = tuple[1];
                commonService.CreateRelationship(Label.Entity.Document, document.Guid, type, guid, Label.Relationship.RefersTo);
            }
        }

        public Document SaveOrUpdate(Document document)
        {
            return commonService.SaveOrUpdate(document, Label.Entity.Document);
        }

        public void SaveOrUpdateAct(Act act, IEnumerable<string[]> entities, IEnumerable<string[]> attachments, IEnumerable<ValueItem> tags)
        {
            var isNew = act.Guid == null;
            if (false == act.Created.HasValue)
            {
                act.Created = DateTimeOffset.UtcNow;
            }
            var node = SaveOrUpdate(act);
            if (false == isNew)
            {
                DeleteActRefersTo(act.Guid);
            }
            if (entities != null)
            {
                foreach (var tuple in entities)
                {
                    var type = tuple[0];
                    var guid = tuple[1];
                    commonService.CreateRelationship(Label.Entity.Act, act.Guid, type, guid, Label.Relationship.RefersTo);
                }
            }
            if (attachments != null)
            {
                foreach (var tuple in attachments)
                {
                    var type = tuple[0];
                    var guid = tuple[1];
                    commonService.CreateRelationship(Label.Entity.Act, act.Guid, type, guid, Label.Relationship.HasAttachment);
                }
            }
            var existingTags = tags.Where(x => x.Guid != null);
            if (existingTags.Any())
            {
                DeleteTagAssociations(act);
                // and recreate them
                foreach (var tag in existingTags)
                {
                    AssociateExistingTagWithAct(act, tag);
                }
            }
            var newTags = tags.Where(x => x.Guid == null);
            if (newTags.Any())
            {
                foreach (var newTag in newTags)
                {
                    AssociateNewTagWithAct(act, newTag);
                }
            }
        }

        private void DeleteTagAssociations(Act act)
        {
            Client.Cypher
                  .Match("(act:Act { Guid: {actGuid} })").WithParams(new { actGuid = act.Guid })
                  .Match("(act)-[r:" + Label.Relationship.HasTag + "]->(tag:Tag)")
                  .OptionalMatch("(tag)-[:" + Label.Relationship.HasValue + "]->(value:" + Label.Entity.TagValue + ")-[:" + Label.Relationship.ValueSource + "]->(act)")
                  .OptionalMatch("(tag)-[r2]-()")
                  .OptionalMatch("(value)-[r3]-()")
                  .Delete("r, r2, r3, value")
                  .ExecuteWithoutResults();
        }

        private void AssociateNewTagWithAct(Act act, ValueItem newTag)
        {
            // 1. if a tag node matching the tag text exists, use that;
            //    otherwise, create a new tag node with that text
            var tag = FindTagByName(newTag.Name);
            if (tag == null)
            {
                tag = commonService.SaveOrUpdate(new Tag { Name = newTag.Name.Trim() }, Label.Entity.Tag, audit: false);
            }
            // 2. create a relationship between the Act and the Tag
            commonService.CreateRelationship(Label.Entity.Act, act.Guid, Label.Entity.Tag, tag.Guid, Label.Relationship.HasTag);
            // 3. create the value and associate it with the Tag
            if (false == string.IsNullOrEmpty(newTag.Value))
            {
                var value = new TagValue { Value = newTag.Value };
                commonService.SaveOrUpdate(value, Label.Entity.TagValue, audit: false);
                commonService.CreateRelationship(Label.Entity.Tag, tag.Guid, Label.Entity.TagValue, value.Guid, Label.Relationship.HasValue);
                commonService.CreateRelationship(Label.Entity.TagValue, value.Guid, Label.Entity.Act, act.Guid, Label.Relationship.ValueSource);
            }
        }

        private void AssociateExistingTagWithAct(Act act, ValueItem tag)
        {
            commonService.CreateRelationship(Label.Entity.Act, act.Guid, Label.Entity.Tag, tag.Guid, Label.Relationship.HasTag);
            if (false == string.IsNullOrEmpty(tag.Value))
            {
                var value = new TagValue { Value = tag.Value };
                commonService.SaveOrUpdate(value, Label.Entity.TagValue, audit: false);
                commonService.CreateRelationship(Label.Entity.Tag, tag.Guid, Label.Entity.TagValue, value.Guid, Label.Relationship.HasValue);
                commonService.CreateRelationship(Label.Entity.TagValue, value.Guid, Label.Entity.Act, act.Guid, Label.Relationship.ValueSource);
            }
        }

        [Obsolete]
        public void SaveOrUpdate(Act act, IEnumerable<string[]> list)
        {
            var isNew = act.Guid == null;
            var node = SaveOrUpdate(act);
            if (false == isNew)
            {
                DeleteActRefersTo(act.Guid);
            }
            foreach (var tuple in list)
            {
                var type = tuple[0];
                var guid = tuple[1];
                commonService.CreateRelationship(Label.Entity.Act, act.Guid, type, guid, Label.Relationship.RefersTo);
            }
        }
        public void DeleteEntityRefersTo(string entityGuid, string entityType)
        {
            Client.Cypher
                .Match("(entity:" + entityType + " { Guid: {entityGuid} })-[r:" + Label.Relationship.RefersTo + "]->()")
                .WithParams(new
                {
                    entityGuid
                })
                .Delete("r")
                .ExecuteWithoutResults();
        }
        public void DeleteDocumentRefersTo(string documentGuid)
        {
            DeleteEntityRefersTo(documentGuid, Label.Entity.Document);
        }
        public void DeleteActRefersTo(string actGuid)
        {
            DeleteEntityRefersTo(actGuid, Label.Entity.Act);
        }
        public BaseActHyperNode FindDiedActByAgent(string agentGuid)
        {
            return FindActByAgent(agentGuid, "Died");
        }
        public BaseActHyperNode FindBornActByAgent(string agentGuid)
        {
            return FindActByAgent(agentGuid, "Born");
        }
        public BaseActHyperNode FindActByAgent(string agentGuid, string actType)
        {
            var result = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.AgentOf + "]->(act:" + Label.Entity.Act + " { Name: {actType} })")
                .WithParams(new
                {
                    agentGuid,
                    actType
                })
                .OptionalMatch("(act)-[:" + Label.Relationship.AtPlace + "]->(place:" + Label.Entity.Place + ")")
                .OptionalMatch("(place)-[:" + Label.Relationship.InsideOf + "*1..4]->(placeContainers:" + Label.Entity.Place + ")")
                .OptionalMatch("(placeContainers)-[:" + Label.Relationship.IsA + "]->(placeCategory:" + Label.Entity.Category + ")")
                .Return(() => new BaseActHyperNode
                {
                    Agent = Return.As<Agent>("agent"),
                    Act = Return.As<Act>("act"),
                    Place = Return.As<Place>("place"),
                    PlaceCategory = Return.As<IEnumerable<PlaceCategoryTuple>>("collect({ Place: placeContainers, Category: placeCategory })")
                })
                .Results
                .FirstOrDefault();
            return result;
        }
        private int DeleteRelationship(string relationshipGuid, string relationType)
        {
            var deleted = Client.Cypher
                    .Match("(x)-[r:" + relationType + " { Guid: {relationshipGuid} }]->(y)")
                    .WithParams(new
                    {
                        relationshipGuid
                    })
                    .Delete("r")
                    .Return(r => Return.As<int>("count(r)"))
                    .Results
                    .FirstOrDefault();
            return deleted;
        }
        public Act SaveOrUpdate(Act act)
        {
            return commonService.SaveOrUpdate(act, Label.Entity.Act);
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
    }
}
