﻿using Data;
using Data.Entities;
using log4net;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public interface ITextService
    {
        string MentionsAgent(string textGuid, string agentGuid, string mentionsGuid = null);
        string MentionsCategory(string textGuid, string categoryGuid, string mentionsGuid = null);
        MentionsCategory FindMentionsCategory(string textGuid, string mentionsGuid);
        string WrittenBy(string textGuid, string authorGuid, string writtenByGuid = null);
        AuthorOfArtefact FindWrittenBy(string textGuid, string writtenByGuid);
        string AboutArtefact(string textGuid, string artefactGuid);
        List<Text> FindTextsByArtefact(string artefactGuid);
        Text Find(string textGuid);
        Text SaveOrUpdate(Text text);
        ArtefactTextHyperNode FindArtefactText(string textGuid);
        List<ArtefactTextHyperNode> FindArtefactTexts(string artefactGuid);
        AgentTextHyperNode FindAgentText(string textGuid);
        List<AgentTextHyperNode> FindAgentTexts(string agentGuid);
        void DeleteWrittenBy(string writtenByGuid);
    }
    public abstract class Mentions<T> where T : class, IEntity
    {
        public Artefact Artefact { get; set; }
        public Text Text { get; set; }
        public T Entity { get; set; }
        public string MentionsGuid { get; set; }
    }
    public class MentionsCategory : Mentions<Category> { }
    public class MentionsArtefact : Mentions<Artefact> { }
    public class MentionsPlace : Mentions<Place> { }
    public class MentionsAgent : Mentions<Agent> { }
    public class AuthorOfArtefact
    {
        public Artefact Artefact { get; set; }
        public Text Text { get; set; }
        public Agent Author { get; set; }
        public string AuthorOfGuid { get; set; }
    }
    public class Relation<TSource, TDest>
        where TSource : IEntity
        where TDest : IEntity
    {
        public string Guid { get; set; }
        public TSource Source { get; set; }
        public TDest Destination { get; set; }
    }
    public class TextHyperNode<TSubject> where TSubject : IEntity
    {
        /**
         * Entities
         */
        public Text Text { get; set; }
        public List<Relation<Text, Agent>> Authors { get; set; }
        public Relation<Text, TSubject> Subject { get; set; }
        public Relation<Text, Artefact> Citation { get; set; }
        public List<Relation<Text, Category>> MentionsByCategory { get; set; }
        public List<Relation<Text, Artefact>> MentionsByArtefact { get; set; }
        public List<Relation<Text, Agent>> MentionsByAgent { get; set; }
        public List<Relation<Text, Place>> MentionsByPlace { get; set; }
        public bool IsNew
        {
            get
            {
                return Text == null || Text.Guid == null;
            }
        }
    }
    public class ArtefactTextHyperNode : TextHyperNode<Artefact>
    {

    }
    public class AgentTextHyperNode : TextHyperNode<Agent>
    {

    }
    public class PlaceTextHyperNode : TextHyperNode<Place>
    {

    }
    public class TextService : ITextService
    {
        private readonly ILog Log = LogManager.GetLogger("DefaultLogger");
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly ICommonService commonService;
        private readonly IRelationService relationService;
        private readonly IAgentService agentService;
        public TextService(
            GraphClient client,
            ICommonService _commonService,
            IRelationService _relationService,
            IAgentService _agentService
        )
        {
            Client = client;
            commonService = _commonService;
            relationService = _relationService;
            agentService = _agentService;
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        public MentionsCategory FindMentionsCategory(string textGuid, string mentionsGuid)
        {
            var result = Client.Cypher
                .Match("(text:" + Label.Entity.Text + " { Guid: {textGuid} })-[mentions:" + Label.Relationship.Mentions + " { Guid: {mentionsGuid} }]->(category:" + Label.Entity.Category + ")")
                .OptionalMatch("(text)-[:" + Label.Relationship.About + "]->(artefact:" + Label.Entity.Artefact + ")")
                .WithParams(new
                {
                    textGuid,
                    mentionsGuid
                })
                .Return((ICypherResultItem text, ICypherResultItem artefact, ICypherResultItem category) => new MentionsCategory
                {
                    Text = text.As<Text>(),
                    Artefact = artefact.As<Artefact>(),
                    Entity = category.As<Category>(),
                    MentionsGuid = Return.As<string>("mentions.Guid")
                })
                .Results.FirstOrDefault();
            return result;
        }
        public AuthorOfArtefact FindWrittenBy(string textGuid, string writtenByGuid)
        {
            var result = Client.Cypher
                .Match("(text:" + Label.Entity.Text + " { Guid: {textGuid} })-[writtenBy:" + Label.Relationship.WrittenBy + " { Guid: {writtenByGuid} }]->(author:" + Label.Entity.Agent + ")")
                .OptionalMatch("(text)-[:" + Label.Relationship.About + "]->(artefact:" + Label.Entity.Artefact + ")")
                .WithParams(new
                {
                    textGuid,
                    writtenByGuid
                })
                .Return((ICypherResultItem text, ICypherResultItem artefact, ICypherResultItem author) => new AuthorOfArtefact
                {
                    Artefact = artefact.As<Artefact>(),
                    Author = author.As<Agent>(),
                    Text = text.As<Text>(),
                    AuthorOfGuid = Return.As<string>("writtenBy.Guid")
                })
                .Results.FirstOrDefault();
            return result;
        }
        private string About(string textGuid, string entityGuid, string entityType)
        {
            var relationshipGuid = Guid.NewGuid().ToString();
            Client.Cypher
                .Match("(text:" + Label.Entity.Text + " { Guid: {textGuid} })", "(entity:" + entityType + " { Guid: {entityGuid} })")
                .WithParams(new
                {
                    textGuid,
                    entityGuid
                })
                .CreateUnique("text-[:" + Label.Relationship.About + " { Guid: {relationshipGuid} }]->entity")
                .WithParams(new
                {
                    relationshipGuid
                })
                .ExecuteWithoutResults();
            return relationshipGuid;
        }
        public string AboutArtefact(string textGuid, string artefactGuid)
        {
            return About(textGuid, artefactGuid, Label.Entity.Artefact);
        }
        public List<Text> FindTextsByArtefact(string artefactGuid)
        {
            return Client.Cypher
                .Match("(text:Text)-[]->(artefact:Artefact { Guid: {artefactGuid} })")
                .WithParams(new
                {
                    artefactGuid
                })
                .Return(text => text.As<Text>())
                .Results
                .ToList();
        }

        public Text Find(string textGuid)
        {
            return Client.Cypher
                .Match("(text:" + Label.Entity.Text + " { Guid: {textGuid} })")
                .WithParams(new
                {
                    textGuid
                })
                .Return(text => text.As<Text>())
                .Results.FirstOrDefault();
        }
        public Text SaveOrUpdate(Text text)
        {
            return commonService.SaveOrUpdate(text, Label.Entity.Text);
        }
        public PlaceTextHyperNode FindPlaceText(string textGuid)
        {
            return AsPlaceTextHyperNode(
                Client.Cypher
                .Match("(author:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[authorOf:" + Label.Relationship.WrittenBy + "]->(text:" + Label.Entity.Text + " { Guid: {textGuid} })"
                )
                .WithParams(new
                {
                    textGuid
                })
                .OptionalMatch("(text)-[pertainsTo:" + Label.Relationship.PertainsToPlace + "]->(subject:" + Label.Entity.Place + ")")
                .OptionalMatch("(text)-[citedIn:" + Label.Relationship.CitedIn + "]->(citation:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(text)-[mentions:" + Label.Relationship.Mentions + "]->(mention:" + Label.Entity.Category + ")")
                .Return(AsPlaceTextHyperNodeTemp())
                .Results
                .FirstOrDefault()
            );
        }
        public AgentTextHyperNode FindAgentText(string textGuid)
        {
            return AsAgentTextHyperNode(
                Client.Cypher
                .Match("(author:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[authorOf:" + Label.Relationship.WrittenBy + "]->(text:" + Label.Entity.Text + " { Guid: {textGuid} })"
                )
                .WithParams(new
                {
                    textGuid
                })
                .OptionalMatch("(text)-[pertainsTo:" + Label.Relationship.PertainsToAgent + "]->(subject:" + Label.Entity.Agent + ")")
                .OptionalMatch("(text)-[citedIn:" + Label.Relationship.CitedIn + "]->(citation:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(text)-[mentions:" + Label.Relationship.Mentions + "]->(mention:" + Label.Entity.Category + ")")
                .Return(AsAgentTextHyperNodeTemp())
                .Results
                .FirstOrDefault()
            );
        }
        public ArtefactTextHyperNode FindArtefactText(string textGuid)
        {
            return AsArtefactTextHyperNode(
                Client.Cypher
                .Match("(text:" + Label.Entity.Text + " { Guid: {textGuid} })"
                )
                .WithParams(new
                {
                    textGuid
                })
                .OptionalMatch("(text)-[writtenBy:" + Label.Relationship.WrittenBy + "]->(author:" + Label.Entity.Agent + ")")
                .OptionalMatch("(text)-[about:" + Label.Relationship.About + "]->(subject:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(text)-[citedIn:" + Label.Relationship.CitedIn + "]->(citation:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(text)-[mentionsCategory:" + Label.Relationship.Mentions + "]->(mentionByCategory:" + Label.Entity.Category + ")")
                .OptionalMatch("(text)-[mentionsAgent:" + Label.Relationship.Mentions + "]->(mentionByAgent:" + Label.Entity.Agent + ")")
                .OptionalMatch("(text)-[mentionsArtefact:" + Label.Relationship.Mentions + "]->(mentionByArtefact:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(text)-[mentionsPlace:" + Label.Relationship.Mentions + "]->(mentionByPlace:" + Label.Entity.Place + ")")
                .Return((ICypherResultItem text,
                    ICypherResultItem author,
                    ICypherResultItem subject,
                    ICypherResultItem citation
                    ) => new ArtefactTextHyperNodeTemp
                    {
                        Text = text.As<Text>(),
                        Authors = Return.As<IEnumerable<Agent>>("collect(author)"),
                        AuthorOfGuids = Return.As<IEnumerable<string>>("collect(writtenBy.Guid)"),
                        Subject = subject.As<Artefact>(),
                        AboutGuid = Return.As<string>("about.Guid"),
                        Citation = citation.As<Artefact>(),
                        CitedInGuid = Return.As<string>("citedIn.Guid"),
                        MentionsByCategory = Return.As<IEnumerable<Category>>("collect(mentionByCategory)"),
                        MentionsByCategoryGuids = Return.As<IEnumerable<string>>("collect(mentionsCategory.Guid)"),
                        MentionsByAgent = Return.As<IEnumerable<Agent>>("collect(mentionByAgent)"),
                        MentionsByAgentGuids = Return.As<IEnumerable<string>>("collect(mentionsAgent.Guid)"),
                        MentionsByArtefact = Return.As<IEnumerable<Artefact>>("collect(mentionByArtefact)"),
                        MentionsByArtefactGuids = Return.As<IEnumerable<string>>("collect(mentionsArtefact.Guid)"),
                        MentionsByPlace = Return.As<IEnumerable<Place>>("collect(mentionByPlace)"),
                        MentionsByPlaceGuids = Return.As<IEnumerable<string>>("collect(mentionsPlace.Guid)")
                    })
                .Results
                .FirstOrDefault()
            );
        }
        public List<PlaceTextHyperNode> FindPlaceTexts(string placeGuid)
        {
            return AsPlaceTextHyperNodes(
                Client.Cypher
                .Match("(text:" + Label.Entity.Text + ")-[pertainsTo:" + Label.Relationship.PertainsToAgent + "]->(subject:" + Label.Entity.Place + " { Guid: {placeGuid} })"
                )
                .WithParams(new
                {
                    placeGuid
                })
                .OptionalMatch("(author:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[authorOf:" + Label.Relationship.WrittenBy + "]->(text)")
                .OptionalMatch("(text)-[citedIn:" + Label.Relationship.CitedIn + "]->(citation:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(text)-[mentions:" + Label.Relationship.Mentions + "]->(mention:" + Label.Entity.Category + ")")
                .Return(AsPlaceTextHyperNodeTemp())
                .Results
            );
        }
        public List<AgentTextHyperNode> FindAgentTexts(string agentGuid)
        {
            return AsAgentTextHyperNodes(
                Client.Cypher
                .Match("(text:" + Label.Entity.Text + ")-[pertainsTo:" + Label.Relationship.PertainsToAgent + "]->(subject:" + Label.Entity.Agent + " { Guid: {agentGuid} })"
                )
                .WithParams(new
                {
                    agentGuid
                })
                .OptionalMatch("(author:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[authorOf:" + Label.Relationship.WrittenBy + "]->(text)")
                .OptionalMatch("(text)-[citedIn:" + Label.Relationship.CitedIn + "]->(citation:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(text)-[mentions:" + Label.Relationship.Mentions + "]->(mention:" + Label.Entity.Category + ")")
                .Return(AsAgentTextHyperNodeTemp())
                .Results
            );
        }
        public List<ArtefactTextHyperNode> FindArtefactTexts(string artefactGuid)
        {
            return AsArtefactTextHyperNodes(
                Client.Cypher
                .Match("(text:" + Label.Entity.Text + ")-[pertainsTo:" + Label.Relationship.PertainsToArtefact + "]->(subject:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })"
                )
                .WithParams(new
                {
                    artefactGuid
                })
                .OptionalMatch("(author:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasRelation + "]->()-[authorOf:" + Label.Relationship.WrittenBy + "]->(text)")
                .OptionalMatch("(text)-[citedIn:" + Label.Relationship.CitedIn + "]->(citation:" + Label.Entity.Artefact + ")")
                .OptionalMatch("(text)-[mentions:" + Label.Relationship.Mentions + "]->(mention:" + Label.Entity.Category + ")")
                .Return(AsArtefactTextHyperNodeTemp())
                .Results
            );
        }
        private static List<PlaceTextHyperNode> AsPlaceTextHyperNodes(IEnumerable<PlaceTextHyperNodeTemp> results)
        {
            var hypernodes = new List<PlaceTextHyperNode>();
            foreach (var result in results)
            {
                var hypernode = AsPlaceTextHyperNode(result);
                hypernodes.Add(hypernode);
            }
            return hypernodes;
        }
        private static List<AgentTextHyperNode> AsAgentTextHyperNodes(IEnumerable<AgentTextHyperNodeTemp> results)
        {
            var hypernodes = new List<AgentTextHyperNode>();
            foreach (var result in results)
            {
                var hypernode = AsAgentTextHyperNode(result);
                hypernodes.Add(hypernode);
            }
            return hypernodes;
        }
        private static List<ArtefactTextHyperNode> AsArtefactTextHyperNodes(IEnumerable<ArtefactTextHyperNodeTemp> results)
        {
            var hypernodes = new List<ArtefactTextHyperNode>();
            foreach (var result in results)
            {
                var hypernode = AsArtefactTextHyperNode(result);
                hypernodes.Add(hypernode);
            }
            return hypernodes;
        }
        private static AgentTextHyperNode AsAgentTextHyperNode(AgentTextHyperNodeTemp result)
        {
            if (result == null)
            {
                return null;
            }

            var hypernode = new AgentTextHyperNode
            {
                Authors = result.Authors.Select(x => new Relation<Text, Agent>
                {
                    Destination = x,
                    Source = result.Text,
                    Guid = result.AuthorOfGuid
                }).ToList(),
                MentionsByCategory = result.Mentions.Select(x => new Relation<Text, Category>
                {
                    Destination = x,
                    Source = result.Text,
                    Guid = result.MentionsGuids.FirstOrDefault()
                }).ToList(),
                Subject = new Relation<Text, Agent>
                {
                    Source = result.Text,
                    Destination = result.Subject,
                    Guid = result.PertainsToGuid
                },
                Citation = new Relation<Text, Artefact>
                {
                    Source = result.Text,
                    Destination = result.Citation,
                    Guid = result.CitedInGuid
                }
            };
            return hypernode;
        }
        private static ArtefactTextHyperNode AsArtefactTextHyperNode(ArtefactTextHyperNodeTemp result)
        {
            if (result == null)
            {
                return null;
            }
            var hypernode = new ArtefactTextHyperNode
            {
                Text = result.Text,
                Authors = new List<Relation<Text, Agent>>(),
                MentionsByCategory = new List<Relation<Text, Category>>(),
                MentionsByAgent = new List<Relation<Text, Agent>>(),
                MentionsByPlace = new List<Relation<Text, Place>>(),
                Subject = new Relation<Text, Artefact>
                {
                    Source = result.Text,
                    Destination = result.Subject,
                    Guid = result.AboutGuid
                },
                Citation = new Relation<Text, Artefact>
                {
                    Source = result.Text,
                    Destination = result.Citation,
                    Guid = result.CitedInGuid
                }
            };
            for (var i = 0; i < result.Authors.Count(); i++)
            {
                var author = result.Authors.ElementAt(i);
                var guid = result.AuthorOfGuids.ElementAt(i);
                var relation = new Relation<Text, Agent>
                {
                    Source = result.Text,
                    Destination = author,
                    Guid = guid
                };
                hypernode.Authors.Add(relation);
            }
            for (var i = 0; i < result.MentionsByCategory.Count(); i++)
            {
                var category = result.MentionsByCategory.ElementAt(i);
                var guid = result.MentionsByCategoryGuids.ElementAt(i);
                var relation = new Relation<Text, Category>
                {
                    Source = result.Text,
                    Destination = category,
                    Guid = guid
                };
                hypernode.MentionsByCategory.Add(relation);
            }
            for (var i = 0; i < result.MentionsByAgent.Count(); i++)
            {
                var agent = result.MentionsByAgent.ElementAt(i);
                var guid = result.MentionsByAgentGuids.ElementAt(i);
                var relation = new Relation<Text, Agent>
                {
                    Source = result.Text,
                    Destination = agent,
                    Guid = guid
                };
                hypernode.MentionsByAgent.Add(relation);
            }
            for (var i = 0; i < result.MentionsByArtefact.Count(); i++)
            {
                var artefact = result.MentionsByArtefact.ElementAt(i);
                var guid = result.MentionsByArtefactGuids.ElementAt(i);
                var relation = new Relation<Text, Artefact>
                {
                    Source = result.Text,
                    Destination = artefact,
                    Guid = guid
                };
                hypernode.MentionsByArtefact.Add(relation);
            }
            for (var i = 0; i < result.MentionsByPlace.Count(); i++)
            {
                var place = result.MentionsByPlace.ElementAt(i);
                var guid = result.MentionsByPlaceGuids.ElementAt(i);
                var relation = new Relation<Text, Place>
                {
                    Source = result.Text,
                    Destination = place,
                    Guid = guid
                };
                hypernode.MentionsByPlace.Add(relation);
            }
            return hypernode;
        }
        private static PlaceTextHyperNode AsPlaceTextHyperNode(PlaceTextHyperNodeTemp result)
        {
            if (result == null)
            {
                return null;
            }
            var hypernode = new PlaceTextHyperNode
            {
                //Author = new Relation<Text, Agent>
                //{
                //    Source = result.Text,
                //    Destination = result.Author,
                //    Guid = result.AuthorOfGuid
                //},
                Subject = new Relation<Text, Place>
                {
                    Source = result.Text,
                    Destination = result.Subject,
                    Guid = result.PertainsToGuid
                },
                Citation = new Relation<Text, Artefact>
                {
                    Source = result.Text,
                    Destination = result.Citation,
                    Guid = result.CitedInGuid
                }
            };
            return hypernode;
        }
        private static Expression<Func<ICypherResultItem, ICypherResultItem, ICypherResultItem, ICypherResultItem, AgentTextHyperNodeTemp>> AsAgentTextHyperNodeTemp()
        {
            return (ICypherResultItem text,
                    ICypherResultItem author,
                    ICypherResultItem subject,
                    ICypherResultItem citation
                    ) => new AgentTextHyperNodeTemp
                    {
                        Text = text.As<Text>(),
                        Authors = Return.As<IEnumerable<Agent>>("collect(author)"),
                        Subject = subject.As<Agent>(),
                        Citation = citation.As<Artefact>(),
                        AuthorOfGuid = Return.As<string>("authorOf.Guid"),
                        PertainsToGuid = Return.As<string>("pertainsTo.Guid"),
                        CitedInGuid = Return.As<string>("citedIn.Guid"),
                        Mentions = Return.As<IEnumerable<Category>>("collect(mention)"),
                        MentionsGuids = Return.As<IEnumerable<string>>("collect(mentions.Guid)")
                    };
        }
        private static Expression<Func<ICypherResultItem, ICypherResultItem, ICypherResultItem, ICypherResultItem, ArtefactTextHyperNodeTemp>> AsArtefactTextHyperNodeTemp()
        {
            return (ICypherResultItem text,
                    ICypherResultItem author,
                    ICypherResultItem subject,
                    ICypherResultItem citation
                    ) => new ArtefactTextHyperNodeTemp
                    {
                        Text = text.As<Text>(),
                        Authors = Return.As<IEnumerable<Agent>>("collect(author)"),
                        AuthorOfGuids = Return.As<IEnumerable<string>>("collect(authorOf.Guid)"),
                        Subject = subject.As<Artefact>(),
                        AboutGuid = Return.As<string>("pertainsTo.Guid"),
                        Citation = citation.As<Artefact>(),
                        CitedInGuid = Return.As<string>("citedIn.Guid"),
                        MentionsByCategory = Return.As<IEnumerable<Category>>("collect(mention)"),
                        MentionsByCategoryGuids = Return.As<IEnumerable<string>>("collect(mentions.Guid)")
                    };
        }
        private static Expression<Func<ICypherResultItem, ICypherResultItem, ICypherResultItem, ICypherResultItem, PlaceTextHyperNodeTemp>> AsPlaceTextHyperNodeTemp()
        {
            return (ICypherResultItem text,
                    ICypherResultItem author,
                    ICypherResultItem subject,
                    ICypherResultItem citation
                    ) => new PlaceTextHyperNodeTemp
                    {
                        Text = text.As<Text>(),
                        Author = author.As<Agent>(),
                        Subject = subject.As<Place>(),
                        Citation = citation.As<Artefact>(),
                        AuthorOfGuid = Return.As<string>("authorOf.Guid"),
                        PertainsToGuid = Return.As<string>("pertainsTo.Guid"),
                        CitedInGuid = Return.As<string>("citedIn.Guid"),
                        Mentions = Return.As<IEnumerable<Category>>("collect(mention)"),
                        MentionsGuids = Return.As<IEnumerable<string>>("collect(mentions.Guid)")
                    };
        }
        private class AgentTextHyperNodeTemp
        {
            public Text Text { get; set; }
            public IEnumerable<Agent> Authors { get; set; }
            public Agent Subject { get; set; }
            public Artefact Citation { get; set; }
            public IEnumerable<Category> Mentions { get; set; }
            public string AuthorOfGuid { get; set; }
            public string PertainsToGuid { get; set; }
            public string CitedInGuid { get; set; }
            public IEnumerable<string> MentionsGuids { get; set; }
        }
        private class ArtefactTextHyperNodeTemp
        {
            public Text Text { get; set; }
            public IEnumerable<Agent> Authors { get; set; }
            public IEnumerable<string> AuthorOfGuids { get; set; }
            public Artefact Subject { get; set; }
            public string AboutGuid { get; set; }
            public Artefact Citation { get; set; }
            public string CitedInGuid { get; set; }
            public IEnumerable<Category> MentionsByCategory { get; set; }
            public IEnumerable<string> MentionsByCategoryGuids { get; set; }
            public IEnumerable<Agent> MentionsByAgent { get; set; }
            public IEnumerable<string> MentionsByAgentGuids { get; set; }
            public IEnumerable<Artefact> MentionsByArtefact { get; set; }
            public IEnumerable<string> MentionsByArtefactGuids { get; set; }
            public IEnumerable<Place> MentionsByPlace { get; set; }
            public IEnumerable<string> MentionsByPlaceGuids { get; set; }
        }
        private class PlaceTextHyperNodeTemp
        {
            public Text Text { get; set; }
            public Agent Author { get; set; }
            public Place Subject { get; set; }
            public Artefact Citation { get; set; }
            public IEnumerable<Category> Mentions { get; set; }
            public string AuthorOfGuid { get; set; }
            public string PertainsToGuid { get; set; }
            public string CitedInGuid { get; set; }
            public IEnumerable<string> MentionsGuids { get; set; }
        }
        private int DeleteRelationship(string relationshipGuid, string relationType)
        {
            var deleted = Client.Cypher
                    .Match("(x)-[r:" + relationType + " { Guid: {relationshipGuid} }]->(y)")
                    .WithParams(new
                    {
                        relationshipGuid
                    })
                    .Delete("r")
                    .Return(r => Return.As<int>("count(r)"))
                    .Results
                    .FirstOrDefault();
            return deleted;
        }
        public void DeleteWrittenBy(string writtenByGuid)
        {
            DeleteRelationship(writtenByGuid, Label.Relationship.WrittenBy);
        }
        public string MentionsAgent(string textGuid, string agentGuid, string mentionsGuid = null)
        {
            var isNew = string.IsNullOrEmpty(mentionsGuid);
            if (false == isNew)
            {
                var deleted = DeleteRelationship(mentionsGuid, Label.Relationship.Mentions);
                if (deleted == 0)
                {
                    return mentionsGuid;
                }
            }
            else
            {
                mentionsGuid = Guid.NewGuid().ToString(); ;
            }
            Client.Cypher
                .Match(
                    "(text:" + Label.Entity.Text + " { Guid: {textGuid} })",
                    "(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })")
                .WithParams(new
                {
                    textGuid,
                    agentGuid
                })
                .CreateUnique("text-[:" + Label.Relationship.Mentions + " { Guid: {mentionsGuid} }]->agent")
                .WithParams(new
                {
                    mentionsGuid
                })
                .ExecuteWithoutResults();
            return mentionsGuid;
        }
        public string MentionsCategory(string textGuid, string categoryGuid, string mentionsGuid = null)
        {
            var isNew = string.IsNullOrEmpty(mentionsGuid);
            if (false == isNew)
            {
                var deleted = DeleteRelationship(mentionsGuid, Label.Relationship.Mentions);
                if (deleted == 0)
                {
                    return mentionsGuid;
                }
            }
            else
            {
                mentionsGuid = Guid.NewGuid().ToString(); ;
            }
            Client.Cypher
                .Match(
                    "(text:" + Label.Entity.Text + " { Guid: {textGuid} })",
                    "(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })")
                .WithParams(new
                {
                    textGuid,
                    categoryGuid
                })
                .CreateUnique("text-[:" + Label.Relationship.Mentions + " { Guid: {mentionsGuid} }]->category")
                .WithParams(new
                {
                    mentionsGuid
                })
                .ExecuteWithoutResults();
            return mentionsGuid;
        }
        public string WrittenBy(string textGuid, string authorGuid, string writtenByGuid = null)
        {
            var isNew = string.IsNullOrEmpty(writtenByGuid);
            if (false == isNew)
            {
                var deleted = DeleteRelationship(writtenByGuid, Label.Relationship.WrittenBy);
                if (deleted == 0)
                {
                    return writtenByGuid;
                }
            }
            else
            {
                writtenByGuid = Guid.NewGuid().ToString(); ;
            }
            Client.Cypher
                .Match(
                    "(text:" + Label.Entity.Text + " { Guid: {textGuid} })",
                    "(author:" + Label.Entity.Agent + " { Guid: {authorGuid} })")
                .WithParams(new
                {
                    textGuid,
                    authorGuid
                })
                .CreateUnique("text-[:" + Label.Relationship.WrittenBy + " { Guid: {writtenByGuid} }]->author")
                .WithParams(new
                {
                    writtenByGuid
                })
                .ExecuteWithoutResults();
            return writtenByGuid;
        }
    }
}
