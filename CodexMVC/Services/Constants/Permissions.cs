﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Common
{
    public static class Permissions
    {
        public const string CanAccessAdmin = "CanAccessAdmin";
        public const string ListAgents = "ListAgents";
        public const string EditAgent = "EditAgent";
        public const string EditPlace = "EditPlace";
        public const string DeletePlace = "DeletePlace";
        public const string DeleteAct = "DeleteAct";
        public const string AnyPublicAction = "AnyPublicAction";
        public const string ViewAgent = "ViewAgent";
        public const string ViewArtefact = "ViewArtefact";
        public const string AddArtefactImage = "AddArtefactImage";
        public const string EditArtefactImage = "EditArtefactImage";
        public const string AddArtefactRelation = "AddArtefactRelation";
        public const string EditArtefactRelation = "EditArtefactRelation";
        public const string DeleteArtefactRelation = "DeleteArtefactRelation";
        public const string AddContext = "AddContext";
        public const string EditContext = "EditContext";
        public const string DeleteContext = "DeleteContext";
        public const string DeleteArtefactImage = "DeleteArtefactImage";
        public const string ViewUser = "ViewUser";
        public const string ListUsers = "ListUsers";
        public const string AddRoleToUser = "AddRoleToUser";
        public const string ViewRole = "ViewRole";
        public const string ListRoles = "ListRoles";
        public const string EditRole = "EditRole";
        public const string EditUser = "EditUser";
        public const string DeletePermission = "DeletePermission";
        public const string DeleteUser = "DeleteUser";
        public const string ListPermissions = "ListPermissions";
        public const string EditPermission = "EditPermission";
        public const string ViewPermission = "ViewPermission";
        public const string ResetPassword = "ResetPassword";
        public const string ViewAuditTrail = "ViewAuditTrail";
        public const string AddPermissionToRole = "AddPermissionToRole";
        public const string RemovePermissionFromRole = "RemovePermissionFromRole";
    }
}
