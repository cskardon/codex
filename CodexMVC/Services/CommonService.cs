﻿using Data;
using Data.Entities;
using log4net;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Humanizer;
using System.Configuration;
using System.IO;

namespace Services
{
    public interface ICommonService : INeo4jClientService
    {
        Task<int> DeleteRelationshipBetweenAsync(string sourceGuid, string targetGuid, string relationType);
        Task<int> DeleteRelationshipAsync(string relationType, string relationshipGuid);
        Task AuditAsync(AuditAction action, string destType, string destGuid, string description = null, string userGuid = null, string IPAddress = null);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelations(int startYear, int endYear);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelations(int startYear, int endYear);
        List<NewAct> ConvertIncomingToActs(IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> attributes);
        List<NewAct> ConvertOutgoingToActs(IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> attributes);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelationsByArtefactGuid(string artefactGuid);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelationsByPlaceGuid(string placeGuid);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelationsByAgentTypeGuid(string agentTypeGuid);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelationsByAgentGuid(string agentGuid);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelations(string startYear);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelationsByAgentTypeGuid(string artefactGuid);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelationsByArtefactGuid(string artefactGuid);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelationsByPlaceGuid(string placeGuid);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelationsByAgentGuid(string agentGuid);
        IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelations(string startYear);
        GenericRelationNode<TSource, TDest> FindGenericRelationNode<TSource, TDest>(string agentGuid, string relationToDestinationType, string relationToDestinationGuid = null, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity;
        int DeleteRelationship(string relationType, string relationshipGuid);
        void CreateRelationship(string sourceType, string sourceGuid, string destType, string destGuid, string relationshipType, string relationshipGuid = null);
        Task CreateRelationshipAsync(string sourceType, string sourceGuid, string destType, string destGuid, string relationshipType, string relationshipGuid = null);
        void SaveOrUpdate(Relation relation);
        T SaveOrUpdate<T>(T entity, string entityType, bool audit = true) where T : class, IIdentifiable;
        Task<T> SaveOrUpdateAsync<T>(T entity, string entityType, bool audit = true) where T : class, IIdentifiable;
        GenericRelationHyperNode<TSource, TDest> FindGenericRelation<TSource, TDest>(string agentGuid, string relationToDestinationType, string hasRelationGuid = null, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity;

        void SaveOrUpdateRelation<TSource, TDest>(GenericRelationHyperNode<TSource, TDest> node, string relationToDestinationType, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity;

        void SaveOrUpdateRelation<TSource, TDest>(GenericRelationNode<TSource, TDest> node, string relationToDestinationType, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity;

        IEnumerable<GenericRelationNode<TSource, IEntity>> FindBasicOutgoingRelations<TSource>(string sourceGuid) where TSource : class, IEntity;
        IEnumerable<GenericRelationHyperNode<TSource, IEntity>> FindOutgoingRelations<TSource>(string sourceGuid) where TSource : class, IEntity;

        IEnumerable<GenericRelationNode<IEntity, TDest>> FindBasicIncomingRelations<TDest>(string destGuid) where TDest : class, IEntity;
        IEnumerable<GenericRelationHyperNode<IEntity, TDest>> FindIncomingRelations<TDest>(string destGuid) where TDest : class, IEntity;

        void DeleteRelationHyperNode(string relationGuid);
        void Audit(AuditAction action, string destType, string destGuid, string description = null, string userGuid = null, string IPAddress = null);
        void AuditLogin(string userGuid, string IPAddress);
        void AuditLogout(string userGuid = null);
        IEnumerable<AuditEntityTuple> GetAuditTrail(string userGuid);
        void log4net(log4net.Core.LoggingEvent loggingEvent);
        Video FindVideoByGuid(string guid);
        IEnumerable<Video> GetVideos();

    }
    public class VideoComparer : EqualityComparer<Video>
    {
        public override bool Equals(Video x, Video y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(Video obj)
        {
            return base.GetHashCode();

        }
    }
    public class TagValueItemNodeComparer : EqualityComparer<TagValueItemNode>
    {
        public override bool Equals(TagValueItemNode x, TagValueItemNode y)
        {
            return x.Tag.Guid == y.Tag.Guid;
        }

        public override int GetHashCode(TagValueItemNode obj)
        {
            return base.GetHashCode();

        }
    }
    public class ValueItemComparer : EqualityComparer<ValueItem>
    {
        public override bool Equals(ValueItem x, ValueItem y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(ValueItem obj)
        {
            return base.GetHashCode();

        }
    }
    public class TagComparer : EqualityComparer<Tag>
    {
        public override bool Equals(Tag x, Tag y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(Tag obj)
        {
            return base.GetHashCode();

        }
    }
    public class ArtefactComparer : EqualityComparer<Artefact>
    {
        public override bool Equals(Artefact x, Artefact y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(Artefact obj)
        {
            return base.GetHashCode();

        }
    }
    public class PlaceComparer : EqualityComparer<Place>
    {
        public override bool Equals(Place x, Place y)
        {
            return x.Guid == y.Guid;
        }

        public override int GetHashCode(Place obj)
        {
            return base.GetHashCode();

        }
    }
    public class TagHyperNodeComparer : EqualityComparer<TagHyperNode>
    {
        public override bool Equals(TagHyperNode x, TagHyperNode y)
        {
            return x.Tag.Guid == y.Tag.Guid;
        }

        public override int GetHashCode(TagHyperNode obj)
        {
            return base.GetHashCode();

        }
    }
    public class NewActComparer : EqualityComparer<NewAct>
    {
        public override bool Equals(NewAct x, NewAct y)
        {
            return x.Act.Guid == y.Act.Guid;
        }

        public override int GetHashCode(NewAct obj)
        {
            return base.GetHashCode();

        }
    }
    public class AuditEntityTuple
    {
        public Audit Audit { get; set; }
        public IEntity Entity { get; set; }
    }
    public class Neo4jClientLog : IIdentifiable
    {
        public string Guid { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
        public DateTimeOffset Created { get; set; }
    }

    public class CommonService : ICommonService
    {
        private readonly ILog Log = LogManager.GetLogger("DefaultLogger");
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly IRelationService relationService;
        public CommonService(
            GraphClient client,
            IRelationService _relationService

            )
        {
            Client = client;
            relationService = _relationService;
        }

        public IEnumerable<Video> GetVideos()
        {
            var query = Client.Cypher
                .Match("(video:" + Label.Entity.Video + ")")
                ;

            var result = query
                .Return(() => Return.As<Video>("video"))
                .Results;

            return result;
        }

        public Video FindVideoByGuid(string videoGuid)
        {
            var query = Client.Cypher
                .Match("(video:" + Label.Entity.Video + " { Guid: {videoGuid} })").WithParams(new { videoGuid })
                ;

            var result = query
                .Return(() => Return.As<Video>("video"))
                .Results;

            return result.FirstOrDefault();
        }

        public void log4net(log4net.Core.LoggingEvent loggingEvent)
        {
            var log = new Neo4jClientLog
            {
                Guid = Guid.NewGuid().ToString(),
                Created = DateTimeOffset.Now,
                Level = loggingEvent.Level.Name,
                Message = loggingEvent.RenderedMessage,
                Exception = loggingEvent.GetExceptionString()
            };
            try
            {
                Client.Cypher
                    .Merge("(log:" + Label.Entity.ErrorLog + " { Guid: {guid} })")
                    .OnCreate().Set("log = {log}")
                    .OnMatch().Set("log = {log}")
                    .WithParams(new
                    {
                        guid = log.Guid,
                        log
                    })
                    .ExecuteWithoutResults();
            }
            catch (Exception ex)
            {

            }
        }

        public IEnumerable<AuditEntityTuple> GetAuditTrail(string userGuid)
        {
            var query = Client.Cypher
                .Match("(user:User { Guid: {userGuid} })").WithParams(new { userGuid })
                .Match("(user)-[:AUDIT_FROM]->(audit:Audit)")
                .OptionalMatch("(audit)-[:AUDIT_TO]->(entity)")
                ;
            var results = query
                .Return(() => new
                {
                    Audit = Return.As<Audit>("audit"),
                    EntityNode = Return.As<Node<string>>("entity"),
                    EntityType = Return.As<string>("head(labels(entity))")
                })
                .Results
                .Select(x => new AuditEntityTuple
                {
                    Audit = x.Audit,
                    Entity = Helpers.Common.ToEntity(x.EntityType, x.EntityNode)
                })
                .ToList();
            return results;
        }

        private void AuditSingle(AuditAction action, string userGuid = null, string IPAddress = null)
        {
            var principal = System.Security.Claims.ClaimsPrincipal.Current as CustomPrincipal;
            if (userGuid == null)
            {
                userGuid = principal.Identity.Name;
            }
            if (IPAddress == null)
            {
                IPAddress = principal.IPAddress;
            }
            var date = DateTimeOffset.Now;
            var audit = new Audit
            {
                Guid = Guid.NewGuid().ToString(),
                IPAddress = IPAddress,
                Action = action,
                Date = date
            };
            /**
             * Create the Audit node.
             */
            Client.Cypher
                .Merge("(audit:" + Label.Entity.Audit + " { Guid: {guid} })")
                .OnCreate().Set("audit = {audit}")
                .WithParams(new
                {
                    guid = audit.Guid,
                    audit
                })
                .ExecuteWithoutResults();
            /**
             * Relate the User and the Audit nodes.
             */
            var auditFromGuid = Guid.NewGuid().ToString();
            Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
                .Match("(audit:" + Label.Entity.Audit + " { Guid: {auditGuid} })")
                .WithParams(new
                {
                    userGuid,
                    auditGuid = audit.Guid
                })
                .CreateUnique("user-[:" + Label.Relationship.AuditFrom + " { Guid: {auditFromGuid} }]->audit")
                .WithParams(new
                {
                    auditFromGuid
                })
                .ExecuteWithoutResults();
        }
        public void AuditLogin(string userGuid, string IPAddress)
        {
            AuditSingle(AuditAction.LogIn, userGuid, IPAddress);
        }
        public void AuditLogout(string userGuid = null)
        {
            AuditSingle(AuditAction.LogOut, userGuid);
        }

        public async Task AuditAsync(AuditAction action, string destType, string destGuid, string description = null, string userGuid = null, string IPAddress = null)
        {
            if (action == AuditAction.LogIn || action == AuditAction.LogOut)
            {
                return;
            }
            var principal = System.Security.Claims.ClaimsPrincipal.Current as CustomPrincipal;
            if (userGuid == null)
            {
                if (principal == null)
                {
                    return;
                }
                userGuid = principal.Identity.Name;
            }
            if (IPAddress == null)
            {
                IPAddress = principal.IPAddress;
            }
            var actualDestType = destType == typeof(Entity).Name ? "" : ":" + destType;
            var date = DateTimeOffset.Now;
            var audit = new Audit
            {
                Guid = Guid.NewGuid().ToString(),
                IPAddress = IPAddress,
                Action = action,
                Description = description,
                Date = date
            };
            /**
             * Create the Audit node.
             */
            await Client.Cypher
                .Merge("(audit:" + Label.Entity.Audit + " { Guid: {guid} })")
                .OnCreate().Set("audit = {audit}")
                .WithParams(new
                {
                    guid = audit.Guid,
                    audit
                })
                .ExecuteWithoutResultsAsync();
            /**
             * Relate the User and the Audit nodes.
             */
            var auditFromGuid = Guid.NewGuid().ToString();
            await Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
                .Match("(audit:" + Label.Entity.Audit + " { Guid: {auditGuid} })")
                .WithParams(new
                {
                    userGuid,
                    auditGuid = audit.Guid
                })
                .CreateUnique("user-[:" + Label.Relationship.AuditFrom + " { Guid: {auditFromGuid} }]->audit")
                .WithParams(new
                {
                    auditFromGuid
                })
                .ExecuteWithoutResultsAsync();
            if (action != AuditAction.LogIn && action != AuditAction.LogOut)
            {
                /**
                 * Relate the Audit and the Destination nodes.
                 */
                var auditToGuid = Guid.NewGuid().ToString();
                await Client.Cypher
                    .Match("(audit:" + Label.Entity.Audit + " { Guid: {auditGuid} })")
                    .Match("(dest" + actualDestType + " { Guid: {destGuid} })")
                    .WithParams(new
                    {
                        auditGuid = audit.Guid,
                        destGuid
                    })
                    .CreateUnique("audit-[:" + Label.Relationship.AuditTo + " { Guid: {auditToGuid} }]->dest")
                    .WithParams(new
                    {
                        auditToGuid
                    })
                    .ExecuteWithoutResultsAsync();
            }
        }

        public void Audit(AuditAction action, string destType, string destGuid, string description = null, string userGuid = null, string IPAddress = null)
        {
            if (action == AuditAction.LogIn || action == AuditAction.LogOut)
            {
                return;
            }
            var principal = System.Security.Claims.ClaimsPrincipal.Current as CustomPrincipal;
            if (userGuid == null)
            {
                if (principal == null)
                {
                    return;
                }
                userGuid = principal.Identity.Name;
            }
            if (IPAddress == null)
            {
                IPAddress = principal.IPAddress;
            }
            var actualDestType = destType == typeof(Entity).Name ? "" : ":" + destType;
            var date = DateTimeOffset.Now;
            var audit = new Audit
            {
                Guid = Guid.NewGuid().ToString(),
                IPAddress = IPAddress,
                Action = action,
                Description = description,
                Date = date
            };
            /**
             * Create the Audit node.
             */
            Client.Cypher
                .Merge("(audit:" + Label.Entity.Audit + " { Guid: {guid} })")
                .OnCreate().Set("audit = {audit}")
                .WithParams(new
                {
                    guid = audit.Guid,
                    audit
                })
                .ExecuteWithoutResults();
            /**
             * Relate the User and the Audit nodes.
             */
            var auditFromGuid = Guid.NewGuid().ToString();
            Client.Cypher
                .Match("(user:" + Label.Entity.User + " { Guid: {userGuid} })")
                .Match("(audit:" + Label.Entity.Audit + " { Guid: {auditGuid} })")
                .WithParams(new
                {
                    userGuid,
                    auditGuid = audit.Guid
                })
                .CreateUnique("user-[:" + Label.Relationship.AuditFrom + " { Guid: {auditFromGuid} }]->audit")
                .WithParams(new
                {
                    auditFromGuid
                })
                .ExecuteWithoutResults();
            if (action != AuditAction.LogIn && action != AuditAction.LogOut)
            {
                /**
                 * Relate the Audit and the Destination nodes.
                 */
                var auditToGuid = Guid.NewGuid().ToString();
                Client.Cypher
                    .Match("(audit:" + Label.Entity.Audit + " { Guid: {auditGuid} })")
                    .Match("(dest" + actualDestType + " { Guid: {destGuid} })")
                    .WithParams(new
                    {
                        auditGuid = audit.Guid,
                        destGuid
                    })
                    .CreateUnique("audit-[:" + Label.Relationship.AuditTo + " { Guid: {auditToGuid} }]->dest")
                    .WithParams(new
                    {
                        auditToGuid
                    })
                    .ExecuteWithoutResults();
            }
        }
        public void SaveOrUpdateRelation<TSource, TDest>(GenericRelationNode<TSource, TDest> node, string relationToDestinationType, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            var sourceType = GetNodeLabel<TSource>();
            var destType = GetNodeLabel<TDest>();
            var isNew = node.RelationToDestinationGuid == null;
            if (false == isNew)
            {
                relationService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
            }
            if (false == reverse)
            {
                relationService.CreateRelationship(sourceType, node.Source.Guid, destType, node.Destination.Guid, relationToDestinationType);
            }
            else
            {
                relationService.CreateRelationship(destType, node.Destination.Guid, sourceType, node.Source.Guid, relationToDestinationType);
            }

        }

        public void DeleteRelationHyperNode(string relationGuid)
        {
            Client.Cypher
                .Match("(relation:" + Label.Entity.Relation + " { Guid: {relationGuid} })").WithParams(new { relationGuid })
                .Match("(source)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation)")
                .Match("(relation)-[r]->(entity)")
                .Delete("r, hasRelation, relation")
                .ExecuteWithoutResults();
        }

        public void SaveOrUpdateRelation<TSource, TDest>(GenericRelationHyperNode<TSource, TDest> node, string relationToDestinationType, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            if (node.Relation == null)
            {
                node.Relation = new Relation();
            }
            var isNew = node.Relation.Guid == null;
            var sourceType = GetNodeLabel<TSource>();
            var destType = GetNodeLabel<TDest>();
            relationService.SaveOrUpdate(node.Relation);
            if (node.HasRelationGuid == null)
            {
                if (false == reverse)
                {
                    relationService.CreateRelationship(sourceType, node.Source.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
                else
                {
                    relationService.CreateRelationship(destType, node.Destination.Guid, Label.Entity.Relation, node.Relation.Guid, Label.Relationship.HasRelation);
                }
            }
            if (node.RelationToDestinationGuid == null)
            {
                if (node.Destination.Guid != null)
                {
                    if (false == reverse)
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, destType, node.Destination.Guid, relationToDestinationType);
                    }
                    else
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, sourceType, node.Source.Guid, relationToDestinationType);
                    }
                }
            }
            else
            {
                relationService.DeleteRelationship(relationToDestinationType, node.RelationToDestinationGuid);
                if (false == reverse)
                {
                    if (node.Destination.Guid != null)
                    {
                        relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, destType, node.Destination.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                    }
                }
                else
                {
                    relationService.CreateRelationship(Label.Entity.Relation, node.Relation.Guid, sourceType, node.Source.Guid, relationToDestinationType, node.RelationToDestinationGuid);
                }
            }
            UpdateReferrers(node, isNew);
        }
        public void UpdateReferrers<TDest, TSource>(GenericRelationHyperNode<TDest, TSource> node, bool isNew)
            where TDest : IEntity
            where TSource : IEntity
        {
            if (false == isNew)
            {
                DeleteRelationRefersTo(node.Relation.Guid);
            }
            if (node.References != null)
            {
                foreach (var tuple in node.References)
                {
                    var type = tuple[0];
                    var guid = tuple[1];
                    CreateRelationship(Label.Entity.Relation, node.Relation.Guid, type, guid, Label.Relationship.RefersTo);
                }
            }
        }
        public void DeleteRelationRefersTo(string relationGuid)
        {
            DeleteEntityRefersTo(relationGuid, Label.Entity.Relation);
        }
        public void DeleteEntityRefersTo(string entityGuid, string entityType)
        {
            Client.Cypher
                .Match("(entity:" + entityType + " { Guid: {entityGuid} })-[r:" + Label.Relationship.RefersTo + "]->()")
                .WithParams(new
                {
                    entityGuid
                })
                .Delete("r")
                .ExecuteWithoutResults();
        }
        /// <summary>
        /// Although the label and the type names are currently equivalent this could be rewritten
        /// to check the type and return the label constant explicitly.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public string GetNodeLabel<TEntity>() where TEntity : class, IEntity
        {
            return typeof(TEntity).UnderlyingSystemType.Name;
        }
        public IEnumerable<GenericRelationNode<IEntity, TDest>> FindBasicIncomingRelations<TDest>(string destGuid) where TDest : class, IEntity
        {
            var destType = GetNodeLabel<TDest>();
            var query = Client.Cypher
                .Match("(destination:" + destType + " { Guid: {destGuid} })<-[toDest]-(source)").WithParams(new { destGuid })
                .Where("type(toDest) <> 'HAS_RELATION'");
            var result = query
                .Return(() => new
                {
                    SourceNode = Return.As<Node<string>>("source"),
                    SourceType = Return.As<string>("head(labels(source))"),
                    Destination = Return.As<TDest>("destination"),
                    RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                    RelationToDestinationType = Return.As<string>("type(toDest)")
                })
                .Results
                .Select(x => new GenericRelationNode<IEntity, TDest>
                {
                    Source = Helpers.Common.ToEntity(x.SourceType, x.SourceNode),
                    DestinationType = destType,
                    Destination = x.Destination,
                    RelationToDestinationGuid = x.RelationToDestinationGuid,
                    RelationToDestinationType = x.RelationToDestinationType
                })
                .ToList();
            return result;
        }
        public IEnumerable<GenericRelationHyperNode<IEntity, TDest>> FindIncomingRelations<TDest>(string destGuid) where TDest : class, IEntity
        {
            var destType = GetNodeLabel<TDest>();
            var query = Client.Cypher
                .Match("(destination:" + destType + " { Guid: {destGuid} })").WithParams(new { destGuid })
                .Match("(relation:" + Label.Entity.Relation + ")-[toDest]->(destination)")
                .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
                .With("destination, toDest, relation")
                .Match("(relation)<-[hasRelation:" + Label.Relationship.HasRelation + "]-(source)")
                .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .OptionalMatch("(destination)-[:" + Label.Relationship.HasRelation + "]->()-[:IS_A]->(destinationAttribute:Category)")
                .OptionalMatch("(source)-[:" + Label.Relationship.HasRelation + "]->()-[:IS_A]->(sourceAttribute:Category)")
                .OptionalMatch("(source)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(source)-[:HAS_RELATION]->(sourceBirth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(source)-[:HAS_RELATION]->(sourceDeath:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(destination)-[:HAS_RELATION]->(destinationBirth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(destination)-[:HAS_RELATION]->(destinationDeath:Relation)-[:DIED_AT]->(:Place)")
                ;
            var result = query
                .Return(() => new
                {
                    SourceNode = Return.As<Node<string>>("source"),
                    SourceType = Return.As<string>("head(labels(source))"),
                    SourceBirth = Return.As<Relation>("sourceBirth"),
                    DestinationBirth = Return.As<Relation>("destinationBirth"),
                    SourceDeath = Return.As<Relation>("sourceDeath"),
                    DestinationDeath = Return.As<Relation>("destinationDeath"),
                    HasRelationGuid = Return.As<string>("hasRelation.Guid"),
                    Relation = Return.As<Relation>("relation"),
                    Destination = Return.As<TDest>("destination"),
                    RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                    RelationToDestinationType = Return.As<string>("type(toDest)"),
                    Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })"),
                    SourceAttributes = Return.As<IEnumerable<Category>>("collect(distinct(sourceAttribute))"),
                    DestinationAttributes = Return.As<IEnumerable<Category>>("collect(distinct(destinationAttribute))"),
                    EntityImages = Return.As<IEnumerable<EntityNodeWithImage>>("collect(distinct { Entity: source, Type: head(labels(source)), Image: image })")
                })
                .Results
                .Select(x => new GenericRelationHyperNode<IEntity, TDest>
                {
                    Source = Helpers.Common.ToEntity(x.SourceType, x.SourceNode),
                    SourceBirth = x.SourceBirth,
                    SourceDeath = x.SourceDeath,
                    DestinationBirth = x.DestinationBirth,
                    DestinationDeath = x.DestinationDeath,
                    HasRelationGuid = x.HasRelationGuid,
                    Relation = x.Relation,
                    DestinationType = destType,
                    Destination = x.Destination,
                    RelationToDestinationGuid = x.RelationToDestinationGuid,
                    RelationToDestinationType = x.RelationToDestinationType,
                    Entities = x.Entities.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity)).ToList(),
                    EntityImages = x.EntityImages.Select(y => new EntityImage { Birth = y.Birth, Death = y.Death, Image = y.Image, Entity = Helpers.Common.ToEntity(y.Type, y.Entity) }),
                    SourceAttributes = x.SourceAttributes,
                    DestinationAttributes = x.DestinationAttributes
                })
                .ToList();
            return result;
        }
        public IEnumerable<GenericRelationNode<TSource, IEntity>> FindBasicOutgoingRelations<TSource>(string sourceGuid) where TSource : class, IEntity
        {
            var sourceType = GetNodeLabel<TSource>();
            var query = Client.Cypher
                .Match("(source:" + sourceType + " { Guid: {sourceGuid} })-[toDest]->(destination)").WithParams(new { sourceGuid })
                .Where("type(toDest) <> 'HAS_RELATION'");
            var result = query.Return(() => new
            {
                Source = Return.As<TSource>("source"),
                DestinationNode = Return.As<Node<string>>("destination"),
                DestinationType = Return.As<string>("head(labels(destination))"),
                RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                RelationToDestinationType = Return.As<string>("type(toDest)")
            })
               .Results
               .Select(x => new GenericRelationNode<TSource, IEntity>
               {
                   Source = x.Source,
                   DestinationType = x.DestinationType,
                   Destination = Helpers.Common.ToEntity(x.DestinationType, x.DestinationNode),
                   RelationToDestinationGuid = x.RelationToDestinationGuid,
                   RelationToDestinationType = x.RelationToDestinationType
               })
               .ToList();
            return result;
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelationsByArtefactGuid(string artefactGuid)
        {
            var selector = Client.Cypher
                .Match("(source:Artefact { Guid: { artefactGuid } })").WithParams(new { artefactGuid })
                .Match("(source)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                ;

            return FindOutgoingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelationsByPlaceGuid(string placeGuid)
        {
            var selector = Client.Cypher
                .Match("(source:Place { Guid: { placeGuid } })").WithParams(new { placeGuid })
                .Match("(source)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                ;

            return FindOutgoingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelationsByAgentTypeGuid(string agentTypeGuid)
        {
            var selector = Client.Cypher
                .Match("(agentType:Category { Guid: {agentTypeGuid} })").WithParams(new { agentTypeGuid })
                .Match("(agentTypes:Category)-[:KIND_OF*0..]->(agentType)")
                .Match("(source:Agent)-[:HAS_RELATION]->(:Relation)-[:IS_A]->(agentTypes)")
                .Match("(source)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                ;

            return FindOutgoingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelationsByAgentGuid(string agentGuid)
        {
            var selector = Client.Cypher
                .Match("(source:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(source)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                ;

            return FindOutgoingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelations(string startYear)
        {
            var selector = Client.Cypher
                .Match("(source:Agent)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                .Where("relation.Year = {startYear}").WithParams(new { startYear = int.Parse(startYear) })
                ;

            return FindOutgoingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelations(int startYear, int endYear)
        {
            var selector = Client.Cypher
                .Match("(source:Agent)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                .Where("relation.Year >= {startYear} and relation.Year <= {endYear}").WithParams(new { startYear, endYear })
                ;

            return FindOutgoingRelations(selector);
        }

        private IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindOutgoingRelations(ICypherFluentQuery selector)
        {
            var query = selector
                .Match("(relation)-[toDest]->(destination)")
                .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
                .With("source, relation, destination, hasRelation, toDest")
                .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .OptionalMatch("(entity)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(destination)-[:" + Label.Relationship.HasRelation + "]->()-[:IS_A]->(destinationAttribute:Category)")
                .OptionalMatch("(source)-[:" + Label.Relationship.HasRelation + "]->()-[:IS_A]->(sourceAttribute:Category)")
                .OptionalMatch("(source)-[:HAS_RELATION]->(sourceBirth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(source)-[:HAS_RELATION]->(sourceDeath:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(destination)-[:HAS_RELATION]->(destinationBirth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(destination)-[:HAS_RELATION]->(destinationDeath:Relation)-[:DIED_AT]->(:Place)")
                ;
            ;
            var result = query.Return(() => new
            {
                SourceNode = Return.As<Node<string>>("source"),
                SourceType = Return.As<string>("head(labels(source))"),
                SourceBirth = Return.As<Relation>("sourceBirth"),
                DestinationBirth = Return.As<Relation>("destinationBirth"),
                SourceDeath = Return.As<Relation>("sourceDeath"),
                DestinationDeath = Return.As<Relation>("destinationDeath"),
                HasRelationGuid = Return.As<string>("hasRelation.Guid"),
                Relation = Return.As<Relation>("relation"),
                DestinationNode = Return.As<Node<string>>("destination"),
                DestinationType = Return.As<string>("head(labels(destination))"),
                RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                RelationToDestinationType = Return.As<string>("type(toDest)"),
                Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })"),
                SourceAttributes = Return.As<IEnumerable<Category>>("collect(distinct(sourceAttribute))"),
                DestinationAttributes = Return.As<IEnumerable<Category>>("collect(distinct(destinationAttribute))")
            })
               .Results
               .Select(x => new GenericRelationHyperNode<IEntity, IEntity>
               {
                   Source = Helpers.Common.ToEntity(x.SourceType, x.SourceNode),
                   SourceBirth = x.SourceBirth,
                   SourceDeath = x.SourceDeath,
                   DestinationBirth = x.DestinationBirth,
                   DestinationDeath = x.DestinationDeath,
                   HasRelationGuid = x.HasRelationGuid,
                   Relation = x.Relation,
                   DestinationType = x.DestinationType,
                   Destination = Helpers.Common.ToEntity(x.DestinationType, x.DestinationNode),
                   RelationToDestinationGuid = x.RelationToDestinationGuid,
                   RelationToDestinationType = x.RelationToDestinationType,
                   Entities = x.Entities.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity)).ToList(),
                   SourceAttributes = x.SourceAttributes,
                   DestinationAttributes = x.DestinationAttributes
               })
               .ToList();

            return result;

        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelations(int startYear, int endYear)
        {
            var selector = Client.Cypher
                .Match("(relation:" + Label.Entity.Relation + ")-[toDest]->(destination)")
                .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
                .AndWhere("relation.Year >= {startYear} and relation.Year <= {endYear}").WithParams(new { startYear, endYear })
                ;

            return FindIncomingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelationsByArtefactGuid(string artefactGuid)
        {
            var selector = Client.Cypher
                            .Match("(destination:Artefact { Guid: {artefactGuid} })").WithParams(new { artefactGuid })
                            .Match("(relation:" + Label.Entity.Relation + ")-[toDest]->(destination)")
                            .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
                            ;

            return FindIncomingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelationsByPlaceGuid(string placeGuid)
        {
            var selector = Client.Cypher
                .Match("(destination:Place { Guid: {placeGuid} })").WithParams(new { placeGuid })
                .Match("(relation:" + Label.Entity.Relation + ")-[toDest]->(destination)")
                .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
                ;

            return FindIncomingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelationsByAgentTypeGuid(string agentTypeGuid)
        {
            var selector = Client.Cypher
               .Match("(agentType:Category { Guid: {agentTypeGuid} })").WithParams(new { agentTypeGuid })
               .Match("(agentTypes:Category)-[:KIND_OF*0..]->(agentType)")
               .Match("(destination:Agent)-[:HAS_RELATION]->(:Relation)-[:IS_A]->(agentTypes)")
               .Match("(relation:" + Label.Entity.Relation + ")-[toDest]->(destination)")
               .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
               ;

            return FindIncomingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelationsByAgentGuid(string agentGuid)
        {
            var selector = Client.Cypher
                .Match("(destination:Agent { Guid: {agentGuid} })").WithParams(new { agentGuid })
                .Match("(relation:" + Label.Entity.Relation + ")-[toDest]->(destination)")
                .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
                ;

            return FindIncomingRelations(selector);
        }

        public IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelations(string startYear)
        {
            var selector = Client.Cypher
                .Match("(relation:" + Label.Entity.Relation + ")-[toDest]->(destination)")
                .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
                .AndWhere("relation.Year = {startYear}").WithParams(new { startYear = int.Parse(startYear) })
                ;

            return FindIncomingRelations(selector);
        }

        private IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> FindIncomingRelations(ICypherFluentQuery selector)
        {
            var query = selector
                .Match("(source)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                .With("source, relation, destination, hasRelation, toDest")
                .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .OptionalMatch("(destination)-[:" + Label.Relationship.HasRelation + "]->()-[:IS_A]->(destinationAttribute:Category)")
                .OptionalMatch("(source)-[:" + Label.Relationship.HasRelation + "]->()-[:IS_A]->(sourceAttribute:Category)")
                .OptionalMatch("(source)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(source)-[:HAS_RELATION]->(sourceBirth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(source)-[:HAS_RELATION]->(sourceDeath:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(destination)-[:HAS_RELATION]->(destinationBirth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(destination)-[:HAS_RELATION]->(destinationDeath:Relation)-[:DIED_AT]->(:Place)")
                ;
            ;
            var result = query.Return(() => new
            {
                SourceNode = Return.As<Node<string>>("source"),
                SourceType = Return.As<string>("head(labels(source))"),
                SourceBirth = Return.As<Relation>("sourceBirth"),
                DestinationBirth = Return.As<Relation>("destinationBirth"),
                SourceDeath = Return.As<Relation>("sourceDeath"),
                DestinationDeath = Return.As<Relation>("destinationDeath"),
                HasRelationGuid = Return.As<string>("hasRelation.Guid"),
                Relation = Return.As<Relation>("relation"),
                DestinationNode = Return.As<Node<string>>("destination"),
                DestinationType = Return.As<string>("head(labels(destination))"),
                RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                RelationToDestinationType = Return.As<string>("type(toDest)"),
                Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })"),
                SourceAttributes = Return.As<IEnumerable<Category>>("collect(distinct(sourceAttribute))"),
                DestinationAttributes = Return.As<IEnumerable<Category>>("collect(distinct(destinationAttribute))"),
                EntityImages = Return.As<IEnumerable<EntityNodeWithImage>>("collect({ Entity: source, Type: head(labels(source)), Image: image })")
            })
               .Results
               .Select(x => new GenericRelationHyperNode<IEntity, IEntity>
               {
                   Source = Helpers.Common.ToEntity(x.SourceType, x.SourceNode),
                   SourceBirth = x.SourceBirth,
                   SourceDeath = x.SourceDeath,
                   DestinationBirth = x.DestinationBirth,
                   DestinationDeath = x.DestinationDeath,
                   HasRelationGuid = x.HasRelationGuid,
                   Relation = x.Relation,
                   DestinationType = x.DestinationType,
                   Destination = Helpers.Common.ToEntity(x.DestinationType, x.DestinationNode),
                   RelationToDestinationGuid = x.RelationToDestinationGuid,
                   RelationToDestinationType = x.RelationToDestinationType,
                   Entities = x.Entities.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity)).ToList(),
                   EntityImages = x.EntityImages.Select(y => new EntityImage { Image = y.Image, Entity = Helpers.Common.ToEntity(y.Type, y.Entity) }),
                   SourceAttributes = x.SourceAttributes,
                   DestinationAttributes = x.DestinationAttributes
               })
               .ToList();

            return result;
        }

        private string NameOf(IEntity entity)
        {
            if (entity is Data.Agent)
            {
                return ((Data.Agent)entity).FullName2;
            }
            if (entity is Artefact)
            {
                return ((Artefact)entity).ToDisplay();
            }
            return entity.Name;
        }

        private Data.Entities.Range ToRange(string scope)
        {
            switch (scope)
            {
                case ">-<": return Data.Entities.Range.Between;
                case "-->": return Data.Entities.Range.Continuous;
                default: return Data.Entities.Range.Continuous;
            }
        }

        private string Range(Relation relation)
        {
            var diff = relation.YearEnd.Value - relation.Year.Value;
            if (diff == 0)
            {
                return "";
            }
            if (diff == 1)
            {
                return " for a year";
            }
            if (ToRange(relation.Range) == Data.Entities.Range.Continuous)
            {
                return string.Format(" over the course of {0} years until {1}", diff.ToWords(), relation.YearEnd.Value);
            }
            return string.Format(" some time between now and {0}", relation.YearEnd.Value);
        }

        private static string AdverbOutgoing(GenericRelationHyperNode<IEntity, IEntity> node)
        {
            if (node.RelationToDestinationType == Label.Relationship.MadeBy)
            {
                if (false == node.Relation.YearEnd.HasValue)
                {
                    return "creates";
                }
            }
            return Label.Relationship.ToPresentTense(node.RelationToDestinationType);
        }

        private string OutgoingText(GenericRelationHyperNode<IEntity, IEntity> node)
        {
            var attributes = "";
            if (node.DestinationAttributes != null && node.DestinationAttributes.Any())
            {
                var pronoun = node.Destination is Agent ? "called" : "of";
                attributes = " a " + string.Join(" and ", node.DestinationAttributes.Select(x => x.Name.ToLower())) + " " + pronoun + " ";
            }
            var sourceAge = "";
            if (node.SourceBirth != null && node.SourceBirth.Year.HasValue && node.SourceDeath != null && node.SourceDeath.Year.HasValue)
            {
                if (node.Relation.Year.HasValue)
                {
                    if (node.Relation.Year.Value > node.SourceBirth.Year.Value && node.Relation.Year.Value <= node.SourceDeath.Year.Value)
                    {
                        var age1 = node.Relation.Year.Value - node.SourceBirth.Year.Value;
                        // sourceAge = string.Format("(then aged {0}) ", age1);
                    }
                }
            }
            var destinationAge = "";
            if (node.DestinationBirth != null && node.DestinationBirth.Year.HasValue && node.DestinationDeath != null && node.DestinationDeath.Year.HasValue)
            {
                if (node.Relation.Year.HasValue)
                {
                    if (node.Relation.Year.Value > node.DestinationBirth.Year.Value && node.Relation.Year.Value <= node.DestinationDeath.Year.Value)
                    {
                        var age1 = node.Relation.Year.Value - node.DestinationBirth.Year.Value;
                        // destinationAge = string.Format("(then aged {0}) ", age1);
                    }
                }
            }
            var result = "<a href=\"/Time/ByAgent/" + node.Source.Guid + "\" target=\"_blank\">" + NameOf(node.Source) + "</a> " + sourceAge + AdverbOutgoing(node) + attributes + " <a href=\"/Time/By" + node.Destination.GetType().UnderlyingSystemType.Name + "/" + node.Destination.Guid + "\" target=\"_blank\">" + NameOf(node.Destination) + "</a>" + destinationAge + (node.Relation.YearEnd.HasValue ? Range(node.Relation) : "") + ".";
            return result;
        }

        public List<NewAct> ConvertIncomingToActs(IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> attributes)
        {
            var result = attributes
                .Where(x => x.Destination != null && x.Source != null)
                .Select(x => new NewAct
                {
                    Act = new Act
                    {
                        Guid = x.Relation.Guid,
                        Name = x.Relation.Name,
                        Description = IncomingText(x, x.EntityImages.FirstOrDefault(x2 => x2.Entity.Guid == x.Source.Guid)),
                        Proximity = x.Relation.Proximity,
                        Decade = x.Relation.Decade,
                        Milennium = x.Relation.Milennium,
                        Year = x.Relation.Year,
                        Month = x.Relation.Month,
                        Day = x.Relation.Day,
                        Hour = x.Relation.Hour,
                        Minute = x.Relation.Minute,
                        ProximityEnd = x.Relation.ProximityEnd,
                        DecadeEnd = x.Relation.DecadeEnd,
                        MilenniumEnd = x.Relation.MilenniumEnd,
                        YearEnd = x.Relation.YearEnd,
                        MonthEnd = x.Relation.MonthEnd,
                        DayEnd = x.Relation.DayEnd,
                        HourEnd = x.Relation.HourEnd,
                        MinuteEnd = x.Relation.MinuteEnd
                    },
                    EntityImages = x.EntityImages
                });
            return result.ToList();
        }

        private bool StartsWithVowel(string value)
        {
            var vowels = new string[] { "a", "e", "i", "o", "u" };
            return vowels.Contains(value.Substring(0, 1).ToLower());
        }

        private static string AdverbIncoming(GenericRelationHyperNode<IEntity, IEntity> node)
        {
            if (node.RelationToDestinationType == Label.Relationship.MadeBy)
            {
                if (false == node.Relation.YearEnd.HasValue)
                {
                    return "creates";
                }
            }
            return Label.Relationship.ToPresentTense(node.RelationToDestinationType, true);
        }

        private string IncomingText(GenericRelationHyperNode<IEntity, IEntity> node, EntityImage sourceEntityImage = null)
        {
            var attributes = "";
            if (node.SourceAttributes != null && node.SourceAttributes.Any())
            {
                //var pronoun = node.Source is Agent ? "called" : "of";
                var pronoun = "called";
                var qualifier = StartsWithVowel(node.SourceAttributes.First().Name) ? "an" : "a";
                attributes = " " + qualifier + " " + string.Join(" and ", node.SourceAttributes.Select(x => x.Name.ToLower())) + " " + pronoun + " ";
            }
            var sourceAge = "";
            if (node.SourceBirth != null && node.SourceBirth.Year.HasValue && node.SourceDeath != null && node.SourceDeath.Year.HasValue)
            {
                if (node.Relation.Year.HasValue)
                {
                    if (node.Relation.Year.Value > node.SourceBirth.Year.Value && node.Relation.Year.Value <= node.SourceDeath.Year.Value)
                    {
                        var age1 = node.Relation.Year.Value - node.SourceBirth.Year.Value;
                        // sourceAge = string.Format("(then aged {0}) ", age1);
                    }
                }
            }
            var destinationAge = "";
            if (node.DestinationBirth != null && node.DestinationBirth.Year.HasValue && node.DestinationDeath != null && node.DestinationDeath.Year.HasValue)
            {
                if (node.Relation.Year.HasValue)
                {
                    if (node.Relation.Year.Value > node.DestinationBirth.Year.Value && node.Relation.Year.Value <= node.DestinationDeath.Year.Value)
                    {
                        var age1 = node.Relation.Year.Value - node.DestinationBirth.Year.Value;
                        // destinationAge = string.Format("(then aged {0}) ", age1);
                    }
                }
            }
            var linkContent = "";
            if (sourceEntityImage.Image != null)
            {
                linkContent = "data-toggle='popover' data-title='" + NameOf(node.Source) + "' class='popover' data-image='" + sourceEntityImage.Image.VirtualPath().Replace("~", "") + "?mode=fit&height=300&width=300' ";
            }
            var result = "<a href=\"/Time/ByAgent/" + node.Destination.Guid + "\" target=\"_blank\">" + NameOf(node.Destination) + "</a> " + destinationAge + AdverbIncoming(node) + " " + attributes + "<a href=\"/Time/By" + node.Source.GetType().UnderlyingSystemType.Name + "/" + node.Source.Guid + "\" " + /*linkContent +*/ " target=\"_blank\">" + NameOf(node.Source) + "</a> " + sourceAge + (node.Relation.YearEnd.HasValue ? Range(node.Relation) : "") + ".";
            return result;
        }


        public List<NewAct> ConvertOutgoingToActs(IEnumerable<GenericRelationHyperNode<IEntity, IEntity>> attributes)
        {
            var result = attributes
                .Where(x => x.Destination != null && x.Source != null)
                .Select(x => new NewAct
                {
                    Act = new Act
                    {
                        Guid = x.Relation.Guid,
                        Name = x.Relation.Name,
                        Description = OutgoingText(x),
                        Proximity = x.Relation.Proximity,
                        Decade = x.Relation.Decade,
                        Milennium = x.Relation.Milennium,
                        Year = x.Relation.Year,
                        Month = x.Relation.Month,
                        Day = x.Relation.Day,
                        Hour = x.Relation.Hour,
                        Minute = x.Relation.Minute,
                        ProximityEnd = x.Relation.ProximityEnd,
                        DecadeEnd = x.Relation.DecadeEnd,
                        MilenniumEnd = x.Relation.MilenniumEnd,
                        YearEnd = x.Relation.YearEnd,
                        MonthEnd = x.Relation.MonthEnd,
                        DayEnd = x.Relation.DayEnd,
                        HourEnd = x.Relation.HourEnd,
                        MinuteEnd = x.Relation.MinuteEnd
                    },
                    EntityImages = new List<EntityImage> { new EntityImage { Entity = x.Destination } }
                });
            return result.ToList();
        }

        public IEnumerable<GenericRelationHyperNode<TSource, IEntity>> FindOutgoingRelations<TSource>(string sourceGuid)
            where TSource : class, IEntity
        {
            var sourceType = GetNodeLabel<TSource>();
            var query = Client.Cypher
                .Match("(source:" + sourceType + " { Guid: {sourceGuid} })").WithParams(new { sourceGuid })
                .Match("(source)-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                .Match("(relation)-[toDest]->(destination)")
                .Where("NOT ((relation)-[toDest:" + Label.Relationship.RefersTo + "]->(destination))")
                .With("source, relation, destination, hasRelation, toDest")
                .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                .OptionalMatch("(entity)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(destination)-[:" + Label.Relationship.HasRelation + "]->()-[:IS_A]->(destinationAttribute:Category)")
                .OptionalMatch("(source)-[:" + Label.Relationship.HasRelation + "]->()-[:IS_A]->(sourceAttribute:Category)")
                .OptionalMatch("(destination)-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Primary: true })")
                .OptionalMatch("(source)-[:HAS_RELATION]->(sourceBirth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(source)-[:HAS_RELATION]->(sourceDeath:Relation)-[:DIED_AT]->(:Place)")
                .OptionalMatch("(destination)-[:HAS_RELATION]->(destinationBirth:Relation)-[:BORN_AT]->(:Place)")
                .OptionalMatch("(destination)-[:HAS_RELATION]->(destinationDeath:Relation)-[:DIED_AT]->(:Place)")
                ;
            ;
            var result = query.Return(() => new
               {
                   Source = Return.As<TSource>("source"),
                   SourceBirth = Return.As<Relation>("sourceBirth"),
                   DestinationBirth = Return.As<Relation>("destinationBirth"),
                   SourceDeath = Return.As<Relation>("sourceDeath"),
                   DestinationDeath = Return.As<Relation>("destinationDeath"),
                   HasRelationGuid = Return.As<string>("hasRelation.Guid"),
                   Relation = Return.As<Relation>("relation"),
                   DestinationNode = Return.As<Node<string>>("destination"),
                   DestinationType = Return.As<string>("head(labels(destination))"),
                   RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                   RelationToDestinationType = Return.As<string>("type(toDest)"),
                   Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })"),
                   SourceAttributes = Return.As<IEnumerable<Category>>("collect(distinct(sourceAttribute))"),
                   DestinationAttributes = Return.As<IEnumerable<Category>>("collect(distinct(destinationAttribute))"),
                   EntityImages = Return.As<IEnumerable<EntityNodeWithImage>>("collect(distinct { Entity: destination, Type: head(labels(destination)), Image: image })")
               })
               .Results
               .Select(x => new GenericRelationHyperNode<TSource, IEntity>
               {
                   Source = x.Source,
                   SourceBirth = x.SourceBirth,
                   SourceDeath = x.SourceDeath,
                   DestinationBirth = x.DestinationBirth,
                   DestinationDeath = x.DestinationDeath,
                   HasRelationGuid = x.HasRelationGuid,
                   Relation = x.Relation,
                   DestinationType = x.DestinationType,
                   Destination = Helpers.Common.ToEntity(x.DestinationType, x.DestinationNode),
                   RelationToDestinationGuid = x.RelationToDestinationGuid,
                   RelationToDestinationType = x.RelationToDestinationType,
                   Entities = x.Entities.Select(x2 => Helpers.Common.ToEntity(x2.Type, x2.Entity)).ToList(),
                   EntityImages = x.EntityImages.Select(y => new EntityImage { Birth = y.Birth, Death = y.Death, Image = y.Image, Entity = Helpers.Common.ToEntity(y.Type, y.Entity) }),
                   SourceAttributes = x.SourceAttributes,
                   DestinationAttributes = x.DestinationAttributes
               })
               .ToList();
            return result;
        }

        public GenericRelationNode<TSource, TDest> FindGenericRelationNode<TSource, TDest>(string agentGuid, string relationToDestinationType, string relationToDestinationGuid = null, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            ICypherFluentQuery query;
            var sourceType = GetNodeLabel<TSource>();
            var destType = GetNodeLabel<TDest>();
            if (false == reverse)
            {
                if (relationToDestinationGuid != null)
                {
                    query = Client.Cypher
                           .Match("(source:" + sourceType + " { Guid: {agentGuid} })-[toDest:" + relationToDestinationType + " { Guid: {relationToDestinationGuid} }]->(destination:" + destType + ")")
                           .WithParams(new
                           {
                               agentGuid,
                               relationToDestinationGuid
                           });
                }
                else
                {
                    query = Client.Cypher
                           .Match("(source:" + sourceType + " { Guid: {agentGuid} })-[toDest:" + relationToDestinationType + "]->(destination:" + destType + ")")
                           .WithParams(new
                           {
                               agentGuid
                           });
                }
            }
            else
            {
                if (relationToDestinationGuid != null)
                {
                    query = Client.Cypher
                               .Match("(destination:" + destType + " { Guid: {agentGuid} })-[toDest:" + relationToDestinationType + " { Guid: {relationToDestinationGuid} }]->(source:" + sourceType + ")")
                               .WithParams(new
                               {
                                   agentGuid,
                                   relationToDestinationGuid
                               });
                }
                else
                {
                    query = Client.Cypher
                               .Match("(destination:" + destType + " { Guid: {agentGuid} })-[toDest:" + relationToDestinationType + "]->(source:" + sourceType + ")")
                               .WithParams(new
                               {
                                   agentGuid
                               });
                }
            }
            var result = query.Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new
            {
                Source = source.As<TSource>(),
                RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                RelationToDestinationType = Return.As<string>("type(toDest)"),
                Destination = destination.As<TDest>()
            })
                   .Results
                   .Select(x => new GenericRelationNode<TSource, TDest>
                   {
                       Source = x.Source,
                       RelationToDestinationGuid = x.RelationToDestinationGuid,
                       RelationToDestinationType = x.RelationToDestinationType,
                       Destination = x.Destination
                   })
                   .FirstOrDefault();
            return result;
        }
        public GenericRelationHyperNode<TSource, TDest> FindGenericRelation<TSource, TDest>(string agentGuid, string relationToDestinationType, string hasRelationGuid = null, bool reverse = false)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            ICypherFluentQuery query;
            var sourceType = GetNodeLabel<TSource>();
            var destType = GetNodeLabel<TDest>();
            if (false == reverse)
            {
                if (hasRelationGuid != null)
                {
                    query = Client.Cypher
                           .Match("(source:" + sourceType + " { Guid: {agentGuid} })-[hasRelation:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                           .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + destType + ")")
                           .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                           .WithParams(new
                           {
                               agentGuid,
                               hasRelationGuid
                           });
                }
                else
                {
                    query = Client.Cypher
                           .Match("(source:" + sourceType + " { Guid: {agentGuid} })-[hasRelation:" + Label.Relationship.HasRelation + "]->(relation:" + Label.Entity.Relation + ")")
                           .Match("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + destType + ")")
                           .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                           .WithParams(new
                           {
                               agentGuid
                           });
                }
            }
            else
            {
                // (SOURCE) leonardo -[STUDENT_OF]-> verrocchio (DEST)
                // (DEST) verrocchio <-[STUDENT_OF]- leonardo (SOURCE)
                query = Client.Cypher
                           .Match("(source:" + sourceType + " { Guid: {agentGuid} })-[hasRelation:" + Label.Relationship.HasRelation + " { Guid: {hasRelationGuid} }]->(relation:" + Label.Entity.Relation + ")")
                           .OptionalMatch("(relation)-[toDest:" + relationToDestinationType + "]->(destination:" + destType + ")")
                           .OptionalMatch("(relation)-[:" + Label.Relationship.RefersTo + "]->(entity)")
                           .WithParams(new
                           {
                               agentGuid,
                               hasRelationGuid
                           });
            }
            var result = query.Return((ICypherResultItem source, ICypherResultItem relation, ICypherResultItem destination) => new
            {
                Source = source.As<TSource>(),
                RelationToDestinationGuid = Return.As<string>("toDest.Guid"),
                Relation = relation.As<Relation>(),
                Destination = destination.As<TDest>(),
                HasRelationGuid = Return.As<string>("hasRelation.Guid"),
                Entities = Return.As<IEnumerable<EntityNode>>("collect({ Entity: entity, Type: head(labels(entity)) })")
            })
                   .Results
                   .Select(x => new GenericRelationHyperNode<TSource, TDest>
                   {
                       Source = x.Source,
                       RelationToDestinationGuid = x.RelationToDestinationGuid,
                       Relation = x.Relation,
                       HasRelationGuid = x.HasRelationGuid,
                       Destination = x.Destination,
                       Entities = x.Entities.Select(y => Helpers.Common.ToEntity(y.Type, y.Entity))
                   })
                   .FirstOrDefault();
            return result;
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }

        public async Task<T> SaveOrUpdateAsync<T>(T entity, string entityType, bool audit = true) where T : class, IIdentifiable
        {
            var creating = entity.Guid == null;
            if (creating)
            {
                entity.Guid = Guid.NewGuid().ToString();
            }

            await Client.Cypher
                .Merge("(entity:" + entityType + " { Guid: {guid} })")
                .OnCreate().Set("entity = {entity}")
                .OnMatch().Set("entity = {entity}")
                .WithParams(new
                {
                    guid = entity.Guid,
                    entity
                })
                .ExecuteWithoutResultsAsync();

            if (audit)
            {
                var action = creating ? AuditAction.Create : AuditAction.Modify;
                await AuditAsync(action, entityType, entity.Guid);
            }

            return entity;
        }

        public T SaveOrUpdate<T>(T entity, string entityType, bool audit = true) where T : class, IIdentifiable
        {
            var creating = entity.Guid == null;
            if (creating)
            {
                entity.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(entity:" + entityType + " { Guid: {guid} })")
                .OnCreate().Set("entity = {entity}")
                .OnMatch().Set("entity = {entity}")
                .WithParams(new
                {
                    guid = entity.Guid,
                    entity
                })
                .ExecuteWithoutResults();
            if (audit)
            {
                var action = creating ? AuditAction.Create : AuditAction.Modify;
                Audit(action, entityType, entity.Guid);
            }
            return entity;
        }

        public void SaveOrUpdate(Relation relation)
        {
            SaveOrUpdate(relation, Label.Entity.Relation);
        }

        public async Task<int> DeleteRelationshipBetweenAsync(string sourceGuid, string targetGuid, string relationType)
        {
            var deleted = (await Client.Cypher
                    .Match("(x { Guid: {sourceGuid} })-[r:" + relationType + "]->(y { Guid: {targetGuid} })")
                    .WithParams(new
                    {
                        sourceGuid,
                        targetGuid
                    })
                    .Delete("r")
                    .Return(r => Return.As<int>("count(r)"))
                    .ResultsAsync)
                    .FirstOrDefault();
            return deleted;
        }

        public async Task<int> DeleteRelationshipAsync(string relationType, string relationshipGuid)
        {
            var deleted = (await Client.Cypher
                    .Match("(x)-[r:" + relationType + " { Guid: {relationshipGuid} }]->(y)")
                    .WithParams(new
                    {
                        relationshipGuid
                    })
                    .Delete("r")
                    .Return(r => Return.As<int>("count(r)"))
                    .ResultsAsync)
                    .FirstOrDefault();
            return deleted;
        }

        public int DeleteRelationship(string relationType, string relationshipGuid)
        {
            var deleted = Client.Cypher
                    .Match("(x)-[r:" + relationType + " { Guid: {relationshipGuid} }]->(y)")
                    .WithParams(new
                    {
                        relationshipGuid
                    })
                    .Delete("r")
                    .Return(r => Return.As<int>("count(r)"))
                    .Results
                    .FirstOrDefault();
            return deleted;
        }

        public async Task CreateRelationshipAsync(string sourceType, string sourceGuid, string destType, string destGuid, string relationshipType, string relationshipGuid = null)
        {
            if (relationshipGuid == null)
            {
                relationshipGuid = Guid.NewGuid().ToString();
            }

            var safeSourceType = string.IsNullOrEmpty(sourceType) ? "" : ":" + sourceType;
            var safeDestType = string.IsNullOrEmpty(destType) ? "" : ":" + destType;

            await Client.Cypher
                .Match(
                    "(x" + safeSourceType + " { Guid: {sourceGuid} })",
                    "(y" + safeDestType + " { Guid: {destGuid} })")
                .WithParams(new
                {
                    sourceGuid,
                    destGuid
                })
                .CreateUnique("x-[:" + relationshipType + " { Guid: {relationshipGuid} }]->y")
                .WithParams(new
                {
                    relationshipGuid
                })
                .ExecuteWithoutResultsAsync();
        }

        public void CreateRelationship(string sourceType, string sourceGuid, string destType, string destGuid, string relationshipType, string relationshipGuid = null)
        {
            if (relationshipGuid == null)
            {
                relationshipGuid = Guid.NewGuid().ToString();
            }
            var safeSourceType = string.IsNullOrEmpty(sourceType) ? "" : ":" + sourceType;
            var safeDestType = string.IsNullOrEmpty(destType) ? "" : ":" + destType;
            Client.Cypher
                .Match(
                    "(x" + safeSourceType + " { Guid: {sourceGuid} })",
                    "(y" + safeDestType + " { Guid: {destGuid} })")
                .WithParams(new
                {
                    sourceGuid,
                    destGuid
                })
                .CreateUnique("x-[:" + relationshipType + " { Guid: {relationshipGuid} }]->y")
                .WithParams(new
                {
                    relationshipGuid
                })
                .ExecuteWithoutResults();
        }

    }
}
