﻿using Data.Entities;
using log4net;
using Neo4jClient;
using Neo4jClient.Cypher;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
// using edu.stanford.nlp.ie.crf;
using System.Xml.Linq;

namespace Services
{
    //public interface IStanfordService
    //{
    //    List<string> GetPersonalNames(string text);
    //}

    //public class StanfordService : IStanfordService
    //{
    //    private readonly CRFClassifier classifier;
    //    public StanfordService()
    //    {
    //        classifier = CRFClassifier.getClassifierNoExceptions(EnglishModelPath);
    //    }

    //    public List<string> GetPersonalNames(string text)
    //    {
    //        var xml = ClassifyToXml(text);
    //        var xdoc = XDocument.Parse(xml);
    //        var people = from node in xdoc.Descendants("wi")
    //                     where node.Attribute("entity").Value == "PERSON"
    //                     select node.Value;
    //        return people.ToList();
    //    }

    //    string ClassifyToXml(string text)
    //    {
    //        var xml = "<root>" + classifier.classifyToString(text, "xml", true) + "</root>";
    //        return xml;
    //    }

    //    string EnglishModelPath
    //    {
    //        get
    //        {
    //            return @"C:\Users\Dean Neil\Documents\Projects\codex\CodexMVC\CodexMVC\App_Data\stanford-ner-2015-04-20\classifiers\english.all.3class.distsim.crf.ser.gz";
    //        }
    //    }
    //}

    public interface ICategoryService : INeo4jClientService
    {
        List<CategoryRelatedToOtherCategoryHyperNode> FindAllAttributes(string guid);
        List<CategoryRelatedToOtherCategoryHyperNode> FindAllAttributesIncoming(string guid);
        Category Find(string guid);
        List<Category> All();
        CategoryRelatedToOtherCategoryHyperNode FindKindOfHyperNode(string categoryGuid, string parentGuid);
        void KindOf(string categoryGuid, string parentGuid);
        void UpdateKindOf(string categoryGuid, string otherCategoryGuid, string relationshipGuid);
        int DeleteAttribute(string relationshipGuid);
        void SimilarTo(string categoryGuid, string otherCategoryGuid);
        CategoryRelatedToOtherCategoryHyperNode FindSimilarToHyperNode(string categoryGuid, string parentGuid);
        CategoryRelatedToOtherCategoryHyperNode FindOppositeOfHyperNode(string categoryGuid, string otherCategoryGuid);
        CategoryRelatedToOtherCategoryHyperNode FindProducedByHyperNode(string categoryGuid, string otherCategoryGuid);
        CategoryRelatedToOtherCategoryHyperNode FindQualifiedByHyperNode(string categoryGuid, string otherCategoryGuid);
        CategoryRelatedToOtherCategoryHyperNode FindPartOfHyperNode(string categoryGuid, string otherCategoryGuid);
        void OppositeOf(string categoryGuid, string otherCategoryGuid);
        void ProducedBy(string categoryGuid, string otherCategoryGuid);
        void UpdateSimilarTo(string categoryGuid, string otherCategoryGuid, string relationshipGuid);
        void UpdateOppositeOf(string categoryGuid, string otherCategoryGuid, string relationshipGuid);
        void UpdateProducedBy(string categoryGuid, string otherCategoryGuid, string relationshipGuid);
        void QualifiedBy(string categoryGuid, string otherCategoryGuid);
        void UpdateQualifiedBy(string categoryGuid, string otherCategoryGuid, string relationshipGuid);
        void PartOf(string categoryGuid, string otherCategoryGuid);
        void UpdatePartOf(string categoryGuid, string otherCategoryGuid, string relationshipGuid);
        Category SaveOrUpdate(Category category);
        Category FindByName(string p);
        List<AgentService.CategoryGroupItem> FindAncestors(string childCategoryName);
        List<Category> FindChildren(string ancestorCategoryName);
        List<CategoryResult> AllWithAncestors(int maxDepth = 4);
        List<CategoryResult> FindChildrenWithAncestors(string ancestorCategoryName);
        List<CategoryToArtefactHyperNode> FindIncomingArtefactRelationships(string categoryGuid);
        int Delete(string categoryGuid);
    }
    public class CategoryRelatedToOtherCategoryHyperNode
    {
        public Category Category { get; set; }
        public Category OtherCategory { get; set; }
        public string Relationship { get; set; }
        public string RelationshipGuid { get; set; }
    }
    public class CategoryService : ICategoryService
    {
        private readonly ILog Log = LogManager.GetLogger("DefaultLogger");
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly ICommonService commonService;
        public CategoryService(
            GraphClient client,
            ICommonService _commonService
        )
        {
            Client = client;
            commonService = _commonService;
        }
        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        public void TransferRelationships(string sourceCategory, string destinationCategory)
        {
            var outgoing = Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {sourceCategory} })-[r]->(entity)")
                .Return((ICypherResultItem category, ICypherResultItem r, ICypherResultItem entity) => new
                {
                    RelationshipGuid = Return.As<string>("r.Guid"),
                    RelationshipType = Return.As<string>("type(r)"),
                    EntityGuid = Return.As<string>("entity.Guid")
                })
                .Results;
            /**
             * Work in progress
             */
        }
        public int Delete(string categoryGuid)
        {
            var deleted = Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })-[r]-(x)")
                .WithParams(new { categoryGuid })
                .Delete("r, category")
                .Return(() => Return.As<int>("count(r)"))
                .Results
                .FirstOrDefault();
            return deleted;
        }
        public List<CategoryResult> AllWithAncestors(int maxDepth = 2)
        {
            var query = Client.Cypher
                .Match("(child:Category)")
                .OptionalMatch("(child)-[:KIND_OF*1..]->(ancestor:Category)")
                .With("{ Child: child, Ancestors: collect(distinct(ancestor)) } as Result")
                ;

            var result = query
                .Return(() => Return.As<CategoryResult>("distinct Result"))
                .OrderBy("Result.Child.Name")
                .Results
                .ToList();

            //var result = Client.Cypher
            //    .Match("(child:" + Label.Entity.Category + ")-[:" + Label.KindOf + "*0..]->(parent:" + Label.Entity.Category + ")")
            //    .OptionalMatch("parent-[:" + Label.KindOf + "*0.." + maxDepth + "]->(ancestor:" + Label.Entity.Category + ")")
            //    .With("child")
            //    .OptionalMatch("(child)-[:" + Label.Relationship.KindOf + "]->()-[:" + Label.Relationship.KindOf + "*0.." + maxDepth + "]->(parent)")
            //    .Return((ICypherResultItem child, ICypherResultItem parent) => new CategoryResult
            //    {
            //        Child = child.As<Category>(),
            //        Ancestors = Return.As<IEnumerable<Category>>("collect(distinct(parent))")
            //    })
            //    .OrderBy("child.Name")
            //    .Results
            //    .ToList();

            return result;
        }
        /**
         * match x=(child:Category)-[:KIND_OF*0..]->(ancestor:Category { Name: "Artwork" })
            match (child)-[:KIND_OF*0..]->(parent:Category)-[:KIND_OF*0..]->(ancestor)
            with collect(parent) as parents, child
            return child, filter(p in parents where p.Guid <> child.Guid)
            order by child.Name
         */
        public List<CategoryResult> FindChildrenWithAncestors(string ancestorCategoryName)
        {
            var results = Client.Cypher
                .Match("(child:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(ancestor:" + Label.Entity.Category + " { Name: {ancestorCategoryName} })")
                .WithParams(new
                {
                    ancestorCategoryName
                })
                .Match("(child)-[:" + Label.Relationship.KindOf + "*0..]->(parent:" + Label.Entity.Category + ")-[:" + Label.Relationship.KindOf + "*0..]->(ancestor)")
                .With("child, collect(parent) as parents, ancestor")
                .Where("child.Guid <> ancestor.Guid")
                .Return((ICypherResultItem child) => new CategoryResult
                {
                    Child = child.As<Category>(),
                    Ancestors = Return.As<IEnumerable<Category>>("filter(p in parents where p.Guid <> child.Guid)")
                })
                .OrderBy("child.Name")
                .Results
                .ToList();
            return results;
        }
        public List<CategoryToArtefactHyperNode> FindIncomingArtefactRelationships(string categoryGuid)
        {
            var results = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasRelation + "]->(:" + Label.Entity.Relation + ")-[relationship]->(other:Category { Guid: {categoryGuid} })")
                .WithParams(new
                {
                    categoryGuid
                })
                .Return((ICypherResultItem artefact, ICypherResultItem other, ICypherResultItem relationship) =>
                    new CategoryToArtefactHyperNode
                    {
                        Artefact = artefact.As<Artefact>(),
                        Category = other.As<Category>(),
                        RelationshipType = relationship.Type()
                    })
                    .Results
                    .ToList();
            return results;
        }
        public List<Category> FindChildren(string ancestorCategoryName)
        {
            var results = Client.Cypher
                .Match("(ancestor:" + Label.Entity.Category + " { Name: {ancestorCategoryName} })<-[:" + Label.KindOf + "*0..]-(child:" + Label.Entity.Category + ")")
                .WithParams(new
                {
                    ancestorCategoryName
                })
                .Return(child => child.As<Category>())
                .Results
                .ToList();
            return results;
        }
        public List<AgentService.CategoryGroupItem> FindAncestors(string childCategoryName)
        {
            var results = Client.Cypher
                .Match("(child:" + Label.Entity.Category + " { Name: {childCategoryName} })-[:" + Label.KindOf + "*0..]->(ancestors:" + Label.Entity.Category + ")")
                .WithParams(new
                {
                    childCategoryName
                })
                .Where("child.Guid <> ancestors.Guid")
                .Return((ICypherResultItem child, ICypherResultItem ancestors) =>
                new AgentService.CategoryGroupItem
                {
                    Main = child.As<Category>(),
                    Ancestors = ancestors.CollectAs<Node<Category>>()
                })
                .OrderBy("child.Name")
                .Results
                .ToList();
            return results;
        }
        public Category SaveOrUpdate(Category category)
        {
            commonService.SaveOrUpdate(category, Label.Entity.Category);
            Cache.RemoveFromCache(CacheKey.CategoryService.AllWithAncestors);
            return category;
        }
        public int DeleteAttribute(string relationshipGuid)
        {
            try
            {
                var deleted = Client.Cypher
                    .Match("()-[r { Guid: {relationshipGuid} }]->()")
                    .WithParams(new
                    {
                        relationshipGuid = relationshipGuid
                    })
                    .Delete("r")
                    .Return(r => Return.As<int>("count(r)"))
                    .Results
                    .FirstOrDefault();
                Cache.RemoveFromCache(CacheKey.CategoryService.AllWithAncestors);
                return deleted;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return 0;
            }
        }
        private void CategoryRelatedToCategory(string categoryGuid, string otherCategoryGuid, string relationship)
        {
            var relationshipGuid = Guid.NewGuid().ToString();
            Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })", "(other:" + Label.Entity.Category + " { Guid: {otherCategoryGuid} })")
                .WithParams(new
                {
                    categoryGuid,
                    otherCategoryGuid
                })
                .CreateUnique("category-[:" + relationship + " { Guid: {relationshipGuid} }]->other")
                .WithParams(new
                {
                    relationshipGuid
                })
                .ExecuteWithoutResults();
        }
        private void UpdateCategoryRelatedToCategory(string categoryGuid, string otherCategoryGuid, string relationshipGuid, string relationship)
        {
            /**
             * Delete the old relationship.
             */
            var deleted = Client.Cypher.Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })-[r { Guid: {relationshipGuid} }]->(other:" + Label.Entity.Category + ")")
                .WithParams(new
                {
                    categoryGuid,
                    relationshipGuid
                })
                .Delete("r")
                .Return(r => Return.As<int>("count(r)"))
                .Results
                .FirstOrDefault();
            /**
             * If the relationship was not found don't proceed for now as we don't want to create duplicates.
             */
            if (deleted == 0)
            {
                return;
            }
            /**
             * Rebuild the relationship with the same guid.
             */
            Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })", "(other:" + Label.Entity.Category + " { Guid: {otherCategoryGuid} })")
                .WithParams(new
                {
                    categoryGuid,
                    otherCategoryGuid
                })
                .CreateUnique("category-[:" + relationship + " { Guid: {relationshipGuid} }]->other")
                .WithParams(new
                {
                    relationshipGuid
                })
                .ExecuteWithoutResults();
        }
        public void ProducedBy(string categoryGuid, string otherCategoryGuid)
        {
            CategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.ProducedBy);
        }
        public void OppositeOf(string categoryGuid, string otherCategoryGuid)
        {
            CategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.OppositeOf);
        }
        public void QualifiedBy(string categoryGuid, string otherCategoryGuid)
        {
            CategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.QualifiedBy);
        }
        public void SimilarTo(string categoryGuid, string otherCategoryGuid)
        {
            CategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.SimilarTo);
        }
        public void PartOf(string categoryGuid, string otherCategoryGuid)
        {
            CategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.PartOf);
        }
        public void UpdatePartOf(string categoryGuid, string otherCategoryGuid, string relationshipGuid)
        {
            UpdateCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, relationshipGuid, Label.Relationship.PartOf);
        }
        public void UpdateProducedBy(string categoryGuid, string otherCategoryGuid, string relationshipGuid)
        {
            UpdateCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, relationshipGuid, Label.Relationship.ProducedBy);
        }
        public void UpdateQualifiedBy(string categoryGuid, string otherCategoryGuid, string relationshipGuid)
        {
            UpdateCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, relationshipGuid, Label.Relationship.QualifiedBy);
        }
        public void UpdateOppositeOf(string categoryGuid, string otherCategoryGuid, string relationshipGuid)
        {
            UpdateCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, relationshipGuid, Label.Relationship.OppositeOf);
        }
        public void UpdateSimilarTo(string categoryGuid, string otherCategoryGuid, string relationshipGuid)
        {
            UpdateCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, relationshipGuid, Label.Relationship.SimilarTo);
        }
        public void UpdateKindOf(string categoryGuid, string otherCategoryGuid, string relationshipGuid)
        {
            UpdateCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, relationshipGuid, Label.Relationship.KindOf);
            Cache.RemoveFromCache(CacheKey.CategoryService.AllWithAncestors);
        }
        public void KindOf(string categoryGuid, string otherCategoryGuid)
        {
            CategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.KindOf);
            Cache.RemoveFromCache(CacheKey.CategoryService.AllWithAncestors);
        }
        private CategoryRelatedToOtherCategoryHyperNode FindCategoryRelatedToCategory(string categoryGuid, string otherCategoryGuid, string relationship)
        {
            return Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })-[r:" + relationship + "]->(other:" + Label.Entity.Category + " { Guid: {otherCategoryGuid} })")
                .WithParams(new
                {
                    categoryGuid,
                    otherCategoryGuid
                })
                .Return((ICypherResultItem category, ICypherResultItem other, ICypherResultItem r) => new CategoryRelatedToOtherCategoryHyperNode
                {
                    Category = category.As<Category>(),
                    OtherCategory = other.As<Category>(),
                    RelationshipGuid = Return.As<string>("r.Guid"),
                    Relationship = r.Type()
                })
                .Results
                .FirstOrDefault();
        }
        public CategoryRelatedToOtherCategoryHyperNode FindPartOfHyperNode(string categoryGuid, string otherCategoryGuid)
        {
            return FindCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.PartOf);
        }
        public CategoryRelatedToOtherCategoryHyperNode FindQualifiedByHyperNode(string categoryGuid, string otherCategoryGuid)
        {
            return FindCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.QualifiedBy);
        }
        public CategoryRelatedToOtherCategoryHyperNode FindProducedByHyperNode(string categoryGuid, string otherCategoryGuid)
        {
            return FindCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.ProducedBy);
        }
        public CategoryRelatedToOtherCategoryHyperNode FindOppositeOfHyperNode(string categoryGuid, string otherCategoryGuid)
        {
            return FindCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.OppositeOf);
        }
        public CategoryRelatedToOtherCategoryHyperNode FindSimilarToHyperNode(string categoryGuid, string otherCategoryGuid)
        {
            return FindCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.SimilarTo);
        }
        public CategoryRelatedToOtherCategoryHyperNode FindKindOfHyperNode(string categoryGuid, string otherCategoryGuid)
        {
            return FindCategoryRelatedToCategory(categoryGuid, otherCategoryGuid, Label.Relationship.KindOf);
        }
        public List<Category> All()
        {
            return Client.Cypher
                .Match("(category:" + Label.Entity.Category + ")")
                .Return(category => category.As<Category>())
                .Results.ToList();
        }
        public Category FindByName(string name)
        {
            return Client.Cypher
                .Match("(category:" + Label.Entity.Category + ")")
                .WithParams(new
                {
                    name
                })
                .Where("category.Name =~ '(?i)" + name + "'")
                .Return(category => category.As<Category>())
                .Results.FirstOrDefault();
        }
        public Category Find(string guid)
        {
            return Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })")
                .WithParams(new
                {
                    categoryGuid = guid
                })
                .Return(category => category.As<Category>())
                .Results.FirstOrDefault();
        }
        public List<CategoryRelatedToOtherCategoryHyperNode> FindAllAttributesIncoming(string guid)
        {
            var results = Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })<-[r]-(other:" + Label.Entity.Category + ")")
                .WithParams(new
                {
                    categoryGuid = guid
                })
                .Return((ICypherResultItem category, ICypherResultItem other, ICypherResultItem r) =>
                    new CategoryRelatedToOtherCategoryHyperNode
                    {
                        Category = category.As<Category>(),
                        OtherCategory = other.As<Category>(),
                        Relationship = r.Type(),
                        RelationshipGuid = Return.As<string>("r.Guid")
                    })
                    .Results
                    .ToList();
            return results;
        }
        public List<CategoryRelatedToOtherCategoryHyperNode> FindAllAttributes(string guid)
        {
            var results = Client.Cypher
                .Match("(category:" + Label.Entity.Category + " { Guid: {categoryGuid} })-[r]->(other:" + Label.Entity.Category + ")")
                .WithParams(new
                {
                    categoryGuid = guid
                })
                .Return((ICypherResultItem category, ICypherResultItem other, ICypherResultItem r) =>
                    new CategoryRelatedToOtherCategoryHyperNode
                    {
                        Category = category.As<Category>(),
                        OtherCategory = other.As<Category>(),
                        Relationship = r.Type(),
                        RelationshipGuid = Return.As<string>("r.Guid")
                    })
                    .Results
                    .ToList();
            return results;
        }
    }
    public class CategoryToArtefactHyperNode
    {
        public Artefact Artefact { get; set; }
        public Category Category { get; set; }
        public string RelationshipType { get; set; }
    }
}
