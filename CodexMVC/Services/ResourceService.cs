﻿using Data;
using Data.Entities;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ImageHyperNode
    {
        public Image Image { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
    }
    public interface INeo4jClientService
    {
        void SetCurrentUser(User user);
    }
    public interface IResourceService : INeo4jClientService
    {
        ImageHyperNode FindImageHyperNode(string imageGuid);
        Website SaveOrUpdate(Website website);
        void HasAgentImage(string agentGuid, string imageGuid);
        void HasImage(Place place, Image image);
        Image SaveOrUpdate(Image image);
        void HasArtefactImage(string artefactGuid, string imageGuid);
        void HasVideo(Agent agent, Video video);
        void HasVideo(Place place, Video video);
        void HasVideo(Artefact artefact, Video video);
        void HasWebsite(Agent agent, Website website);
        void HasWebsite(Place place, Website website);
        void HasWebsite(Artefact artefact, Website website);
        ArtefactImageHyperNode FindArtefactImageHyperNode(string imageGuid);
        AgentImageHyperNode FindAgentImageHyperNode(string imageGuid);
        Image FindImageByGuid(string guid);
        List<Image> FindArtefactImages(string artefactGuid);
        void DeleteArtefactImageHyperNode(string imageGuid);
        void DeleteAgentImageHyperNode(string imageGuid);
        void DeleteAgentWebsiteHyperNode(string websiteGuid);
        List<Image> FindAgentImages(string guid);
        Website FindWebsiteByGuid(string websiteGuid);
        void HasAgentWebsite(string agentGuid, string websiteGuid);
        AgentWebsiteHyperNode FindAgentWebsiteHyperNode(string websiteGuid);

        List<Website> FindAgentWebsites(string guid);

        Image SaveOrUpdateImage(Image image, IEnumerable<EntityItem> tags);

        List<Image> GetImages();

        List<Image> FindImagesByNameOrTag(string query);
        List<Video> FindVideosByNameOrTag(string query);
    }
    public class AgentWebsiteHyperNode
    {
        public Agent Agent { get; set; }
        public Website Website { get; set; }
    }
    public class AgentImageHyperNode
    {
        public Agent Agent { get; set; }
        public Image Image { get; set; }
    }
    public class ArtefactImageHyperNode
    {
        public Artefact Artefact { get; set; }
        public Image Image { get; set; }
    }
    public class ResourceService : IResourceService
    {
        public User CurrentUser { get; set; }
        private readonly GraphClient Client;
        private readonly ICommonService commonService;

        public ResourceService(
            GraphClient client,
            ICommonService _commonService

            )
        {
            Client = client;
            commonService = _commonService;
        }

        public List<Video> FindVideosByNameOrTag(string value)
        {
            value = "(?i).*" + value.Trim() + ".*";

            var results = new List<Video>();

            var queryImage = Client.Cypher
                .Match("(video:Video)")
                .Where("video.Name =~ \"" + value + "\" ")
                ;

            var resultImage = queryImage
                .Return(() => Return.As<Video>("video"))
                .Results
                .ToList();

            results.AddRange(resultImage);

            var queryTag = Client.Cypher
                .Match("(tag:Tag)")
                .Where("tag.Name =~ \"" + value + "\" ")
                .Match("(video:Video)-[:" + Label.Relationship.HasTag + "]->(tag)")
                ;

            var resultTag = queryTag
                .Return(() => Return.As<Video>("video"))
                .Results
                .ToList();

            results.AddRange(resultTag);

            return results.Distinct(new VideoComparer()).ToList();
        }

        public List<Image> FindImagesByNameOrTag(string value)
        {
            value = "(?i).*" + value.Trim() + ".*";

            var results = new List<Image>();

            var queryImage = Client.Cypher
                .Match("(image:Image)")
                .Where("image.Name =~ \"" + value + "\" ")
                ;

            var resultImage = queryImage
                .Return(() => Return.As<Image>("image"))
                .Results
                .ToList();

            results.AddRange(resultImage);

            var queryTag = Client.Cypher
                .Match("(tag:Tag)")
                .Where("tag.Name =~ \"" + value + "\" ")
                .Match("(image:Image)-[:" + Label.Relationship.HasTag + "]->(tag)")
                ;

            var resultTag = queryTag
                .Return(() => Return.As<Image>("image"))
                .Results
                .ToList();

            results.AddRange(resultTag);

            return results.Distinct(new ImageComparer()).ToList();
        }

        public List<Image> GetImages()
        {
            var query = Client.Cypher
                .Match("(image:Image)")
                ;

            var result = query
                .Return(() => Return.As<Image>("image"))
                .Results
                .ToList();

            return result;
        }


        public void SetCurrentUser(User user)
        {
            CurrentUser = user;
        }
        public void DeleteAgentWebsiteHyperNode(string websiteGuid)
        {
            Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + ")-[r:" + Label.Relationship.HasWebsite + "]->(w:" + Label.Entity.Website + " { Guid: {websiteGuid} })")
                .WithParams(new
                {
                    websiteGuid
                })
                .Delete("r, w")
                .ExecuteWithoutResults();
        }
        public void DeleteAgentImageHyperNode(string imageGuid)
        {
            Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + ")-[r:" + Label.Relationship.HasImage + "]->(i:" + Label.Entity.Image + " { Guid: {imageGuid} })")
                .WithParams(new
                {
                    imageGuid = imageGuid
                })
                .Delete("r, i")
                .ExecuteWithoutResults();
        }
        public void DeleteArtefactImageHyperNode(string imageGuid)
        {
            Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + ")-[r:" + Label.Relationship.HasImage + "]->(i:" + Label.Entity.Image + " { Guid: {imageGuid} })")
                .WithParams(new
                {
                    imageGuid = imageGuid
                })
                .Delete("r, i")
                .ExecuteWithoutResults();
        }
        public List<Website> FindAgentWebsites(string agentGuid)
        {
            var results = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasWebsite + "]->(website:" + Label.Entity.Website + ")")
                .WithParams(new
                {
                    agentGuid = agentGuid
                })
                .Return(website => website.As<Website>())
                .Results.ToList();
            return results;
        }
        public List<Image> FindAgentImages(string agentGuid)
        {
            var results = Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + " { Guid: {agentGuid} })-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .WithParams(new
                {
                    agentGuid = agentGuid
                })
                .Return(image => image.As<Image>())
                .Results.ToList();
            return results;
        }
        public List<Image> FindArtefactImages(string artefactGuid)
        {
            var results = Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + " { Guid: {artefactGuid} })-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + ")")
                .WithParams(new
                {
                    artefactGuid = artefactGuid
                })
                .Return(image => image.As<Image>())
                .Results.ToList();
            return results;
        }
        public Website SaveOrUpdate(Website website)
        {
            if (website.Guid == null)
            {
                website.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(website:" + Label.Entity.Website + " { Guid: {guid} })")
                .OnCreate().Set("website = {website}")
                .OnMatch().Set("website = {website}")
                .WithParams(new
                {
                    guid = website.Guid,
                    website
                })
                .ExecuteWithoutResults();
            return website;
        }

        public Image SaveOrUpdateImage(Image image, IEnumerable<EntityItem> tags)
        {
            SaveOrUpdate(image);

            var existingTags = tags.Where(x => x.Guid != null);
            if (existingTags.Any())
            {
                // remove existing tags            
                Client.Cypher
                      .Match("(image:Image { Guid: {imageGuid} })").WithParams(new { imageGuid = image.Guid })
                      .Match("(image)-[r:" + Label.Relationship.HasTag + "]->(tag:Tag)")
                      .Delete("r")
                      .ExecuteWithoutResults();
                // and recreate them
                foreach (var tag in existingTags)
                {
                    commonService.CreateRelationship(Label.Entity.Image, image.Guid, Label.Entity.Tag, tag.Guid, Label.Relationship.HasTag);
                }
            }
            var newTags = tags.Where(x => x.Guid == null);
            if (newTags.Any())
            {
                foreach (var newTag in newTags)
                {
                    // 1. if a tag node matching the tag text exists, use that;
                    //    otherwise, create a new tag node with that text
                    var tag = FindTagByName(newTag.Name);
                    if (tag == null)
                    {
                        tag = commonService.SaveOrUpdate(new Tag { Name = newTag.Name.Trim() }, Label.Entity.Tag);
                    }
                    // 2. create a relationship between the Image and the Tag.
                    commonService.CreateRelationship(Label.Entity.Image, image.Guid, Label.Entity.Tag, tag.Guid, Label.Relationship.HasTag);
                }
            }

            return image;
        }

        public Tag FindTagByName(string tagName)
        {
            var query = Client.Cypher
                .Match("(tag:Tag)")
                .Where("tag.Name =~ \"" + tagName.Trim() + "\" ")
                ;

            var results = query
                .Return(() => Return.As<Tag>("tag"))
                .Results
                .FirstOrDefault();

            return results;
        }

        public Image SaveOrUpdate(Image image)
        {
            if (image.Guid == null)
            {
                image.Guid = Guid.NewGuid().ToString();
            }
            Client.Cypher
                .Merge("(image:" + Label.Entity.Image + " { Guid: {guid} })")
                .OnCreate().Set("image = {image}")
                .OnMatch().Set("image = {image}")
                .WithParams(new
                {
                    guid = image.Guid,
                    image
                })
                .ExecuteWithoutResults();



            return image;
        }
        public Website FindWebsiteByGuid(string websiteGuid)
        {
            return Client.Cypher
                .Match("(website:" + Label.Entity.Website + " { Guid: {websiteGuid} })")
                .WithParams(new
                {
                    websiteGuid
                })
                .Return(website => website.As<Website>())
                .Results.FirstOrDefault();
        }
        public Image FindImageByGuid(string guid)
        {
            return Client.Cypher
                .Match("(image:" + Label.Entity.Image + " { Guid: {imageGuid} })")
                .WithParams(new
                {
                    imageGuid = guid
                })
                .Return(image => image.As<Image>())
                .Results.FirstOrDefault();
        }
        public AgentWebsiteHyperNode FindAgentWebsiteHyperNode(string websiteGuid)
        {
            return Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasWebsite + "]->(website:" + Label.Entity.Website + " { Guid: {websiteGuid} })")
                .WithParams(new
                {
                    websiteGuid
                })
                .Return((ICypherResultItem agent, ICypherResultItem website) => new AgentWebsiteHyperNode
                {
                    Agent = agent.As<Agent>(),
                    Website = website.As<Website>()
                })
                .Results.FirstOrDefault();
        }
        public AgentImageHyperNode FindAgentImageHyperNode(string imageGuid)
        {
            return Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + ")-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Guid: {imageGuid} })")
                .WithParams(new
                {
                    imageGuid = imageGuid
                })
                .Return((ICypherResultItem agent, ICypherResultItem image) => new AgentImageHyperNode
                {
                    Agent = agent.As<Agent>(),
                    Image = image.As<Image>()
                })
                .Results.FirstOrDefault();
        }



        public ImageHyperNode FindImageHyperNode(string imageGuid)
        {
            var query = Client.Cypher
                .Match("(image:" + Label.Entity.Image + " { Guid: {imageGuid} })").WithParams(new { imageGuid })
                .OptionalMatch("(image)-[:" + Label.Relationship.HasTag + "]->(tag:Tag)")
            ;

            var result = query
                .Return(() => new ImageHyperNode
                {
                    Image = Return.As<Image>("image"),
                    Tags = Return.As<IEnumerable<Tag>>("collect(distinct(tag))")
                })
                .Results
                .FirstOrDefault();

            return result;
        }


        public ArtefactImageHyperNode FindArtefactImageHyperNode(string imageGuid)
        {
            return Client.Cypher
                .Match("(artefact:" + Label.Entity.Artefact + ")-[:" + Label.Relationship.HasImage + "]->(image:" + Label.Entity.Image + " { Guid: {imageGuid} })")
                .WithParams(new
                {
                    imageGuid = imageGuid
                })
                .Return((ICypherResultItem artefact, ICypherResultItem image) => new ArtefactImageHyperNode
                {
                    Artefact = artefact.As<Artefact>(),
                    Image = image.As<Image>()
                })
                .Results.FirstOrDefault();
        }

        public void HasImageGeneric<T>(T thing, Image image)
            where T : Entity
        {
            var relationshipGuid = Guid.NewGuid().ToString();
            Client.Cypher
                .Match("(x:" + typeof(T).Name + ")",
                       "(i:" + Label.Entity.Image + ")")
                .Where((T x) => x.Guid == thing.Guid)
                .AndWhere((Image i) => i.Guid == image.Guid)
                .CreateUnique("x-[:" + Label.Relationship.HasImage + " { Guid: {guid} }]->i")
                .WithParams(new
                {
                    guid = relationshipGuid
                })
                .ExecuteWithoutResults();
        }
        public void HasAgentWebsite(string agentGuid, string websiteGuid)
        {
            Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + ")",
                       "(website:" + Label.Entity.Website + ")")
                .Where((Agent agent) => agent.Guid == agentGuid)
                .AndWhere((Website website) => website.Guid == websiteGuid)
                .CreateUnique("agent-[:" + Label.Relationship.HasWebsite + "]->website")
                .ExecuteWithoutResults();
        }
        public void HasAgentImage(string agentGuid, string imageGuid)
        {
            Client.Cypher
                .Match("(agent:" + Label.Entity.Agent + ")",
                       "(image:" + Label.Entity.Image + ")")
                .Where((Agent agent) => agent.Guid == agentGuid)
                .AndWhere((Image image) => image.Guid == imageGuid)
                .CreateUnique("agent-[:" + Label.Relationship.HasImage + "]->image")
                .ExecuteWithoutResults();
        }
        public void HasImage(Place place, Image image)
        {
            HasImageGeneric(place, image);
        }
        public void HasArtefactImage(string artefactGuid, string imageGuid)
        {
            Client.Cypher
                .Match("(x:" + Label.Entity.Artefact + ")",
                       "(i:" + Label.Entity.Image + ")")
                .Where((Artefact x) => x.Guid == artefactGuid)
                .AndWhere((Image i) => i.Guid == imageGuid)
                .CreateUnique("x-[:" + Label.Relationship.HasImage + "]->i")
                .ExecuteWithoutResults();
        }
        public void HasVideo(Agent agent, Video video)
        {
            throw new NotImplementedException();
        }
        public void HasVideo(Place place, Video video)
        {
            throw new NotImplementedException();
        }
        public void HasVideo(Artefact artefact, Video video)
        {
            throw new NotImplementedException();
        }
        public void HasWebsite(Agent agent, Website website)
        {
            throw new NotImplementedException();
        }
        public void HasWebsite(Place place, Website website)
        {
            throw new NotImplementedException();
        }
        public void HasWebsite(Artefact artefact, Website website)
        {
            throw new NotImplementedException();
        }
    }
}
