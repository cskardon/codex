﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public enum ResultStatus
    {
        Success,
        Info,
        Warning,
        Danger
    }
    public class Result
    {
        public ResultStatus Status { get; set; }
        public Exception Exception { get; set; }
        public string Message { get; set; }
        public static Result Danger(string message = null, Exception exception = null)
        {
            return new Result { Status = ResultStatus.Danger, Message = message, Exception = exception };
        }
        public static Result Success()
        {
            return new Result { Status = ResultStatus.Success };
        }
        public static Result<T> Success<T>(T data)
        {
            return new Result<T> { Status = ResultStatus.Success, Data = data };
        }
        public static Result<T> Info<T>(T data = default(T), string message = null)
        {
            return new Result<T> { Status = ResultStatus.Info, Message = message, Data = data };
        }
        public static Result<T> Warning<T>(string message = null, Exception exception = null)
        {
            return new Result<T> { Status = ResultStatus.Warning, Message = message, Exception = exception };
        }
        public static Result<T> Danger<T>(string message = null, Exception exception = null)
        {
            return new Result<T> { Status = ResultStatus.Danger, Message = message, Exception = exception };
        }
    }
    public class Result<T> : Result
    {
        public T Data { get; set; }
    }
}
