﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class SubjectController : BaseController
    {
        protected readonly IArtefactService artefactService;
        protected readonly ICategoryService categoryService;
        public SubjectController(
            IArtefactService _artefactService,
            ICategoryService _categoryService
        )
        {
            artefactService = _artefactService;
            categoryService = _categoryService;
        }

        [HttpGet]
        //[OutputCache(Duration = Constants.DefaultOutputCacheDuration, VaryByParam = "none")]
        public ActionResult List()
        {
            var subjects = artefactService.AllSubjectsWithArtefacts();
            var model = new ViewSubjectList
            {
                Subjects = subjects
            };
            return View(model);
        }

        [HttpGet]
        //[OutputCache(Duration = Constants.DefaultOutputCacheDuration, VaryByParam = "guid")]
        public ActionResult ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }
            var subject = categoryService.Find(guid);
            if (subject == null)
            {
                return new HttpNotFoundResult();
            }
            var containerWithArtefacts = artefactService.FindContainerWithArtefactsBySubject(guid);
            var model = new ViewSubjectPublic
            {
                Subject = subject,
                ContainerWithArtefacts = containerWithArtefacts
            };
            return View(model);
        }
    }
}