﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class CharacterController : BaseController
    {
        protected readonly IArtefactService artefactService;
        protected readonly ICategoryService categoryService;
        protected readonly IAgentService agentService;
        public CharacterController(
            IArtefactService _artefactService,
            ICategoryService _categoryService,
            IAgentService _agentService
        )
        {
            artefactService = _artefactService;
            categoryService = _categoryService;
            agentService = _agentService;
        }

        [HttpGet]
        [OutputCache(Duration = Constants.DefaultOutputCacheDuration, VaryByParam = "none")]
        public ActionResult List()
        {
            var characters = artefactService.AllCharactersWithArtefacts();
            var model = new ViewCharacterList
            {
                Characters = characters
            };
            return View(model);
        }

        [HttpGet]
        [OutputCache(Duration = Constants.DefaultOutputCacheDuration, VaryByParam = "guid")]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }
            var character = agentService.FindPersonalDetailsAsync(guid);
            if (character == null)
            {
                return new HttpNotFoundResult();
            }
            var containerWithArtefacts = artefactService.FindContainerWithArtefactsByCharacter(guid);
            var model = new ViewCharacterPublic
            {
                Character = character,
                ContainerWithArtefacts = containerWithArtefacts
            };
            return View(model);
        }
    }
}