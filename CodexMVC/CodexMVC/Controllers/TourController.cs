﻿using CodexMVC.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class TourController : BaseController
    {
        private readonly IArtefactService artefactService;

        public TourController(
            IArtefactService _artefactService
            )
        {
            artefactService = _artefactService;
        }

        [HttpGet]
        public ActionResult List(string mode = null)
        {

            var tours = artefactService.AllTours();
            var model = new ViewTourList
            {
                Tours = tours
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }
            var tour = artefactService.FindTour(guid);
            if (tour == null)
            {
                return new HttpNotFoundResult();
            }
            var model = new ViewTourModel
            {
                Tour = tour
            };

            return View(model);

        }
    }
}