﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class StyleController : BaseController
    {
        protected readonly IArtefactService artefactService;
        protected readonly ICategoryService categoryService;
        public StyleController(
            IArtefactService _artefactService,
            ICategoryService _categoryService
        )
        {
            artefactService = _artefactService;
            categoryService = _categoryService;
        }

        [HttpGet]
        // [OutputCache(Duration = Constants.DefaultOutputCacheDuration, VaryByParam = "guid")]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }
            var style = categoryService.Find(guid);
            if (style == null)
            {
                return new HttpNotFoundResult();
            }
            var containerWithArtefacts = await artefactService.FindContainerWithArtefactsByStyleAsync(guid);
            var model = new ViewStylePublic
            {
                Style = style,
                ContainerWithArtefacts = containerWithArtefacts
            };
            return View(model);
        }
    }
}