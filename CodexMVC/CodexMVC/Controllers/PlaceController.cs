﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Humanizer;
using System.Web.UI;
using System.Threading.Tasks;
using Data;

namespace CodexMVC.Controllers
{
    public class PlaceController : BaseController
    {
        private readonly IPlaceService placeService;
        private readonly IActService actService;
        private readonly ICommonService commonService;
        public PlaceController(
            IPlaceService _placeService,
            IActService _actService,
            ICommonService _commonService
            )
        {
            placeService = _placeService;
            actService = _actService;
            commonService = _commonService;
        }

        [HttpGet]
        [OutputCache(Duration = Constants.DefaultOutputCacheDuration)]
        public async Task<ActionResult> List()
        {
            var places = await placeService.AllPlacesWithArtworks();
            return View(places);
        }

        [HttpGet]
        public ActionResult ByArtefactType(string guid)
        {
            var place = placeService.FindPlaceByGuid(guid);
            var types = placeService.FindArtefactTypesByPlace(guid);
            var model = new ViewPlaceByArtefactType
            {
                Place = place,
                ArtefactTypes = types
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult ByMovement(string guid)
        {
            var place = placeService.FindPlaceByGuid(guid);
            var movements = placeService.FindMovementsByPlace(guid);
            var model = new ViewPlaceByMovement
            {
                Place = place,
                Movements = movements
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult BySubject(string guid)
        {
            var place = placeService.FindPlaceByGuid(guid);
            var subjects = placeService.FindSubjectsByPlace(guid);
            var model = new ViewPlaceBySubject
            {
                Place = place,
                Subjects = subjects
            };
            return View(model);
        }
        private string Until(Relation relation)
        {
            var diff = relation.YearEnd.Value - relation.Year.Value;
            if (diff == 0)
            {
                return "";
            }
            if (diff == 1)
            {
                return " for a year";
            }
            return string.Format(" for {0} years until {1}", diff.ToWords(), relation.YearEnd.Value);
        }
        public string NameOf(IEntity entity)
        {
            if (entity is Data.Agent)
            {
                return ((Data.Agent)entity).FullName2;
            }
            if (entity is Artefact)
            {
                return ((Artefact)entity).ToDisplay();
            }
            return entity.Name;
        }
        private async Task<IEnumerable<NewAct>> GetTimelineAsync(string guid)
        {
            var newActs = await actService.FindNewActsByPlaceAsync(guid);
            var outgoing = commonService.FindOutgoingRelations<Agent>(guid)
                .AsParallel()
                .Where(x => x.Relation.Year.HasValue)
                .Where(x => x.Destination != null && x.Source != null)
                .Where(x => x.Destination is Place)
                .Select(x => new NewAct
                {
                    Act = new Act
                    {
                        Name = x.Relation.Name,
                        Description = "<a href=\"/Agent/ViewDetail/" + x.Source.Guid + "\">" + x.Source.FullName2 + "</a> " + Label.Relationship.ToPresentTense(x.RelationToDestinationType) + " <a href=\"/" + x.Destination.GetType().UnderlyingSystemType.Name + "/ViewDetail/" + x.Destination.Guid + "\">" + NameOf(x.Destination) + "</a>" + (x.Relation.YearEnd.HasValue ? Until(x.Relation) : "") + ".",
                        Proximity = x.Relation.Proximity,
                        Decade = x.Relation.Decade,
                        Milennium = x.Relation.Milennium,
                        Year = x.Relation.Year,
                        Month = x.Relation.Month,
                        Day = x.Relation.Day,
                        Hour = x.Relation.Hour,
                        Minute = x.Relation.Minute,
                        ProximityEnd = x.Relation.ProximityEnd,
                        DecadeEnd = x.Relation.DecadeEnd,
                        MilenniumEnd = x.Relation.MilenniumEnd,
                        YearEnd = x.Relation.YearEnd,
                        MonthEnd = x.Relation.MonthEnd,
                        DayEnd = x.Relation.DayEnd,
                        HourEnd = x.Relation.HourEnd,
                        MinuteEnd = x.Relation.MinuteEnd
                    },
                    EntityImages = new List<EntityImage> { new EntityImage { Entity = x.Destination } }
                });
            newActs = newActs.Concat(outgoing);
            return newActs;
        }

        [HttpGet]
        [OutputCache(
            Duration = Constants.DefaultOutputCacheDuration,
            Location = OutputCacheLocation.Client,
            NoStore = true,
            VaryByParam = "guid")]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }
            var exists = placeService.PlaceExists(guid);
            if (false == exists)
            {
                return new HttpNotFoundResult();
            }

            var model = await GetViewDetailModel(guid);

            var history = new History
            {
                EntityGuid = guid,
                EntityType = typeof(Place).Name,
                Date = DateTimeOffset.Now,
                Event = "<a href='/Place/ViewDetail/" + guid + "'>" + model.Place.Name + "</a>"
            };
            RazorHelper.AddToHistory(history);

            return View(model);
        }

        private async Task<ViewPlace> GetViewDetailModel(string guid)
        {
            var KEY = "PlaceController.GetViewDetailModel[guid=" + guid + "]";
            var model = Cache.GetFromCache<ViewPlace>(KEY);
            if (model == null)
            {
                var place = placeService.FindPlaceByGuid(guid);
                var exhibitions = await placeService.FindExhibitionsAtAsync(guid);
                var tours = await placeService.FindToursAtAsync(guid);
                var containers = await placeService.FindContainersOfAsync(guid);
                var placeType = await placeService.FindPlaceTypeAsync(guid);
                var timeline = await GetTimelineAsync(guid);
                var artists = await placeService.ArtistsWithArtefactsAtPlaceAsync(guid);
                var collections = await placeService.CollectionsWithArtefactsAsync(guid);

                model = new ViewPlace
                {
                    Place = place,
                    Tours = tours,
                    Exhibitions = exhibitions,
                    PlaceType = placeType,
                    Containers = containers,
                    Timeline = timeline,
                    Artists = artists,
                    Collections = collections
                };

                Cache.AddToCache(KEY, model, Constants.DefaultOutputCacheDuration);
            }

            return model;
        }
    }
}