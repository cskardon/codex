﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Services.Helpers;
using System.Web.UI;

namespace CodexMVC.Controllers
{
    public class ArtefactController : BaseController
    {
        private readonly IArtefactService artefactService;
        private readonly IAgentService agentService;
        public ArtefactController(
            IArtefactService _artefactService,
            IAgentService _agentService
        )
        {
            artefactService = _artefactService;
            agentService = _agentService;
        }

        [HttpGet]
        //[OutputCache(Duration = Constants.DefaultOutputCacheDuration, VaryByParam = "mode")]
        public ActionResult List(string mode = null)
        {
            var artworks = artefactService.AllRepresentedArtefacts();
            var model = new ViewArtefactList
            {
                Artefacts = artworks
            };
            if (mode == null || mode == ArtefactListMode.ByArtwork.ToString())
            {
                model.Mode = ArtefactListMode.ByArtwork;
            }
            else if (mode == ArtefactListMode.ByMedium.ToString())
            {
                model.Mode = ArtefactListMode.ByMedium;
            }
            else
            {
                model.Mode = ArtefactListMode.ByArtwork;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Represents(string agentGuid, string placeGuid)
        {
            var result = artefactService.CollectionsRepresentingAgent(agentGuid);
            return View(result);
        }

        [HttpGet]
        [OutputCache(Duration = Constants.DefaultOutputCacheDuration, VaryByParam = "search")]
        public async Task<ActionResult> UniversalSearch(string search)
        {
            var results = await agentService.UniversalSearch(search);
            var model = new UniversalSearchModel
            {
                Search = search,
                Results = results
            };
            return PartialView("~/Views/Shared/Partials/UniversalSearch.cshtml", model);
        }

        [HttpGet]
        [OutputCache(
            Duration = Constants.DefaultOutputCacheDuration,
            Location = OutputCacheLocation.Client,
            NoStore = true,
            VaryByParam = "guid")]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }

            var exists = artefactService.ArtefactExists(guid);
            if (false == exists)
            {
                return new HttpNotFoundResult();
            }
            
            var item = await GetViewDetailItem(guid);
            if (CurrentUser != null)
            {
                artefactService.UpdateArtefactPublicWithUserDetails(item, CurrentUser.Guid);
            }

            AddToHistory(guid, item);

            var model = new ViewArtefactPublicModel
            {
                Item = item
            };

            if (item.Artefact.TripAdvisorLocationId != null)
            {
                model.TripAdvisorJson = Cache.GetFromOrAddToCache("TripAdvisorLocationId/" + item.Artefact.TripAdvisorLocationId.Value, () => TripAdvisor(item.Artefact.TripAdvisorLocationId.Value).Result, TripAdvisorCacheDuration);
            }

            if (CurrentUser != null)
            {
                model.Item.UserArtefactDetails = userService.FindUserArtefactDetails(CurrentUser.Guid, guid);
            }

            return View(model);
        }

        private async Task<ViewArtefactPublic> GetViewDetailItem(string guid)
        {
            var KEY = "ArtefactController.GetViewDetailItem[guid=" + guid + "]";
            var item = Cache.GetFromCache<ViewArtefactPublic>(KEY);
            if (item == null)
            {
                item = await artefactService.FindArtefactDetailAsync(guid);
                Cache.AddToCache(KEY, item, Constants.DefaultOutputCacheDuration);
            }
            return item;
        }

        private readonly string TripAdvisorAPIKey = ConfigurationManager.AppSettings["TripAdvisorAPIKey"];
        private readonly int TripAdvisorCacheDuration = int.Parse(ConfigurationManager.AppSettings["TripAdvisorCacheDuration"]);
        private async Task<string> TripAdvisor(int locationId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://api.tripadvisor.com");

                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync("/api/partner/2.0/location/" + locationId + "?key=" + TripAdvisorAPIKey).ConfigureAwait(false);
                if (response.IsSuccessStatusCode)
                {
                    var product = await response.Content.ReadAsStringAsync();
                    return product;
                }
                return null;
            }
        }

        private static void AddToHistory(string guid, ViewArtefactPublic item)
        {
            if (item.Images != null && item.Images.Any())
            {
                var image = item.Images.FirstOrDefault();
                var artist = item.Creators.FirstOrDefault();
                var artistText = artist != null ? " by " + artist.FullName2 : "";
                var history = new History
                {
                    EntityGuid = guid,
                    EntityType = typeof(Artefact).Name,
                    Date = DateTimeOffset.Now,
                    Event = "<a href='/Artefact/ViewDetail/" + guid + "'><img alt='" + item.Artefact.ToDisplay() + artistText + "' src='" + image.VirtualPath().Replace("~", "") + "?height=300&mode=crop' /></a>"
                };
                RazorHelper.AddToHistory(history);
            }
        }
    }
}