﻿using Data.Entities;
using Neo4jClient;
using Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Services.Helpers;
using System.Runtime.Caching;
using CodexMVC.Models;
using System.IO;
using Newtonsoft.Json;
using CodexMVC.Helpers;
using System.Net;
using System.Drawing.Imaging;
using log4net;
using Data;
using Humanizer;
using System.Threading.Tasks;

namespace CodexMVC.Controllers
{
    public class BaseController : Controller
    {
        protected readonly ILog Log = LogManager.GetLogger("DefaultLogger");
        protected readonly IUserService userService;
        protected readonly ICategoryService categoryService;
        protected readonly IResourceService resourceService;
        protected readonly IActService actService;
        protected readonly ICommonService commonService;
        protected readonly IDataSetService dataSetService;
        protected readonly IDataPointService dataPointService;
        public BaseController()
        {
            userService = GetService<IUserService>();
            categoryService = GetService<ICategoryService>();
            resourceService = GetService<IResourceService>();
            actService = GetService<IActService>();
            commonService = GetService<ICommonService>();
            dataSetService = GetService<IDataSetService>();
            dataPointService = GetService<IDataPointService>();
        }

        public void Alert(Result result)
        {
            switch (result.Status)
            {
                case ResultStatus.Success: Success(result.Message); break;
                case ResultStatus.Info: Info(result.Message); break;
                case ResultStatus.Warning: Warning(result.Message); break;
                case ResultStatus.Danger: Danger(result.Message, result.Exception); break;
                default: break;
            }
        }

        public void Success(string message)
        {
            Alerts.Add(new Alert { Status = ResultStatus.Success, Message = message });
        }

        public void Warning(string message)
        {
            Alerts.Add(new Alert { Status = ResultStatus.Warning, Message = message });
        }

        public void Info(string message)
        {
            Alerts.Add(new Alert { Status = ResultStatus.Info, Message = message });
        }

        public void Danger(string message, Exception exception = null)
        {
            Alerts.Add(new Alert { Status = ResultStatus.Danger, Message = message, Exception = exception });
        }

        public void Danger(Result result)
        {
            Alerts.Add(new Alert { Status = ResultStatus.Danger, Message = result.Message, Exception = result.Exception });
        }

        public void AlertOnDanger(Result result)
        {
            if (result.Status == ResultStatus.Danger)
            {
                Danger(result);
            }
        }

        public void AlertOnSuccess(Result result, string message)
        {
            if (result.Status == ResultStatus.Success)
            {
                Success(message);
            }
        }

        public List<Alert> Alerts
        {
            get
            {
                var alerts = (List<Alert>)TempData["Alerts"];
                if (alerts == null)
                {
                    alerts = new List<Alert>();
                    TempData["Alerts"] = alerts;
                }
                return alerts;
            }
        }


        protected async Task<IEnumerable<NewAct>> GetCategoryTimeline(string categoryGuid)
        {
            var newActs = await actService.FindNewActsByCategory(categoryGuid);
            return newActs;
        }

        protected async Task<IEnumerable<NewAct>> GetAgentTimeline(string agentGuid)
        {
            var newActs = await actService.FindActsByAgent(agentGuid);
            var outgoing2 = GetOutgoing(agentGuid);
            var incoming2 = GetIncoming(agentGuid);
            return newActs.Concat(outgoing2).Concat(incoming2);
        }

        private IEnumerable<NewAct> GetIncoming(string agentGuid)
        {
            var incoming = commonService.FindIncomingRelations<Agent>(agentGuid);
            IEnumerable<NewAct> incoming2 = null;
            Parallel.Invoke(() =>
            {
                incoming2 = incoming
                    .Where(x => x.Relation.Year.HasValue)
                    .Select(x => new NewAct
                    {
                        Act = new Act
                        {
                            Name = x.Relation.Name,
                            Description = IncomingText(x),
                            Proximity = x.Relation.Proximity,
                            Decade = x.Relation.Decade,
                            Milennium = x.Relation.Milennium,
                            Year = x.Relation.Year,
                            Month = x.Relation.Month,
                            Day = x.Relation.Day,
                            Hour = x.Relation.Hour,
                            Minute = x.Relation.Minute,
                            ProximityEnd = x.Relation.ProximityEnd,
                            DecadeEnd = x.Relation.DecadeEnd,
                            MilenniumEnd = x.Relation.MilenniumEnd,
                            YearEnd = x.Relation.YearEnd,
                            MonthEnd = x.Relation.MonthEnd,
                            DayEnd = x.Relation.DayEnd,
                            HourEnd = x.Relation.HourEnd,
                            MinuteEnd = x.Relation.MinuteEnd
                        },
                        EntityImages = x.EntityImages
                    });
            });
            return incoming2;
        }

        private IEnumerable<NewAct> GetOutgoing(string agentGuid)
        {
            var outgoing = commonService.FindOutgoingRelations<Agent>(agentGuid);
            IEnumerable<NewAct> outgoing2 = null;
            Parallel.Invoke(() =>
            {
                outgoing2 = outgoing
                .Where(x => x.Relation.Year.HasValue)
                .Where(x => x.Destination != null && x.Source != null)
                .Select(x => new NewAct
                {
                    Act = new Act
                    {
                        Name = x.Relation.Name,
                        Description = OutgoingText(x),
                        Proximity = x.Relation.Proximity,
                        Decade = x.Relation.Decade,
                        Milennium = x.Relation.Milennium,
                        Year = x.Relation.Year,
                        Month = x.Relation.Month,
                        Day = x.Relation.Day,
                        Hour = x.Relation.Hour,
                        Minute = x.Relation.Minute,
                        ProximityEnd = x.Relation.ProximityEnd,
                        DecadeEnd = x.Relation.DecadeEnd,
                        MilenniumEnd = x.Relation.MilenniumEnd,
                        YearEnd = x.Relation.YearEnd,
                        MonthEnd = x.Relation.MonthEnd,
                        DayEnd = x.Relation.DayEnd,
                        HourEnd = x.Relation.HourEnd,
                        MinuteEnd = x.Relation.MinuteEnd
                    },
                    EntityImages = x.EntityImages
                });
            });
            return outgoing2;
        }

        private string OutgoingText(GenericRelationHyperNode<Agent, IEntity> node)
        {
            var attributes = "";
            if (node.DestinationAttributes != null && node.DestinationAttributes.Any())
            {
                var pronoun = node.Destination is Agent ? "called" : "of";
                attributes = " a " + string.Join(" and ", node.DestinationAttributes.Select(x => x.Name.ToLower())) + " " + pronoun + " ";
            }
            var sourceAge = "";
            if (node.SourceBirth != null && node.SourceBirth.Year.HasValue && node.SourceDeath != null && node.SourceDeath.Year.HasValue)
            {
                if (node.Relation.Year.Value > node.SourceBirth.Year.Value && node.Relation.Year.Value <= node.SourceDeath.Year.Value)
                {
                    var age1 = node.Relation.Year.Value - node.SourceBirth.Year.Value;
                    // sourceAge = string.Format("(then aged {0}) ", age1);
                }
            }
            var destinationAge = "";
            if (node.DestinationBirth != null && node.DestinationBirth.Year.HasValue && node.DestinationDeath != null && node.DestinationDeath.Year.HasValue)
            {
                if (node.Relation.Year.Value > node.DestinationBirth.Year.Value && node.Relation.Year.Value <= node.DestinationDeath.Year.Value)
                {
                    var age1 = node.Relation.Year.Value - node.DestinationBirth.Year.Value;
                    // destinationAge = string.Format("(then aged {0}) ", age1);
                }
            }
            var result = "<a href=\"/Agent/ViewDetail/" + node.Source.Guid + "\" target=\"_blank\">" + node.Source.FullName2 + "</a> " + sourceAge + AdverbOutgoing(node) + attributes + " <a href=\"/" + node.Destination.GetType().UnderlyingSystemType.Name + "/ViewDetail/" + node.Destination.Guid + "\" target=\"_blank\">" + NameOf(node.Destination) + "</a>" + destinationAge + (node.Relation.YearEnd.HasValue ? Range(node.Relation) : "") + ".";
            return result;
        }


        private string NameOf(IEntity entity)
        {
            if (entity is Data.Agent)
            {
                return ((Data.Agent)entity).FullName2;
            }
            if (entity is Artefact)
            {
                return ((Artefact)entity).ToDisplay();
            }
            return entity.Name;
        }


        private string Range(Relation relation)
        {
            var diff = relation.YearEnd.Value - relation.Year.Value;
            if (diff == 0)
            {
                return "";
            }
            if (diff == 1)
            {
                return " for a year";
            }
            if (RazorHelper.ToRange(relation.Range) == Data.Entities.Range.Continuous)
            {
                return string.Format(" over the course of {0} years until {1}", diff.ToWords(), relation.YearEnd.Value);
            }
            return string.Format(" some time between now and {0}", relation.YearEnd.Value);
        }
        private static string AdverbOutgoing(GenericRelationHyperNode<Agent, IEntity> node)
        {
            if (node.RelationToDestinationType == Label.Relationship.MadeBy)
            {
                if (false == node.Relation.YearEnd.HasValue)
                {
                    return "creates";
                }
            }
            return Label.Relationship.ToPresentTense(node.RelationToDestinationType);
        }

        private static string AdverbIncoming(GenericRelationHyperNode<IEntity, Agent> node)
        {
            if (node.RelationToDestinationType == Label.Relationship.MadeBy)
            {
                if (false == node.Relation.YearEnd.HasValue)
                {
                    return "creates";
                }
            }
            return Label.Relationship.ToPresentTense(node.RelationToDestinationType, true);
        }

        private string IncomingText(GenericRelationHyperNode<IEntity, Agent> node)
        {
            var attributes = "";
            if (node.SourceAttributes != null && node.SourceAttributes.Any())
            {
                //var pronoun = node.Source is Agent ? "called" : "of";
                var pronoun = "called";
                var qualifier = StartsWithVowel(node.SourceAttributes.First().Name) ? "an" : "a";
                attributes = " " + qualifier + " " + string.Join(" and ", node.SourceAttributes.Select(x => x.Name.ToLower())) + " " + pronoun + " ";
            }
            var sourceAge = "";
            if (node.SourceBirth != null && node.SourceBirth.Year.HasValue && node.SourceDeath != null && node.SourceDeath.Year.HasValue)
            {
                if (node.Relation.Year.Value > node.SourceBirth.Year.Value && node.Relation.Year.Value <= node.SourceDeath.Year.Value)
                {
                    var age1 = node.Relation.Year.Value - node.SourceBirth.Year.Value;
                    // sourceAge = string.Format("(then aged {0}) ", age1);
                }
            }
            var destinationAge = "";
            if (node.DestinationBirth != null && node.DestinationBirth.Year.HasValue && node.DestinationDeath != null && node.DestinationDeath.Year.HasValue)
            {
                if (node.Relation.Year.Value > node.DestinationBirth.Year.Value && node.Relation.Year.Value <= node.DestinationDeath.Year.Value)
                {
                    var age1 = node.Relation.Year.Value - node.DestinationBirth.Year.Value;
                    // destinationAge = string.Format("(then aged {0}) ", age1);
                }
            }
            var result = "<a href=\"/Agent/ViewDetail/" + node.Destination.Guid + "\" target=\"_blank\">" + node.Destination.FullName2 + "</a> " + destinationAge + AdverbIncoming(node) + " " + attributes + "<a href=\"/" + node.Source.GetType().UnderlyingSystemType.Name + "/ViewDetail/" + node.Source.Guid + "\" target=\"_blank\">" + NameOf(node.Source) + "</a> " + sourceAge + (node.Relation.YearEnd.HasValue ? Range(node.Relation) : "") + ".";
            return result;
        }

        private bool StartsWithVowel(string value)
        {
            var vowels = new string[] { "a", "e", "i", "o", "u" };
            return vowels.Contains(value.Substring(0, 1).ToLower());
        }

        private string Filenameize(string value)
        {
            return value.ToLower().Replace(" ", "-").Replace(":", "_");
        }

        protected void SaveImage(IImageUploadModel model, Image image)
        {
            var uploadingToServer = model.Image != null;
            var serverDownloadingFrom = false == string.IsNullOrEmpty(model.ImageUrl) && false == uploadingToServer;
            if (serverDownloadingFrom || uploadingToServer)
            {
                var filename = uploadingToServer ? Path.GetFileName(model.Image.FileName) : "external-image";
                var extension = uploadingToServer ? GetFileExtension(model.Image) : "jpg";
                var source = model.Source;
                if (serverDownloadingFrom)
                {
                    var uri = new Uri(model.ImageUrl);
                    if (string.IsNullOrWhiteSpace(model.Source))
                    {
                        source = uri.Host;
                    }
                    var hasExtension = Path.HasExtension(uri.LocalPath);
                    filename = hasExtension ? Path.GetFileName(uri.LocalPath) : Filenameize(model.Name);
                    // extension = hasExtension ? Path.GetExtension(uri.LocalPath).Replace(".", "").ToLower() : "jpg";
                }
                image.PublicDomain = model.PublicDomain;
                image.FileName = filename;
                image.Extension = extension;
                image.Source = source;
                image.SourceUrl = model.ImageUrl;
                image.Primary = true;
                resourceService.SaveOrUpdate(image);
                if (uploadingToServer)
                {
                    SaveFile(model.Image, image.Guid, image.Extension);
                }
                else if (serverDownloadingFrom)
                {
                    DownloadAndSaveFile(model.ImageUrl, image.Guid);
                }
                else
                {
                    // We shouldn't be here.
                }
            }
        }

        protected async Task SaveArtefactImage(IImageUploadModel model, Artefact artefact)
        {
            var uploadingToServer = model.Image != null;
            var serverDownloadingFrom = false == string.IsNullOrEmpty(model.ImageUrl) && false == uploadingToServer;
            if (serverDownloadingFrom || uploadingToServer)
            {
                var image = new Image();
                var filename = uploadingToServer ? Path.GetFileName(model.Image.FileName) : "external-image";
                var extension = uploadingToServer ? GetFileExtension(model.Image) : "jpg";
                var source = model.Source;
                if (serverDownloadingFrom)
                {
                    var uri = new Uri(model.ImageUrl);
                    if (string.IsNullOrWhiteSpace(model.Source))
                    {
                        source = uri.Host;
                    }
                    var hasExtension = Path.HasExtension(uri.LocalPath);
                    filename = hasExtension ? Path.GetFileName(uri.LocalPath) : Filenameize(model.Name);
                    // extension = hasExtension ? Path.GetExtension(uri.LocalPath).Replace(".", "").ToLower() : "jpg";
                }
                image.PublicDomain = model.PublicDomain;
                image.FileName = filename;
                image.Extension = extension;
                image.Source = source;
                image.SourceUrl = model.ImageUrl;
                image.Primary = true;
                resourceService.SaveOrUpdate(image);
                if (artefact != null)
                {
                    resourceService.HasArtefactImage(artefact.Guid, image.Guid);
                }
                if (uploadingToServer)
                {
                    await SaveFileAsync(model.Image, image.Guid, image.Extension);
                }
                else if (serverDownloadingFrom)
                {
                    await DownloadAndSaveFileAsync(model.ImageUrl, image.Guid);
                }
                else
                {
                    // We shouldn't be here.
                }
            }
        }
        protected TService GetService<TService>()
        {
            return DependencyResolver.Current.GetService<TService>();
        }
        protected ActionResult SaveRelationTest<TSource, TDest>(GenericRelationNodeModel<TSource, TDest> model, Action<GenericRelationNode<TSource, TDest>> updater, Func<string, TSource> sourceGetter)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            if (ModelState.IsValid)
            {
                var node = new GenericRelationNode<TSource, TDest>
                {
                    Source = model.Node.Source,
                    RelationToDestinationGuid = model.Node.RelationToDestinationGuid,
                    Destination = model.Node.Destination
                };
                updater(node);
                return RedirectToAction("ViewDetail", new { guid = model.Node.Source.Guid });
            }
            var type = typeof(TDest).UnderlyingSystemType.Name;
            model.Node.Source = sourceGetter(model.Node.Source.Guid);
            return View("EditBasicRelatedTo" + type, model);
        }
        protected ActionResult SaveRelation<TSource, TDest>(GenericRelationModel<TSource, TDest> model, Action<GenericRelationHyperNode<TSource, TDest>> updater, Func<string, TSource> sourceGetter, string descendantsOf = null)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            if (ModelState.IsValid)
            {
                var pairs = RazorHelper.GetPairs(model.Relation.Description);
                var list = pairs.Select(x => x.Split(new char[] { '|' }));
                var contexts = RazorHelper.GetContexts(model);
                var fullList = contexts != null ? list.Concat(contexts) : list;
                var node = new GenericRelationHyperNode<TSource, TDest>
                {
                    Source = model.Node.Source,
                    HasRelationGuid = model.Node.HasRelationGuid,
                    RelationToDestinationGuid = model.Node.RelationToDestinationGuid,
                    Relation = model.Relation,
                    Destination = model.Node.Destination,
                    References = fullList
                };
                updater(node);
                return RedirectToAction("ViewDetail", new { guid = model.Node.Source.Guid });
            }
            var type = typeof(TDest).UnderlyingSystemType.Name;
            model.Node.Source = sourceGetter(model.Node.Source.Guid);
            if (descendantsOf != null)
            {
                model.Categories = RazorHelper.SelectListOfDescendants(descendantsOf);
            }
            return View("EditRelatedTo" + type, model);
        }
        protected ActionResult FindRelation<TSource, TDest>(string guid, string hasRelationGuid, string action, string title, Func<string, string, GenericRelationHyperNode<TSource, TDest>> getter, string descendantsOf = null)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            var node = getter(guid, hasRelationGuid);
            var model = new GenericRelationModel<TSource, TDest>
            {
                Action = action,
                Title = node.Source.Name + " " + title,
                Node = node,
                Relation = node.Relation,
                Range = RazorHelper.ToRange(node.Relation.Range),
                Proximity = RazorHelper.ToProximity(node.Relation.Proximity),
                ProximityEnd = RazorHelper.ToProximity(node.Relation.ProximityEnd),
                Context = JsonConvert.SerializeObject(node.Context())
            };
            if (descendantsOf != null)
            {
                model.Categories = RazorHelper.SelectListOfDescendants(descendantsOf);
            }
            var type = typeof(TDest).UnderlyingSystemType.Name;
            return View("EditRelatedTo" + type, model);
        }
        protected ActionResult FindRelationTest<TSource, TDest>(string guid, string toDestGuid, string action, string title, Func<string, string, GenericRelationNode<TSource, TDest>> getter)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            var node = getter(guid, toDestGuid);
            var model = new GenericRelationNodeModel<TSource, TDest>
            {
                Action = action,
                Title = node.Source.Name + " " + title,
                Node = node
            };
            var type = typeof(TDest).UnderlyingSystemType.Name;
            return View("EditBasicRelatedTo" + type, model);
        }
        protected ActionResult AddRelation<TSource, TDest>(TSource source, string action, string title, string descendantsOf = null)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            var model = new GenericRelationModel<TSource, TDest>
            {
                Action = action,
                Title = source.Name + " " + title,
                Node = new GenericRelationHyperNode<TSource, TDest>
                {
                    Source = source,
                    Relation = new Relation()
                }
            };
            if (descendantsOf != null)
            {
                model.Categories = RazorHelper.SelectListOfDescendants(descendantsOf);
            }
            var type = typeof(TDest).UnderlyingSystemType.Name;
            return View("EditRelatedTo" + type, model);
        }

        protected ActionResult AddRelationTest<TSource, TDest>(TSource source, string action, string title)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            var model = new GenericRelationNodeModel<TSource, TDest>
            {
                Action = action,
                Title = source.Name + " " + title,
                Node = new GenericRelationNode<TSource, TDest>
                {
                    Source = source
                }
            };
            var type = typeof(TDest).UnderlyingSystemType.Name;
            return View("EditBasicRelatedTo" + type, model);
        }
        public void DownloadAndSaveFile(string url, string guid)
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    var externalData = webClient.DownloadData(url);
                    using (var memoryStream = new MemoryStream(externalData))
                    {
                        var filePath = Path.Combine(Server.MapPath(FileUploadPath), guid + ".jpg");
                        using (var fileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
                        {
                            var image = System.Drawing.Image.FromStream(memoryStream);
                            image.Save(fileStream, ImageFormat.Jpeg);
                            var contentLength = (int)fileStream.Length;
                            var fileData = new byte[contentLength];
                            fileStream.Write(fileData, 0, contentLength);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unable to download the file from the external site and save it locally.", ex);
            }
        }
        public async Task DownloadAndSaveFileAsync(string url, string guid)
        {
            try
            {
                using (var webClient = new WebClient())
                {
                    var externalData = await webClient.DownloadDataTaskAsync(url);
                    using (var memoryStream = new MemoryStream(externalData))
                    {
                        var filePath = Path.Combine(Server.MapPath(FileUploadPath), guid + ".jpg");
                        using (var fileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
                        {
                            var image = System.Drawing.Image.FromStream(memoryStream);
                            image.Save(fileStream, ImageFormat.Jpeg);
                            var contentLength = (int)fileStream.Length;
                            var fileData = new byte[contentLength];
                            await fileStream.WriteAsync(fileData, 0, contentLength);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unable to download the file from the external site and save it locally.", ex);
            }
        }
        protected readonly string FileUploadPath = ConfigurationManager.AppSettings["FileUploadPath"];
        protected async Task SaveFileAsync(HttpPostedFileBase file, string guid, string extension)
        {
            try
            {
                var filePath = Path.Combine(Server.MapPath(FileUploadPath), guid + "." + extension);
                var fileData = new byte[file.ContentLength];
                await file.InputStream.ReadAsync(fileData, 0, file.ContentLength);
                using (var stream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
                {
                    await stream.WriteAsync(fileData, 0, file.ContentLength);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unable to save uploaded file.", ex);
            }
        }
        protected void SaveFile(HttpPostedFileBase file, string guid, string extension)
        {
            try
            {
                var filePath = Path.Combine(Server.MapPath(FileUploadPath), guid + "." + extension);
                var fileData = new byte[file.ContentLength];
                file.InputStream.Read(fileData, 0, file.ContentLength);
                using (var stream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
                {
                    stream.Write(fileData, 0, file.ContentLength);
                }
            }
            catch (Exception ex)
            {
                Log.Error("Unable to save uploaded file.", ex);
            }
        }
        protected string GetFileExtension(HttpPostedFileBase file)
        {
            var filename = Path.GetFileName(file.FileName);
            var lastPeriod = filename.LastIndexOf('.');
            return filename.Substring(lastPeriod + 1);
        }
        public Relation Convert(RelationModel model)
        {
            if (model.Relation == null)
            {
                return null;
            }
            var data = model.Relation;
            data.IsCurrent = model.IsCurrent;
            return data;
        }
        protected void AddToCache<T>(string key, T value)
        {
            MemoryCache.Default.Add(key, value, null);
        }
        protected T GetFromCache<T>(string key)
        {
            return (T)MemoryCache.Default.Get(key);
        }
        public User CurrentUser
        {
            get
            {
                var user = Session["LoggedInUser"] as User;
                if (user == null)
                {
                    user = userService.FindUserByGuid(User.Identity.Name);
                    Session["LoggedInUser"] = user;
                }
                return user;
            }
        }
        public string GeneralMessage
        {
            get
            {
                return (string)TempData["GeneralMessage"];
            }
            set
            {
                TempData["GeneralMessage"] = value;
            }
        }
        public JsonResult Failed()
        {
            return new JsonResult { Data = new { Success = false }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult Succeeded<T>(T model)
        {
            return new JsonResult { Data = new { Success = true, Data = model }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        public JsonResult Succeeded()
        {
            return new JsonResult { Data = new { Success = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}
