﻿
using CodexMVC.Helpers;
using CodexMVC.Models;
using Data;
using Data.Entities;
using Neo4jClient;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class APIController : BaseController
    {
        private readonly IActService actService;
        protected readonly IAgentService agentService;
        protected readonly IPlaceService placeService;
        protected readonly IArtefactService artefactService;
        protected readonly IUserService userService;
        protected readonly ICommonService commonService;
        public APIController(
            IAgentService _personService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IActService _actService,
            IUserService _userService,
            ICommonService _commonService
        )
        {
            agentService = _personService;
            placeService = _placeService;
            artefactService = _artefactService;
            actService = _actService;
            userService = _userService;
            commonService = _commonService;
        }
        public class NodeJson
        {
            public string id { get; set; }
            public string label { get; set; }
            public int x { get; set; }
            public int y { get; set; }
            public int size { get; set; }
        }
        public class EdgeJson
        {
            public string id { get; set; }
            public string source { get; set; }
            public string target { get; set; }
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public ActionResult UpdateAgentRating(decimal? rating, string agentGuid)
        {
            if (CurrentUser != null)
            {
                userService.UpdateAgentRating(CurrentUser.Guid, agentGuid, rating);
                // Invalidate the user agent ratings session cache.
                var key = "AgentController.UserAgentDetails[guid=" + agentGuid + "]";
                Session[key] = null;
            }
            return new JsonResult
            {
                Data = new { Success = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize(Roles = Permissions.CanAccessAdmin)]
        [HttpGet]
        public ActionResult CreatePlace(string name, string insideOfGuid, decimal? latitude, decimal? longitude)
        {
            var place = new Place
            {
                Name = name,
                Latitude = latitude,
                Longitude = longitude,
                Created = DateTimeOffset.Now,
                QuickAdded = true
            };
            placeService.SaveOrUpdate(place);
            if (insideOfGuid != null)
            {
                var container = new Place { Guid = insideOfGuid };
                placeService.InsideOf(place, container);
            }
            return new JsonResult
            {
                Data = new { Success = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize(Roles = Permissions.CanAccessAdmin)]
        [HttpGet]
        public ActionResult CreateArtefact(string name, string artefactTypeGuid, string madeByGuid, string locatedInGuid, string locatedAtGuid)
        {
            var artefact = new Artefact
            {
                Name = name,
                Created = DateTimeOffset.Now,
                QuickAdded = true
            };
            artefactService.SaveOrUpdate(artefact);
            if (artefactTypeGuid != null)
            {
                artefactService.IsA(new GenericRelationHyperNode<Artefact, Category>
                {
                    Source = artefact,
                    Destination = new Category { Guid = artefactTypeGuid }
                });
            }
            if (madeByGuid != null)
            {
                artefactService.MadeBy(new GenericRelationHyperNode<Artefact, Agent>
                {
                    Source = artefact,
                    Destination = new Agent { Guid = madeByGuid }
                });
            }
            if (locatedInGuid != null)
            {
                artefactService.LocatedIn(new GenericRelationHyperNode<Artefact, Artefact>
                {
                    Source = artefact,
                    Destination = new Artefact { Guid = locatedInGuid }
                });
            }
            if (locatedAtGuid != null)
            {
                artefactService.LocatedAt(new GenericRelationHyperNode<Artefact, Place>
                {
                    Source = artefact,
                    Destination = new Place { Guid = locatedAtGuid }
                });
            }
            return new JsonResult
            {
                Data = new { Success = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize(Roles = Permissions.CanAccessAdmin)]
        [HttpGet]
        public ActionResult CreateAgent(string firstname, string lastname, string fullname, string citizenshipGuid, string professionGuid, Sex? sex)
        {
            var agent = new Agent
            {
                Name = fullname,
                FirstName = firstname,
                LastName = lastname,
                Sex = sex,
                Created = DateTimeOffset.Now,
                QuickAdded = true
            };
            agentService.SaveOrUpdate(agent);
            if (citizenshipGuid != null)
            {
                agentService.CitizenOf(new GenericRelationHyperNode<Agent, Place>
                {
                    Source = agent,
                    Destination = new Place { Guid = citizenshipGuid }
                });
            }
            if (professionGuid != null)
            {
                agentService.IsA(new GenericRelationHyperNode<Agent, Category>
                {
                    Source = agent,
                    Destination = new Category { Guid = professionGuid }
                });
            }
            return new JsonResult
            {
                Data = new { Success = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public ActionResult UpdateArtefactRating(decimal? rating, string artefactGuid)
        {
            if (CurrentUser != null)
            {
                userService.UpdateArtefactRating(CurrentUser.Guid, artefactGuid, rating);
            }
            return new JsonResult
            {
                Data = new { Success = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GraphStudentOf(string agentGuid)
        {
            var attrs = commonService.FindOutgoingRelations<Agent>(agentGuid);
            var studentOf = attrs.Where(x => x.RelationToDestinationType == Label.Relationship.StudentOf);
            var agent = agentService.FindAgentByGuid(agentGuid);
            var nodes = new List<Agent>();
            nodes.Add(agent);
            nodes.AddRange(studentOf.Select(x => (Agent)x.Destination));
            var rnd = new Random();
            return new JsonResult
            {
                Data = new
                {
                    Success = true,
                    nodes = nodes.Select(node => new NodeJson
                    {
                        id = node.Guid,
                        x = rnd.Next(0, 10),
                        y = rnd.Next(0, 10),
                        label = node.FullName2
                    }),
                    edges = nodes.Select(node => new EdgeJson
                    {
                        id = node.Guid + "-edge",
                        source = agent.Guid,
                        target = node.Guid
                    })
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult FindImages(string query)
        {
            var images = resourceService.FindImagesByNameOrTag(query);
            var videos = resourceService.FindVideosByNameOrTag(query);
            return new JsonResult
            {
                Data = new
                {
                    Success = true,
                    Images = images.Select(x => new
                    {
                        Guid = x.Guid,
                        Name = x.Name,
                        Label = Label.Entity.Image,
                        Url = x.VirtualPath().Replace("~", "")
                    }),
                    Videos = videos.Select(x => new
                    {
                        Guid = x.Guid,
                        Name = x.Name,
                        Label = Label.Entity.Video,
                        Url = x.Uri
                    })
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult SearchTagJson(string search)
        {
            var results = actService.SearchTags(search);
            return new JsonResult
            {
                Data = new
                {
                    Results = results.Select(x => new
                    {
                        Text = x.Tag.Name,
                        Value = x.Tag.Guid
                    }),
                    Success = true
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult SearchAgentJson(string search)
        {
            var results = agentService.EntitySearch(search);
            return new JsonResult
            {
                Data = new
                {
                    Search = search,
                    Results = results.Agents,
                    Success = true
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult EntitySearch(string search)
        {
            var results = agentService.EntitySearch(search);
            var hasResults = results.Artefacts.Any() || results.Agents.Any() || results.Places.Any() || results.Categories.Any();
            return new JsonResult
            {
                Data = new
                {
                    Search = search,
                    HasResults = hasResults,
                    Results = results,
                    Success = true
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult ControlPanelSearch(string search)
        {
            var results = agentService.EntitySearch(search);
            var hasResults = results.Artefacts.Any() || results.Agents.Any() || results.Places.Any() || results.Categories.Any();
            return new JsonResult
            {
                Data = new
                {
                    Search = search,
                    HasResults = hasResults,
                    Results = results,
                    Success = true
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetArtistsList()
        {
            var artists = RazorHelper.SelectListOfAgentsByType(Label.Categories.Artist);
            artists.Insert(0, new System.Web.Mvc.SelectListItem { });
            return new JsonResult
            {
                Data = new
                {
                    Items = artists
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetCollectionsList()
        {
            var collections = RazorHelper.SelectListOfArtefactsByType(Label.Categories.Collection);
            collections.Insert(0, new System.Web.Mvc.SelectListItem { });
            return new JsonResult
            {
                Data = new
                {
                    Items = collections
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetArtistTypes()
        {
            var types = RazorHelper.SelectListOfDescendants(Label.Categories.Artist);
            types.Insert(0, new System.Web.Mvc.SelectListItem { });
            return new JsonResult
            {
                Data = new
                {
                    Items = types
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetStylesList()
        {
            var styles = RazorHelper.SelectListOfDescendants(Label.Categories.ArtMovement);
            styles.Insert(0, new System.Web.Mvc.SelectListItem { });
            return new JsonResult
            {
                Data = new
                {
                    Items = styles
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetSubjectsList()
        {
            var subjects = RazorHelper.SelectListOfDescendants(Label.Categories.ArtisticSubject);
            subjects.Insert(0, new System.Web.Mvc.SelectListItem { });
            return new JsonResult
            {
                Data = new
                {
                    Items = subjects
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetAgentsList()
        {
            var agents = RazorCache.AgentService.AllWithAttributes();
            agents.Insert(0, new System.Web.Mvc.SelectListItem { });
            return new JsonResult
            {
                Data = new
                {
                    Items = agents
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public async Task<JsonResult> GetDataSetsList()
        {
            var items = await dataSetService.GetListAsync();
            return new JsonResult
            {
                Data = new
                {
                    Items = items.Select(x => new SelectListItem
                    {
                        Text = x.Name,
                        Value = x.Guid
                    }).ToList()
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetCategoriesList()
        {
            var categories = RazorCache.CategoryService.AllWithAncestors();
            categories.Insert(0, new SelectListItem { });
            return new JsonResult
            {
                Data = new
                {
                    Items = categories
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetPlacesList()
        {
            var places = RazorCache.PlaceService.AllWithContainers();
            places.Insert(0, new SelectListItem { });
            return new JsonResult
            {
                Data = new
                {
                    Items = places
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }
        [HttpGet]
        public async Task<JsonResult> GetTimeline(string agentGuid)
        {
            var acts = await actService.FindActsByAgent(agentGuid);
            var timeline = acts.Select(x => new ActJson
            {
                Act = x.Act,
                RefersTo = x.EntityImages.Select(x2 => new EntityJsonModel
                {
                    Guid = x2.Entity.Guid,
                    Type = x2.Entity.GetType().UnderlyingSystemType.Name,
                    Name = x2.Entity.Name,
                    DescriptiveName = null
                }),
                AssociatedWith = new List<EntityJsonModel>(),
                Source = null,
                CreatedBy = null,
                Latitude = null,
                Longitude = null
            });
            return new JsonResult
            {
                Data = new
                {
                    Timeline = timeline
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public ActionResult AddToActContext(string actGuid, string entityGuid, string entityType)
        {
            actService.AddToActContext(actGuid, entityGuid, entityType);
            return new JsonResult { Data = new { Success = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpGet]
        public ActionResult RemoveFromActContext(string actGuid, string entityGuid, string entityType)
        {
            actService.RemoveFromActContext(actGuid, entityGuid, entityType);
            return new JsonResult { Data = new { Success = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpGet]
        public ActionResult AddBookmark(string guid, string type)
        {
            userService.AddBookmark(CurrentUser.Guid, type, guid);
            return new JsonResult { Data = new { Success = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpGet]
        public ActionResult RemoveBookmark(string guid, string type)
        {
            userService.DeleteBookmark(CurrentUser.Guid, type, guid);
            return new JsonResult { Data = new { Success = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpGet]
        public ActionResult AddSeenIt(string artefactGuid)
        {
            userService.AddSeenIt(CurrentUser.Guid, artefactGuid);
            return new JsonResult { Data = new { Success = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        [HttpGet]
        public ActionResult RemoveSeenIt(string artefactGuid)
        {
            userService.DeleteSeenIt(CurrentUser.Guid, artefactGuid);
            return new JsonResult { Data = new { Success = true }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
        //[HttpPost]
        //public JsonResult FindAllEntities()
        //{
        //    var results = agentService.FindAllEntities();
        //    return Succeeded(results);
        //}
        [HttpPost]
        public JsonResult FindCategoryHierarchy(string entityGuid)
        {
            var result = agentService.FindCategoryHierarchy(entityGuid);
            return Succeeded(result);
        }
        //[HttpPost]
        //public JsonResult AllTempNodes()
        //{
        //    var results = agentService.FindServedTest().Select(x =>
        //        new
        //        {
        //            Agent = x.Agent,
        //            Fact = x.Fact,
        //            Place = x.Place,
        //            Places = x.Places.Select(x2 => x2.Data).ToList(),
        //            Capacities = x.Capacities.Select(x2 => x2.Data).ToList(),
        //            Master = x.Master
        //        });
        //    return Succeeded(results);
        //}
        //[HttpPost]
        //public JsonResult FindServedTest()
        //{
        //    var results = agentService.FindServedTest().Select(x =>
        //        new
        //        {
        //            Agent = x.Agent,
        //            Fact = x.Fact,
        //            Place = x.Place,
        //            Places = x.Places.Select(x2 => x2.Data).ToList(),
        //            Capacities = x.Capacities.Select(x2 => x2.Data).ToList(),
        //            Master = x.Master
        //        });
        //    return Succeeded(results);
        //}
        [HttpPost]
        public JsonResult FindParentCategories(string categoryGuid)
        {
            var parents = agentService.FindParentCategories(categoryGuid);
            return Succeeded(parents);
        }
        //[HttpPost]
        //public JsonResult FindAllPlaceFacts(string placeGuid)
        //{
        //    var hyperNodes = agentService.FindAllPlaceFacts(placeGuid);
        //    return Succeeded(hyperNodes);
        //}
        //[HttpPost]
        //public JsonResult PlaceIs(string placeGuid, string categoryGuid)
        //{
        //    personService.AgentIs(placeGuid, categoryGuid);
        //    return Succeeded(personService.FindAllAttributes(placeGuid));
        //}
        //[HttpPost]
        //public JsonResult AgentIs(string agentGuid, string categoryGuid)
        //{
        //    personService.AgentIs(agentGuid, categoryGuid);
        //    return Succeeded(personService.FindAllAttributes(agentGuid));
        //}
        [HttpPost]
        public JsonResult FindAllSubcategories(string ancestorName)
        {
            var categories = agentService.AllSubcategoriesOf(ancestorName);
            return Succeeded(categories);
        }
        //[HttpPost]
        //public JsonResult QueryFacts()
        //{
        //    var facts = agentService.QueryFacts();
        //    return Succeeded(facts);
        //}
        //[HttpPost]
        //public JsonResult FindAllAttributes(string agentGuid)
        //{
        //    var attributes = personService.FindAllAttributes(agentGuid);
        //    return Succeeded(attributes);
        //}
        [HttpPost]
        public JsonResult FindAllRelationships(string agentGuid)
        {
            var attributes = agentService.FindOutgoingRelationships(agentGuid);
            var reverse = agentService.FindIncomingRelationships(agentGuid).ToList();
            attributes.AddRange(reverse);
            attributes.OrderBy(x => x.Relationship);
            return Succeeded(attributes);
        }
        [HttpPost]
        public JsonResult AllPeople()
        {
            var people = agentService.AllPeople();
            return Succeeded(people);
        }
        [HttpPost]
        public JsonResult AllPlaces()
        {
            var places = placeService.All();
            return Succeeded(places);
        }
        [HttpPost]
        public JsonResult AllArtworks()
        {
            var people = artefactService.All();
            return Succeeded(people);
        }
        [HttpPost]
        public JsonResult AllCategories()
        {
            var people = agentService.AllCategories();
            return Succeeded(people);
        }
        [HttpPost]
        public JsonResult CreatePlace(string name)
        {
            var user = Session["LoggedInUser"] as User;
            var place = placeService.SaveOrUpdate(new Place { Name = name });
            return Succeeded(place);
        }
        public JsonResult SaveAgent(Agent person)
        {
            agentService.SaveOrUpdate(person);
            return Succeeded(person);
        }
        [HttpPost]
        public JsonResult FindPlaces(string name)
        {
            var places = placeService.FindPlaces(name);
            return Succeeded(places);
        }
    }
}
