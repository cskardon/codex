﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Humanizer;
using System.Web.Mvc;
using Data;
using Services.Helpers;
using System.Threading.Tasks;
using System.Configuration;

namespace CodexMVC.Controllers
{
    public class TimeController : BaseController
    {

        private readonly IArtefactService artefactService;
        private readonly IAgentService agentService;

        private readonly IPlaceService placeService;
        public TimeController(

            IAgentService _agentService,

            IPlaceService _placeService,
            IArtefactService _artefactService
            )
        {

            agentService = _agentService;
            placeService = _placeService;
            artefactService = _artefactService;
        }

        [HttpGet]
        public ActionResult ImportHistoricEvents(int? counter)
        {
            var model = new ImportHistoricEventsModel();
            model.Start = counter;
            return View(model);
        }

        [HttpGet]
        public ActionResult Act(string guid)
        {
            var timeline = new List<NewAct> { actService.FindAct(guid) };

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });
            var model = new ViewTimeline
            {
                Mode = TimelineMode.ByAct,
                Year = timeline.Min(x => x.Act.Year.Value),
                EndYear = timeline.Max(x => x.Act.Year.Value),
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpPost]
        public ActionResult ImportHistoricEvents(ImportHistoricEventsModel model)
        {
            if (ModelState.IsValid)
            {
                var start = model.Start ?? 0;
                var counter = actService.ImportHistoricEvents(start, model.Rows);
                return RedirectToAction("ImportHistoricEvents", new { counter = counter });
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult AgentTypes()
        {
            var agentTypes = actService.GetNewActAgentTypes();
            return View(agentTypes);
        }

        [HttpGet]
        public ActionResult Places()
        {
            var places = actService.GetNewActPlaces();
            return View(places);
        }

        [HttpGet]
        public ActionResult Artworks()
        {
            var artefacts = actService.GetNewActArtefacts();
            return View(artefacts);
        }

        [HttpGet]
        public ActionResult People()
        {
            var agents = actService.GetNewActAgents();
            return View(agents);
        }

        [HttpGet]
        public ActionResult Themes()
        {
            var tags = actService.GetNewActThemes();
            return View(tags);
        }

        [HttpGet]
        public ActionResult Tags()
        {
            var tags = actService.GetNewActTags();
            return View(tags);
        }

        [HttpGet]
        // [OutputCache(Duration = Constants.DefaultOutputCacheDuration)]
        public async Task<ActionResult> List()
        {
            var years = await actService.GetYears();
            var model = new ViewTimeList
            {
                Years = years
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult Compare()
        {
            var model = new ViewCompareModel
            {
                Timelines = new Timelines()
            };
            return View(model);
        }

        private async Task<IEnumerable<NewAct>> GetCachedAgentTimeline(string agentGuid)
        {
            return await Cache.GetFromOrAddToCache("TimeController.GetCachedAgentTimeline[guid=" + agentGuid + "]", async () => await GetAgentTimeline(agentGuid), 10);
        }

        [HttpGet]
        public ActionResult ControlPanel()
        {
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> GetAgentActs(string guid)
        {
            var agent = agentService.FindAgentByGuid(guid);
            var acts = actService.FindNewActsByAgentAndSource(guid);
            // var acts = await GetAgentTimeline(guid);
            var stream = new ActStreamJson();
            stream.owner = new EntityJson
            {
                type = Label.Entity.Agent,
                name = agent.FullName2,
                guid = agent.Guid
            };
            ActsToJson(acts, stream);
            return new JsonResult
            {
                Data = stream,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        private void ActsToJson(IEnumerable<NewAct> acts, ActStreamJson stream)
        {
            var markdown = new MarkdownDeep.Markdown();
            try
            {
                foreach (var act in acts)
                {
                    var item = new ActItem();
                    item.act = new ActJson
                    {
                        year = act.Act.Year,
                        month = act.Act.Month,
                        day = act.Act.Day,
                        weekday = "",
                        text = markdown.Transform(act.ToHtmlJson())
                    };
                    item.agents = act.EntityImages.Where(x => x.Entity is Agent).Select(x => (Agent)x.Entity).Select(a => new EntityJson
                    {
                        guid = a.Guid,
                        name = a.FullName2
                    }).ToList();
                    item.places = act.EntityImages.Where(x => x.Entity is Place).Select(x => (Place)x.Entity).Select(x => new EntityJson
                    {
                        guid = x.Guid,
                        name = x.Name,
                        latitude = x.Latitude,
                        longitude = x.Longitude
                    }).ToList();
                    item.tags = new List<EntityJson>();
                    if (act.Tags != null)
                    {
                        item.tags.AddRange(act.Tags.Select(tag => new EntityJson
                        {
                            guid = tag.Guid,
                            name = tag.Name
                        }).ToList());
                    }
                    if (act.TagValues != null)
                    {
                        item.tags.AddRange(act.TagValues.Select(tag => new EntityJson
                        {
                            guid = tag.Tag.Guid,
                            name = tag.Tag.Name,
                            parents = tag.Parents != null && tag.Parents.Any() ? tag.Parents.Select(parent => new EntityJson
                            {
                                guid = parent.Guid,
                                name = parent.Name
                            }) : null
                        }).ToList());
                    }
                    if (act.StatedBy != null && act.Source != null)
                    {
                        item.source = new SourceJson
                        {
                            type = act.Act.Source,
                            agent = new EntityJson
                            {
                                guid = act.StatedBy.Guid,
                                name = act.StatedBy.FullName2
                            },
                            artefact = new EntityJson
                            {
                                guid = act.Source.Guid,
                                name = act.Source.Name
                            },
                            location = (string)null
                        };
                    }
                    stream.items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        [HttpGet]
        public async Task<JsonResult> GetPlaceActs(string guid)
        {
            var Place = placeService.FindPlaceByGuid(guid);
            var acts = (await actService.FindNewActsByPlaceAsync(guid)).Where(x => x.Act.Year.HasValue);
            var stream = new ActStreamJson();
            stream.owner = new EntityJson
            {
                type = Label.Entity.Place,
                name = Place.Name,
                guid = Place.Guid
            };
            ActsToJson(acts, stream);
            return new JsonResult
            {
                Data = stream,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public JsonResult GetTagActs(string guid)
        {
            var Tag = actService.FindTagByGuid(guid);
            var acts = actService.FindNewActsByTag(guid).Where(x => x.Act.Year.HasValue);
            var stream = new ActStreamJson();
            stream.owner = new EntityJson
            {
                type = Label.Entity.Tag,
                name = Tag.Name,
                guid = Tag.Guid
            };
            ActsToJson(acts, stream);
            return new JsonResult
            {
                Data = stream,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        [HttpGet]
        public async Task<JsonResult> GetDateRange(int from, int to)
        {
            var acts = await GetYearTimelineRange(from, to);
            var stream = new ActStreamJson();
            stream.owner = new EntityJson
            {
                type = "DateRange",
                name = from + "-" + to,
                guid = Guid.NewGuid().ToString()
            };
            ActsToJson(acts, stream);
            return new JsonResult
            {
                Data = stream,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public class ActJson
        {
            public int? year { get; set; }
            public int? month { get; set; }
            public int? day { get; set; }
            public string weekday { get; set; }
            public string text { get; set; }
        }
        public class EntityJson
        {
            public string type { get; set; }
            public string guid { get; set; }
            public string name { get; set; }
            public decimal? latitude { get; set; }
            public decimal? longitude { get; set; }
            public IEnumerable<EntityJson> parents { get; set; }
        }
        public class SourceJson
        {
            public string type { get; set; }
            public EntityJson agent = new EntityJson();
            public EntityJson artefact = new EntityJson();
            public string location { get; set; }
        }
        public class ActStreamJson
        {
            public EntityJson owner { get; set; }
            public List<ActItem> items = new List<ActItem>();
        }
        public class ActItem
        {
            public EntityJson owner { get; set; }
            public ActJson act { get; set; }
            public List<EntityJson> agents { get; set; }
            public List<EntityJson> tags { get; set; }
            public List<EntityJson> places { get; set; }
            public SourceJson source = new SourceJson();
        }

        [HttpPost]
        public async Task<ActionResult> Compare(ViewCompareModel model)
        {
            if (ModelState.IsValid)
            {
                var agent1 = agentService.FindAgentByGuid(model.ArtistGuid1);
                var acts1 = await GetAgentTimeline(model.ArtistGuid1);

                var agent2 = agentService.FindAgentByGuid(model.ArtistGuid2);
                var acts2 = await GetAgentTimeline(model.ArtistGuid2);

                var years1 = acts1.Select(x => x.Act.TimelineDate());
                var years2 = acts2.Select(x => x.Act.TimelineDate());
                var years = years1.Concat(years2).Distinct().OrderBy(x => x);

                var timelines = new Timelines
                {
                    Time = new Dictionary<string, List<Timeline>>()
                };
                foreach (var year in years)
                {
                    var timeline1 = new Timeline
                    {
                        Agent = agent1,
                        NewActs = acts1.Where(x => x.Act.TimelineDate() == year).ToList()
                    };
                    var timeline2 = new Timeline
                    {
                        Agent = agent2,
                        NewActs = acts2.Where(x => x.Act.TimelineDate() == year).ToList()
                    };
                    timelines.Time.Add(year, new List<Timeline> { timeline1, timeline2 });
                }
                model.Timelines = new Timelines();
                model.Timelines = timelines;

                var markers1 = RazorHelper.GetTimelineMarkers(acts1);
                var markers2 = RazorHelper.GetTimelineMarkers(acts2);
                var heatmap1 = RazorHelper.GetTimelineHeatMap(markers1);
                var heatmap2 = RazorHelper.GetTimelineHeatMap(markers2);

                model.Agent1 = agent1;
                model.Agent2 = agent2;
                model.Markers1 = markers1;
                model.Markers2 = markers2;
                model.Heatmap1 = heatmap1;
                model.Heatmap2 = heatmap2;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Slideshow(int year)
        {
            var timeline = GetYearTimeline(year);

            var model = new TimelineJS
            {
                Visitors = null,
                Name = year.ToString(),
                Timeline = timeline
            };

            return View("~/Views/Agent/Timeline.cshtml", model);
        }

        [HttpGet]
        public async Task<ActionResult> SlideshowRange(int startYear, int endYear)
        {
            var timeline = await GetYearTimelineRange(startYear, endYear);

            var model = new TimelineJS
            {
                Visitors = null,
                Name = startYear + " to " + endYear,
                Timeline = timeline
            };

            return View("~/Views/Agent/Timeline.cshtml", model);
        }

        [HttpGet]
        public ActionResult ViewDetail(int year)
        {
            var timeline = GetYearTimeline(year);

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });

            var model = new ViewTimeline
            {
                Year = year,
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Latest()
        {
            var timeline = actService.FindNewActsLatest().Where(x => x.Act.Year.HasValue).ToList();
            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });
            var model = new ViewTimeline
            {
                Mode = TimelineMode.ByLatest,
                Year = timeline.Any() ? timeline.Min(x => x.Act.Year.Value) : 0,
                EndYear = timeline.Any() ? timeline.Max(x => x.Act.Year.Value) : 0,
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpGet]
        public ActionResult Charts()
        {
            var data = actService.TagCharts();
            return View(data);
        }

        [HttpGet]
        public JsonResult CooccurencesData()
        {
            var data = actService.CooccurencesData();
            var matrix = new MatrixNodes { nodes = new List<NodeItem>(), links = new List<List<NodeLink>>() };
            var i = 0;
            var agentIndexes = data.Select(x => x.Agent.Guid).Distinct().ToList();
            // var categoryIndexes = data.Where(x => x.Attributes.Any()).Select(x => x.Attributes.First().Guid).Distinct().ToList();
            var categoryIndexes = data.Where(x => x.Attributes.Any()).SelectMany(x => x.Attributes.Select(x2 => x2.Guid)).Distinct().ToList();
            var placeIndexes = data.Where(x => x.BirthPlace != null).Select(x => x.BirthPlace.Guid).Distinct().ToList();
            var birthDateIndexes = data.Where(x => x.BirthYear.HasValue).Select(x => (int)(x.BirthYear / 10) * 10).Distinct().ToList();
            foreach (var row in data)
            {
                var source = agentIndexes.IndexOf(row.Agent.Guid);
                matrix.nodes.Add(new NodeItem
                {
                    guid = row.Agent.Guid,
                    name = row.Agent.FullName2,
                    index = source,
                    // group = row.Attributes.Any() ? categoryIndexes.IndexOf(row.Attributes.First().Guid) : 0
                    group = row.Attributes.Any() ? row.Attributes.Sum(x => categoryIndexes.IndexOf(x.Guid)) : 0,
                    birthPlace = row.BirthPlace != null ? placeIndexes.IndexOf(row.BirthPlace.Guid) : 0,
                    birthDate = row.BirthYear.HasValue ? birthDateIndexes.IndexOf((int)(row.BirthYear / 10)) : 0
                });
                matrix.links.Add(row.Links.Select((CooccurenceLink x, int j) => new NodeLink
                {
                    name = x.Agent.FullName2,
                    source = source,
                    target = agentIndexes.IndexOf(x.Agent.Guid),
                    value = x.ActCount
                }).ToList());
                i++;
            }
            return new JsonResult { Data = matrix, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public class MatrixNodes
        {
            public List<NodeItem> nodes { get; set; }
            public List<List<NodeLink>> links { get; set; }
        }
        public class NodeItem
        {
            public string name;
            public int index;
            public int group;
            public int birthPlace;
            public int birthDate;
            public string guid;
        }
        public class NodeLink
        {
            public int target;
            public int source;
            public string name { get; set; }
            public int value { get; set; }
        }
        [HttpGet]
        public ActionResult Cooccurences()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ByTag(string guid)
        {
            var tag = actService.FindTagByGuid(guid);
            var timeline = actService.FindNewActsByTag(guid).Where(x => x.Act.Year.HasValue);
            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });
            var model = new ViewTimeline
            {
                Mode = TimelineMode.ByTag,
                TagEntity = tag,
                Tag = tag.Name.Trim(),
                Year = timeline.Min(x => x.Act.Year.Value),
                EndYear = timeline.Max(x => x.Act.Year.Value),
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpGet]
        public ActionResult ByTagCategory(string guid)
        {
            var tagCategory = categoryService.Find(guid);
            var timeline = actService.FindNewActsByTagCategory(guid).Where(x => x.Act.Year.HasValue);
            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });
            var model = new ViewTimeline
            {
                Mode = TimelineMode.ByTagCategory,
                TagCategory = tagCategory,
                Year = timeline.Min(x => x.Act.Year.Value),
                EndYear = timeline.Max(x => x.Act.Year.Value),
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpGet]
        public ActionResult ByAgents(string agentGuids)
        {
            var guids = agentGuids.Split(',');
            IEnumerable<NewAct> timelines = new List<NewAct>();
            foreach (var guid in guids)
            {
                var agent = agentService.FindAgentByGuid(guid);
                var timeline = actService.FindActsByAgent(guid).Result.Where(x => x.Act.Year.HasValue);
                foreach (var act in timeline)
                {
                    act.Actors = new List<IEntity> { agent };
                }
                timelines = timelines.Concat(timeline);
            }
            var group = timelines.GroupBy(x => x.Act.Guid);
            var combined = group.Select(x => new NewAct
            {
                Actors = x.SelectMany(x2 => x2.Actors).ToList(),
                Act = x.First().Act,
                Attachments = x.First().Attachments,
                Attributes = x.First().Attributes,
                Bookmarked = x.First().Bookmarked,
                EntityImages = x.First().EntityImages,
                HasSourceGuid = x.First().HasSourceGuid,
                InItinerary = x.First().InItinerary,
                Source = x.First().Source,
                StatedBy = x.First().StatedBy,
                SubsetOf = x.First().SubsetOf,
                Tags = x.First().Tags,
                TagValues = x.First().TagValues
            });
            combined = combined.Distinct(new NewActComparer()).ToList();

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(combined);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });

            var model = new ViewTimeline
            {
                Mode = TimelineMode.ByAgents,
                Year = combined.Min(x => x.Act.Year.Value),
                EndYear = combined.Max(x => x.Act.Year.Value),
                Timeline = combined.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpGet]
        public ActionResult ByAgent(string guid)
        {
            var agent = agentService.FindAgentByGuid(guid);
            // var timeline = actService.FindNewActsByAgent(guid).Result.Where(x => x.Act.Year.HasValue);
            var timeline = actService.FindNewActsByAgentAndSource(guid).Where(x => x.Act.Year.HasValue);
            timeline = timeline.Distinct(new NewActComparer()).ToList();

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });

            var personalDetails = agentService.FindPersonalDetailsAsync(guid);
            var connections = agentService.FindOutgoingWithArtworksAsync(guid);

            var model = new ViewTimeline
            {
                Selectors = new List<IEntity> { agent },
                Agent = agent,
                PersonalDetails = personalDetails,
                Connections = connections,
                Mode = TimelineMode.ByAgent,
                Year = timeline.Min(x => x.Act.Year.Value),
                EndYear = timeline.Max(x => x.Act.Year.Value),
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpGet]
        public ActionResult ByAgentType(string guid)
        {
            var agentType = categoryService.Find(guid);
            var timeline = actService.FindNewActsByAgentType(guid).Result.Where(x => x.Act.Year.HasValue);

            var outgoing = commonService.FindOutgoingRelationsByAgentGuid(guid).Where(x => x.Relation.Year.HasValue);
            IEnumerable<NewAct> outgoingActs = null;
            Parallel.Invoke(() =>
            {
                outgoingActs = commonService.ConvertOutgoingToActs(outgoing);
            });
            var incoming = commonService.FindIncomingRelationsByAgentGuid(guid).Where(x => x.Relation.Year.HasValue);
            IEnumerable<NewAct> incomingActs = null;
            Parallel.Invoke(() =>
            {
                incomingActs = commonService.ConvertIncomingToActs(incoming);
            });
            timeline = timeline.Concat(outgoingActs).ToList();
            timeline = timeline.Concat(incomingActs).ToList();
            timeline = timeline.Distinct(new NewActComparer()).ToList();

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });
            var model = new ViewTimeline
            {
                AgentType = agentType,
                Mode = TimelineMode.ByAgentType,
                Year = timeline.Min(x => x.Act.Year.Value),
                EndYear = timeline.Max(x => x.Act.Year.Value),
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpGet]
        public ActionResult ByPlace(string guid)
        {
            var place = placeService.FindPlaceByGuid(guid);
            var timeline = actService.FindNewActsByPlaceAsync(guid).Result.Where(x => x.Act.Year.HasValue);

            //var outgoing = commonService.FindOutgoingRelationsByPlaceGuid(guid).Where(x => x.Relation.Year.HasValue);
            //IEnumerable<NewAct> outgoingActs = null;
            //Parallel.Invoke(() =>
            //{
            //    outgoingActs = commonService.ConvertOutgoingToActs(outgoing);
            //});
            //var incoming = commonService.FindIncomingRelationsByPlaceGuid(guid).Where(x => x.Relation.Year.HasValue);
            //IEnumerable<NewAct> incomingActs = null;
            //Parallel.Invoke(() =>
            //{
            //    incomingActs = commonService.ConvertIncomingToActs(incoming);
            //});
            //timeline = timeline.Concat(outgoingActs).ToList();
            //timeline = timeline.Concat(incomingActs).ToList();
            timeline = timeline.Distinct(new NewActComparer()).ToList();

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });
            var model = new ViewTimeline
            {
                Place = place,
                Mode = TimelineMode.ByPlace,
                Year = timeline.Min(x => x.Act.Year.Value),
                EndYear = timeline.Max(x => x.Act.Year.Value),
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpGet]
        public ActionResult ByArtefact(string guid)
        {
            var artefact = artefactService.FindArtefactDetailBasic(guid);

            var timeline = actService.FindNewActsByArtefact(guid).Result.Where(x => x.Act.Year.HasValue);

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });
            var model = new ViewTimeline
            {
                Artefact = artefact,
                Mode = TimelineMode.ByArtefact,
                Year = timeline.Min(x => x.Act.Year.Value),
                EndYear = timeline.Max(x => x.Act.Year.Value),
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("ViewDetail", model);
        }

        [HttpGet]
        public async Task<ActionResult> Range(int startYear, int endYear)
        {
            var timeline = await GetYearTimelineRange(startYear, endYear);

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });

            var model = new ViewTimeline
            {
                Year = startYear,
                EndYear = endYear,
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };

            return View("ViewDetail", model);
        }
        private readonly bool EnableTimelineRangeCache = bool.Parse(ConfigurationManager.AppSettings["EnableTimelineRangeCache"]);
        private async Task<List<NewAct>> GetYearTimelineRange(int startYear, int endYear)
        {
            var KEY = "TimeController.GetYearTimelineRange[startYear=" + startYear.ToString() + "&endYear=" + endYear + "]";
            var timelineRange = Cache.GetFromCache<List<NewAct>>(KEY);
            if (false == EnableTimelineRangeCache || timelineRange == null)
            {
                timelineRange = actService.FindNewActsByDates(startYear.ToString(), endYear.ToString());
                timelineRange = timelineRange.Distinct(new NewActComparer()).ToList();

                Cache.AddToCache(KEY, timelineRange, Constants.DefaultOutputCacheDuration);
            }
            return timelineRange;
        }

        private List<NewAct> GetYearTimeline(int year)
        {
            var KEY = "TimeController.GetYearTimeline[year=" + year.ToString() + "]";
            var timeline = Cache.GetFromCache<List<NewAct>>(KEY);
            if (timeline == null)
            {
                timeline = actService.FindNewActsByDates(year.ToString(), null);

                var outgoing = commonService.FindOutgoingRelations(year.ToString());
                IEnumerable<NewAct> outgoingActs = null;
                Parallel.Invoke(() =>
                {
                    outgoingActs = commonService.ConvertOutgoingToActs(outgoing);
                });

                var incoming = commonService.FindIncomingRelations(year.ToString());
                IEnumerable<NewAct> incomingActs = null;
                Parallel.Invoke(() =>
                {
                    incomingActs = commonService.ConvertIncomingToActs(incoming);
                });

                timeline = timeline.Concat(outgoingActs).ToList();
                timeline = timeline.Concat(incomingActs).ToList();

                timeline = timeline.Distinct(new NewActComparer()).ToList();

                Cache.AddToCache(KEY, timeline);
            }

            return timeline;
        }
    }
}