﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class BookController : BaseController
    {
        private readonly IAgentService agentService;
        public BookController(
            IAgentService _agentService
        )
        {
            agentService = _agentService;
        }

        [HttpGet]
        public ActionResult Landucci()
        {
            var landucciGuid = "65a202f8-f3af-4dd4-abb1-e93a36d0f88b";
            var agent = agentService.FindAgentByGuid(landucciGuid);            
            var timeline = actService.FindNewActsByAgentAndSource(landucciGuid).Where(x => x.Act.Year.HasValue);
            timeline = timeline.Distinct(new NewActComparer()).ToList();

            IEnumerable<IGrouping<string, MarkerJson>> markers = null;
            Parallel.Invoke(() =>
            {
                markers = RazorHelper.GetTimelineMarkers(timeline);
            });
            IEnumerable<HeatMapJson> heatmap = null;
            Parallel.Invoke(() =>
            {
                heatmap = RazorHelper.GetTimelineHeatMap(markers);
            });

            var personalDetails = agentService.FindPersonalDetailsAsync(landucciGuid);
            var connections = agentService.FindOutgoingWithArtworksAsync(landucciGuid);

            var model = new ViewTimeline
            {
                Selectors = new List<IEntity> { agent },
                Agent = agent,
                PersonalDetails = personalDetails,
                Connections = connections,
                Mode = TimelineMode.ByAgent,
                Year = timeline.Min(x => x.Act.Year.Value),
                EndYear = timeline.Max(x => x.Act.Year.Value),
                Timeline = timeline.Distinct(new NewActComparer()).ToList(),
                Markers = markers,
                Heatmap = heatmap
            };
            return View("Landucci", model);
        }
    }
}