﻿using Data;
using Data.Entities;
using Neo4jClient;
using Neo4jClient.Cypher;
using Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class HomeController : BaseController
    {
        [HttpGet]
        public ActionResult Error()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult News()
        {
            return View();
        }
    }
}
