﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Data.Entities;
using Microsoft.Web.WebPages.OAuth;
using Services;
using Services.Common;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace CodexMVC.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IArtefactService artefactService;
        private readonly ICommonService commonService;
        public AccountController(
            IArtefactService _artefactService,
            ICommonService _commonService
        )
        {
            artefactService = _artefactService;
            commonService = _commonService;
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public ActionResult Notifications()
        {
            var notifications = userService.FindArtworkNotifications(CurrentUser.Guid, 30);
            return PartialView("~/Views/Shared/Partials/Notifications.cshtml", notifications);
        }

        [HttpGet]
        public ActionResult LoginModal()
        {
            var model = new LoginModel();
            return PartialView("~/Views/Shared/Partials/Login.cshtml", model);
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public ActionResult Achievements()
        {
            return View();
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public ActionResult Assistant()
        {
            var model = new AssistantModel();
            return View(model);
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpPost]
        public ActionResult Assistant(AssistantModel model)
        {
            if (ModelState.IsValid)
            {
                var userGuid = CurrentUser.Guid;
                foreach (var styleGuid in model.Style)
                {
                    userService.InterestedIn(userGuid, styleGuid);
                }
                return RedirectToAction("AssistantComplete");
            }
            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public void ExternalLogin(string provider)
        {
            OAuthWebSecurity.RequestAuthentication(provider, Url.Action("ExternalLoginCallback"));
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ExternalLoginCallback()
        {
            var result = OAuthWebSecurity.VerifyAuthentication();
            if (result.IsSuccessful == false)
            {
                return RedirectToAction("Error", "Account");
            }
            var userNode = userService.GetUserWithPermissionsFromOAuth(result.UserName);
            return HandleLogin(userNode);
        }

        [HttpGet]
        public ActionResult History()
        {
            var history = RazorHelper.GetHistory();
            var model = new ViewHistory
            {
                Items = history
            };
            return View(model);
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public ActionResult Seen()
        {
            var artefacts = artefactService.FindSeenArtefacts(CurrentUser.Guid);
            var model = new ViewSeen
            {
                User = CurrentUser,
                Artefacts = artefacts,
            };
            return View(model);
        }

        [Authorize(Roles = Permissions.AnyPublicAction)]
        [HttpGet]
        public ActionResult Favourites()
        {
            var userGuid = CurrentUser.Guid;
            var newActs = userService.FindBookmarkedActs(userGuid);
            var agents = userService.FindBookmarkedAgents(userGuid);
            var artefacts = artefactService.FindBookmarkedArtefacts(userGuid);
            var artefactsSeen = artefactService.FindSeenArtefacts(userGuid);
            var similar = artefactService.SimilarArtefactsByUser(userGuid);
            var recommended = artefactService.RecommendedArtefactsByUser(userGuid);
            var model = new ViewFavourites
            {
                User = CurrentUser,
                NewActs = newActs,
                Agents = agents,
                Artefacts = artefacts,
                ArtefactsSeen = artefactsSeen,
                Similar = similar,
                Recommended = recommended
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult Join()
        {
            var model = new JoinModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult Join(JoinModel model)
        {
            if (ModelState.IsValid)
            {
                userService.Join(model.Email, model.Password, model.FirstName, model.LastName, model.Age, model.Profession);
                return RedirectToAction("JoinComplete");
            }
            return View(model);
        }
        [Authorize]
        [HttpGet]
        public ActionResult ResetPassword()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ResetPasswordFromEmail(string username, string tokenGuid)
        {
            var authorised = userService.IsResetPasswordAuthorised(username, tokenGuid);
            if (false == authorised)
            {
                return RedirectToAction("ResetPasswordFromEmailRejected", "Account");
            }
            var model = new ResetPasswordFromEmailModel
            {
                UserName = username,
                TokenGuid = tokenGuid
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult ResetPasswordFromEmail(ResetPasswordFromEmailModel model)
        {
            if (ModelState.IsValid)
            {
                var result = userService.ResetPasswordFromEmail(model.UserName, model.TokenGuid, model.NewPassword);
                if (result == ResetPasswordFromEmailResult.Success)
                {
                    return RedirectToAction("ResetPasswordComplete", "Account");
                }
                else
                {
                    return RedirectToAction("ResetPasswordFromEmailError", "Account");
                }
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult ResetPasswordComplete()
        {
            return View();
        }
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            var model = new ForgotPasswordModel();
            return View(model);
        }
        [HttpGet]
        public ActionResult ForgotPasswordComplete()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgotPassword(ForgotPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var result = userService.ForgotPassword(model.Email);
                if (result == ForgotPasswordResult.Success)
                {
                    return RedirectToAction("ForgotPasswordComplete", "Account");
                }
                else
                {
                    var message = result == ForgotPasswordResult.UserNotFound
                        ? "Sorry, there was a technical issue. Please try again later." :
                        result == ForgotPasswordResult.EmailAlreadySentWithinWindow
                        ? "Please check your inbox as we have already sent the reset password email."
                        : "Sorry, there was a technical issue. Please try again later.";
                    ModelState.AddModelError("", message);
                }
            }
            return View(model);
        }
        [Authorize]
        [HttpPost]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                userService.ResetPassword(User.Identity.Name, model.NewPassword);
                SignOut();
                return RedirectToAction("ResetPasswordComplete");
            }
            return View(model);
        }
        private void SignOut()
        {
            if (CurrentUser != null)
            {
                Log.Info(string.Format("User {0} logged out.", CurrentUser.Guid));
                commonService.AuditLogout();
            }
            FormsAuthentication.SignOut();
            Session.Abandon();
        }

        [HttpGet]
        public ActionResult JoinComplete()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View(new LoginModel());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                var node = userService.GetUserWithPermissions(model.Username, model.Password);
                if (node == null)
                {
                    ModelState.AddModelError("", "Username or password is incorrect.");
                }
                else
                {
                    return HandleLogin(node, model);
                }
            }
            return View(model);
        }

        private ActionResult HandleLogin(UserNode node)
        {
            SetCredentials(node);
            commonService.AuditLogin(node.User.Guid, Request.UserHostAddress);
            return RedirectToAction("List", "Agent");
        }

        private ActionResult HandleLogin(UserNode node, LoginModel model)
        {
            SetCredentials(node);
            //commonService.AuditLogin(node.User.Guid, Request.UserHostAddress);
            if (string.IsNullOrEmpty(model.ReturnUrl) && Request.UrlReferrer != null)
            {
                model.ReturnUrl = Server.UrlEncode(Request.UrlReferrer.PathAndQuery);
            }

            if (Url.IsLocalUrl(model.ReturnUrl) && !string.IsNullOrEmpty(model.ReturnUrl))
            {
                return Redirect(model.ReturnUrl);
            }
            return RedirectToAction("List", "Agent");
        }

        [Authorize]
        [HttpGet]
        public ActionResult Logout()
        {
            SignOut();
            return RedirectToAction("Login");
        }
        private void SetCredentials(UserNode node)
        {
            Log.Info(string.Format("User {0} logged in.", node.User.Guid));

            var permissions = string.Join("|", node.Permissions);

            var serialiseModel = new CustomPrincipalSerialiseModel();
            serialiseModel.Guid = node.User.Guid;
            serialiseModel.FirstName = node.User.FirstName;
            serialiseModel.StayLoggedIn = false;
            serialiseModel.Permissions = permissions;

            var serialiser = new JavaScriptSerializer();
            var userData = serialiser.Serialize(serialiseModel);

            var ticket = new FormsAuthenticationTicket(
                1,
                node.User.Guid,
                DateTime.Now,
                DateTime.Now.AddMinutes(15),
                false,
                userData
            );

            var encTicket = FormsAuthentication.Encrypt(ticket);
            var cookie = new HttpCookie(
                FormsAuthentication.FormsCookieName,
                encTicket
            );

            Response.Cookies.Add(cookie);
        }
    }
}