﻿using CodexMVC.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    public class MotifController : BaseController
    {
        protected readonly IArtefactService artefactService;
        protected readonly ICategoryService categoryService;
        public MotifController(
            IArtefactService _artefactService,
            ICategoryService _categoryService
        )
        {
            artefactService = _artefactService;
            categoryService = _categoryService;
        }
        [HttpGet]
        public ActionResult List()
        {
            var motifs = artefactService.AllMotifsWithArtefacts();
            var model = new ViewMotifList
            {
                Motifs = motifs
            };
            return View(model);
        }
        [HttpGet]
        public ActionResult ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }
            var motif = categoryService.Find(guid);
            if (motif == null)
            {
                return new HttpNotFoundResult();
            }
            var containerWithArtefacts = artefactService.FindContainerWithArtefactsByMotif(guid);
            var model = new ViewMotifPublic
            {
                Motif = motif,
                ContainerWithArtefacts = containerWithArtefacts
            };
            return View(model);
        }
    }
}