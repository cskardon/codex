﻿using CodexMVC.Models;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Controllers
{
    
    public class ExhibitionController : BaseController
    {
        private readonly IArtefactService artefactService;

        public ExhibitionController(
            IArtefactService _artefactService
            )
        {
            artefactService = _artefactService;
        }

        [HttpGet]
        public ActionResult List(string mode = null)
        {

            var exhibitions = artefactService.AllExhibitions();
            var model = new ViewExhibitionList
            {
                Exhibitions = exhibitions
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult ViewDetail(string guid)
        {
            if (guid == null)
            {
                return new HttpNotFoundResult();
            }
            var exhibition = artefactService.FindExhibition(guid);
            if (exhibition == null)
            {
                return new HttpNotFoundResult();
            }
            var model = new ViewExhibitionModel
            {
                Exhibition = exhibition
            };

            return View(model);

        }
    }
}