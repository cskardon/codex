﻿using CodexMVC.App_Start;
using Data.Entities;
using log4net;
using System;
using System.Security.Principal;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;

namespace CodexMVC
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected readonly ILog Log = LogManager.GetLogger("DefaultLogger");
        protected void Application_Start()
        {
            log4net.Config.XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();
            var routes = RouteTable.Routes;

            routes.MapRoute(
                "API"
                , "API/{action}/{id}"
                , new { controller = "API", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                "friendlyName",
                "Agent/Artist/{name}",
                new { controller = "Agent", action = "Artist" },
                namespaces: new[] { "CodexMVC.Controllers" }
            );

            routes.MapRoute(
                "public",
                "{controller}/{action}/{guid}",
                new { action = "ViewDetail", guid = UrlParameter.Optional },
                namespaces: new[] { "CodexMVC.Controllers" }
            );

            MapperConfig.RegisterMappers();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            DependencyInjectionConfig.CreateAndRegisterContainer();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            // RegisterStaticProperties();
        }
        //public static IStanfordService StanfordService = null;
        protected void RegisterStaticProperties()
        {
            //StanfordService = new StanfordService();
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            var url = Request != null ? Request.RawUrl : "NO_URL_FOUND";
            Log.Fatal(new { Message = "Fatal exception caught at the global level.", URL = url }, exception);
            //Server.ClearError();
            Response.Redirect("/Home/Error");
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            ThreadContext.Properties["addr"] = Request.UserHostAddress;
        }
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                var guid = authTicket.Name;

                var serialiser = new JavaScriptSerializer();
                var serialiseModel = serialiser.Deserialize<CustomPrincipalSerialiseModel>(authTicket.UserData);

                var identity = new GenericIdentity(guid);
                var roles = serialiseModel.Permissions.Split('|');
                var principal = new CustomPrincipal(identity, roles);

                principal.Guid = serialiseModel.Guid;
                principal.IPAddress = Request.UserHostAddress;
                principal.FirstName = serialiseModel.FirstName;
                principal.StayLoggedIn = serialiseModel.StayLoggedIn;
                principal.Permissions = serialiseModel.Permissions;

                Context.User = principal;
            }
        }
    }
}