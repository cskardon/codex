﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodexMVC.Areas.Admin.Models
{
    public class EditDataSetModel
    {
        public string Guid { get; set; }
        
        /// <summary>
        /// E.g., "Deaths from the plague in Italy", "Weather in Florence", "Wind speed", etc.
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// E.g., "Number of People", "Weather Event", "kms/hour", etc.
        /// </summary>
        [Required]
        public string UnitOfMeasure { get; set; }

        public string Description { get; set; }
        
        /// <summary>
        /// [:about]->(subject:Tag)
        /// </summary>
        [Required]
        public string AboutGuid { get; set; }

        /// <summary>
        /// [:subset_of]->(parent:DataSet)
        /// </summary>
        public string SubsetOfGuid { get; set; }
    }
    /**
     * A DataPoint represents some value that is minimally qualified by a containing dataset, a place, and a time.
     */
    public class EditDataPointModel
    {
        public string Guid { get; set; }

        [Required]
        public string Value { get; set; }

        public string Description { get; set; }

        [Required]
        /// <summary>
        /// [:part_of]->(set:DataSet)
        /// </summary>
        public string PartOfGuid { get; set; }

        /// <summary>
        /// [:stated_in]->(book:Artefact)
        /// </summary>
        public string StatedInGuid { get; set; }

        /// <summary>
        /// Description of where the fact was stated. If 'StatedInGuid' is supplied the 'Citation' will be a reference within
        /// the book, but if there is no 'StatedInGuid' it is just a free-form text reference to the source.
        /// </summary>
        public string Citation { get; set; }

        /// <summary>
        /// [:stated_by]->(agent:Agent)
        /// </summary>
        public string StatedByGuid { get; set; }

        [Required]
        /// <summary>
        /// [:at]->(place:Place)
        /// </summary>
        public string AtGuid { get; set; }

        [Required]
        /// <summary>
        /// AD; BC; default is AD
        /// </summary>
        public string Milennium { get; set; }

        [Required]
        /// <summary>
        /// The minimal time reference.
        /// </summary>
        public int Year { get; set; }

        public int? Month { get; set; }

        public int? Day { get; set; }

        public int? Hour { get; set; }

        public int? Minute { get; set; }

        public EditDataPointModel()
        {
            Milennium = "AD";
        }
    }
}