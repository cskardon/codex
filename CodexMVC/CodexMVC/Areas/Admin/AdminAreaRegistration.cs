﻿using System.Web.Mvc;

namespace CodexMVC.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Admin_default",
                "Admin/{controller}/{action}",
                new { action = "List", id = UrlParameter.Optional },
                namespaces: new[] { "CodexMVC.Areas.Admin.Controllers" }
            );
            context.MapRoute(
                "Admin_default2",
                "Admin/{controller}/{action}/{guid}",
                new { action = "ViewDetail", guid = UrlParameter.Optional },
                namespaces: new[] { "CodexMVC.Areas.Admin.Controllers" }
            );
            //context.MapRoute(
            //    "AgentCompareTimeline",
            //    "Admin/Agent/{action}",
            //    new { controller = "Agent", guid = UrlParameter.Optional },
            //    namespaces: new[] { "CodexMVC.Areas.Admin.Controllers" }
            //);
            //context.MapRoute(
            //    "AgentSearchResult",
            //    "Admin/Agent/SearchResult/{query}",
            //    new { controller = "Agent", guid = UrlParameter.Optional },
            //    namespaces: new[] { "CodexMVC.Areas.Admin.Controllers" }
            //);
            //context.MapRoute(
            //    "Query",
            //    "Admin/Search/{action}/{search}",
            //    new { controller = "Search", search = UrlParameter.Optional },
            //    namespaces: new[] { "CodexMVC.Areas.Admin.Controllers" }
            //);
            //context.MapRoute(
            //    "EditAgent",
            //    "Admin/Agent/{action}/{guid}",
            //    new { controller = "Agent", guid = UrlParameter.Optional },
            //    namespaces: new[] { "CodexMVC.Areas.Admin.Controllers" }
            //);
        }
    }
}