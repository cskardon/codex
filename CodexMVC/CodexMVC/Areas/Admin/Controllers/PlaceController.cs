﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Neo4jClient;
using Services;
using Services.Common;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class PlaceController : BaseController
    {
        private readonly IAgentService personService;
        private readonly IPlaceService placeService;
        protected readonly IActService actService;
        protected readonly ICommonService commonService;
        public PlaceController(
            IAgentService _personService,
            IPlaceService _placeService,
            IActService _actService,
            ICommonService _commonService
        )
        {
            personService = _personService;
            placeService = _placeService;
            actService = _actService;
            commonService = _commonService;
        }
        [HttpGet]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            var place = placeService.FindPlaceByGuid(guid);
            var node = new ViewPlace();
            node.Place = place;
            node.Timeline = await actService.FindNewActsByPlaceAsync(guid);
            node.Attributes = personService.FindAllPlaceAttributes(guid);
            node.ReverseAttributes = personService.FindAllPlaceAttributesReverse(guid);
            return View("ViewDetail", node);
        }
        [HttpGet]
        public ActionResult AddInsideOf(string placeGuid)
        {
            var node = new AddInsideOf();
            var place = placeService.FindPlaceByGuid(placeGuid);
            node.PlaceGuid = placeGuid;
            node.Name = place.Name;
            return View(node);
        }
        [HttpPost]
        public ActionResult AddInsideOf(AddInsideOf node)
        {
            if (ModelState.IsValid)
            {
                var place = placeService.FindPlaceByGuid(node.PlaceGuid);
                var container = placeService.FindPlaceByGuid(node.ContainerGuid);
                placeService.InsideOf(place, container);
                Cache.RemoveFromCache(CacheKey.PlaceService.AllWithContainers);
                return RedirectToAction("ViewDetail", new { guid = node.PlaceGuid });
            }
            return View(node);
        }
        [HttpGet]
        public ActionResult DeleteBasicRelationship(string relationshipType, string relationshipGuid, string placeGuid)
        {
            commonService.DeleteRelationship(relationshipType, relationshipGuid);
            return RedirectToAction("ViewDetail", "Place", new { guid = placeGuid });
        }
        [HttpGet]
        public ActionResult AddIsA(string placeGuid)
        {
            var node = new AddIsA();
            node.PlaceGuid = placeGuid;
            return View(node);
        }
        [HttpPost]
        public ActionResult AddIsA(AddIsA node)
        {
            if (ModelState.IsValid)
            {
                var place = placeService.FindPlaceByGuid(node.PlaceGuid);
                var category = personService.FindCategoryByGuid(node.CategoryGuid);
                placeService.IsA(place, category);
                return RedirectToAction("ViewDetail", new { guid = node.PlaceGuid });
            }
            return View(node);
        }
        public ActionResult List()
        {
            var places = GetList();
            return View(places);
        }
        private List<Place> GetList()
        {
            if (GetFromCache<List<Place>>(CacheKey.PlaceService.All) == null)
            {
                AddToCache(CacheKey.PlaceService.All, placeService.All());
            }
            return GetFromCache<List<Place>>(CacheKey.PlaceService.All);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.EditPlace)]
        public ActionResult Add()
        {
            return View("Edit", new EditPlaceModel());
        }
        [HttpGet]
        [Authorize(Roles = Permissions.EditPlace)]
        public ActionResult Edit(string placeGuid)
        {
            var place = placeService.FindPlaceByGuid(placeGuid);
            return View("Edit", new EditPlaceModel
            {
                PlaceGuid = placeGuid,
                Adjective = place.Adjective,
                Article = place.Article,
                Name = place.Name,
                Description = place.Description,
                Longitude = place.Longitude,
                Latitude = place.Latitude,
                TimeZoneOffset = place.TimeZoneOffset
            });
        }
        [HttpGet]
        [Authorize(Roles = Permissions.DeletePlace)]
        public ActionResult Delete(string placeGuid)
        {
            var place = placeService.FindPlaceByGuid(placeGuid);
            var result = placeService.Delete(placeGuid);
            GeneralMessage = string.Format(result ? "{0} [{1}] was deleted" : "{0} [{1}] could not be deleted", place.Name, place.Guid);
            return RedirectToAction("List");
        }
        [HttpPost]
        [Authorize(Roles = Permissions.EditPlace)]
        public ActionResult Edit(EditPlaceModel model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.PlaceGuid == null;
                var place = false == isNew ? placeService.FindPlaceByGuid(model.PlaceGuid) : new Data.Entities.Place();
                place.Name = model.Name;
                place.Adjective = model.Adjective;
                place.Article = model.Article;
                place.Description = model.Description;
                place.Latitude = model.Latitude;
                place.Longitude = model.Longitude;
                place.TimeZoneOffset = model.TimeZoneOffset;
                placeService.SaveOrUpdate(place);
                GeneralMessage = string.Format("Created place '{0}'.", model.Name);
                Cache.RemoveFromCache(CacheKey.PlaceService.AllWithContainers);
                return RedirectToAction("ViewDetail", "Place", new { guid = place.Guid });
            }
            return View(model);
        }
    }
}