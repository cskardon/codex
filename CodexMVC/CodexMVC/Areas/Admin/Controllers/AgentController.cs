﻿using CodexMVC.Controllers;
using CodexMVC.Helpers;
using CodexMVC.Models;
using Data;
using Data.Entities;
using Newtonsoft.Json;
using Services;
using Services.Common;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class AgentController : BaseController
    {
        private readonly IAgentService agentService;
        private readonly IPlaceService placeService;
        private readonly IArtefactService artefactService;
        private readonly IResourceService resourceService;
        private readonly ICategoryService categoryService;
        private readonly IActService actService;
        private readonly ICommonService commonService;
        private readonly IRelationService relationService;
        public AgentController(
            IAgentService _agentService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IActService _actService,
            IResourceService _resourceService,
            IRelationService _relationService,
            ICategoryService _categoryService,
            ICommonService _commonService
        )
        {
            agentService = _agentService;
            placeService = _placeService;
            artefactService = _artefactService;
            resourceService = _resourceService;
            actService = _actService;
            relationService = _relationService;
            categoryService = _categoryService;
            commonService = _commonService;
        }

        [HttpGet]
        public ActionResult CheckForDuplicates(string name, string firstname, string lastname)
        {
            var agents = agentService.CheckForDuplicates(name, firstname, lastname)
                .Select(x => new { Text = x.Agent.FullName2 + " <small>" + x.Life() + " " + x.SubTitle() + "</small>", Guid = x.Agent.Guid });

            return new JsonResult
            {
                Data = new
                {
                    Agents = agents
                },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }


        [HttpGet]
        public ActionResult AddHasMotif(string agentGuid)
        {
            return AddRelation<Category>(agentGuid, "EditHasMotif", " has a motif ...", Label.Categories.ArtistMotif);
        }
        [HttpGet]
        public ActionResult EditHasMotif(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(agentGuid, hasRelationGuid, "EditHasMotif", "has a motif ...", agentService.FindHasMotif, Label.Categories.ArtistMotif);
        }
        [HttpPost]
        public ActionResult EditHasMotif(GenericRelationModel<Agent, Category> model)
        {
            return SaveRelation<Category>(model, agentService.HasMotif, Label.Categories.ArtistMotif);
        }
        [HttpGet]
        public ActionResult AddBuriedIn(string agentGuid)
        {
            return AddRelation<Artefact>(agentGuid, "EditBuriedIn", " is buried in ...");
        }
        [HttpGet]
        public ActionResult EditBuriedIn(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(agentGuid, hasRelationGuid, "EditBuriedIn", " is buried in ...", artefactService.FindBuriedIn);
        }
        [HttpPost]
        public ActionResult EditBuriedIn(GenericRelationModel<Agent, Artefact> model)
        {
            return SaveRelation<Artefact>(model, artefactService.BuriedIn);
        }
        [HttpGet]
        public ActionResult AddAppliesTo(string agentGuid)
        {
            return AddRelation<Artefact>(agentGuid, "EditAppliesTo", " pass applies to art collection ...");
        }
        [HttpGet]
        public ActionResult EditAppliesTo(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(agentGuid, hasRelationGuid, "EditAppliesTo", " pass applies to art collection ...", artefactService.FindAppliesTo);
        }
        [HttpPost]
        public ActionResult EditAppliesTo(GenericRelationModel<Agent, Artefact> model)
        {
            return SaveRelation<Artefact>(model, artefactService.AppliesTo);
        }
        [HttpGet]
        public ActionResult DeleteBasicRelationship(string relationshipType, string relationshipGuid, string agentGuid)
        {
            commonService.DeleteRelationship(relationshipType, relationshipGuid);
            return ToViewDetail(agentGuid);
        }
        private ActionResult ToViewDetail(string guid)
        {
            return RedirectToAction("ViewDetail", "Agent", new { guid });
        }
        private ActionResult AddBasicRelation<TDest>(string agentGuid, string action, string title)
            where TDest : class, IEntity
        {
            var artefact = agentService.FindAgentByGuid(agentGuid);
            return AddRelationTest<Agent, TDest>(artefact, action, title);
        }
        private ActionResult FindBasicRelation<TDest>(string agentGuid, string toDestGuid, string action, string title, Func<string, string, GenericRelationNode<Agent, TDest>> getter)
           where TDest : class, IEntity
        {
            return FindRelationTest<Agent, TDest>(agentGuid, toDestGuid, action, title, getter);
        }
        private ActionResult SaveBasicRelation<TDest>(GenericRelationNodeModel<Agent, TDest> model, Action<GenericRelationNode<Agent, TDest>> updater)
            where TDest : class, IEntity
        {
            return SaveRelationTest<Agent, TDest>(model, updater, agentService.FindAgentByGuid);
        }
        public ActionResult AddAgent()
        {
            var model = new AddAgentModel
            {
                Sex = Sex.Male
            };
            return View("AddAgent", model);
        }

        [HttpGet]
        public JsonResult QuickAddAgentModal(string name, string firstname, string lastname, string citizenOfGuid)
        {
            var agent = new Agent();
            agent.Name = name;
            agent.FirstName = firstname;
            agent.LastName = lastname;
            agentService.SaveOrUpdate(agent);
            if (false == string.IsNullOrWhiteSpace(citizenOfGuid))
            {
                agentService.CitizenOf(new GenericRelationHyperNode<Agent, Place>
                {
                    Source = agent,
                    Destination = new Place
                    {
                        Guid = citizenOfGuid
                    }
                });
            }
            return new JsonResult
            {
                Data = new { Success = true },
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        public ActionResult AddArtist()
        {
            var model = new AddArtistModel
            {
                Sex = Sex.Male
            };
            return View("AddArtist", model);
        }

        [HttpPost]
        public async Task<ActionResult> AddArtist(AddArtistModel model)
        {
            if (ModelState.IsValid)
            {
                var agent = new Agent();
                agent.Created = DateTimeOffset.Now;
                agent.Name = model.Name;
                agent.Article = model.Article;
                agent.FirstName = model.FirstName;
                agent.LastName = model.LastName;
                agent.Rating = model.Rating;
                agent.Description = model.Description;
                agent.Sex = model.Sex;
                agentService.SaveOrUpdate(agent);

                if (false == string.IsNullOrEmpty(model.ImageUrl) || model.Image != null)
                {
                    var image = new Image();
                    image.FileName = Path.GetFileName(model.Image.FileName);
                    image.Extension = GetFileExtension(model.Image);
                    image.Name = model.ImageName;
                    image.Uri = model.ImageUrl;
                    image.Primary = true;
                    resourceService.SaveOrUpdate(image);
                    resourceService.HasArtefactImage(agent.Guid, image.Guid);
                    await SaveFileAsync(model.Image, image.Guid, image.Extension);
                }

                if (false == string.IsNullOrEmpty(model.BornAtGuid))
                {
                    agentService.BornAt(new GenericRelationHyperNode<Agent, Place>
                    {
                        Source = agent,
                        Relation = new Relation
                        {
                            Primary = true,
                            Milennium = model.BornAtMilennium,
                            Year = model.BornAtYear,
                            Month = model.BornAtMonth,
                            Day = model.BornAtDay,
                            Decade = model.BornAtDecade,
                        },
                        Destination = new Place
                        {
                            Guid = model.BornAtGuid
                        }
                    });
                }

                if (false == string.IsNullOrEmpty(model.DiedAtGuid))
                {
                    agentService.DiedAt(new GenericRelationHyperNode<Agent, Place>
                    {
                        Source = agent,
                        Relation = new Relation
                        {
                            Primary = true,
                            Milennium = model.DiedAtMilennium,
                            Year = model.DiedAtYear,
                            Month = model.DiedAtMonth,
                            Day = model.DiedAtDay,
                            Decade = model.DiedAtDecade,
                        },
                        Destination = new Place
                        {
                            Guid = model.DiedAtGuid
                        }
                    });
                }

                if (false == string.IsNullOrEmpty(model.IsA1Guid))
                {
                    agentService.IsA(new GenericRelationHyperNode<Agent, Category>
                    {
                        Source = agent,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.IsA1Guid
                        }
                    });
                }

                if (false == string.IsNullOrEmpty(model.IsA2Guid))
                {
                    agentService.IsA(new GenericRelationHyperNode<Agent, Category>
                    {
                        Source = agent,
                        Relation = new Relation
                        {
                            Primary = false
                        },
                        Destination = new Category
                        {
                            Guid = model.IsA2Guid
                        }
                    });
                }

                if (false == string.IsNullOrEmpty(model.CitizenOfGuid))
                {
                    agentService.CitizenOf(new GenericRelationHyperNode<Agent, Place>
                    {
                        Source = agent,
                        Relation = new Relation(),
                        Destination = new Place { Guid = model.CitizenOfGuid }
                    });
                }

                if (false == string.IsNullOrEmpty(model.RepresentativeOfGuid))
                {
                    agentService.RepresentativeOf(new GenericRelationHyperNode<Agent, Category>
                    {
                        Source = agent,
                        Relation = new Relation(),
                        Destination = new Category { Guid = model.RepresentativeOfGuid }
                    });
                }

                if (false == string.IsNullOrEmpty(model.StudentOfGuid))
                {
                    agentService.StudentOf(new GenericRelationHyperNode<Agent, Agent>
                    {
                        Source = agent,
                        Relation = new Relation(),
                        Destination = new Agent { Guid = model.StudentOfGuid }
                    });
                }

                GeneralMessage = string.Format("Created place '{0}'.", model.Name);
                return RedirectToAction("ViewDetail", new { guid = agent.Guid });
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> AddAgent(AddAgentModel model)
        {
            if (ModelState.IsValid)
            {
                var agent = new Agent();
                agent.Created = DateTimeOffset.Now;
                agent.Name = model.Name;
                agent.Article = model.Article;
                agent.FirstName = model.FirstName;
                agent.LastName = model.LastName;
                agent.Rating = model.Rating;
                agent.Description = model.Description;
                agent.Sex = model.Sex;
                agentService.SaveOrUpdate(agent);
                if (false == string.IsNullOrEmpty(model.ImageUrl) || model.Image != null)
                {
                    var image = new Image();
                    image.FileName = Path.GetFileName(model.Image.FileName);
                    image.Extension = GetFileExtension(model.Image);
                    image.Name = model.ImageName;
                    image.Uri = model.ImageUrl;
                    image.Primary = true;
                    resourceService.SaveOrUpdate(image);
                    resourceService.HasArtefactImage(agent.Guid, image.Guid);
                    await SaveFileAsync(model.Image, image.Guid, image.Extension);
                }
                if (false == string.IsNullOrEmpty(model.IsA1Guid))
                {
                    agentService.IsA(new GenericRelationHyperNode<Agent, Category>
                    {
                        Source = agent,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.IsA1Guid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.IsA2Guid))
                {
                    agentService.IsA(new GenericRelationHyperNode<Agent, Category>
                    {
                        Source = agent,
                        Relation = new Relation
                        {
                            Primary = false
                        },
                        Destination = new Category
                        {
                            Guid = model.IsA2Guid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.CitizenOfGuid))
                {
                    agentService.CitizenOf(new GenericRelationHyperNode<Agent, Place>
                    {
                        Source = agent,
                        Relation = new Relation(),
                        Destination = new Place { Guid = model.CitizenOfGuid }
                    });
                }
                if (false == string.IsNullOrEmpty(model.RepresentativeOfGuid))
                {
                    agentService.RepresentativeOf(new GenericRelationHyperNode<Agent, Category>
                    {
                        Source = agent,
                        Relation = new Relation(),
                        Destination = new Category { Guid = model.RepresentativeOfGuid }
                    });
                }
                GeneralMessage = string.Format("Created place '{0}'.", model.Name);
                return RedirectToAction("ViewDetail", new { guid = agent.Guid });
            }
            return View(model);
        }
        private ActionResult AddRelation<TDest>(string agentGuid, string action, string title, string descendantsOf = null)
            where TDest : class, IEntity
        {
            var artefact = agentService.FindAgentByGuid(agentGuid);
            return AddRelation<Agent, TDest>(artefact, action, title, descendantsOf);
        }
        private ActionResult FindRelation<TDest>(string agentGuid, string hasRelationGuid, string action, string title, Func<string, string, GenericRelationHyperNode<Agent, TDest>> getter, string descendantsOf = null)
            where TDest : class, IEntity
        {
            return FindRelation<Agent, TDest>(agentGuid, hasRelationGuid, action, title, getter, descendantsOf);
        }
        private ActionResult SaveRelation<TDest>(GenericRelationModel<Agent, TDest> model, Action<GenericRelationHyperNode<Agent, TDest>> updater, string descendantsOf = null)
            where TDest : class, IEntity
        {
            return SaveRelation<Agent, TDest>(model, updater, agentService.FindAgentByGuid, descendantsOf);
        }
        [HttpPost]
        public async Task<ActionResult> CompareTimeline(string agent1, string agent2)
        {
            var agent1Timeline = await actService.FindActsByAgent(agent1);
            var agent2Timeline = await actService.FindActsByAgent(agent2);
            var years1 = agent1Timeline.Select(x => x.Act.TimelineDate());
            var years2 = agent2Timeline.Select(x => x.Act.TimelineDate());
            var years = years1.Concat(years2).Distinct().OrderBy(x => x);
            var timelines = new Timelines { Time = new Dictionary<string, List<Timeline>>() };
            var a1 = agentService.FindAgentByGuid(agent1);
            var a2 = agentService.FindAgentByGuid(agent2);
            foreach (var year in years)
            {
                var timeline1 = new Timeline
                {
                    Agent = a1,
                    NewActs = agent1Timeline.Where(x => x.Act.TimelineDate() == year).ToList()
                };
                var timeline2 = new Timeline
                {
                    Agent = a2,
                    NewActs = agent2Timeline.Where(x => x.Act.TimelineDate() == year).ToList()
                };
                timelines.Time.Add(year, new List<Timeline> { timeline1, timeline2 });
            }
            var model = new ViewCompareTimeline
            {
                Timelines = timelines
            };
            return View("CompareTimeline", model);
        }
        [HttpPost]
        public ActionResult Search(string query)
        {
            var parts = query.Trim().Split(' ').ToList();
            var artefactTypeName = parts[0].Trim();
            var artistName = parts[2].Trim();
            var artefacts = artefactService.SearchBy(artistName, artefactTypeName);
            var model = new ViewSearchResults
            {
                Artefacts = artefacts
            };
            return View("SearchResults", model);
        }
        [HttpGet]
        public ActionResult AddDocument(string agentGuid)
        {
            var agent = agentService.FindAgentByGuid(agentGuid);
            var model = new EditDocument
            {
                Agent = agent,
                Document = new Document { Milennium = "AD", MilenniumEnd = "AD" },
                ProximityModel = Proximity.On,
                ProximityEndModel = Proximity.On
            };
            return View("EditDocument", model);
        }
        [HttpGet]
        public ActionResult DeleteDocument(string documentGuid, string agentGuid)
        {
            actService.DeleteDocument(documentGuid);
            GeneralMessage = string.Format("Document '{0}' was deleted.", documentGuid);
            return RedirectToAction("ViewDetail", "Agent", new { guid = agentGuid });
        }
        [HttpGet]
        public ActionResult EditDocument(string documentGuid)
        {
            var node = actService.FindDocumentHyperNode(documentGuid);
            var model = new EditDocument
            {
                Agent = node.MadeBy,
                Document = node.Document,
                HyperNode = node,
                ProximityModel = RazorHelper.ToProximity(node.Document.Proximity),
                ProximityEndModel = RazorHelper.ToProximity(node.Document.ProximityEnd)
            };
            return View("EditDocument", model);
        }
        [HttpPost]
        public ActionResult EditDocument(EditDocument model)
        {
            if (ModelState.IsValid)
            {
                var pairs = RazorHelper.GetPairs(model.Document.Description);
                var list = pairs.Select(x => x.Split(new char[] { '|' }));
                model.Document.Proximity = RazorHelper.FromProximity(model.ProximityModel);
                model.Document.ProximityEnd = RazorHelper.FromProximity(model.ProximityEndModel);
                actService.SaveOrUpdate(model.Agent.Guid, model.Document, list);
                return RedirectToAction("ViewDetail", "Agent", new { guid = model.Agent.Guid });
            }
            model.HyperNode = actService.FindDocumentHyperNode(model.Document.Guid);
            return View("EditDocument", model);
        }
        [HttpGet]
        public ActionResult AddMemberOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditMemberOf", " is a member of ...");
        }
        [HttpGet]
        public ActionResult EditMemberOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditMemberOf", " is a member of ...", agentService.FindMemberOf);
        }
        [HttpPost]
        public ActionResult EditMemberOf(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.MemberOf);
        }
        [HttpGet]
        public ActionResult AddPartOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditPartOf", " is part of ...");
        }
        [HttpGet]
        public ActionResult EditPartOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditPartOf", " is part of ...", agentService.FindPartOf);
        }
        [HttpPost]
        public ActionResult EditPartOf(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.PartOf);
        }
        [HttpGet]
        public ActionResult AddWorkedFor(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditWorkedFor", " worked for ...");
        }
        [HttpGet]
        public ActionResult EditWorkedFor(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditWorkedFor", " worked for ...", agentService.FindWorkedFor);
        }
        [HttpPost]
        public ActionResult EditWorkedFor(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.WorkedFor);
        }
        [HttpGet]
        public ActionResult AddMakerOf(string agentGuid)
        {
            var agent = agentService.FindAgentByGuid(agentGuid);
            var model = new AgentToArtefactRelationModel
            {
                Action = "EditMakerOf",
                Title = agent.FullName2 + " is the maker of ...",
                Node = new AgentToArtefactRelationHyperNode
                {
                    Source = agent
                }
            };
            return View("EditRelatedToArtefact", model);
        }
        [HttpGet]
        public ActionResult EditMakerOf(string agentGuid, string hasRelationGuid)
        {
            var node = agentService.FindMade(agentGuid, hasRelationGuid);
            var model = new AgentToArtefactRelationModel
            {
                Action = "EditMakerOf",
                Title = node.Source.FullName2 + " is the maker of ...",
                Node = node,
                Relation = node.Relation
            };
            return View("EditRelatedToArtefact", model);
        }
        [HttpPost]
        public ActionResult EditMakerOf(AgentToArtefactRelationModel model)
        {
            if (ModelState.IsValid)
            {
                var node = new AgentToArtefactRelationHyperNode
                {
                    Source = model.Node.Source,
                    HasRelationGuid = model.Node.HasRelationGuid,
                    RelationToDestinationGuid = model.Node.RelationToDestinationGuid,
                    Relation = model.Relation,
                    Destination = model.Node.Destination
                };
                agentService.MakerOf(node);
                return RedirectToAction("ViewDetail", new { guid = model.Node.Source.Guid });
            }
            model.Node.Source = agentService.FindAgentByGuid(model.Node.Source.Guid);
            return View("EditRelatedToArtefact", model);
        }
        [HttpGet]
        public ActionResult AddRepresentedBy(string agentGuid)
        {
            return AddRelation<Artefact>(agentGuid, "EditRepresentedBy", " is represented by the artefact ...");
        }
        [HttpGet]
        public ActionResult EditRepresentedBy(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(agentGuid, hasRelationGuid, "EditRepresentedBy", " is represented by the artefact ...", agentService.FindRepresentedBy);
        }
        [HttpPost]
        public ActionResult EditRepresentedBy(GenericRelationModel<Agent, Artefact> model)
        {
            return SaveRelation<Artefact>(model, agentService.RepresentedBy);
        }
        [HttpGet]
        public ActionResult AddWorkedOn(string agentGuid)
        {
            return AddRelation<Artefact>(agentGuid, "EditWorkedOn", " worked on ...");
        }
        [HttpGet]
        public ActionResult EditWorkedOn(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(agentGuid, hasRelationGuid, "EditWorkedOn", " worked on ...", agentService.FindWorkedOn);
        }
        [HttpPost]
        public ActionResult EditWorkedOn(GenericRelationModel<Agent, Artefact> model)
        {
            return SaveRelation<Artefact>(model, agentService.WorkedOn);
        }
        [HttpGet]
        public ActionResult AddExhibitingIn(string agentGuid)
        {
            return AddRelation<Artefact>(agentGuid, "EditExhibitingIn", " is exhibiting in ...", Label.Categories.Exhibition);
        }
        [HttpGet]
        public ActionResult EditExhibitingIn(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(agentGuid, hasRelationGuid, "EditExhibitingIn", " is exhibiting in ...", agentService.FindExhibitingIn, Label.Categories.Exhibition);
        }
        [HttpPost]
        public ActionResult EditExhibitingIn(GenericRelationModel<Agent, Artefact> model)
        {
            return SaveRelation<Artefact>(model, agentService.ExhibitingIn, Label.Categories.Exhibition);
        }
        [HttpGet]
        public ActionResult AddCommissioned(string agentGuid)
        {
            return AddRelation<Artefact>(agentGuid, "EditCommissioned", " commissioned ...");
        }
        [HttpGet]
        public ActionResult EditCommissioned(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(agentGuid, hasRelationGuid, "EditCommissioned", " commissioned ...", agentService.FindCommissioned);
        }
        [HttpPost]
        public ActionResult EditCommissioned(GenericRelationModel<Agent, Artefact> model)
        {
            return SaveRelation<Artefact>(model, agentService.Commissioned);
        }
        [HttpGet]
        public ActionResult AddChildOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditChildOf", " is a child of ...");
        }
        [HttpGet]
        public ActionResult EditChildOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditChildOf", " is a child of ...", agentService.FindChildOf);
        }
        [HttpGet]
        public ActionResult EditChildOfReverse(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditChildOf", " is a child of ...", agentService.FindChildOfReverse);
        }
        [HttpPost]
        public ActionResult EditChildOf(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.ChildOf);
        }
        [HttpGet]
        public ActionResult AddCollaboratedWith(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditCollaboratedWith", "collaborated with ...");
        }
        [HttpGet]
        public ActionResult EditCollaboratedWith(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditCollaboratedWith", "collaborated with ...", agentService.FindCollaboratedWith);
        }
        [HttpPost]
        public ActionResult EditCollaboratedWith(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.CollaboratedWith);
        }
        [HttpGet]
        public ActionResult AddInfluenceOn(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditInfluenceOn", " influenced ...");
        }
        [HttpGet]
        public ActionResult EditInfluenceOn(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditInfluenceOn", " influenced ...", agentService.FindInfluenceOn);
        }
        [HttpPost]
        public ActionResult EditInfluenceOn(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.InfluenceOn);
        }
        [HttpGet]
        public ActionResult AddBornAt(string agentGuid)
        {
            return AddRelation<Place>(agentGuid, "EditBornAt", " was born at ...");
        }
        [HttpGet]
        public ActionResult EditBornAt(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Place>(agentGuid, hasRelationGuid, "EditBornAt", " was born at ...", agentService.FindBornAt);
        }
        [HttpPost]
        public ActionResult EditBornAt(GenericRelationModel<Agent, Place> model)
        {
            return SaveRelation<Place>(model, agentService.BornAt);
        }
        [HttpGet]
        public ActionResult AddDiedAt(string agentGuid)
        {
            return AddRelation<Place>(agentGuid, "EditDiedAt", " died at ...");
        }
        [HttpGet]
        public ActionResult EditDiedAt(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Place>(agentGuid, hasRelationGuid, "EditBornAt", " died at ...", agentService.FindDiedAt);
        }
        [HttpPost]
        public ActionResult EditDiedAt(GenericRelationModel<Agent, Place> model)
        {
            return SaveRelation<Place>(model, agentService.DiedAt);
        }
        [HttpGet]
        public ActionResult AddSymbolOfPlace(string agentGuid)
        {
            return AddRelation<Place>(agentGuid, "EditSymbolOfPlace", " symbolises place ...");
        }
        [HttpGet]
        public ActionResult EditSymbolOfPlace(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Place>(agentGuid, hasRelationGuid, "EditSymbolOfPlace", " symbolises place ...", agentService.FindSymbolOfPlace);
        }
        [HttpPost]
        public ActionResult EditSymbolOfPlace(GenericRelationModel<Agent, Place> model)
        {
            return SaveRelation<Place>(model, agentService.SymbolOfPlace);
        }
        [HttpGet]
        public ActionResult AddSymbolOfAgent(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditSymbolOfAgent", " symbolises agent ...");
        }
        [HttpGet]
        public ActionResult EditSymbolOfAgent(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditSymbolOfAgent", " symbolises agent ...", agentService.FindSymbolOf);
        }
        [HttpPost]
        public ActionResult EditSymbolOfAgent(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.SymbolOf);
        }
        [HttpGet]
        public ActionResult AddPatronOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditPatronOf", " is a patron of ...");
        }
        [HttpGet]
        public ActionResult EditPatronOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditPatronOf", " is a patron of ...", agentService.FindBenefactorOf);
        }
        [HttpPost]
        public ActionResult EditPatronOf(AgentToAgentRelationModel model)
        {
            return SaveRelation<Agent>(model, agentService.BenefactorOf);
        }
        [HttpGet]
        public ActionResult AddAdversaryOf(string agentGuid)
        {
            var agent = agentService.FindAgentByGuid(agentGuid);
            var model = new AgentToAgentRelationModel
            {
                Action = "EditAdversaryOf",
                Title = agent.FullName2 + " is an adversary of ...",
                Node = new AgentToAgentRelationHyperNode
                {
                    Source = agent
                }
            };
            return View("EditRelatedToAgent", model);
        }
        [HttpGet]
        public ActionResult EditAdversaryOf(string agentGuid, string hasRelationGuid)
        {
            var node = agentService.FindAdversaryOf(agentGuid, hasRelationGuid);
            var model = new AgentToAgentRelationModel
            {
                Action = "EditAdversaryOf",
                Title = node.Source.FullName2 + " is an adversary of ...",
                Node = node,
                Relation = node.Relation
            };
            return View("EditRelatedToAgent", model);
        }
        [HttpPost]
        public ActionResult EditAdversaryOf(AgentToAgentRelationModel model)
        {
            if (ModelState.IsValid)
            {
                var node = new AgentToAgentRelationHyperNode
                {
                    Source = model.Node.Source,
                    HasRelationGuid = model.Node.HasRelationGuid,
                    RelationToDestinationGuid = model.Node.RelationToDestinationGuid,
                    Relation = model.Relation,
                    Destination = model.Node.Destination
                };
                agentService.AdversaryOf(node);
                return RedirectToAction("ViewDetail", new { guid = model.Node.Source.Guid });
            }
            model.Node.Source = agentService.FindAgentByGuid(model.Node.Source.Guid);
            return View("EditRelatedToAgent", model);
        }
        [HttpGet]
        public ActionResult AddAspectOf(string agentGuid)
        {
            return AddBasicRelation<Agent>(agentGuid, "EditAspectOf", " is an aspect of ...");
        }
        [HttpGet]
        public ActionResult EditAspectOf(string agentGuid, string hasRelationGuid)
        {
            return FindBasicRelation<Agent>(agentGuid, hasRelationGuid, "EditAspectOf", " is an aspect of ...", agentService.FindAspectOf);
        }
        [HttpPost]
        public ActionResult EditAspectOf(GenericRelationNodeModel<Agent, Agent> model)
        {
            return SaveBasicRelation<Agent>(model, agentService.AspectOf);
        }
        [HttpGet]
        public ActionResult AddTeacherOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditTeacherOf", " is a teacher of ...");
        }
        [HttpGet]
        public ActionResult EditTeacherOf(string agentGuid, string hasRelationGuid)
        {
            // The find method still needs to do a reverse-lookup from StudentOf....
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditTeacherOf", " is a teacher of ...", agentService.FindTeacherOf);
        }
        [HttpPost]
        public ActionResult EditTeacherOf(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.TeacherOf);
        }
        [HttpGet]
        public ActionResult AddStudentOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditStudentOf", " is a student of ...");
        }
        [HttpGet]
        public ActionResult EditStudentOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditStudentOf", " is a student of ...", agentService.FindStudentOf);
        }
        [HttpPost]
        public ActionResult EditStudentOf(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.StudentOf);
        }
        [HttpGet]
        public ActionResult AddStudentAt(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditStudentAt", " studied at ...");
        }
        [HttpGet]
        public ActionResult EditStudentAt(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditStudentAt", " studied at ...", agentService.FindStudentAt);
        }
        [HttpPost]
        public ActionResult EditStudentAt(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.StudentAt);
        }
        [HttpGet]
        public ActionResult AddTeacherAt(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditTeacherAt", " taught at ...");
        }
        [HttpGet]
        public ActionResult EditTeacherAt(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditTeacherAt", "taught at ...", agentService.FindTeacherAt);
        }
        [HttpPost]
        public ActionResult EditTeacherAt(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.TeacherAt);
        }
        [HttpGet]
        public ActionResult AddFriendOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditFriendOf", " is a friend of ...");
        }
        [HttpGet]
        public ActionResult EditFriendOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditFriendOf", " is a friend of ...", agentService.FindFriendOf);
        }
        [HttpPost]
        public ActionResult EditFriendOf(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.FriendOf);
        }
        [HttpGet]
        public ActionResult AddPartnerOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditPartnerOf", " is the sexual/romantic partner of ...");
        }
        [HttpGet]
        public ActionResult EditPartnerOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditPartnerOf", " is the sexual/romantic partner of ...", agentService.FindPartnerOf);
        }
        [HttpPost]
        public ActionResult EditPartnerOf(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.PartnerOf);
        }
        [HttpGet]
        public ActionResult AddSimilarTo(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditSimilarTo", " is similar to ...");
        }
        [HttpGet]
        public ActionResult EditSimilarTo(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditSimilarTo", " is similar to ...", agentService.FindSimilarTo);
        }
        [HttpPost]
        public ActionResult EditSimilarTo(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.SimilarTo);
        }
        [HttpGet]
        public ActionResult AddSpouseOf(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditSpouseOf", " is the spouse of ...");
        }
        [HttpGet]
        public ActionResult EditSpouseOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditSpouseOf", " is the spouse of ...", agentService.FindSpouseOf);
        }
        [HttpPost]
        public ActionResult EditSpouseOf(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.SpouseOf);
        }
        [HttpGet]
        public ActionResult AddKnows(string agentGuid)
        {
            return AddRelation<Agent>(agentGuid, "EditKnows", " knows ...");
        }
        [HttpGet]
        public ActionResult EditKnows(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Agent>(agentGuid, hasRelationGuid, "EditKnows", " knows ...", agentService.FindKnows);
        }
        [HttpPost]
        public ActionResult EditKnows(GenericRelationModel<Agent, Agent> model)
        {
            return SaveRelation<Agent>(model, agentService.Knows);
        }
        [HttpGet]
        public ActionResult AddCitizenOf(string agentGuid)
        {
            return AddRelation<Place>(agentGuid, "EditCitizenOf", " is a citizen of ...");
        }
        [HttpGet]
        public ActionResult EditCitizenOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Place>(agentGuid, hasRelationGuid, "EditCitizenOf", " is a citizen of ...", agentService.FindCitizenOf);
        }
        [HttpPost]
        public ActionResult EditCitizenOf(AgentToPlaceRelationModel model)
        {
            return SaveRelation<Place>(model, agentService.CitizenOf);
        }
        [HttpGet]
        public ActionResult AddLocatedAt(string agentGuid)
        {
            return AddRelation<Place>(agentGuid, "EditLocatedAt", " is located at ...");
        }
        [HttpGet]
        public ActionResult EditLocatedAt(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Place>(agentGuid, hasRelationGuid, "EditLocatedAt", " is located at ...", agentService.FindLocatedAt);
        }
        [HttpPost]
        public ActionResult EditLocatedAt(AgentToPlaceRelationModel model)
        {
            return SaveRelation<Place>(model, agentService.LocatedAt);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.DeleteAct)]
        public ActionResult DeleteAct(string agentGuid, string actGuid)
        {
            agentService.DeleteAct(actGuid);
            return RedirectToAction("ViewDetail", new { guid = agentGuid });
        }
        [HttpGet]
        public ActionResult AddImage(string agentGuid)
        {
            return View("EditImage", new EditEntityImage
            {
                EntityGuid = agentGuid
            });
        }
        [HttpGet]
        public ActionResult EditImage(string imageGuid)
        {
            var node = resourceService.FindAgentImageHyperNode(imageGuid);
            return View("EditImage", new EditEntityImage
            {
                EntityGuid = node.Agent.Guid,
                ImageGuid = imageGuid,
                Name = node.Image.Name,
                Description = node.Image.Description,
                Uri = node.Image.Uri,
                Width = node.Image.Width,
                Height = node.Image.Height,
                Primary = node.Image.Primary ?? false
            });
        }
        [HttpGet]
        public ActionResult DeleteImage(string imageGuid, string agentGuid)
        {
            resourceService.DeleteAgentImageHyperNode(imageGuid);
            return RedirectToAction("ViewDetail", "Agent", new { guid = agentGuid });
        }
        [HttpPost]
        public ActionResult EditImage(EditEntityImage model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.ImageGuid == null;
                var image = isNew ? new Image() : resourceService.FindImageByGuid(model.ImageGuid);
                image.Name = model.Name;
                image.Description = model.Description;
                image.Uri = model.Uri;
                image.Primary = model.Primary;
                resourceService.SaveOrUpdate(image);
                resourceService.HasAgentImage(model.EntityGuid, image.Guid);
                return RedirectToAction("ViewDetail", "Agent", new { guid = model.EntityGuid });
            }
            return View(model);
        }
        [Authorize(Roles = Permissions.ListAgents)]
        public ActionResult List()
        {
            var userGuid = CurrentUser != null ? CurrentUser.Guid : null;
            var agents = agentService.AllWithAttributesWithBookmarks(userGuid);
            return View(agents);
        }
        [HttpGet]
        public ActionResult OrderedList()
        {
            var model = new ViewOrderedList();
            return View(model);
        }
        [HttpPost]
        public ActionResult OrderedList(ViewOrderedList model)
        {
            if (ModelState.IsValid)
            {
                var discipline = categoryService.Find(model.DisciplineGuid);
                var results = agentService.GetArtistsWithTaxonomy(discipline.Name);
                model.Results = results;
            }
            return View(model);
        }
        private List<AgentWithAttribute> GetList()
        {
            if (GetFromCache<List<AgentWithAttribute>>(CacheKey.AgentService.AllWithAttributes) == null)
            {
                var userGuid = CurrentUser != null ? CurrentUser.Guid : null;
                AddToCache(CacheKey.AgentService.AllWithAttributes, agentService.AllWithAttributesWithBookmarks(userGuid)
                .OrderBy(x => x.Agent.FullName)
                .ToList());
            }
            return GetFromCache<List<AgentWithAttribute>>(CacheKey.AgentService.AllWithAttributes);
        }
        [HttpGet]
        public ActionResult DeleteAttribute(string relationGuid, string agentGuid)
        {
            commonService.DeleteRelationHyperNode(relationGuid);
            return RedirectToAction("ViewDetail", "Agent", new { guid = agentGuid });
        }
        public Act ConvertTo(ActNode node)
        {
            return new Act
                    {
                        Year = node.Year,
                        Month = node.Month,
                        Day = node.Day,
                        Proximity = node.Proximity,
                        YearEnd = node.YearEnd,
                        MonthEnd = node.MonthEnd,
                        DayEnd = node.DayEnd,
                        ProximityEnd = node.ProximityEnd
                    };
        }
        [HttpGet]
        public ActionResult DeleteRelation(string relationGuid, string actGuid, string actType)
        {
            var deleted = relationService.DeleteAttribute(relationGuid);
            GeneralMessage = string.Format("{0} relationship(s) deleted.", deleted);
            return RedirectToAction("View" + actType, "Agent", new
            {
                actGuid
            });
        }
        [HttpGet]
        public ActionResult AddAlias(string agentGuid)
        {
            var agent = agentService.FindAgentByGuid(agentGuid);
            var model = new EditAlias
            {
                Agent = agent
            };
            return View("EditAlias", model);
        }
        [HttpPost]
        public ActionResult EditAlias(EditAlias model)
        {
            agentService.AddAlias(model.Agent.Guid, model.Alias);
            return RedirectToAction("ViewDetail", "Agent", new { guid = model.Agent.Guid });
        }
        [HttpGet]
        public ActionResult AddIsA(string agentGuid)
        {
            return AddRelation<Category>(agentGuid, "EditIsA", " is a ...");
        }
        [HttpGet]
        public ActionResult EditIsA(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(agentGuid, hasRelationGuid, "EditIsA", " is a ...", agentService.FindIsA);
        }
        [HttpPost]
        public ActionResult EditIsA(GenericRelationModel<Agent, Category> model)
        {
            return SaveRelation<Category>(model, agentService.IsA);
        }
        [HttpGet]
        public ActionResult AddAssociatedWith(string agentGuid)
        {
            return AddRelation<Category>(agentGuid, "EditAssociatedWith", " is associated with ...", Label.Categories.Interest);
        }
        [HttpGet]
        public ActionResult EditAssociatedWith(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(agentGuid, hasRelationGuid, "EditAssociatedWith", " is associated with ...", agentService.FindAssociatedWith, Label.Categories.Interest);
        }
        [HttpPost]
        public ActionResult EditAssociatedWith(GenericRelationModel<Agent, Category> model)
        {
            return SaveRelation<Category>(model, agentService.AssociatedWith, Label.Categories.Interest);
        }
        [HttpGet]
        public ActionResult AddRepresentativeOf(string agentGuid)
        {
            return AddRelation<Category>(agentGuid, "EditRepresentativeOf", " is representative of ...");
        }
        [HttpGet]
        public ActionResult EditRepresentativeOf(string agentGuid, string hasRelationGuid)
        {
            return FindRelation<Category>(agentGuid, hasRelationGuid, "EditRepresentativeOf", " is representative of ...", agentService.FindRepresentativeOf);
        }
        [HttpPost]
        public ActionResult EditRepresentativeOf(GenericRelationModel<Agent, Category> model)
        {
            return SaveRelation<Category>(model, agentService.RepresentativeOf);
        }
        public ActionResult Add()
        {
            return View("Edit", new EditAgent
            {
                Sex = Sex.None
            });
        }
        [HttpGet]
        public ActionResult Edit(string agentGuid)
        {
            var agent = agentService.FindAgentByGuid(agentGuid);
            return View(new EditAgent
            {
                AgentGuid = agentGuid,
                Article = agent.Article,
                Name = agent.Name,
                FirstName = agent.FirstName,
                LastName = agent.LastName,
                Description = agent.Description,
                Rating = agent.Rating,
                Sex = agent.Sex ?? Sex.None
            });
        }
        [HttpPost]
        public ActionResult Edit(EditAgent model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.AgentGuid == null;
                var agent = model.AgentGuid == null ? new Data.Agent() : agentService.FindAgentByGuid(model.AgentGuid);
                if (false == isNew)
                {
                    var born = actService.FindBornActByAgent(model.AgentGuid);
                    if (born != null)
                    {
                        agent.BirthYear = born.Act.Year;
                    }
                }
                agent.Sex = model.Sex;
                agent.Article = model.Article;
                agent.Name = model.Name;
                agent.FirstName = model.FirstName;
                agent.LastName = model.LastName;
                agent.Rating = model.Rating;
                agent.Description = model.Description;
                agentService.SaveOrUpdate(agent);
                return RedirectToAction("ViewDetail", "Agent", new { guid = agent.Guid });
            }
            return View(model);
        }

        [Authorize(Roles = Permissions.ViewAgent)]
        public async Task<ActionResult> ViewDetail(string guid)
        {
            var basicOutgoing = commonService.FindBasicOutgoingRelations<Agent>(guid);
            var basicIncoming = commonService.FindBasicIncomingRelations<Agent>(guid);
            var outgoingRelations = commonService.FindOutgoingRelations<Agent>(guid);
            var incomingRelations = commonService.FindIncomingRelations<Agent>(guid);
            var timeline = await actService.FindActsByAgent(guid, userGuid: CurrentUser.Guid);
            var bookmarked = userService.IsAgentBookmarked(guid, CurrentUser.Guid);
            var documents = actService.FindDocumentHyperNodesByAgent(guid);
            var referredDocs = await actService.FindDocumentHyperNodesByReferrerAsync(guid);
            var person = agentService.FindAgentByGuid(guid);
            var attributeHierarchies = agentService.FindCategoryHierarchy(guid).OrderBy(x => x.First().Name).ToList();
            var agentWithAttributes = GetList().FirstOrDefault(x => x.Agent.Guid == guid);

            var timelineMarkers = RazorHelper.GetTimelineMarkers(timeline).ToList();
            var timelineHeatMap = RazorHelper.GetTimelineHeatMap(timelineMarkers).ToList();

            var model = new ViewAgent
            {
                TimelineHeatMap = timelineHeatMap,
                TimelineMarkers = timelineMarkers,
                ArtworkHeatMap = new List<HeatMapJson>(),
                ArtworkMarkers = new List<IGrouping<string, ArtworkMarkerJson>>(),
                Bookmarked = bookmarked,
                Documents = documents,
                ReferredDocs = referredDocs,
                NewActs = timeline,
                Agent = person,
                AgentWithAttribute = agentWithAttributes,
                Images = resourceService.FindAgentImages(guid),
                Websites = resourceService.FindAgentWebsites(guid),
                OutgoingRelations = outgoingRelations,
                IncomingRelations = incomingRelations,
                BasicIncomingRelations = basicIncoming,
                BasicOutgoingRelations = basicOutgoing
            };
            ViewBag.GeneralMessage = GeneralMessage;
            return View(model);
        }
    }
}