﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Newtonsoft.Json;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class ImageController : BaseController
    {
        [HttpGet]
        public ActionResult Add()
        {
            var model = new EditImage();
            return View("Edit", model);
        }

        private static string ToEntityItemJson(IEnumerable<IEntity> entities)
        {
            return JsonConvert.SerializeObject(entities.Where(x => x != null).Select(x => new EntityItem { Guid = x.Guid, Name = x.Name }));
        }

        [HttpGet]
        [Authorize(Roles = Permissions.EditArtefactImage)]
        public ActionResult Edit(string guid)
        {
            var node = resourceService.FindImageHyperNode(guid);
            
            return View("Edit", new EditImage
            {
                Tags = ToEntityItemJson(node.Tags),
               
                ImageNode = node.Image,
                ImageGuid = guid,
                Name = node.Image.Name,
                Description = node.Image.Description,
                Uri = node.Image.Uri,
                Width = node.Image.Width,
                Height = node.Image.Height,
                Source = node.Image.Source,
                ImageUrl = node.Image.SourceUrl,
                PublicDomain = node.Image.PublicDomain ?? false,
                Primary = node.Image.Primary ?? false
            });
        }

        protected ActionResult ToViewDetail(string guid)
        {
            return RedirectToAction("Edit", "Image", new { guid });
        }

        [HttpGet]
        [Authorize(Roles = Permissions.DeleteArtefactImage)]
        public ActionResult Delete(string imageGuid, string artefactGuid)
        {
            resourceService.DeleteArtefactImageHyperNode(imageGuid);
            return ToViewDetail(artefactGuid);
        }

        public IEnumerable<EntityItem> GetItems(string values)
        {
            if (values == null || values.ToLower() == "[null]")
            {
                return new EntityItem[] { };
            }
            try
            {
                var items = JsonConvert.DeserializeObject<EntityItem[]>(values);
                return items;
            }
            catch (Exception ex)
            {
                return new EntityItem[] { };
            }
        }

        [HttpGet]
        public ActionResult List()
        {
            var images = resourceService.GetImages()
                .ToList();

            return View(images);
        }

        [HttpPost]
        [Authorize(Roles = Permissions.EditArtefactImage)]
        public ActionResult Edit(EditImage model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.ImageGuid == null;
                var image = isNew ? new Image() : resourceService.FindImageByGuid(model.ImageGuid);
                var tags = GetItems(model.Tags);
                image.Name = model.Name;
                image.Description = model.Description;
                image.Primary = model.Primary;
                image.PublicDomain = model.PublicDomain;
                image.Source = model.Source;
                resourceService.SaveOrUpdateImage(image, tags);
                if (isNew)
                {
                    SaveImage(model, image);
                }
                return ToViewDetail(image.Guid);
            }
            return View(model);
        }
    }
}