﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public class VideoController : BaseController
    {
        public ActionResult Add()
        {
            var model = new EditVideo();
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult List()
        {
            var results = commonService.GetVideos();
            return View(results);
        }

        [HttpGet]
        public ActionResult Edit(string guid)
        {
            var video = commonService.FindVideoByGuid(guid);
            var model = new EditVideo
            {
                Guid = video.Guid,
                Name = video.Name,
                Description = video.Description,
                StartTime = video.StartTime,
                EndTime = video.EndTime,
                Source = video.Source,
                Uri = video.Uri,
                Width = video.Width,
                Height = video.Height
            };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(EditVideo model)
        {
            if (ModelState.IsValid)
            {
                var video = new Video
                {
                    Guid = model.Guid,
                    Name = model.Name,
                    Description = model.Description,
                    StartTime = model.StartTime,
                    EndTime = model.EndTime,
                    Source = model.Source,
                    Uri = model.Uri,
                    Width = model.Width,
                    Height = model.Height
                };
                await commonService.SaveOrUpdateAsync(video, Label.Entity.Video);
                return RedirectToAction("List");
            }
            return View(model);
        }
    }
}