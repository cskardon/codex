﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public class TourController : ArtefactBaseController
    {
        public TourController(
            IAgentService _personService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IResourceService _resourceService,
            IActService _actService,
            IRelationService _relationService,
            ITextService _textService,
            ICommonService _commonService)
            : base(_personService, _placeService, _artefactService, _resourceService, _actService, _relationService, _textService, _commonService)
        {

        }
        [HttpGet]
        public ActionResult Edit(string artefactGuid)
        {
            var artefact = artefactService.FindArtefactByGuid(artefactGuid);
            return View("Edit", new EditArtefactModel
            {
                ArtefactGuid = artefactGuid,
                Ordinal = artefact.Ordinal,
                Name = artefact.Name,
                CatalogueIdentPrefix = artefact.CatalogueIdentPrefix,
                CatalogueIdent = artefact.CatalogueIdent,
                Article = artefact.Article,
                Rating = artefact.Rating,
                Description = artefact.Description,
                Subtitle = artefact.Subtitle,
                Width = artefact.Width,
                Height = artefact.Height,
                Depth = artefact.Depth,
                Latitude = artefact.Latitude,
                Longitude = artefact.Longitude
            });
        }
        [HttpPost]
        public ActionResult Edit(EditArtefactModel model)
        {
            if (ModelState.IsValid)
            {
                var artefact = artefactService.FindArtefactByGuid(model.ArtefactGuid);
                artefact.Name = model.Name;
                artefact.Ordinal = model.Ordinal;
                artefact.CatalogueIdent = model.CatalogueIdent;
                artefact.CatalogueIdentPrefix = model.CatalogueIdentPrefix;
                artefact.Article = model.Article;
                artefact.Description = model.Description;
                artefact.Rating = model.Rating;
                artefact.Subtitle = model.Subtitle;
                artefact.Width = model.Width;
                artefact.Height = model.Height;
                artefact.Depth = model.Depth;
                artefact.Latitude = model.Latitude;
                artefact.Longitude = model.Longitude;
                artefactService.SaveOrUpdate(artefact);
                return ToViewDetail(model.ArtefactGuid);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult List(string mode = null)
        {
            var tours = artefactService.AllTours();
            var model = new ViewTourList
            {
                Tours = tours
            };
            return View(model);
        }

        public ActionResult Add()
        {
            var model = new AddTourModel
            {

            };
            return View("Add", model);
        }

        [HttpPost]
        public ActionResult Add(AddTourModel model)
        {
            if (ModelState.IsValid)
            {
                var tour = new Artefact();
                tour.Created = DateTimeOffset.Now;
                tour.Name = model.Name;
                tour.Article = model.Article;
                tour.Description = model.Description;
                tour.Rating = model.Rating;
                artefactService.SaveOrUpdate(tour);

                var detail = new CollectionDetail();
                detail.Website = model.Url;

                artefactService.SaveOrUpdate(detail);
                artefactService.HasDetail(tour.Guid, detail.Guid);

                var tourType = categoryService.FindByName(Label.Categories.Tour);
                artefactService.IsA(new GenericRelationHyperNode<Artefact, Category>
                {
                    Source = tour,
                    Relation = new Relation
                    {
                        Primary = true
                    },
                    Destination = tourType
                });
                if (false == string.IsNullOrEmpty(model.LocatedAtGuid))
                {
                    artefactService.StopsAt(new GenericRelationNode<Artefact, Place>
                    {
                        Source = tour,
                        Destination = new Place
                        {
                            Guid = model.LocatedAtGuid
                        }
                    });
                }

                if (false == string.IsNullOrEmpty(model.StopsIn1Guid))
                {
                    artefactService.StopsIn(new GenericRelationNode<Artefact, Artefact>
                    {
                        Source = tour,
                        Destination = new Artefact
                        {
                            Guid = model.StopsIn1Guid
                        }
                    });
                }

                if (false == string.IsNullOrEmpty(model.StopsIn2Guid))
                {
                    artefactService.StopsIn(new GenericRelationNode<Artefact, Artefact>
                    {
                        Source = tour,
                        Destination = new Artefact
                        {
                            Guid = model.StopsIn2Guid
                        }
                    });
                }

                if (false == string.IsNullOrEmpty(model.StopsIn3Guid))
                {
                    artefactService.StopsIn(new GenericRelationNode<Artefact, Artefact>
                    {
                        Source = tour,
                        Destination = new Artefact
                        {
                            Guid = model.StopsIn3Guid
                        }
                    });
                }

                if (false == string.IsNullOrEmpty(model.Artist1Guid))
                {
                    artefactService.Covers(new GenericRelationHyperNode<Artefact, Data.Agent>
                    {
                        Source = tour,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Data.Agent
                        {
                            Guid = model.Artist1Guid
                        },
                    });
                }

                if (false == string.IsNullOrEmpty(model.Artist2Guid))
                {
                    artefactService.Covers(new GenericRelationHyperNode<Artefact, Data.Agent>
                    {
                        Source = tour,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Data.Agent
                        {
                            Guid = model.Artist2Guid
                        },
                    });
                }

                if (false == string.IsNullOrEmpty(model.Artist3Guid))
                {
                    artefactService.Covers(new GenericRelationHyperNode<Artefact, Data.Agent>
                    {
                        Source = tour,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Data.Agent
                        {
                            Guid = model.Artist3Guid
                        },
                    });
                }

                if (false == string.IsNullOrEmpty(model.Artwork1Guid))
                {
                    artefactService.TourFeatures(new GenericRelationHyperNode<Artefact, Artefact>
                    {
                        Source = tour,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Artefact
                        {
                            Guid = model.Artwork1Guid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.Artwork2Guid))
                {
                    artefactService.TourFeatures(new GenericRelationHyperNode<Artefact, Artefact>
                    {
                        Source = tour,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Artefact
                        {
                            Guid = model.Artwork2Guid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.Artwork3Guid))
                {
                    artefactService.TourFeatures(new GenericRelationHyperNode<Artefact, Artefact>
                    {
                        Source = tour,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Artefact
                        {
                            Guid = model.Artwork3Guid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.StyleGuid))
                {
                    artefactService.AssociatedWith(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = tour,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.StyleGuid
                        }
                    });
                }
                if (false == string.IsNullOrEmpty(model.GenreGuid))
                {
                    artefactService.AssociatedWith(new GenericRelationHyperNode<Artefact, Category>
                    {
                        Source = tour,
                        Relation = new Relation
                        {
                            Primary = true
                        },
                        Destination = new Category
                        {
                            Guid = model.GenreGuid
                        }
                    });
                }
                GeneralMessage = string.Format("Created art exhibition '{0}'.", model.Name);
                return ToViewDetail(tour.Guid);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddTourFeatures(string artefactGuid)
        {
            return AddRelation<Artefact>(artefactGuid, "EditTourFeatures", " features ...");
        }
        [HttpGet]
        public ActionResult EditTourFeatures(string artefactGuid, string hasRelationGuid)
        {
            return FindRelation<Artefact>(artefactGuid, hasRelationGuid, "EditTourFeatures", " features ...", artefactService.FindTourFeatures);
        }
        [HttpPost]
        public ActionResult EditTourFeatures(ArtefactToArtefactRelationModel model)
        {
            return SaveRelation<Artefact>(model, artefactService.TourFeatures);
        }
        [HttpGet]
        public ActionResult AddStopsIn(string artefactGuid)
        {
            return AddBasicRelation<Artefact>(artefactGuid, "EditStopsIn", " stops in ...");
        }
        [HttpGet]
        public ActionResult EditStopsIn(string artefactGuid, string hasRelationGuid)
        {
            return FindBasicRelation<Artefact>(artefactGuid, hasRelationGuid, "EditStopsIn", " stops in ...", artefactService.FindStopsIn);
        }
        [HttpPost]
        public ActionResult EditStopsIn(GenericRelationNodeModel<Artefact, Artefact> model)
        {
            return SaveBasicRelation<Artefact>(model, artefactService.StopsIn);
        }
        [HttpGet]
        public ActionResult AddStopsAt(string artefactGuid)
        {
            return AddBasicRelation<Place>(artefactGuid, "EditStopsAt", " stops at ...");
        }
        [HttpGet]
        public ActionResult EditStopsAt(string artefactGuid, string hasRelationGuid)
        {
            return FindBasicRelation<Place>(artefactGuid, hasRelationGuid, "EditStopsAt", " stops at ...", artefactService.FindStopsAt);
        }
        [HttpPost]
        public ActionResult EditStopsAt(GenericRelationNodeModel<Artefact, Place> model)
        {
            return SaveBasicRelation<Place>(model, artefactService.StopsAt);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.ViewArtefact)]
        public ActionResult ViewDetail(string guid)
        {
            var node = new ViewTour();
            node.Artefact = artefactService.FindArtefactByGuid(guid);
            node.Detail = artefactService.FindDetailForArtefact(guid);
            node.OutgoingRelations = commonService.FindOutgoingRelations<Artefact>(guid);
            node.IncomingRelations = commonService.FindIncomingRelations<Artefact>(guid);
            node.BasicOutgoingRelations = commonService.FindBasicOutgoingRelations<Artefact>(guid);
            node.BasicIncomingRelations = commonService.FindBasicIncomingRelations<Artefact>(guid);
            node.IsA = GetDestinationByRelationshipType<Category>(node.OutgoingRelations, Label.Relationship.IsA);
            node.Images = resourceService.FindArtefactImages(guid);
            return View("ViewDetail", node);
        }
        public List<TDest> GetDestinationByRelationshipType<TDest>(IEnumerable<GenericRelationHyperNode<Artefact, IEntity>> nodes, string relationshipType)
           where TDest : class, IEntity
        {
            return nodes
                .Where(x => x.RelationToDestinationType == relationshipType)
                .Select(x => x.Destination)
                .Cast<TDest>()
                .ToList();
        }
        protected override ActionResult ToList()
        {
            return RedirectToAction("List", "Exhibition");
        }
        protected override ActionResult ToViewDetail(string guid)
        {
            return RedirectToAction("ViewDetail", "Tour", new { guid });
        }
    }
}