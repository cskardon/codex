﻿using AutoMapper;
using CodexMVC.Controllers;
using CodexMVC.Helpers;
using CodexMVC.Models;
using Data;
using Data.Entities;
using Newtonsoft.Json;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{

    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class ActController : BaseController
    {
        private readonly IActService actService;
        private readonly IAgentService agentService;
        private readonly IArtefactService artefactService;
        public ActController(
            IActService _actService,
            IArtefactService _artefactService,
            IAgentService _agentService
            )
        {
            actService = _actService;
            artefactService = _artefactService;
            agentService = _agentService;
        }



        [HttpGet]
        public ActionResult AddAct()
        {
            var model = new EditAct
            {
                Act = new EditActModel
                {
                    Milennium = "AD",
                    MilenniumEnd = "AD",
                    Experienceable = false,
                    HistoricalSource = HistoricalSource.Primary
                },
                ProximityModel = Proximity.On,
                ProximityEndModel = Proximity.On,
                SubsetOf = new GenericRelationNode<Act, Act>
                {
                    Destination = new Act()
                }
            };
            return View(model);
        }

        ViewResult View(EditAct model)
        {
            var datasets = dataSetService.GetList()
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Guid
                })
                .ToList();
            ViewBag.DataSets = datasets;
            return base.View("EditAct", model);
        }

        [HttpGet]
        public ActionResult EditAct(string actGuid)
        {
            try
            {
                var newAct = actService.FindNewAct(actGuid);
                var model = new EditAct
                {
                    Act = Mapper.Map<EditActModel>(newAct.Act),
                    NewAct = newAct,
                    SourceGuid = newAct.Source != null ? newAct.Source.Guid : null,
                    HasSourceGuid = newAct.HasSourceGuid,
                    StatedByGuid = newAct.StatedBy != null ? newAct.StatedBy.Guid : null,
                    StatedByRelationGuid = newAct.StatedByGuid,
                    Attachments = ToEntityItemJson(newAct.Attachments),
                    ProximityModel = RazorHelper.ToProximity(newAct.Act.Proximity),
                    ProximityEndModel = RazorHelper.ToProximity(newAct.Act.ProximityEnd),
                    Tags = ToValueItemJson(newAct.TagValues),
                    SubsetOf = newAct.SubsetOf
                };
                return View(model);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return new HttpNotFoundResult();
            }
        }

        private static string ToEntityItemJson(IEnumerable<IEntity> entities)
        {
            if (entities == null)
            {
                return null;
            }
            return JsonConvert.SerializeObject(entities.Where(x => x != null).Select(x => new EntityItem { Guid = x.Guid, Name = x.Name }));
        }

        private static string ToValueItemJson(IEnumerable<TagValueItemNode> entities)
        {
            if (entities == null)
            {
                return null;
            }
            var items = entities
                .Where(x => x != null && x.Tag != null)
                .Select(x => new ValueItem
                {
                    Guid = x.Tag.Guid,
                    Label = Label.Entity.Tag,
                    Name = x.Tag.Name,
                    Value = x.Value != null ? x.Value.Value : null
                });
            return JsonConvert.SerializeObject(items);
        }

        public IEnumerable<EntityItem> GetEntityItems(string values)
        {
            if (values == null || values.ToLower() == "[null]")
            {
                return new EntityItem[] { };
            }
            try
            {
                var items = JsonConvert.DeserializeObject<EntityItem[]>(values);
                return items;
            }
            catch (Exception ex)
            {
                return new EntityItem[] { };
            }
        }

        public IEnumerable<ValueItem> GetValueItems(string values)
        {
            if (values == null || values.ToLower() == "[null]")
            {
                return new ValueItem[] { };
            }
            try
            {
                var items = JsonConvert.DeserializeObject<ValueItem[]>(values);
                return items;
            }
            catch (Exception ex)
            {
                return new ValueItem[] { };
            }
        }

        public IEnumerable<string[]> GetAttachments(EditAct model)
        {
            if (model.Attachments == null || model.Attachments.ToLower() == "[null]")
            {
                return null;
            }
            try
            {
                var json = JsonConvert.DeserializeObject<dynamic[]>(model.Attachments);
                var result = json.Select((dynamic x) => new string[] { (string)x.Label, (string)x.Guid });
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private void AddToStatedBy(Agent agent)
        {
            var statedBy = Session["StatedBy"] as List<Agent>;
            if (statedBy == null)
            {
                statedBy = new List<Agent>();
            }
            if (false == statedBy.Any(x => x.Guid == agent.Guid))
            {
                statedBy.Insert(0, agent);
            }
            Session["StatedBy"] = statedBy;
        }

        [HttpPost]
        public ActionResult EditAct(EditAct model)
        {
            if (ModelState.IsValid)
            {
                var pairs = RazorHelper.GetPairs(model.Act.Description);
                var entities = pairs.Select(x => x.Split(new char[] { '|' }));
                var attachments = GetAttachments(model);
                var tags = GetValueItems(model.Tags);
                model.Act.Proximity = RazorHelper.FromProximity(model.ProximityModel);
                model.Act.ProximityEnd = RazorHelper.FromProximity(model.ProximityEndModel);
                actService.SaveOrUpdateAct(model.Act, entities, attachments, tags);
                if (model.SubsetOf.IsSaveable || model.SubsetOf.IsExisting)
                {
                    actService.SubsetOf(new GenericRelationNode<Act, Act>
                    {
                        Source = model.Act,
                        RelationToDestinationGuid = model.SubsetOf.RelationToDestinationGuid,
                        Destination = model.SubsetOf.Destination
                    });
                }
                model.NewAct = actService.FindNewAct(model.Act.Guid);
                if (false == string.IsNullOrEmpty(model.StatedByGuid))
                {
                    var statedBy = agentService.FindAgentByGuid(model.StatedByGuid);
                    AddToStatedBy(statedBy);
                }
                actService.StatedBy(new GenericRelationNode<Act, Agent>
                {
                    Source = model.Act,
                    RelationToDestinationGuid = model.StatedByRelationGuid,
                    Destination = new Agent
                    {
                        Guid = model.StatedByGuid
                    }
                });
                actService.HasSource(new GenericRelationNode<Act, Artefact>
                {
                    Source = model.Act,
                    RelationToDestinationGuid = model.HasSourceGuid,
                    Destination = new Artefact
                    {
                        Guid = model.SourceGuid
                    }
                });
                return RedirectToAction("EditAct", new { actGuid = model.Act.Guid });
            }
            return View(model);
        }

        private static List<string> Separate(string value, char separator)
        {
            var values = string.IsNullOrEmpty(value) ? new List<string>() : value.Split(separator).ToList();
            return values;
        }

        [HttpGet]
        public ActionResult Delete(string actGuid, string entityType, string entityGuid)
        {
            var result = actService.DeleteNewAct(actGuid);
            AlertOnDanger(result);
            AlertOnSuccess(result, "The act was successfully deleted.");
            return RedirectToAction("ViewDetail", entityType, new { guid = entityGuid });
        }

        [HttpGet]
        public ActionResult DeleteAct(string actGuid)
        {
            var result = actService.DeleteNewAct(actGuid);
            AlertOnDanger(result);
            AlertOnSuccess(result, "The act was successfully deleted.");
            return Redirect(Request.UrlReferrer.AbsoluteUri);
        }
    }
}