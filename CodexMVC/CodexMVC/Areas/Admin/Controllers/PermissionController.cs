﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class PermissionController : BaseController
    {
        private readonly IUserService userService;
        public PermissionController(IUserService _userService)
        {
            userService = _userService;
        }
        [HttpGet]
        public ActionResult List()
        {
            return View(userService.AllPermissions());
        }
        [HttpGet]
        // [Authorize(Roles = "EditPermission")]
        public ActionResult Add()
        {
            return View("Edit", new EditPermissionModel());
        }
        [HttpGet]
        // [Authorize(Roles = "EditPermission")]
        public ActionResult Edit(string permissionGuid)
        {
            var permission = userService.FindPermission(permissionGuid);
            return View(new EditPermissionModel
            {
                PermissionGuid = permissionGuid,
                Name = permission.Name,
                Description = permission.Description
            });
        }
        [HttpPost]
        // [Authorize(Roles = Permissions.EditPermission)]
        public ActionResult Edit(EditPermissionModel model)
        {
            if (ModelState.IsValid)
            {
                var isModified = model.PermissionGuid != null;
                var permission = isModified ? userService.FindPermission(model.PermissionGuid) : new Permission();
                permission.Name = model.Name;
                permission.Description = model.Description;
                if (isModified)
                {
                    userService.SaveOrUpdate(permission);
                }
                else
                {
                    userService.SaveOrUpdate(permission);
                }
                return RedirectToAction("ViewDetail", "Permission", new { permissionGuid = permission.Guid });
            }
            return View(model);
        }
        [HttpGet]
        [Authorize(Roles = "ViewPermission")]
        public ActionResult ViewDetail(string permissionGuid)
        {
            var permission = userService.FindPermission(permissionGuid);
            var roles = userService.FindRolesAssociatedWithPermission(permissionGuid);
            return View(new ViewPermissionModel
            {
                Permission = permission,
                RolesAssociatedWith = roles
            });
        }
    }
}