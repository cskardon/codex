﻿using CodexMVC.Helpers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class StatsController : Controller
    {
        private readonly IAgentService agentService;
        public StatsController(IAgentService _agentService)
        {
            agentService = _agentService;
        }
        [HttpGet]
        public ActionResult Index()
        {
            var stats = agentService.AllStats();
            return View("Index", stats);
        }
    }
}