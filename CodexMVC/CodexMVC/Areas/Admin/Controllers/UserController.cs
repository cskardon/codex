﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using Services.Common;
using System;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    [Authorize(Roles = Permissions.ListUsers)]
    public class UserController : BaseController
    {
        private readonly IUserService userService;
        private readonly IAgentService agentService;
        private readonly IArtefactService artefactService;
        private readonly IActService actService;
        private readonly ICommonService commonService;
        public UserController(
            IUserService _userService,
            IActService _actService,
            IAgentService _agentService,
            IArtefactService _artefactService,
            ICommonService _commonService
            )
        {
            userService = _userService;
            actService = _actService;
            agentService = _agentService;
            artefactService = _artefactService;
            commonService = _commonService;
        }
        [Authorize(Roles = Permissions.ResetPassword)]
        [HttpGet]
        public ActionResult ResetPassword(string guid)
        {
            var model = new AdminResetPasswordModel
            {
                UserGuid = guid
            };
            return View(model);
        }
        [Authorize(Roles = Permissions.ResetPassword)]
        [HttpPost]
        public ActionResult ResetPassword(AdminResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                userService.ResetPassword(model.UserGuid, model.NewPassword);
                return RedirectToAction("ResetPasswordComplete");
            }
            return View(model);
        }
        [Authorize(Roles = Permissions.ResetPassword)]
        [HttpGet]
        public ActionResult ResetPasswordComplete()
        {
            return View();
        }
        [Authorize(Roles = Permissions.ViewAuditTrail)]
        [HttpGet]
        public ActionResult AuditTrail(string guid)
        {
            var user = userService.FindUserByGuid(guid);
            var trail = commonService.GetAuditTrail(guid);
            var model = new ViewAuditTrail
            {
                User = user,
                Trail = trail
            };
            return View(model);
        }
        [HttpGet]
        public ActionResult Itinerary()
        {
            var itineraryItems = userService.FindItineraryItemHyperNodes(CurrentUser.Guid);
            var model = new ViewItinerary
            {
                User = CurrentUser,
                ItineraryItems = itineraryItems
            };
            return View(model);
        }
        [HttpGet]
        public ActionResult List()
        {
            var users = userService.AllUsers();
            return View(users);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.AddRoleToUser)]
        public ActionResult AddRole(string userGuid)
        {
            return View(new AddRoleToUserModel
            {
                UserGuid = userGuid
            });
        }
        [HttpPost]
        [Authorize(Roles = Permissions.AddRoleToUser)]
        public ActionResult AddRole(AddRoleToUserModel model)
        {
            var result = userService.AddRoleToUser(model.UserGuid, model.RoleGuid);
            GeneralMessage = result ? "Succeeded in adding role to user" : "Failed in adding role to user";
            return RedirectToAction("ViewDetail", "User", new { userGuid = model.UserGuid });
        }
        [HttpGet]
        [Authorize(Roles = Permissions.ViewRole)]
        public ActionResult ViewDetail(string userGuid)
        {
            var user = userService.FindUserByGuid(userGuid);
            var userRoles = userService.GetRolesForUser(userGuid);
            return View(new ViewUserModel
            {
                User = user,
                Roles = userRoles
            });
        }
        [HttpGet]
        [Authorize(Roles = Permissions.EditUser)]
        public ActionResult Add()
        {
            return View("Edit", new EditUserModel());
        }
        [HttpGet]
        [Authorize(Roles = Permissions.EditUser)]
        public ActionResult Edit(string userGuid)
        {
            var user = userService.FindUser(userGuid);
            return View(new EditUserModel
            {
                UserGuid = userGuid,
                Username = user.Username
            });
        }
        [HttpPost]
        [Authorize(Roles = Permissions.EditUser)]
        public ActionResult Edit(EditUserModel model)
        {
            if (ModelState.IsValid)
            {
                var user = model.UserGuid != null ? userService.FindUserByGuid(model.UserGuid) : new User();
                user.Username = model.Username;
                user.Password = model.Password;
                userService.SaveOrUpdate(user);
                return RedirectToAction("ViewDetail", "User", new { userGuid = user.Guid });
            }
            return View(model);
        }
    }
}