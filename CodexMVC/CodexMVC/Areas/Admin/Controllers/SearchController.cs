﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Services;
using Services.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class SearchController : BaseController
    {
        private readonly IActService actService;
        private readonly IAgentService agentService;
        private readonly ICategoryService categoryService;
        private readonly IAgentService personService;
        private readonly IPlaceService placeService;
        private readonly IArtefactService artefactService;
        public SearchController(
            IAgentService _personService,
            IPlaceService _placeService,
            IAgentService _agentService,
            IArtefactService _artefactService,
            ICategoryService _categoryService,
            IActService _actService
        )
        {
            personService = _personService;
            placeService = _placeService;
            artefactService = _artefactService;
            actService = _actService;
            agentService = _agentService;
            categoryService = _categoryService;
        }
       
    }
}