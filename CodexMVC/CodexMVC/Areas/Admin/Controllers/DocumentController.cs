﻿using CodexMVC.Controllers;
using Services;
using Services.Common;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class DocumentController : BaseController
    {
        private readonly IActService actService;
        public DocumentController(IActService _actService)
        {
            actService = _actService;
        }
        public ActionResult ViewDetail(string guid)
        {
            var node = actService.FindDocumentHyperNode(guid);
            return View(node);
        }
    }
}