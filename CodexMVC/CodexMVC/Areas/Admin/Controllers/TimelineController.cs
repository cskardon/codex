﻿using CodexMVC.Controllers;
using CodexMVC.Helpers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using Services.Common;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class TimelineController : BaseController
    {
        private readonly IAgentService agentService;
        private readonly IPlaceService placeService;
        private readonly IArtefactService artefactService;
        private readonly IResourceService resourceService;
        private readonly ICategoryService categoryService;
        private readonly IActService actService;
        private readonly IRelationService relationService;
        public TimelineController(
            IAgentService _agentService,
            IPlaceService _placeService,
            IArtefactService _artefactService,
            IActService _actService,
            IResourceService _resourceService,
            IRelationService _relationService,
            ICategoryService _categoryService
        )
        {
            agentService = _agentService;
            placeService = _placeService;
            artefactService = _artefactService;
            resourceService = _resourceService;
            actService = _actService;
            relationService = _relationService;
            categoryService = _categoryService;
        }
        [HttpGet]
        public ActionResult IntersectCategory()
        {
            var model = new ViewIntersectCategoryTimeline();
            return View(model);
        }
        [HttpPost]
        public ActionResult IntersectCategory(ViewIntersectCategoryTimeline model)
        {
            if (ModelState.IsValid)
            {
                var timeline = actService.FindNewActsThatIntersectWithCategory(model.AgentGuid, model.CategoryGuid);
                model.Timeline = timeline;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Dates()
        {
            var model = new ViewTimelineDates();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Dates(ViewTimelineDates model)
        {
            if (ModelState.IsValid)
            {
                var timeline = actService.FindNewActsByDates(model.Year, model.YearEnd);
                model.Timeline = timeline;
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Index()
        {
            var timelines = new Timelines
            {
                Time = new Dictionary<string, List<Timeline>>()
            };
            var model = new ViewCompareTimeline
            {
                Timelines = timelines
            };
            return View("Compare", model);
        }

        public async Task<IEnumerable<NewAct>> GetFirstTimeline(ViewCompareTimeline model)
        {
            if (model.Agent1 != null)
            {
                return await actService.FindActsByAgent(model.Agent1);
            }
            if (model.Place1 != null)
            {
                return await actService.FindNewActsByPlaceAsync(model.Place1);
            }
            if (model.Artefact1 != null)
            {
                return await actService.FindNewActsByArtefact(model.Artefact1);
            }
            if (model.Category1 != null)
            {
                return await actService.FindNewActsByCategory(model.Category1);
            }
            return null;
        }

        public async Task<IEnumerable<NewAct>> GetSecondTimeline(ViewCompareTimeline model)
        {
            if (model.Agent2 != null)
            {
                return await actService.FindActsByAgent(model.Agent2);
            }
            if (model.Place2 != null)
            {
                return await actService.FindNewActsByPlaceAsync(model.Place2);
            }
            if (model.Artefact2 != null)
            {
                return await actService.FindNewActsByArtefact(model.Artefact2);
            }
            if (model.Category2 != null)
            {
                return await actService.FindNewActsByCategory(model.Category2);
            }
            return null;
        }
        public IEntity GetFirstEntity(ViewCompareTimeline model)
        {
            if (model.Agent1 != null)
            {
                return agentService.FindAgentByGuid(model.Agent1);
            }
            if (model.Place1 != null)
            {
                return placeService.FindPlaceByGuid(model.Place1);
            }
            if (model.Artefact1 != null)
            {
                return artefactService.FindArtefactByGuid(model.Artefact1);
            }
            if (model.Category1 != null)
            {
                return CategoryServiceCache.AllWithAncestors().FirstOrDefault(x => x.Child.Guid == model.Category1).Child;
            }
            return null;
        }
        public IEntity GetSecondEntity(ViewCompareTimeline model)
        {
            if (model.Agent2 != null)
            {
                return agentService.FindAgentByGuid(model.Agent2);
            }
            if (model.Place2 != null)
            {
                return placeService.FindPlaceByGuid(model.Place2);
            }
            if (model.Artefact2 != null)
            {
                return artefactService.FindArtefactByGuid(model.Artefact2);
            }
            if (model.Category2 != null)
            {
                return CategoryServiceCache.AllWithAncestors().FirstOrDefault(x => x.Child.Guid == model.Category2).Child;
            }
            return null;
        }

        [HttpPost]
        public async Task<ActionResult> Compare(ViewCompareTimeline model)
        {
            if (ModelState.IsValid)
            {
                var firstTimline = await GetFirstTimeline(model);
                var secondTimeline = await GetSecondTimeline(model);
                var years1 = firstTimline.Select(x => x.Act.TimelineDate());
                var years2 = secondTimeline.Select(x => x.Act.TimelineDate());
                var years = years1.Concat(years2).Distinct().OrderBy(x => x);
                var timelines = new Timelines
                {
                    Time = new Dictionary<string, List<Timeline>>()
                };
                var entity1 = GetFirstEntity(model);
                var entity2 = GetSecondEntity(model);
                foreach (var year in years)
                {
                    var timeline1 = new Timeline
                    {
                        Agent = entity1,
                        NewActs = firstTimline.Where(x => x.Act.TimelineDate() == year).ToList()
                    };
                    var timeline2 = new Timeline
                    {
                        Agent = entity2,
                        NewActs = secondTimeline.Where(x => x.Act.TimelineDate() == year).ToList()
                    };
                    timelines.Time.Add(year, new List<Timeline> { timeline1, timeline2 });
                }
                model.Timelines = timelines;
            }
            else
            {
                var timelines = new Timelines
                {
                    Time = new Dictionary<string, List<Timeline>>()
                };
                model.Timelines = timelines;
            }
            return View("Compare", model);
        }
    }
}