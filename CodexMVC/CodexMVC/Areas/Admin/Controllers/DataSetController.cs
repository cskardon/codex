﻿using CodexMVC.Controllers;
using Data.Entities;
using Services;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public class DataSetController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult> Index(string guid)
        {
            var data = await dataSetService.FindDataSetAsync(guid);
            return View("Index", data);
        }

        public ActionResult Add()
        {
            var model = new DataSetHyperNode
            {
                DataSet = new DataSet(),
                Parent = new DataSet(),
                About = new Tag(),
                Source = new Artefact()
            };
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(string guid)
        {
            var model = await dataSetService.FindDataSetAsync(guid);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(DataSetHyperNode model)
        {
            if (ModelState.IsValid)
            {
                await dataSetService.SaveOrUpdateAsync(model);
                return RedirectToAction("List");
            }
            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> List()
        {
            var results = (await dataSetService.GetListAsync()).OrderBy(x => x.Name).ToList();
            return View(results);
        }

        ViewResult View(DataSetHyperNode model)
        {
            var tags = actService.GetTags()
                .Select(x => new SelectListItem { Text = x.Tag.Name, Value = x.Tag.Guid })
                .OrderBy(x => x.Text)
                .ToList();
            var datasets = dataSetService.GetList()
                .Where(x => model.DataSet != null && x.Guid != model.DataSet.Guid)
                .Select(x => new SelectListItem { Text = x.Name, Value = x.Guid })
                .ToList();
            var sources = DependencyResolver.Current.GetService<IArtefactService>().All().Select(x => new SelectListItem
            {
                Text = x.ToDisplay(),
                Value = x.Guid
            })
            .OrderBy(x => x.Text)
            .ToList();
            tags.Insert(0, new SelectListItem { });
            datasets.Insert(0, new SelectListItem { });
            sources.Insert(0, new SelectListItem { });
            ViewBag.Tags = tags;
            ViewBag.DataSets = datasets;
            ViewBag.Sources = sources;
            return base.View("Edit", model);
        }
    }
}