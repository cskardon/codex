﻿using CodexMVC.Models;
using Data.Entities;
using Services;
using System.Collections.Generic;
using System.Web.Mvc;
using Services.Helpers;
using CodexMVC.Controllers;
using Services.Common;
using System.Threading.Tasks;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    public class CategoryController : BaseController
    {
        private readonly IAgentService personService;
        private readonly IRelationService relationService;
        private readonly ICategoryService categoryService;
        private readonly IActService actService;
        public CategoryController(
            IAgentService _personService,
            ICategoryService _categoryService,
            IActService _actService,
            IRelationService _relationService
        )
        {
            actService = _actService;
            personService = _personService;
            categoryService = _categoryService;
            relationService = _relationService;
        }

        [HttpGet]
        public ActionResult Delete(string categoryGuid)
        {
            var category = categoryService.Find(categoryGuid);
            var deletedRelationships = categoryService.Delete(categoryGuid);
            GeneralMessage = string.Format("Category '{0}' ({1}) and {2} relationships were deleted.", category.Name, categoryGuid, deletedRelationships);
            return RedirectToAction("List", "Category");
        }
       
        [HttpGet]
        public ActionResult AddAestheticConvention()
        {
            var model = new AddAestheticConventionModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddAestheticConvention(AddAestheticConventionModel model)
        {
            if (ModelState.IsValid)
            {
                var motif = new Category
                {
                    Name = model.Name,
                    Description = model.Description
                };
                categoryService.SaveOrUpdate(motif);
                var parentGuid = model.ParentGuid != null ? model.ParentGuid : categoryService.FindByName(Label.Categories.AestheticConvention).Guid;
                categoryService.KindOf(motif.Guid, parentGuid);
                return RedirectToAction("ViewDetail", "Category", new { guid = motif.Guid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddImplement()
        {
            var model = new AddImplementModel
            {
                Action = "AddImplement",
                Title = "Add Implement"
            };
            return View("AddBaseCategory", model);
        }
        [HttpPost]
        public ActionResult AddImplement(AddImplementModel model)
        {
            if (ModelState.IsValid)
            {
                return SaveCategory(model, Label.Categories.Implement);
            }
            return View(model);
        }
        private ActionResult SaveCategory(BaseAddCategoryModel model, string parentName = null)
        {
            var category = new Category
            {
                Name = model.Name,
                Description = model.Description
            };
            categoryService.SaveOrUpdate(category);
            var parentGuid = model.ParentGuid != null ? model.ParentGuid : categoryService.FindByName(parentName).Guid;
            categoryService.KindOf(category.Guid, parentGuid);
            return RedirectToAction("ViewDetail", "Category", new { guid = category.Guid });
        }
        [HttpGet]
        public ActionResult AddInterest()
        {
            var model = new AddInterestModel
            {
                Action = "AddInterest",
                Title = "Add Interest"
            };
            return View("AddBaseCategory", model);
        }
        [HttpPost]
        public ActionResult AddInterest(AddInterestModel model)
        {
            if (ModelState.IsValid)
            {
                return SaveCategory(model, Label.Categories.Interest);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddArtistType()
        {
            var model = new AddArtistTypeModel
            {
                Action = "AddArtistType",
                Title = "Add Artist Type"
            };
            return View("AddBaseCategory", model);
        }
        [HttpPost]
        public ActionResult AddArtistType(AddArtistTypeModel model)
        {
            if (ModelState.IsValid)
            {
                return SaveCategory(model, Label.Categories.Artist);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddStyle()
        {
            var model = new AddStyleModel
            {
                Action = "AddStyle",
                Title = "Add Style"
            };
            return View("AddBaseCategory", model);
        }
        [HttpPost]
        public ActionResult AddStyle(AddStyleModel model)
        {
            if (ModelState.IsValid)
            {
                return SaveCategory(model, Label.Categories.ArtMovement);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddSubstance()
        {
            var model = new AddSubstanceModel
            {
                Action = "AddSubstance",
                Title = "Add Substance"
            };
            return View("AddBaseCategory", model);
        }
        [HttpPost]
        public ActionResult AddSubstance(AddSubstanceModel model)
        {
            if (ModelState.IsValid)
            {
                return SaveCategory(model, Label.Categories.Substance);
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddGenre()
        {
            var model = new AddGenreModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddGenre(AddGenreModel model)
        {
            if (ModelState.IsValid)
            {
                var motif = new Category
                {
                    Name = model.Name,
                    Description = model.Description
                };
                categoryService.SaveOrUpdate(motif);
                var parentGuid = model.ParentGuid != null ? model.ParentGuid : categoryService.FindByName(Label.Categories.ArtisticGenre).Guid;
                categoryService.KindOf(motif.Guid, parentGuid);
                return RedirectToAction("ViewDetail", "Category", new { guid = motif.Guid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddSubject()
        {
            var model = new AddSubjectModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddSubject(AddSubjectModel model)
        {
            if (ModelState.IsValid)
            {
                var motif = new Category
                {
                    Name = model.Name,
                    Description = model.Description
                };
                categoryService.SaveOrUpdate(motif);
                var parentGuid = model.ParentGuid != null ? model.ParentGuid : categoryService.FindByName(Label.Categories.ArtisticSubject).Guid;
                categoryService.KindOf(motif.Guid, parentGuid);
                return RedirectToAction("ViewDetail", "Category", new { guid = motif.Guid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddMotif()
        {
            var model = new AddMotifModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult AddMotif(AddMotifModel model)
        {
            if (ModelState.IsValid)
            {
                var motif = new Category
                {
                    Name = model.Name,
                    Description = model.Description
                };
                categoryService.SaveOrUpdate(motif);
                var parentGuid = model.ParentGuid != null ? model.ParentGuid : categoryService.FindByName("Motif").Guid;
                categoryService.KindOf(motif.Guid, parentGuid);
                return RedirectToAction("ViewDetail", "Category", new { guid = motif.Guid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult DeleteAttribute(string relationshipGuid, string categoryGuid)
        {
            var deleted = relationService.DeleteAttribute(relationshipGuid);
            GeneralMessage = string.Format("{0} relationship(s) deleted.", deleted);
            return RedirectToAction("ViewDetail", "Category", new { guid = categoryGuid });
        }
        [HttpGet]
        public ActionResult AddPartOf(string guid)
        {
            var category = categoryService.Find(guid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = guid,
                Name = category.Name
            };
            return View("EditPartOf", model);
        }
        [HttpGet]
        public ActionResult EditPartOf(string categoryGuid, string partOfGuid)
        {
            var node = categoryService.FindPartOfHyperNode(categoryGuid, partOfGuid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = categoryGuid,
                OtherCategoryGuid = partOfGuid,
                RelationshipGuid = node.RelationshipGuid
            };
            return View("EditPartOf", model);
        }
        [HttpPost]
        public ActionResult EditPartOf(EditCategoryRelatedToCategory model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.RelationshipGuid == null;
                if (isNew)
                {
                    categoryService.PartOf(model.CategoryGuid, model.OtherCategoryGuid);
                }
                else
                {
                    categoryService.UpdatePartOf(model.CategoryGuid, model.OtherCategoryGuid, model.RelationshipGuid);
                }
                return RedirectToAction("ViewDetail", new { guid = model.CategoryGuid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddQualifiedBy(string guid)
        {
            var category = categoryService.Find(guid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = guid,
                Name = category.Name
            };
            return View("EditQualifiedBy", model);
        }
        [HttpGet]
        public ActionResult EditQualifiedBy(string categoryGuid, string qualifierGuid)
        {
            var node = categoryService.FindQualifiedByHyperNode(categoryGuid, qualifierGuid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = categoryGuid,
                OtherCategoryGuid = qualifierGuid,
                RelationshipGuid = node.RelationshipGuid
            };
            return View("EditQualifiedBy", model);
        }
        [HttpPost]
        public ActionResult EditQualifiedBy(EditCategoryRelatedToCategory model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.RelationshipGuid == null;
                if (isNew)
                {
                    categoryService.QualifiedBy(model.CategoryGuid, model.OtherCategoryGuid);
                }
                else
                {
                    categoryService.UpdateQualifiedBy(model.CategoryGuid, model.OtherCategoryGuid, model.RelationshipGuid);
                }
                return RedirectToAction("ViewDetail", new { guid = model.CategoryGuid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddKindOf(string guid)
        {
            var category = categoryService.Find(guid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = guid,
                Name = category.Name
            };
            return View("EditKindOf", model);
        }
        [HttpGet]
        public ActionResult EditKindOf(string categoryGuid, string otherCategoryGuid)
        {
            var node = categoryService.FindKindOfHyperNode(categoryGuid, otherCategoryGuid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = categoryGuid,
                Name = node.Category.Name,
                OtherCategoryGuid = otherCategoryGuid,
                RelationshipGuid = node.RelationshipGuid
            };
            return View("EditKindOf", model);
        }
        [HttpPost]
        public ActionResult EditKindOf(EditCategoryRelatedToCategory model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.RelationshipGuid == null;
                if (isNew)
                {
                    categoryService.KindOf(model.CategoryGuid, model.OtherCategoryGuid);
                }
                else
                {
                    categoryService.UpdateKindOf(model.CategoryGuid, model.OtherCategoryGuid, model.RelationshipGuid);
                }
                return RedirectToAction("ViewDetail", new { guid = model.CategoryGuid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddSimilarTo(string guid)
        {
            var category = categoryService.Find(guid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = guid,
                Name = category.Name
            };
            return View("EditSimilarTo", model);
        }
        [HttpGet]
        public ActionResult EditSimilarTo(string categoryGuid, string otherCategoryGuid)
        {
            var node = categoryService.FindSimilarToHyperNode(categoryGuid, otherCategoryGuid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = categoryGuid,
                Name = node.Category.Name,
                OtherCategoryGuid = otherCategoryGuid,
                RelationshipGuid = node.RelationshipGuid
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult EditSimilarTo(EditCategoryRelatedToCategory model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.RelationshipGuid == null;
                if (isNew)
                {
                    categoryService.SimilarTo(model.CategoryGuid, model.OtherCategoryGuid);
                }
                else
                {
                    categoryService.UpdateSimilarTo(model.CategoryGuid, model.OtherCategoryGuid, model.RelationshipGuid);
                }
                return RedirectToAction("ViewDetail", new { guid = model.CategoryGuid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddOppositeOf(string guid)
        {
            var category = categoryService.Find(guid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = guid,
                Name = category.Name
            };
            return View("EditOppositeOf", model);
        }
        [HttpGet]
        public ActionResult EditOppositeOf(string categoryGuid, string otherCategoryGuid)
        {
            var node = categoryService.FindOppositeOfHyperNode(categoryGuid, otherCategoryGuid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = categoryGuid,
                Name = node.Category.Name,
                OtherCategoryGuid = otherCategoryGuid,
                RelationshipGuid = node.RelationshipGuid
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult EditOppositeOf(EditCategoryRelatedToCategory model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.RelationshipGuid == null;
                if (isNew)
                {
                    categoryService.OppositeOf(model.CategoryGuid, model.OtherCategoryGuid);
                }
                else
                {
                    categoryService.UpdateOppositeOf(model.CategoryGuid, model.OtherCategoryGuid, model.RelationshipGuid);
                }
                return RedirectToAction("ViewDetail", new { guid = model.CategoryGuid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult AddProducedBy(string guid)
        {
            var category = categoryService.Find(guid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = guid,
                Name = category.Name
            };
            return View("EditProducedBy", model);
        }
        [HttpGet]
        public ActionResult EditProducedBy(string categoryGuid, string otherCategoryGuid)
        {
            var node = categoryService.FindProducedByHyperNode(categoryGuid, otherCategoryGuid);
            var model = new EditCategoryRelatedToCategory
            {
                CategoryGuid = categoryGuid,
                Name = node.Category.Name,
                OtherCategoryGuid = otherCategoryGuid,
                RelationshipGuid = node.RelationshipGuid
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult EditProducedBy(EditCategoryRelatedToCategory model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.RelationshipGuid == null;
                if (isNew)
                {
                    categoryService.ProducedBy(model.CategoryGuid, model.OtherCategoryGuid);
                }
                else
                {
                    categoryService.UpdateProducedBy(model.CategoryGuid, model.OtherCategoryGuid, model.RelationshipGuid);
                }
                return RedirectToAction("ViewDetail", new { guid = model.CategoryGuid });
            }
            return View(model);
        }
        public async Task<ActionResult> ViewDetail(string guid)
        {
            var category = categoryService.Find(guid);
            var newActs = await actService.FindNewActsByCategory(guid);
            var model = new ViewCategory();
            model.Category = category;
            model.NewActs = newActs;
            model.Attributes = categoryService.FindAllAttributes(guid);
            model.AttributesIncoming = categoryService.FindAllAttributesIncoming(guid);
            model.CategoryToArtefactIncoming = categoryService.FindIncomingArtefactRelationships(guid);
            return View("ViewDetail", model);
        }

        [HttpGet]
        public ActionResult List()
        {
            var categories = GetList();
            ViewBag.GeneralMessage = GeneralMessage;
            return View(categories);
        }

        private List<CategoryResult> GetList()
        {
            if (GetFromCache<List<CategoryResult>>(CacheKey.CategoryService.AllWithAncestors) == null)
            {
                AddToCache(CacheKey.CategoryService.AllWithAncestors, categoryService.AllWithAncestors());
            }
            return GetFromCache<List<CategoryResult>>(CacheKey.CategoryService.AllWithAncestors);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View("Edit", new EditCategory());
        }
        [HttpGet]
        public ActionResult Edit(string guid)
        {
            var category = categoryService.Find(guid);
            var model = new EditCategory
            {
                Guid = guid,
                Name = category.Name,
                Clade = category.Clade,
                Adjective = category.Adjective,
                Description = category.Description
            };
            return View("Edit", model);
        }
        [HttpPost]
        public ActionResult Edit(EditCategory model)
        {
            if (ModelState.IsValid)
            {
                var isNew = model.Guid == null;
                var category = isNew ? new Category() : categoryService.Find(model.Guid);
                category.Name = model.Name;
                category.Clade = model.Clade;
                category.Adjective = model.Adjective;
                category.Description = model.Description;
                categoryService.SaveOrUpdate(category);
                GeneralMessage = string.Format("Created category '{0}'.", model.Name);
                return RedirectToAction("ViewDetail", new { guid = category.Guid });
            }
            return View(model);
        }
    }
}