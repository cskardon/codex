﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public class TagController : BaseController
    {
        [HttpGet]
        public ActionResult AddSubsetOfTag(string tagGuid)
        {
            return AddBasicRelation<Tag>(tagGuid, "EditSubsetOfTag", "is a subset of ...");
        }

        [HttpGet]
        public ActionResult EditSubsetOfTag(string tagGuid, string hasRelationGuid)
        {
            return FindBasicRelation<Tag>(tagGuid, hasRelationGuid, "EditSubsetOfTag", "is a subset of ...", actService.FindTagSubsetOfTag);
        }

        [HttpPost]
        public ActionResult EditSubsetOfTag(GenericRelationNodeModel<Tag, Tag> model)
        {
            return SaveBasicRelation<Tag>(model, actService.TagSubsetOfTag);
        }

        [HttpGet]
        public ActionResult AddRefersToCategory(string tagGuid)
        {
            return AddBasicRelation<Category>(tagGuid, "EditRefersToCategory", "refers to ...");
        }

        [HttpGet]
        public ActionResult EditRefersToCategory(string tagGuid, string hasRelationGuid)
        {
            return FindBasicRelation<Category>(tagGuid, hasRelationGuid, "EditRefersToCategory", "refers to at ...", actService.FindTagRefersToCategory);
        }

        [HttpPost]
        public ActionResult EditRefersToCategory(GenericRelationNodeModel<Tag, Category> model)
        {
            return SaveBasicRelation<Category>(model, actService.TagRefersToCategory);
        }

        protected ActionResult FindBasicRelation<TDest>(string artefactGuid, string toDestGuid, string action, string title, Func<string, string, GenericRelationNode<Tag, TDest>> getter)
            where TDest : class, IEntity
        {
            return FindRelationTest<Tag, TDest>(artefactGuid, toDestGuid, action, title, getter);
        }

        protected ActionResult AddBasicRelation<TDest>(string tagGuid, string action, string title)
            where TDest : class, IEntity
        {
            var tag = actService.FindTagByGuid(tagGuid);
            return AddRelationTest<Tag, TDest>(tag, action, title);
        }

        protected ActionResult SaveBasicRelation<TDest>(GenericRelationNodeModel<Tag, TDest> model, Action<GenericRelationNode<Tag, TDest>> updater)
            where TDest : class, IEntity
        {
            return SaveRelationTest<Tag, TDest>(model, updater, actService.FindTagByGuid);
        }

        protected ActionResult SaveRelationTest<TSource, TDest>(GenericRelationNodeModel<TSource, TDest> model, Action<GenericRelationNode<TSource, TDest>> updater, Func<string, TSource> sourceGetter)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            if (ModelState.IsValid)
            {
                var node = new GenericRelationNode<TSource, TDest>
                {
                    Source = model.Node.Source,
                    RelationToDestinationGuid = model.Node.RelationToDestinationGuid,
                    Destination = model.Node.Destination
                };
                updater(node);
                return RedirectToAction("ViewDetail", new { guid = model.Node.Source.Guid });
            }
            var type = typeof(TDest).UnderlyingSystemType.Name;
            model.Node.Source = sourceGetter(model.Node.Source.Guid);
            return View("EditBasicRelatedTo" + type, model);
        }

        protected ActionResult AddRelationTest<TSource, TDest>(TSource source, string action, string title)
            where TSource : class, IEntity
            where TDest : class, IEntity
        {
            var model = new GenericRelationNodeModel<TSource, TDest>
            {
                Action = action,
                Title = source.Name + " " + title,
                Node = new GenericRelationNode<TSource, TDest>
                {
                    Source = source
                }
            };
            var type = typeof(TDest).UnderlyingSystemType.Name;
            return View("EditBasicRelatedTo" + type, model);
        }

        [HttpGet]
        public ActionResult DeleteBasicRelationship(string relationshipType, string relationshipGuid, string tagGuid)
        {
            commonService.DeleteRelationship(relationshipType, relationshipGuid);
            return ToViewDetail(tagGuid);
        }

        private ActionResult ToViewDetail(string guid)
        {
            return RedirectToAction("ViewDetail", "Tag", new { guid });
        }

        public ActionResult Add()
        {
            var model = new EditTag();
            return View("Edit", model);
        }

        [HttpGet]
        public ActionResult ViewDetail(string guid)
        {
            var outgoing = commonService.FindBasicOutgoingRelations<Tag>(guid);
            var incoming = commonService.FindBasicIncomingRelations<Tag>(guid);
            var tag = actService.FindTagByGuid(guid);
            var result = new ViewTag
            {
                Tag = tag,
                Incoming = incoming,
                Outgoing = outgoing
            };
            return View(result);
        }

        [HttpGet]
        public ActionResult List()
        {
            var results = actService.GetTags().ToList();
            return View(results);
        }

        [HttpGet]
        public ActionResult Edit(string guid)
        {
            var tag = actService.FindTagByGuid(guid);
            var model = new EditTag
            {
                Guid = tag.Guid,
                Name = tag.Name,
                Description = tag.Description,
                IsTheme = tag.IsTheme ?? false
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditTag model)
        {
            if (ModelState.IsValid)
            {
                var tag = new Tag
                {
                    Guid = model.Guid,
                    Name = model.Name,
                    Description = model.Description,
                    IsTheme = model.IsTheme
                };
                commonService.SaveOrUpdate(tag, Label.Entity.Tag);
                return RedirectToAction("List");
            }
            return View(model);
        }
    }
}