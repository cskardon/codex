﻿using CodexMVC.Controllers;
using CodexMVC.Models;
using Services;
using Services.Common;
using System;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    [Authorize(Roles = Permissions.CanAccessAdmin)]
    [Authorize(Roles = Permissions.ListRoles)]
    public class RoleController : BaseController
    {
        private readonly IUserService userService;
        public RoleController(IUserService _userService)
        {
            userService = _userService;
        }
        [Authorize(Roles = Permissions.EditRole)]
        [HttpGet]
        public ActionResult Add()
        {
            return View("Edit", new EditRoleModel());
        }
        [Authorize(Roles = Permissions.EditRole)]
        [HttpGet]
        public ActionResult Edit(string roleGuid)
        {
            var role = userService.FindRoleByGuid(roleGuid);
            return View(new EditRoleModel
            {
                RoleGuid = roleGuid,
                Name = role.Name,
                Description = role.Description
            });
        }
        [Authorize(Roles = Permissions.EditRole)]
        [HttpPost]
        public ActionResult Edit(EditRoleModel model)
        {
            if (ModelState.IsValid)
            {
                var role = model.RoleGuid != null ? userService.FindRoleByGuid(model.RoleGuid) : new Role();
                role.Name = model.Name;
                role.Description = model.Description;
                userService.SaveOrUpdate(role);
                return RedirectToAction("ViewDetail", "Role", new { roleGuid = role.Guid });
            }
            return View(model);
        }
        [HttpGet]
        public ActionResult List()
        {
            var roles = userService.AllRoles();

            return View(roles);
        }
        [HttpGet]
        [Authorize(Roles = Permissions.ViewRole)]
        public ActionResult ViewDetail(string roleGuid)
        {
            var role = userService.FindRoleByGuid(roleGuid);
            var rolePermissions = userService.GetPermissionsForRole(roleGuid);
            return View(new ViewRoleModel
            {
                Role = role,
                Permissions = rolePermissions
            });
        }
        [Authorize(Roles = Permissions.RemovePermissionFromRole)]
        [HttpGet]
        public ActionResult RemovePermissionFromRole(string permissionGuid, string roleGuid)
        {
            userService.RemovePermissionFromRole(permissionGuid, roleGuid);
            return RedirectToAction("ViewDetail", "Role", new { roleGuid = roleGuid });
        }
        [HttpGet]
        //[Authorize(Roles = Permissions.AddPermissionToRole)]
        public ActionResult AddPermission(string roleGuid)
        {
            return View(new AddPermissionToRoleModel
            {
                RoleGuid = roleGuid
            });
        }
        [HttpPost]
        //[Authorize(Roles = Permissions.AddPermissionToRole)]
        public ActionResult AddPermission(AddPermissionToRoleModel model)
        {
            var result = userService.AddPermissionToRole(model.PermissionGuid, model.RoleGuid);
            GeneralMessage = result ? "Succeeded in adding permission to role" : "Failed in adding permission to role";
            return RedirectToAction("ViewDetail", "Role", new { roleGuid = model.RoleGuid });
        }
    }
}