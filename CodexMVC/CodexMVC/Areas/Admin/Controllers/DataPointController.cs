﻿using AutoMapper;
using CodexMVC.Areas.Admin.Models;
using CodexMVC.Controllers;
using CodexMVC.Models;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Areas.Admin.Controllers
{
    public class DataPointController : BaseController
    {
        public async Task<JsonResult> RemovePartOf(string dataPointGuid, string dataSetGuid)
        {
            await dataPointService.RemovePartOfAsync(dataPointGuid, dataSetGuid);
            return Succeeded();
        }

        public async Task<JsonResult> AddPartOf(string dataPointGuid, string dataSetGuid)
        {
            await dataPointService.AddPartOfAsync(dataPointGuid, dataSetGuid);
            return Succeeded();
        }

        public async Task<ActionResult> Add()
        {
            var model = new DataPointHyperNodeModel
            {
                At = new Place(),
                Act = new Act(),
                DataPoint = new DataPoint(),
                DataSets = new List<DataSet>()
            };
            return await View(model);
        }

        public async Task<ActionResult> AddToDataSet(string guid)
        {
            var model = new DataPointHyperNodeModel
            {
                ArrivedFromDataSetGuid = guid,
                AddingToDataSetGuid = guid,
                At = new Place(),
                Act = new Act(),
                DataPoint = new DataPoint(),
                DataSets = new List<DataSet>()
            };
            return await View(model);
        }

        public async Task<ActionResult> AddToDataSetFromAct(AddPointToDataSetFromActModel model)
        {
            var node = new DataPointHyperNodeModel
            {
                At = new Place(),
                DataPoint = new DataPoint
                {
                    Citation = model.Citation,
                    Year = model.Year.GetValueOrDefault(),
                    Milennium = model.Milennium,
                    Month = model.Month,
                    Day = model.Day,
                    Hour = model.Hour,
                    Minute = model.Minute
                },
                Act = new Act
                {
                    Guid = model.ActGuid
                },
                DataSets = new List<DataSet>(),
                AddingToDataSetGuid = model.DataSetGuid
            };
            return await View(node);
        }

        [HttpGet]
        public async Task<ActionResult> Edit(string dataPointGuid, string dataSetGuid)
        {
            var node = await dataPointService.FindDataPointAsync(dataPointGuid);
            var model = Mapper.Map<DataPointHyperNodeModel>(node);
            model.ArrivedFromDataSetGuid = dataSetGuid;
            return await View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(DataPointHyperNodeModel model)
        {
            if (ModelState.IsValid)
            {
                await dataPointService.SaveOrUpdateAsync(model);
                if (model.AddingToDataSetGuid != null)
                {
                    await dataPointService.AddPartOfAsync(model.DataPoint.Guid, model.AddingToDataSetGuid);
                }
                if (model.ArrivedFromDataSetGuid != null)
                {
                    return RedirectToAction("Index", "DataSet", new { guid = model.ArrivedFromDataSetGuid });
                }
                else
                {
                    return RedirectToAction("List", "DataSet");
                }
            }
            return await View(model);
        }

        async Task<ViewResult> View(DataPointHyperNodeModel model)
        {
            var places = DependencyResolver.Current.GetService<IPlaceService>().AllWithContainers().Select(x => new SelectListItem
            {
                Text = x.ToDisplay(),
                Value = x.Place.Guid
            })
            .OrderBy(x => x.Text)
            .ToList();
            places.Insert(0, new SelectListItem { });
            ViewBag.Places = places;

            ViewBag.Milennia = new List<SelectListItem>
            {
                new SelectListItem{},
                new SelectListItem { Text = "AD", Value = "AD"},
                new SelectListItem { Text = "BC", Value = "BC"}
            };

            ViewBag.Months = new List<SelectListItem>
            {
                new SelectListItem{},
                new SelectListItem { Text="01 January", Value = "1" },
                new SelectListItem { Text="02 February", Value = "2" },
                new SelectListItem { Text="03 March", Value = "3" },
                new SelectListItem { Text="04 April", Value = "4" },
                new SelectListItem { Text="05 May", Value = "5" },
                new SelectListItem { Text="06 June", Value = "6" },
                new SelectListItem { Text="07 July", Value = "7" },
                new SelectListItem { Text="08 August", Value = "8" },
                new SelectListItem { Text="09 September", Value = "9" },
                new SelectListItem { Text="10 October", Value = "10" },
                new SelectListItem { Text="11 November", Value = "11" },
                new SelectListItem { Text="12 December", Value = "12" },
                new SelectListItem { Text="------------", Value = "" },
                new SelectListItem { Text="   Spring", Value = "13" },
                new SelectListItem { Text="   Summer", Value = "14" },
                new SelectListItem { Text="   Autumn", Value = "15" },
                new SelectListItem { Text="   Winter", Value = "16" },
                new SelectListItem { Text="------------", Value = "" },
                new SelectListItem { Text="   Early", Value = "17" },
                new SelectListItem { Text="   Late", Value = "18" }

            };

            var available = dataSetService.GetList()
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Guid
                })
                .OrderBy(x => x.Text)
                .ToList();

            if (model.DataPoint.Guid != null)
            {
                var point = await dataPointService.FindDataPointAsync(model.DataPoint.Guid);
                var datasets = point.DataSets.ToList();
                ViewBag.DataSets = point.DataSets.Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Guid
                }).ToList();
                if (datasets.Any())
                {
                    datasets.ForEach(set =>
                    {
                        available.RemoveAll(x => x.Value == set.Guid);
                    });
                }
            }

            available.Insert(0, new SelectListItem { });
            ViewBag.Available = available;

            return base.View("Edit", model);
        }
    }
}