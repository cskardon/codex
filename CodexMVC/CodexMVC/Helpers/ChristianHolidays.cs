﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Helpers
{
    public static class ChristianHolidays
    {
        public static void EasterSunday(int year, out int month, out int day)
        {
            int g = year % 19;
            int c = year / 100;
            int h = h = (c - (int)(c / 4) - (int)((8 * c + 13) / 25)
                                                + 19 * g + 15) % 30;
            int i = h - (int)(h / 28) * (1 - (int)(h / 28) *
                        (int)(29 / (h + 1)) * (int)((21 - g) / 11));

            day = i - ((year + (int)(year / 4) +
                          i + 2 - c + (int)(c / 4)) % 7) + 28;
            month = 3;

            if (day > 31)
            {
                month++;
                day -= 31;
            }
        }

        public static DateTime EasterSunday(int year)
        {
            int month = 0;
            int day = 0;
            EasterSunday(year, out month, out day);

            return new DateTime(year, month, day);
        }

        public static DateTime AscensionDay(int year)
        {
            return EasterSunday(year).AddDays(39);
        }

        public static DateTime WhitSunday(int year)
        {
            return EasterSunday(year).AddDays(49);
        }

        public static DateTime FirstSundayOfAdvent(int year)
        {
            int weeks = 4;
            int correction = 0;
            DateTime christmas = new DateTime(year, 12, 25);

            if (christmas.DayOfWeek != DayOfWeek.Sunday)
            {
                weeks--;
                correction = ((int)christmas.DayOfWeek - (int)DayOfWeek.Sunday);
            }
            return christmas.AddDays(-1 * ((weeks * 7) + correction));
        }
    }
}