﻿using Data.Entities;
using log4net.Appender;
using log4net.Core;
using Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace CodexMVC.Helpers
{
    public class SessionCache<T>
    {
        public T Data { get; set; }
        public DateTime Time { get; set; }
        public bool HasExpired(int minutes)
        {
            return (DateTime.Now - Time).TotalMinutes > minutes;
        }
        public SessionCache()
        {
            Time = DateTime.Now;
        }
        public SessionCache(T data)
            : this()
        {
            Data = data;
        }
    }

    public class Neo4jClientAppender : AppenderSkeleton
    {
        protected override void Append(LoggingEvent loggingEvent)
        {
            try
            {
                var commonService = DependencyResolver.Current.GetService<ICommonService>();
                commonService.log4net(loggingEvent);
            }
            catch (Exception ex)
            {

            }
        }
    }


    public abstract class BaseViewPage : WebViewPage
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }

    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
        public string DefaultItineraryGuid
        {
            get
            {
                return Session["DefaultItineraryGuid"] != null ? Session["DefaultItineraryGuid"].ToString() : null;
            }
        }
        /// <summary>
        /// The Google Maps API Key
        /// </summary>
        public string GoogleMapsAPIKey
        {
            get
            {
                return ConfigurationManager.AppSettings["GoogleMapsAPIKey"];
            }
        }
        public int UpdatesTotal
        {
            get
            {
                if (User == null || User.Guid == null)
                {
                    return 0;
                }
                var updated = Session["FollowedArtistsUpdatedTotal"] as SessionCache<int?>;
                if (updated == null || updated.HasExpired(10))
                {
                    var userService = DependencyResolver.Current.GetService<IUserService>();
                    updated = new SessionCache<int?>(userService.FindFollowedArtistsUpdatedTotal(User.Guid));
                    Session["FollowedArtistsUpdatedTotal"] = updated;
                }
                return updated.Data.Value;
            }
        }
        
        //public T GetFromSession<T>(string key)
        //{
        //    var t = typeof(T);
        //    var u = Nullable.GetUnderlyingType(t);
        //    var value = HttpContext.Current.Session[key];
        //    if (u != null)
        //    {
        //        return (value == null) ? default(T) : (T)Convert.ChangeType(value, u);
        //    }
        //    else
        //    {
        //        return (T)Convert.ChangeType(value, t);
        //    }
        //}
        public IEnumerable<SelectListItem> Itineraries
        {
            get
            {
                if (User == null || User.Guid == null)
                {
                    return null;
                }
                var userService = DependencyResolver.Current.GetService<IUserService>();
                var itineraries = userService.FindItinerariesForUser(User.Guid);
                return itineraries.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid, Selected = DefaultItineraryGuid != null && x.Guid == DefaultItineraryGuid });
            }
        }
    }
}