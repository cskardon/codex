﻿var codex = window.codex || {};
(function (codex) {

    (function (api, $) {

        function toggleFollowClicked(link) {
            var $link = $(link);
            var agentGuid = "@Model.Agent.Agent.Guid";
            var following = $link.data("following");
            if ("false" == following) {
                $.get("/Agent/Follow", { agentGuid: agentGuid }, function (data) {
                    $link.css("background-color", "gray");
                    $link.text("Following");
                    $link.data("following", "true");
                });
            }
            else {
                $.get("/Agent/Unfollow", { agentGuid: agentGuid }, function (data) {
                    $link.css("background-color", "#009688");
                    $link.text("Follow");
                    $link.data("following", "false");
                });
            }
        }

        function addBookmark(element) {
            var bookmark = $(element);
            var guid = bookmark.data("guid");
            var type = bookmark.data("type");
            var bookmarked = bookmark.data("bookmarked");
            var icon = bookmark.find("span");
            if (!bookmarked) {
                bookmark.data("bookmarked", true);
                $.get("/API/AddBookmark", {
                    guid: guid,
                    type: type
                }, function (response) {
                    icon.css("color", "red");
                });
            } else {
                bookmark.data("bookmarked", false);
                $.get("/API/RemoveBookmark", {
                    guid: guid,
                    type: type
                }, function (response) {
                    icon.css("color", "#337ab7");
                });
            }
            return false;
        }

        api.toggleFollowClicked = toggleFollowClicked;
        api.addBookmark = addBookmark;

    })(codex.api || (codex.api = {}), $);

})(codex);