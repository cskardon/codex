﻿var app = window.app || {};
(function (app) {
    (function (Common) {
        var Mode = Common.Mode = {
            "None": "Mode.None",
            "Edit": "Mode.Edit",
            "New": "Mode.New"
        };
        var Labels = Common.Labels = {
            "FOR_PATRON": {
                forward: "for patron",
                back: "patron of"
            },
            "SERVES": {
                forward: "is the servant of",
                back: "is the master of"
            },
            "AT_PLACE": {
                forward: "at",
                back: "is the location of"
            },
            "IS_A": {
                forward: "is a",
                back: "is a"
            },
            "APPRENTICE_TO": {
                forward: "is the apprentice of",
                back: "is the maestro of"
            },
            "RIVAL_OF": {
                forward: "is the rival of",
                back: "is the rival of"
            },
            "SUBJECT_OF": {
                forward: "the subject of",
                back: "is the subject of"
            },
            "TO_STUDENT": {
                forward: "to the student",
                back: "was the student of"
            },
            "FRIEND_OF": {
                forward: "is friends with",
                back: "is friends with"
            },
            "MEMBER_OF": {
                forward: "is a member of",
                back: "contains"
            },
            "KNOWS": {
                forward: "knows",
                back: "knows"
            },
            "MODEL_OF": {
                forward: "was the model for",
                back: "is represented by"
            },
            "CHILD_OF": {
                forward: "is the child of",
                back: "is the parent of"
            },
            "AN_ARTWORK": {
                forward: "an artwork called"
            },
            "IN_CAPACITY": {
                forward: "in the capacity of"
            }
        };
        var Arity = Common.Arity = {
            "One": "Arity.One",
            "Many": "Arity.Many"
        };
        var EntityState = Common.EntityState = {
            "Added": "Added",
            "Modified": "Modified",
            "Unchanged": "Unchanged",
            "Deleted": "Deleted"
        };
        var Direction = Common.Direction = {
            "Incoming": "Incoming",
            "Outgoing": "Outgoing"
        };
        var CardType = Common.CardType = {
            "Fact": "Fact",
            "Relationship": "Relationship",
            "Attribute": "Attribute"
        };
    })(app.Common || (app.Common = {}));
})(app);