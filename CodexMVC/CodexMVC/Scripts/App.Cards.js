﻿var app = window.app || {};
(function (app) {
    var EntityState = {
        "Added": "Added",
        "Modified": "Modified",
        "Unchanged": "Unchanged",
        "Deleted": "Deleted"
    };
    var Arity = {
        "One": "Arity.One",
        "Many": "Arity.Many"
    };
    var Direction = {
        "Incoming": "Incoming",
        "Outgoing": "Outgoing"
    };
    var CardType = {
        "Fact": "Fact",
        "Relationship": "Relationship",
        "Attribute": "Attribute"
    };
    (function (Cards) {
        var utils = app.Utils,
            __extends = utils.__extends,
            defined = utils.defined,
            PubSub = utils.PubSub,
            broadcast = utils.broadcast;
        var Selectors = app.Selectors,
            Nodes = app.Nodes;
        var AgentAgentCard = (function () {
            function AgentAgentCard(component) {
                this.API = app.API;
                this.model = this.model || {};
                this.subjectOfSelector = new Selectors.AgentSelector();
                this.atPlaceSelector = new Selectors.PlaceSelector();
                this.partOfSelector = new Selectors.EventSelector();
                this.text = {};
                this.text.subjectName = ko.computed(function () {
                    if (!this.model.hyperNode) {
                        return;
                    }
                    var name = this.model.hyperNode.unbind().Agent.Name;
                    return name;
                }, this);
                this.text.placeName = ko.computed(function () {
                    if (!this.model.hyperNode) {
                        return;
                    }
                    var name = this.model.hyperNode.unbind().Place.Name;
                    return name;
                }, this);
                this.pubsub = new PubSub();
                this.setupEventHandlers();
                if (component) {
                    this.bindComponent(component);
                }
            }
            AgentAgentCard.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
                if (component.subjectOfSelector) {
                    this.subjectOfSelector.bindComponent(component.subjectOfSelector);
                }
                if (component.atPlaceSelector) {
                    this.atPlaceSelector.bindComponent(component.atPlaceSelector);
                }
                if (component.partOfSelector) {
                    this.partOfSelector.bindComponent(component.partOfSelector);
                }
            };
            AgentAgentCard.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.hyperNode)) this.model.hyperNode.bind(model.hyperNode);
            };
            AgentAgentCard.prototype.setupEventHandlers = function () {
                this.subjectOfSelector.pubsub.subscribe("AgentSelected", function () {
                    var model = this.subjectOfSelector.unbind();
                    this.model.hyperNode.bind({
                        Agent: model.person
                    });
                }, this);
                this.atPlaceSelector.pubsub.subscribe("PlaceSelected", function () {
                    var model = this.atPlaceSelector.unbind();
                    this.model.hyperNode.bind({
                        Place: model.place
                    });
                    var hyperNodeModel = this.model.hyperNode.unbind();
                    if (hyperNodeModel.AtPlace.EntityState == EntityState.Unchanged) {
                        this.model.hyperNode.bind({
                            AtPlace: {
                                EntityState: EntityState.Modified
                            }
                        });
                    }
                }, this);
                this.partOfSelector.pubsub.subscribe("EventSelected", function () {
                    var model = this.partOfSelector.unbind();
                    this.model.hyperNode.bind({
                        Event: model.event
                    });
                    var hyperNodeModel = this.model.hyperNode.unbind();
                    if (hyperNodeModel.PartOf.EntityState == EntityState.Unchanged) {
                        this.model.hyperNode.bind({
                            PartOf: {
                                EntityState: EntityState.Modified
                            }
                        });
                    }
                }, this);
            };
            AgentAgentCard.prototype.saveClicked = function () {
                this.save();
            };
            AgentAgentCard.prototype.cancelClicked = function () {
                this.close();
            };
            AgentAgentCard.prototype.close = function () {
                broadcast("CloseCard", this);
            };
            return AgentAgentCard;
        })();
        Cards.AgentAgentCard = AgentAgentCard;
        var BornCard = (function (__super) {
            __extends(BornCard, __super);
            function BornCard(component) {
                this.type = CardType.Fact;
                this.arity = Arity.One;
                this.model = {};
                this.model.hyperNode = new Nodes.BornHyperNode();
                this.templateName = "person-agent-card-template";
                __super.call(this, component);
                this.arity = Arity.One;
            };
            BornCard.prototype.save = function () {
                var hyperNode = this.model.hyperNode.unbind();
                this.API.createBornHyperNode({
                    data: hyperNode,
                    success: function (response) {
                        this.model.hyperNode.bind({
                            Fact: {
                                Guid: response.Data.Fact.Guid
                            }
                        });
                        this.close();
                        // broadcast("CardSaved", this);
                    }
                }, this);
            };
            BornCard.prototype.toString = function () {
                return "{subject} was born in {place}.".fmt({
                    "subject": this.text.subjectName(),
                    "place": this.text.placeName()
                });
            };
            return BornCard;
        })(AgentAgentCard);
        Cards.BornCard = BornCard;
        var VisitedCard = (function (__super) {
            __extends(VisitedCard, __super);
            function VisitedCard(component) {
                this.type = CardType.Fact;
                this.API = app.API;
                this.arity = Arity.One;
                this.templateName = "person-agent-card-template";
                this.model = {};
                this.model.hyperNode = new Nodes.VisitedHyperNode();
                __super.call(this, component);
            }
            VisitedCard.prototype.save = function () {
                var hyperNode = this.model.hyperNode.unbind();
                this.API.createVisitedHyperNode({
                    data: hyperNode,
                    success: function (response) {
                        this.model.hyperNode.bind({
                            Fact: {
                                Guid: response.Data.Fact.Guid
                            }
                        });
                        this.close();
                        broadcast("RefreshFacts");
                    }
                }, this);
            };
            VisitedCard.prototype.toString = function () {
                return "{agent} visited {place}.".fmt({
                    "agent": this.text.subjectName(),
                    "place": this.text.placeName()
                });
            };
            return VisitedCard;
        })(AgentAgentCard);
        Cards.VisitedCard = VisitedCard;
        var LivedAtCard = (function (__super) {
            __extends(LivedAtCard, __super);
            function LivedAtCard(component) {
                this.API = app.API;
                this.arity = Arity.One;
                this.templateName = "person-agent-card-template";
                this.model = {};
                this.model.hyperNode = new Nodes.LivedAtHyperNode();
                __super.call(this, component);
            }
            LivedAtCard.prototype.save = function () {
                var hyperNode = this.model.hyperNode.unbind();
                this.API.createLivedAtHyperNode({
                    data: hyperNode,
                    success: function (response) {
                        this.model.hyperNode.bind({
                            Fact: {
                                Guid: response.Data.Fact.Guid
                            }
                        });
                        this.close();
                    }
                }, this);
            };
            LivedAtCard.prototype.toString = function () {
                return "{agent} lived at {place}.".fmt({
                    "agent": this.text.subjectName(),
                    "place": this.text.placeName()
                });
            };
            return LivedAtCard;
        })(AgentAgentCard);
        Cards.LivedAtCard = LivedAtCard;
        var DiedCard = (function (__super) {
            __extends(DiedCard, __super);
            function DiedCard(component) {
                this.API = app.API;
                this.arity = Arity.One;
                this.templateName = "person-agent-card-template";
                this.model = {};
                this.model.hyperNode = new Nodes.DiedHyperNode();
                __super.call(this, component);
            }
            DiedCard.prototype.save = function () {
                var hyperNode = this.model.hyperNode.unbind();
                this.API.createDiedHyperNode({
                    data: hyperNode,
                    success: function (response) {
                        this.model.hyperNode.bind({
                            Fact: {
                                Guid: response.Data.Fact.Guid
                            }
                        });
                        this.close();
                    }
                }, this);
            };
            DiedCard.prototype.toString = function () {
                return "{subject} died at {place}.".fmt({
                    "subject": this.subjectOfSelector.model.name(),
                    "place": this.atPlaceSelector.model.name()
                });
            };
            return DiedCard;
        })(AgentAgentCard);
        Cards.DiedCard = DiedCard;
        var RelatedToCard = (function () {
            function RelatedToCard(component) {
                this.templateName = "related-to-card-template";
                this.API = app.API;
                this.model = {};
                this.model.Agent = new Nodes.Agent();
                this.model.Relationship = new Nodes.Property();
                this.model.Other = new Nodes.Agent();
                this.otherSelector = new Selectors.AgentSelector();
                this.relationships = ko.observableArray([]);
                this.selectedRelationship = ko.observable();
                if (component) {
                    this.bindComponent(component);
                }
                this.setupData();
            }
            RelatedToCard.prototype.setupEventHandlers = function () {
                this.otherSelector.pubsub.subscribe("AgentSelected", function () {
                    var model = this.otherSelector.unbind();
                    this.model.Other.bind(model.person);
                }, this);
            };
            RelatedToCard.prototype.cancelClicked = function () {
                this.close();
            };
            RelatedToCard.prototype.close = function () {
                broadcast("CloseCard", this);
            };
            RelatedToCard.prototype.saveClicked = function () {
                this.save();
            };
            RelatedToCard.prototype.save = function () {
                var model = this.unbind();
                this.API.createRelatedToHyperNode({
                    data: model,
                    success: function (response) {
                        broadcast("RefreshRelationships");
                    }
                }, this);
            };
            RelatedToCard.prototype.setupData = function () {
                this.API.allCategories({
                    success: function (response) {
                        this.relationships(response.Data);
                    }
                }, this);
            };
            RelatedToCard.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            RelatedToCard.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Agent)) this.model.Agent.bind(model.Agent);
                if (defined(model.Relationship)) this.model.Relationship.bind(model.Relationship);
                if (defined(model.Other)) this.model.Other.bind(model.Other);
            };
            RelatedToCard.prototype.unbind = function () {
                return {
                    Agent: this.model.Agent(),
                    Relationship: this.model.Relationship(),
                    Other: this.model.Other()
                };
            };
            return RelatedToCard;
        })();
        Cards.RelatedToCard = RelatedToCard;
        var RepresentsCard = (function () {
            function RepresentsCard(component) {
                this.templateName = "represents-card-template";
                this.API = app.API;
                this.model = {};
                this.model.Source = new Nodes.Artwork();
                this.model.Target = new Nodes.Entity();
                this.targets /*: KOArray<Nodes.Entity> */ = ko.observableArray([]);
                if (component) {
                    this.bindComponent(component);
                }
                this.setupData();
            }
            RepresentsCard.prototype.setupData = function () {
                this.API.findAllEntities({
                    success: function (response) {
                        this.targets(response.Data);
                    }
                }, this);
            };            
            RepresentsCard.prototype.cancelClicked = function () {
                this.close();
            };
            RepresentsCard.prototype.close = function () {
                broadcast("CloseCard", this);
            };
            RepresentsCard.prototype.saveClicked = function () {
                this.save();
            };
            RepresentsCard.prototype.save = function () {
                var model = this.unbind();
                this.API.represents({
                    data: {
                        sourceGuid: model.Source.Guid,
                        targetGuid: model.Target.Guid
                    },
                    success: function (response) {
                        broadcast("RefreshRepresentations");
                    }
                }, this);
            };
            RepresentsCard.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            RepresentsCard.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Source)) this.model.Source.bind(model.Source);
                if (defined(model.Target)) this.model.Target.bind(model.Target);
            };
            RepresentsCard.prototype.unbind = function () {
                return {
                    Source: this.model.Source.unbind(),
                    Target: this.model.Target
                };
            };
            return RepresentsCard;
        })();
        Cards.RepresentsCard = RepresentsCard;
        var KnownAsCard = (function () {
            function KnownAsCard(component) {
                this.templateName = "known-as-card-template";
                this.pubsub = new PubSub();
                this.model = {};
                this.model.hyperNode = new Nodes.PropertyHyperNode();
            }
            KnownAsCard.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.hyperNode)) this.model.hyperNode.bind(model.hyperNode);
            };
            KnownAsCard.prototype.saveClicked = function () {
                this.save();
            };
            KnownAsCard.prototype.save = function () {
                var hyperNode = this.model.hyperNode.unbind();
                API.saveKnownAs({
                    data: hyperNode,
                    success: function (response) {
                        broadcast("CloseCard", this);
                    }
                }, this);
            };
            KnownAsCard.prototype.close = function () {
                broadcast("CloseCard", this);
            };
            KnownAsCard.prototype.unbind = function () {
                return {
                    hyperNode: this.hyperNode.unbind()
                };
            };
            return KnownAsCard;
        })();
        Cards.KnownAsCard = KnownAsCard;
        var EditCategoryCard = (function () {
            function EditCategoryCard(component) {
                this.templateName = "create-category-card";
                this.model.guid = ko.observable();
                this.model.name = ko.observable();
                this.model.parentGuid = ko.observable(); // optional
                this.allCategories = ko.observableArray([]);
                this.setupData();
                if (component) {
                    this.bindComponent(component);
                }
            }
            EditCategoryCard.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            EditCategoryCard.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.guid)) this.model.guid(model.guid);
                if (defined(model.name)) this.model.name(model.name);
                if (defined(model.parentGuid)) this.model.parentGuid(model.parentGuid);
            };
            EditCategoryCard.prototype.setupData = function () {
                this.findAllCategories();
            };
            EditCategoryCard.prototype.saveClicked = function () {
                this.save();
            };
            EditCategoryCard.prototype.save = function () {
                var model = this.unbind();
                var parentCategoryGuid = model.parentGuid ? model.parentGuid : null;
                this.API.createCategory({
                    data: {
                        name: model.name,
                        parentCategoryGuid: parentCategoryGuid
                    },
                    success: function (response) {
                        this.model.guid(response.Data.Guid);
                        this.close();
                    }
                }, this);
            };
            EditCategoryCard.prototype.close = function () {
                broadcast("CloseCard", this);
            };
            EditCategoryCard.prototype.unbind = function () {
                return {
                    guid: this.model.guid(),
                    name: this.model.name(),
                    parentGuid: this.model.parentGuid()
                };
            };
            EditCategoryCard.prototype.findAllCategories = function () {
                this.API.allCategories({
                    success: function (response) {
                        this.allCategories(response.Data);
                    }
                }, this);
            };
            return EditCategoryCard;
        })();
        Cards.EditCategoryCard = EditCategoryCard;
        var PropertyCard = (function () {
            function PropertyCard(component) {
                this.model = {};
                this.model.hyperNode = new Nodes.PropertyHyperNode();
                this.selectedPropertyType = ko.observable();
                this.propertyTypes = ko.observableArray([]);
            }
            PropertyCard.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.hyperNode)) this.model.hyperNode.bind(model.hyperNode);
            };
            PropertyCard.prototype.unbind = function () {
                return {
                    hyperNode: this.model.hyperNode.unbind()
                };
            };
            return PropertyCard;
        })();
        Cards.PropertyCard = PropertyCard;
    })(app.Cards || (app.Cards = {}));
})(app);