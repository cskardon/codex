﻿var app = window.app || {};
(function (app) {

    (function (Nodes) {
        var utils = app.Utils,
            __extends = utils.__extends,
            merge = utils.merge,
            defined = utils.defined;
        var Selectors = app.Selectors,
            Common = app.Common,
            EntityState = Common.EntityState,
            Direction = Common.Direction;
        var Name = (function () {
            function Name(component) {
                this.model = {};
                this.model.entityState = ko.observable(EntityState.Added);
                this.model.guid = ko.observable();
                this.model.name = ko.observable();
                if (component) {
                    this.bindComponent(component);
                }
            }
            Name.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            Name.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (model.entityState) this.model.entityState(model.entityStacks);
                if (model.guid) this.model.guid(model.guid);
                if (model.name) this.model.name(model.name);
            };
            Name.prototype.unbind = function () {
                return {
                    entityState: this.model.entityState(),
                    guid: this.model.guid(),
                    name: this.model.name()
                };
            };
            return Name;
        })();
        Nodes.Name = Name;
        var Property = (function () {
            function Property(component) {
                this.model = {};
                this.model.Guid = ko.observable();
                this.model.EntityState = ko.observable(EntityState.Added);
                this.model.Name = ko.observable();
                if (component) {
                    this.bindComponent(component);
                }
            }
            Property.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            Property.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Guid)) this.model.Guid(model.Guid);
                if (defined(model.EntityState)) this.model.EntityState(model.EntityState);
                if (defined(model.Name)) this.model.Name(model.Name);
            };
            Property.prototype.unbind = function () {
                return {
                    Guid: this.model.Guid(),
                    EntityState: this.model.EntityState(),
                    Name: this.model.Name()
                };
            };
            return Property;
        })();
        Nodes.Property = Property;
        var Entity = (function () {
            function Entity() {
                this.model = {
                    Guid: "string",
                    Name: "string",
                    Label: "string"
                };
            }
            Entity.prototype.unbind = function () {
                return this.model;
            };
            Entity.prototype.bind = function (model) {
                if (false == defined(model)) {
                    return;
                }
                this.model = model;
            };
            return Entity;
        })();
        Nodes.Entity = Entity;
        var Artwork = (function () {
            function Artwork() {
                this.model = {
                    Guid: "string",
                    Name: "string",
                    ImageUrl: "string",
                    IsActive: "boolean",
                    IsTemp: "boolean",
                    Label: "string"
                };
            }
            Artwork.prototype.unbind = function () {
                return this.model;
            };
            Artwork.prototype.bind = function (model) {
                if (false == defined(model)) {
                    return;
                }
                this.model = model;
            };
            return Artwork;
        })();
        Nodes.Artwork = Artwork;
        var Agent = (function () {
            function Agent(component) {
                this.model = {};
                this.model.Guid = ko.observable();
                this.model.Name = ko.observable();
                this.model.Sex = ko.observable();
                this.state = {};
                this.state.isMale = ko.computed(function () {
                    return "Male" == this.model.Sex();
                }, this);
                this.state.isFemale = ko.computed(function () {
                    return "Female" == this.model.Sex();
                }, this);
            }
            Agent.prototype.maleSelected = function () {
                this.model.Sex("Male");
                //return true;
            };
            Agent.prototype.femaleSelected = function () {
                this.model.Sex("Female");
                //return true;
            };
            Agent.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Guid)) this.model.Guid(model.Guid);
                if (defined(model.Name)) this.model.Name(model.Name);
                if (defined(model.Sex)) this.model.Sex(model.Sex);
            };
            Agent.prototype.unbind = function () {
                return {
                    Guid: this.model.Guid(),
                    Name: this.model.Name(),
                    Sex: this.model.Sex()
                };
            };
            return Agent;
        })();
        Nodes.Agent = Agent;
        var Place = (function () {
            function Place(component) {
                this.model = {};
                this.model.Guid = ko.observable();
                this.model.Name = ko.observable();
                if (component) {
                    this.bindComponent(component);
                }
            }
            Place.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            Place.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Guid)) this.model.Guid(model.Guid);
                if (defined(model.Name)) this.model.Name(model.Name);
            };
            Place.prototype.unbind = function () {
                return {
                    Guid: this.model.Guid(),
                    Name: this.model.Name()
                };
            };
            Place.prototype.unbindComponent = function () {
                return {
                    model: this.unbind()
                };
            };
            return Place;
        })();
        Nodes.Place = Place;
        var Category = (function () {
            function Category(component) {
                this.model = {};
                this.model.Guid = ko.observable();
                this.model.Name = ko.observable();
            }
            Category.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Guid)) this.model.Guid(model.Guid);
                if (defined(model.Name)) this.model.Name(model.Name);
            };
            Category.prototype.unbind = function () {
                return {
                    Guid: this.model.Guid(),
                    Name: this.model.Name()
                };
            };
            return Category;
        })();
        Nodes.Category = Category;
        var Relation = (function () {
            function Relation(component) {
                this.model = {};
                this.model.Guid = ko.observable();
                this.model.EntityState = ko.observable();
                if (component) {
                    this.bindComponent(component);
                }
            }
            Relation.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            Relation.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Guid)) this.model.Guid(model.Guid);
                if (defined(model.EntityState)) this.model.EntityState(model.EntityState);
            };
            Relation.prototype.unbind = function () {
                return {
                    Guid: this.model.Guid(),
                    EntityState: this.model.EntityState()
                };
            };
            return Relation;
        })();
        Nodes.Relation = Relation;
        var FactNode = (function () {
            function FactNode(model) {
                this.model = {};
                this.model.Guid = ko.observable();
                this.model.Name = ko.observable();
                this.model.Proximity = ko.observable();
                this.model.Milennium = ko.observable();
                this.model.Year = ko.observable();
                this.model.Month = ko.observable();
                this.model.Day = ko.observable();
                this.model.Hour = ko.observable();
                this.model.Minute = ko.observable();
                this.model.ProximityEnd = ko.observable();
                this.model.MilenniumEnd = ko.observable();
                this.model.YearEnd = ko.observable();
                this.model.MonthEnd = ko.observable();
                this.model.DayEnd = ko.observable();
                this.model.HourEnd = ko.observable();
                this.model.MinuteEnd = ko.observable();
            }
            FactNode.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Guid)) this.model.Guid(model.Guid);
                if (defined(model.Name)) this.model.Name(model.Name);
                if (defined(model.Proximity)) this.model.Proximity(model.Proximity);
                if (defined(model.Milennium)) this.model.Milennium(model.Milennium);
                if (defined(model.Year)) this.model.Year(model.Year);
                if (defined(model.Month)) this.model.Month(model.Month);
                if (defined(model.Day)) this.model.Day(model.Day);
                if (defined(model.Hour)) this.model.Hour(model.Hour);
                if (defined(model.Minute)) this.model.Minute(model.Minute);
                if (defined(model.ProximityEnd)) this.model.ProximityEnd(model.ProximityEnd);
                if (defined(model.MilenniumEnd)) this.model.MilenniumEnd(model.MilenniumEnd);
                if (defined(model.YearEnd)) this.model.YearEnd(model.YearEnd);
                if (defined(model.MonthEnd)) this.model.MonthEnd(model.MonthEnd);
                if (defined(model.DayEnd)) this.model.DayEnd(model.DayEnd);
                if (defined(model.HourEnd)) this.model.HourEnd(model.HourEnd);
                if (defined(model.MinuteEnd)) this.model.MinuteEnd(model.MinuteEnd);
            };
            FactNode.prototype.unbind = function () {
                return {
                    Guid: this.model.Guid(),
                    Name: this.model.Name(),
                    Proximity: this.model.Proximity(),
                    Milennium: this.model.Milennium(),
                    Year: this.model.Year(),
                    Month: this.model.Month(),
                    Day: this.model.Day(),
                    Hour: this.model.Hour(),
                    ProximityEnd: this.model.ProximityEnd(),
                    MilenniumEnd: this.model.MilenniumEnd(),
                    YearEnd: this.model.YearEnd(),
                    MonthEnd: this.model.MonthEnd(),
                    DayEnd: this.model.DayEnd(),
                    HourEnd: this.model.HourEnd(),
                    MinuteEnd: this.model.MinuteEnd()
                };
            };
            return FactNode;
        })();
        Nodes.FactNode = FactNode;
        var EventNode = (function () {
            function EventNode(model) {
                this.model = {};
                this.model.Guid = ko.observable();
                this.model.Name = ko.observable();
                this.model.Proximity = ko.observable();
                this.model.Milennium = ko.observable();
                this.model.Year = ko.observable();
                this.model.Month = ko.observable();
                this.model.Day = ko.observable();
                this.model.Hour = ko.observable();
                this.model.Minute = ko.observable();
                this.model.ProximityEnd = ko.observable();
                this.model.MilenniumEnd = ko.observable();
                this.model.YearEnd = ko.observable();
                this.model.MonthEnd = ko.observable();
                this.model.DayEnd = ko.observable();
                this.model.HourEnd = ko.observable();
                this.model.MinuteEnd = ko.observable();
            }
            EventNode.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Guid)) this.model.Guid(model.Guid);
                if (defined(model.Name)) this.model.Name(model.Name);
                if (defined(model.Proximity)) this.model.Proximity(model.Proximity);
                if (defined(model.Milennium)) this.model.Milennium(model.Milennium);
                if (defined(model.Year)) this.model.Year(model.Year);
                if (defined(model.Month)) this.model.Month(model.Month);
                if (defined(model.Day)) this.model.Day(model.Day);
                if (defined(model.Hour)) this.model.Hour(model.Hour);
                if (defined(model.Minute)) this.model.Minute(model.Minute);
                if (defined(model.ProximityEnd)) this.model.ProximityEnd(model.ProximityEnd);
                if (defined(model.MilenniumEnd)) this.model.MilenniumEnd(model.MilenniumEnd);
                if (defined(model.YearEnd)) this.model.YearEnd(model.YearEnd);
                if (defined(model.MonthEnd)) this.model.MonthEnd(model.MonthEnd);
                if (defined(model.DayEnd)) this.model.DayEnd(model.DayEnd);
                if (defined(model.HourEnd)) this.model.HourEnd(model.HourEnd);
                if (defined(model.MinuteEnd)) this.model.MinuteEnd(model.MinuteEnd);
            };
            EventNode.prototype.unbind = function () {
                return {
                    Guid: this.model.Guid(),
                    Name: this.model.Name(),
                    Proximity: this.model.Proximity(),
                    Milennium: this.model.Milennium(),
                    Year: this.model.Year(),
                    Month: this.model.Month(),
                    Day: this.model.Day(),
                    Hour: this.model.Hour(),
                    ProximityEnd: this.model.ProximityEnd(),
                    MilenniumEnd: this.model.MilenniumEnd(),
                    YearEnd: this.model.YearEnd(),
                    MonthEnd: this.model.MonthEnd(),
                    DayEnd: this.model.DayEnd(),
                    HourEnd: this.model.HourEnd(),
                    MinuteEnd: this.model.MinuteEnd()
                };
            };
            return EventNode;
        })();
        Nodes.EventNode = EventNode;
        var AgentAgentHyperNode = (function () {
            function AgentAgentHyperNode(component) {
                this.actionName = "was born";
                this.model = {};
                this.model.Agent = new Agent();
                this.model.AgentOf = new Relation({
                    model: {
                        EntityState: EntityState.Added
                    }
                });
                this.model.Fact = new FactNode();
                this.model.AtPlace = new Relation({
                    model: {
                        EntityState: EntityState.Added
                    }
                });
                this.model.Place = new Place();
                this.model.PartOf = new Relation({
                    model: {
                        EntityState: EntityState.Added
                    }
                });
                this.model.Event = new EventNode();
                this.startDate = new Selectors.DateSelector();
                this.endDate = new Selectors.DateSelector();
                if (component) {
                    this.bind(component);
                }
            }
            AgentAgentHyperNode.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            AgentAgentHyperNode.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Agent)) this.model.Agent.bind(model.Agent);
                if (defined(model.AgentOf)) this.model.AgentOf.bind(model.AgentOf);
                if (defined(model.Fact)) {
                    this.model.Fact.bind(model.Fact);
                    this.startDate.bind(Selectors.toStartDateSelectorModel(model.Fact));
                    this.endDate.bind(Selectors.toEndDateSelectorModel(model.Fact));
                }
                if (defined(model.AtPlace)) this.model.AtPlace.bind(model.AtPlace);
                if (defined(model.Place)) this.model.Place.bind(model.Place);
                if (defined(model.PartOf)) this.model.PartOf.bind(model.PartOf);
                if (defined(model.Event)) this.model.Event.bind(model.Event);
            };
            AgentAgentHyperNode.prototype.unbind = function () {
                var fact = this.model.Fact.unbind();
                fact = merge(fact, Selectors.fromStartDateSelectorModel(this.startDate.unbind()));
                fact = merge(fact, Selectors.fromEndDateSelectorModel(this.endDate.unbind()));
                return {
                    Agent: this.model.Agent.unbind(),
                    AgentOf: this.model.AgentOf.unbind(),
                    Fact: fact,
                    AtPlace: this.model.AtPlace.unbind(),
                    Place: this.model.Place.unbind(),
                    PartOf: this.model.PartOf.unbind(),
                    Event: this.model.Event.unbind()
                };
            };
            return AgentAgentHyperNode;
        })();
        Nodes.AgentAgentHyperNode = AgentAgentHyperNode;
        var BornHyperNode = (function (__super) {
            __extends(BornHyperNode, __super);
            function BornHyperNode(component) {
                __super.call(this, component);
                this.actionName = "was born";
            }
            return BornHyperNode;
        })(AgentAgentHyperNode);
        Nodes.BornHyperNode = BornHyperNode;
        var DiedHyperNode = (function (__super) {
            __extends(DiedHyperNode, __super);
            function DiedHyperNode(component) {
                __super.call(this, component);
                this.actionName = "died";
            }
            return DiedHyperNode;
        })(AgentAgentHyperNode);
        Nodes.DiedHyperNode = DiedHyperNode;
        var VisitedHyperNode = (function (__super) {
            __extends(VisitedHyperNode, __super);
            function VisitedHyperNode(component) {
                __super.call(this, component);
                this.actionName = "visited";
            }
            return VisitedHyperNode;
        })(AgentAgentHyperNode);
        Nodes.VisitedHyperNode = VisitedHyperNode;
        var LivedAtHyperNode = (function (__super) {
            __extends(LivedAtHyperNode, __super);
            function LivedAtHyperNode(component) {
                __super.call(this, component);
                this.actionName = "lived at";
            }
            return LivedAtHyperNode;
        })(AgentAgentHyperNode);
        Nodes.LivedAtHyperNode = LivedAtHyperNode;
        var PropertyHyperNode = (function () {
            function PropertyHyperNode(component) {
                this.model = {};
                this.model.Agent = new Nodes.Agent();
                this.model.Label = ko.observable();
                this.model.Value = ko.observable();
            }
            PropertyHyperNode.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            PropertyHyperNode.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.Agent)) this.model.Agent.bind(model.Agent);
                if (defined(model.Label)) this.model.Label(model.Label);
                if (defined(model.Value)) this.model.Value(model.Value);
            };
            PropertyHyperNode.prototype.unbind = function () {
                return {
                    Agent: this.model.Agent.unbind(),
                    Label: this.model.Label(),
                    Value: this.model.Value()
                };
            };
            return PropertyHyperNode;
        })();
        Nodes.PropertyHyperNode = PropertyHyperNode;
        
    })(app.Nodes || (app.Nodes = {}));
})(app);