﻿(function () {
    var utils = app.Utils,
        log = utils.log;
    (function () {
        var existing = ko.bindingProvider.instance;
        ko.bindingProvider.instance = {
            nodeHasBindings: existing.nodeHasBindings,
            getBindings: function (node, bindingContext) {
                var bindings;
                try {
                    bindings = existing.getBindings(node, bindingContext);
                }
                catch (ex) {
                    log("binding error", ex.message, node, bindingContext);
                }
                return bindings;
            }
        };
    })();
})();