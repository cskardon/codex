/**

Imports.

*/
var ko,
    merge = Utils.merge,
    isobj = Utils.isobj,
    renderDate = Utils.renderDate,
    isarr = Utils.isarr,
    where = Utils.where,
    select = Utils.select,
    each = Utils.each,
    last = Utils.last,
    keys = Utils.keys,
    zip = Utils.zip,
    first = Utils.first,
    clone = Utils.clone,
    isfun = Utils.isfun,
    istr = Utils.istr,
    unull = Utils.unull,
    log = Utils.log,
    obj = Utils.isobj,
    arr = Utils.isarr,
    count = Utils.count,
    copyTo = Utils.copyTo,
    flatten = Utils.flatten,
    contains = Utils.contains,
    compare = Utils.compare;
/**

Set up environmental variables.

Facts 		-- The collection of all facts (property & action statements) relating to entities.
Entities    -- An entity is an individual that is composed of an array of Facts.
Traits		-- Type information to allow the tree functions to make judgements about fact types.

*/
var Facts = [];
var FactsIndex = {};
var Entities = [];
var Index = {};
var Indexers = [];
/**

Helper functions.

*/
String.prototype.fmt = function (hash) {
    var string = this, key;
    for (key in hash) string = string.replace(new RegExp('\\{' + key + '\\}', 'gm'), hash[key]);
    return string
};
function serialise(entity) {
    return zip(entity, {}, function (result, value, key) {
        if (key == "!") {
            if (Traits[value]) {
                result["!"] = "Trait." + value;
            }
            result["!&"] = getType(entity) + "." + entity["&"];
            return;
        }
        if (key == "&") {
            return;
        }
        result[key] = isobj(value) && value["!"]
        ? Traits[value["!"]] ? "Trait." + value["!"] : getType(value) + "." + value["&"]
        : isarr(value) ? zip(value, [], function (res, value) { res.push(serialise(value)) })
        : value
    })
}
function deserialise(json) {

    if (Utils.istr(json)) {
        json = JSON.parse(json);
    }
    var entity = wrap({ "!": "Entity", "&": parseInt(json["!&"].split(".")[1]) }).unwrap();
    var facts = select(json.facts, function (fact) {
        return deserialiseFact(fact);
    });
    entity.facts = facts;
    return entity;

}
function deserialiseFact(json) {

    var T = Traits[json["!"].split(".")[1]],
        guid = parseInt(json["!&"].split(".")[1]);
    var fact = new Fact({ "!": T, "&": guid });
    each(json, function (value, key) {
        if (key == "!" || key == "&" || key == "!&") {
            return;
        }
        if (Utils.istr(value) && value.indexOf("Entity") >= 0) {
            fact[key] = Entities[parseInt(value.split(".")[1])] || deserialise(value);
            return;
        }
        fact[key] = value;
    });

    return fact;
}
function getType(entity) {
    if (entity instanceof Fact) return "Fact";
    if (entity instanceof Trait) return "Trait";
    if (entity instanceof Entity) return "Entity";
    if (entity instanceof Wrapper) return "Wrapper";
    if (entity instanceof FactWrapper) return "FactWrapper";
    return "Object";
}
function pair(key, value) {
    var obj = {};
    obj[key] = value;
    return obj;
}
// works for objects.... I think
function comparePartial(a, b) {
    // For every member on a, check that it exists on b, recursively.
    var result = true;
    each(a, function (aValue, aKey) {
        if (aKey == "__") {
            // loop through every bKey and compare bValue to aValue.
            var found = false;
            each(b, function (bValue, bKey) {
                if (comparePartial(pair(bKey, aValue), pair(bKey, bValue))) {
                    found = true;
                    return false;
                }
            });
            return result = found;
        }
        if (Utils.unull(b[aKey])) return result = false;
        if (isobj(aValue) && Utils.count(aValue) == 0) return;
        var bValue = b[aKey];
        if (isfun(bValue)) return;
        if (isfun(aValue)) {
            return result = result && aValue(bValue);
        }
        if (aValue != bValue && isobj(aValue) && isobj(bValue)) {
            return result = result && comparePartial(aValue, bValue);
        }
        return result = result && aValue == bValue;
    })
    return result;
}
var comparisonGuid = 0;

/**

    source = { "!": "Entity", "&": 0, facts: [] };

    1. for each source-fact, if it has the same type as the target-fact then begin to compare the facts;

    This will enable us to match like for like ... but what about ascending the type tree -- e.g., comparing
    paintings with sculptures and concluding that both are artworks? Isn't the problem that at some point both
    source and target will share a common ancestor; namely, 'Artwork'?  

*/
// Returns a string[] of all the ancestors of trait @T.
function ancestors(T) {
    if (!T) return [];
    var results = [];
    var trait = Traits[T];
    if (!trait) return [];
    if (!trait[">"]) return [trait["!"]];
    while (trait[">"]) {
        results.push(trait["!"]);
        trait = trait[">"];
    }
    results.push(trait["!"]);
    return results.reverse();
}
function commonAncestor(x, y) {
    var xAncestors = ancestors(x);
    var yAncestors = ancestors(y);
    for (var i = xAncestors.length - 1, xDepth = 0; i >= 0; i--, xDepth++) {
        for (var j = yAncestors.length - 1, yDepth = 0; j >= 0; j--, yDepth++) {
            if (xAncestors[i] == yAncestors[j]) {
                return { x: x, y: y, xDepth: xDepth, yDepth: yDepth, common: xAncestors[i] };
            }
        }
    }
    return {};
}
function compareEntity(sourceEntity, targetEntity, parentEntities) {
    parentEntities = parentEntities || [];
    parentEntities.push(sourceEntity);
    parentEntities.push(targetEntity);
    var targetFactsSearched = [];
    return zip(sourceEntity.facts, {}, function (result, fact) {
        result.facts = result.facts || [];
        var T = fact["!"];
        var matchingTargetFact = first(where(targetEntity.facts, function (targetFact) {
            var ca = commonAncestor(T, targetFact["!"]);
            return ca.common != "Artwork" && ca.common != "IsProperty" && ca.common != "Action";// && ca.xDepth == ca.yDepth;
        }));
        // if could not find a match on this type, could try matching against the parent type ...?
        if (!matchingTargetFact)
            return;
        var factComparison = compareFact(fact, matchingTargetFact, parentEntities);
        if (count(factComparison) == 0) {
            return;
        }
        result.facts.push(factComparison)
    });
}
function compareFact(sourceFact, targetFact, parentEntities) {
    return zip(sourceFact, {}, function (result, value, key) {
        if (!targetFact[key]) return;
        if (!obj(value)) {
            if (targetFact[key] != value) {
                return;
            }
            result[key] = value;
            return;
        }
        // if exact object match
        if (targetFact[key] == value) {
            result[key] = value;
            return;
        }
        if (value["!"] == "Entity") {
            if (contains(parentEntities, value)) return;
        }
        result[key] = compareEntity(value, targetFact[key], parentEntities);
    });
}
var compare2 = function (source, target, parents) {
    if (typeof parents === "undefined") { parents = []; }
    return zip(source.facts, {}, function (result, sourceValue) {
        var sourceKey = sourceValue["!"];
        result.facts = result.facts || [];
        if (contains(parents, sourceValue)) {
            return;
        }
        var matching = first(where(target.facts, function (value) {
            return sourceKey == value["!"];
        }));
        if (!matching) {
            return;
        }
        if (!obj(sourceValue) && !obj(matching)) {
            if (sourceValue == matching)
                result[sourceKey] = matching;
            return;
        }
        var results = compare2(sourceValue, matching, parents.concat([sourceValue]));
        results.facts = results.facts || [];
        if (results.facts.length == 0) {
            return;
        }
        result[sourceKey] = results;
        result[sourceKey].__source__ = sourceValue;
        result[sourceKey].__target__ = matching;
    });
};
function Q(query, selector) {
    var facts = queryAll(Facts, query);
    return selector ? select(facts, function (fact) { return selector(fact) })
                    : select(facts, function (fact) { return fact });
}
function queryAll(list, query) {
    return where(list, function (item) {
        return comparePartial(query, item);
    })
}
function sources(facts) {
    return select(facts, function (fact) { return fact.__source__ })
}
function Session() {
    var _ = this;
    var store = {};         // search result store
    var steps = [];         // history
    var step = -1;          // current step
    var context = Facts;    // current search context
    function record(results) {
        steps[++step] = results;
    }
    _.back = function () {
        if (step > 0) {
            step--;
        }
        context = steps[step];
        return _;
    }
    _.forward = function () {
        if (step >= steps.length - 1) {
            step = steps.length - 1;
        } else {
            step++;
        }
        context = steps[step];
        return _;
    }
    _.clear = function () {
        steps = [];
        step = -1;
        context = Facts;
        return _;
    }
    _.store = function () {
        return store;
    }
    _.save = function (key) {
        var entry = store[key] = {};
        entry.steps = steps;
        entry.step = step;
        return _;
    }
    _.load = function (key) {
        var entry = store[key];
        steps = entry.steps;
        step = entry.step;
        context = _.current();
        return _;
    }
    _.combine = function (keys) {
        steps[0] = last(steps) || [];
        each(keys, function (key) {
            steps[0] = flatten(steps[0].concat(last(store[key].steps)));
        });
        step = steps.length - 1;
        context = _.current();
        return _;
    }
    _.and = function (query) {
        var searchContext = !query.__context__ ? context : Utils.last(_.store[__context__].steps);
        var facts = queryAll(searchContext, query);
        if (step == -1 || _.current().length == 0) {
            record(facts);
        } else {
            // not quite right ...
            record(where(_.current(), function (item) {
                return contains(facts, item);
            }));
        }
        context = _.current();
        return _;
    }
    _.step = function () {
        return {
            step: step,
            current: _.current()
        }
    }
    _.or = function (query) {
        var searchContext = !query.__context__ ? Facts : Utils.last(_.store[__context__].steps);
        var facts = queryAll(searchContext, query);
        var results = step == -1 ? [] : _.current();
        record(results.concat(facts));
        context = _.current();
        return _;
    }
    _.where = function (evaluator) {
        record(where(context, evaluator));
        context = _.current();
        return _;
    }
    _.sources = function (evaluator) {
        return _.where(function (fact) {
            return evaluator(wrap(fact.__source__))
        })
    }
    _.targets = function (evaluator) {
        return _.where(function (fact) {
            return where(wrap(fact.__target__), evaluator);
        })
    }
    _.current = function () {
        return steps[step] || [];
    }
    _.current.log = function () { return _.current().log() }
    _.history = function () {
        return steps;
    }
    _.history.log = function () { return _.history().log() }
    _.artworks = function () {
        record(where(Facts, function (fact) { return inheritsFrom(fact["!"], Artwork["!"]) }));
        return _;
    }
    return _;
}

function Fact(_fact) {
    var fact = this;
    _fact = _fact || {};
    copyTo(fact, _fact);
    fact["!"] = fact["!"] || "Fact";
    fact["&"] = fact["&"] || Facts.length;
    return fact;
}
function FactWrapper(fact) {
    var _ = this;
    var _fact;
    function initialise() {
        _fact = fact instanceof Fact ? fact : new Fact(fact);
    }
    _.source = function () {
        return wrap(_fact.__source__);
    }
    _.target = function () {
        return wrap(_fact.__target__);
    }
    _.unwrap = function () {
        return _fact;
    }
    initialise();
    return _;
}
function fwrap(fact) { return new FactWrapper(fact) }
function Entity(_entity) {
    var entity = this;
    _entity = _entity || {};
    copyTo(entity, _entity);
    entity["!"] = entity["!"] || "Entity";
    entity["&"] = entity["&"] || Entities.length;
    entity.facts = entity.facts || [];
    return entity;
}
function inheritsFrom(type, rootType) {
    var trait = Traits[type];
    if (!trait) return false;
    if (type == rootType) return true;
    var traitSuperClassPointer = trait[">"];
    if (!traitSuperClassPointer) return false;
    var parentType = traitSuperClassPointer["!"];
    /**
    NB: multiple trait inheritance is currently possible, so look to converting
    'parentType' to an array 'parentTypes' and ORing the results of the recursion ...
    */
    return unull(parentType) ? false
    : parentType == rootType ? true
                             : inheritsFrom(parentType, rootType);
}
function propertyToString(property) {
    var typename = getTraitTypeName(property);
    var T = Traits[typename];
    if (!inheritsFrom(typename, Traits.IsProperty["!"]))
        return null;
    var result = [];
    result.push(Utils.entityLink(property.__source__));
    // result.push(wrap(property.__source__).getName());
    if (T.from) {
        result.push(T.from);
    }
    else {
        result.push("is a <a href='#' onclick='{click}'>{name}</a>".fmt({
            name: typename.toLowerCase(),
            click: "render(FactsIndex.{property})".fmt({ property: typename })
        }));
        // result.push("is a " + property["!"].toLowerCase())
    }
    if (property.__target__) {
        result.push(Utils.entityLink(property.__target__));
        // result.push(wrap(property.__target__).getName());
    }
    result = result.concat(renderDate(property));
    return result.join(" ") + ".";
}
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
function actionToString(action) {
    var typename = getTraitTypeName(action);
    var T = Traits[typename];
    if (!inheritsFrom(typename, Traits.Action["!"]))
        return null;
    var result = [];
    if (action.__source__) {
        result.push(Utils.entityLink(action.__source__));
    }
    if (T.from) {
        result.push(T.from);
    }
    else {
        result.push("<a href='#' onclick='render(FactsIndex." + typename + ")'>" + typename.toLowerCase() + "</a>");
    }
    if (action.__target__) {
        result.push(Utils.entityLink(action.__target__));
    }
    if (action.atPlace) {
        result.push("in " + Utils.entityLink(action.atPlace));
    }
    /**
    Is the subject of this action the part of another entity? E.g., a detail of a painting.
    */
    var target = action.__target__;
    if (target) {
        var part = first(where(target.facts, function (fact) { return fact["!"]["!"] == Traits.Part["!"] && fact.__target__ == target; }));
        if (part) {
            result.push("in " + wrap(part.__source__).getName());
        }
    }
    result = result.concat(renderDate(action));
    return result.join(" ") + "."
}

function painting(entity) { return new PaintingWrapper(entity) }
function PaintingWrapper(entity) {
    var _ = this;
    var _entity;
    function init() {
        _entity = wrap(entity);
    }
    _.createdBy = function () {
        return wrap(first(_entity.where(function (fact) { return fact["!"] == Traits.Created["!"] && fact.__target__ == entity })).__source__);
    }
    _.withMedium = function () {
        return (first(_entity.where(function (fact) { return fact["!"] == Traits.Painting["!"] && fact.__source__ == entity })) || {}).withMedium;
    }
    init();
    return _;
}
function getAllMembers(trait) {
    var result = Utils.zip(trait["#"], [], function (a, value, key) { a.push(key) });
    return trait[">"] ? result.concat(getAllMembers(trait[">"])) : trait["<"] ? result.concat(getAllMembers(trait["<"])) : result;
}
function wrap(entity) { return entity instanceof Wrapper ? entity : new Wrapper(entity) }
var model = {
    images: ko.observableArray([]),
    facts: ko.observableArray([]),
    traits: ko.observableArray([]),
    search: ko.observable(""),
    members: ko.observableArray([]),
    results: ko.observableArray([]),
    trait: ko.observable(),
    traitParent: function () {
        //console.log(this.trait());
        var trait = Traits[this.trait()];
        return !trait || !trait[">"] ? "<none>" : trait[">"]['!']
    },
    searchClicked: function () {
        eval("render(" + this.search() + ")");
    }
};
model.trait.subscribe(function (trait) {

    var T = Traits[trait];
    //console.log(T);
    if (!T) return;
    var members = getAllMembers(T);
    model.members(members.length > 0 ? members : ["<no members>"]);
});

function Wrapper(entity) {
    var _ = this;
    var _entity;
    function initialise() {
        _entity = entity instanceof Entity ? entity : new Entity(entity);
        if (isNew(Entities, _entity)) Entities.push(_entity);
    }
    Wrapper.create = function (entity) { return new Wrapper(entity); };
    function isNew(list, item) {
        return count(list, function (i) {
            return getTraitTypeName(i) == getTraitTypeName(item) && i["&"] == item["&"]
        }) == 0
    }
    _.fact = function (_fact) {
        var fact = new Fact(_fact);
        fact.__source__ = fact.__source__ || _entity;
        if (isNew(Facts, fact)) Facts.push(fact);
        if (isNew(_entity.facts, fact)) _entity.facts.push(fact);
        if (!FactsIndex[fact["!"]]) {
            FactsIndex[fact["!"]] = [];
        }
        if (isNew(FactsIndex[fact["!"]], fact)) {
            FactsIndex[fact["!"]].push(fact);
        }
        each(where(fact, function (member) { return member && member != _entity && member["!"] == "Entity" }),
                    function (entity) { entity.facts.push(fact) });
        // updateIndexes(fact);
        return fact;
    }
    function updateIndexes(fact) {
        each(Indexers, function (indexer) { indexer(fact) });
    }
    _.called = function (props) {
        props = Utils.istr(props) ? { name: props } : props;
        _.fact(merge({ "!": Traits.IsCalled["!"] }, props));
        return _;
    };
    _.title = function (props) {
        _.fact(merge({ "!": Traits.Title["!"] }, props));
        return _;
    }
    _.person = function (props) {
        _.fact(merge({ "!": Traits.Agent["!"] }, props));
        return _;
    }
    _.place = function (props) {
        _.fact(merge({ "!": Traits.Place["!"] }, props));
        return _;
    }
    _.city = function (props) {
        _.fact(merge({ "!": Traits.City["!"] }, props));
        return _;
    };
    _.guild = function (props) {
        _.fact(merge({ "!": Traits.Guild["!"] }, props));
        return _;
    };
    _.parish = function (props) {
        _.fact(merge({ "!": Traits.Parish["!"] }, props));
        return _;
    };
    _.hamlet = function (props) {
        _.fact(merge({ "!": Traits.Hamlet["!"] }, props));
        return _;
    }
    _.village = function (props) {
        _.fact(merge({ "!": Traits.Village["!"] }, props));
        return _;
    }
    _.chateau = function (props) {
        _.fact(merge({ "!": Traits.Chateau["!"] }, props));
        return _;
    }
    _.river = function (props) {
        _.fact(merge({ "!": Traits.River["!"] }, props));
        return _;
    }
    _.fresco = function (props) {
        _.fact(merge({ "!": Traits.Fresco["!"] }, props));
        return _;
    };
    _.notary = function (props) {
        _.fact(merge({ "!": Traits.Notary["!"] }, props));
        return _;
    }
    _.painter = function (props) {
        _.fact(merge({ "!": Traits.Painter["!"] }, props));
        return _;
    };
    _.story = function (props) {
        var fact = _.fact(merge({ "!": Traits.Story["!"] }, props, { __target__: unwrap(props.of) }));
        //if (fact.of) Wrapper.create(fact.of).fact(fact);
        return _;
    }
    _.architect = function (props) {
        _.fact(merge({ "!": Traits.Architect["!"] }, props));
        return _;
    }
    _.sculptor = function (props) {
        _.fact(merge({ "!": Traits.Sculptor["!"] }, props));
        return _;
    };
    _.wasDuke = function (props) {
        var fact = _.fact(merge({ "!": Traits.Duke["!"] }, props, { __target__: unwrap(props.of) }));
        //if (fact.of) Wrapper.create(fact.of).fact(fact);
        return _;
    };
    _.served = function (props) {
        var fact = _.fact(merge({ "!": Traits.Served["!"] }, props, { __target__: unwrap(props.the) }));
        //if (fact.the) Wrapper.create(fact.the).fact(fact);
        return _;
    };
    _.wasApprenticed = function (props) {
        var fact = _.fact(merge({ "!": Traits.Apprenticed["!"] }, props, { __source__: unwrap(props.to), __target__: _entity }));
        //if (fact.to) Wrapper.create(fact.to).fact(fact);
        //if (fact.atPlace) Wrapper.create(fact.atPlace).fact(fact);
        return _;
    };
    _.writer = function (props) {
        _.fact(merge({ "!": Traits.Writer["!"] }, props));
        return _;
    }
    _.goldsmith = function (props) {
        _.fact(merge({ "!": Traits.Goldsmith["!"] }, props));
        return _;
    }
    _.wasTaught = function (props) {
        var fact = _.fact(merge({ "!": Traits.Taught["!"] }, props, { __target__: _entity, __source__: unwrap(props.by) }));
        //if (fact.by) Wrapper.create(fact.by).fact(fact);
        return _;
    }
    _.wasWritten = function (props) {
        var fact = _.fact(merge({ "!": Traits.Wrote["!"] }, props, { __target__: _entity, __source__: unwrap(props.by) }));
        //if (fact.by) Wrapper.create(fact.by).fact(fact);
        return _;
    }
    _.wasPublished = function (props) {
        var fact = _.fact(merge({ "!": Traits.Published["!"] }, props, { __target__: unwrap(props.by) }));
        //if (fact.by) Wrapper.create(fact.by).fact(fact);
        return _;
    }
    _.book = function (props) {
        _.fact(merge({ "!": Traits.Book["!"] }, props));
        return _;
    }
    _.institution = function (props) {
        _.fact(merge({ "!": Traits.Institution["!"] }, props));
        return _;
    }
    _.country = function (props) {
        _.fact(merge({ "!": Traits.Country["!"] }, props));
        return _;
    }
    _.region = function (props) {
        _.fact(merge({ "!": Traits.Region["!"] }, props));
        return _;
    }
    _.Q = function (query) {
        return queryAll(_entity.facts, query);
    }
    _.wasBuried = function (props) {
        var fact = _.fact(merge({ "!": Traits.Buried["!"] }, props, { __target__: _entity, __source__: null }));
        //if (fact.atPlace) Wrapper.create(fact.atPlace).fact(fact);
        return _;
    };
    _.wasBorn = function (props) {
        var fact = _.fact(merge({ "!": Traits.Born["!"] }, props));
        //if (fact.toMother) Wrapper.create(fact.toMother).fact(fact);
        //if (fact.toFather) Wrapper.create(fact.toFather).fact(fact);
        //if (fact.atPlace) Wrapper.create(fact.atPlace).fact(fact);
        return _;
    };
    _.wasCharged = function (props) {
        var fact = _.fact(merge({ "!": Traits.Charged["!"] }, props, { __source__: props.by, __target__: _entity }));
        //if (fact['with']) Wrapper.create(fact['with']).fact(fact);
        return _;
    };
    _.wasAcquitted = function (props) {
        var fact = _.fact(merge({ "!": Traits.Acquitted["!"] }, props, { __source__: props.by, __target__: _entity }));
        //if (fact['of']) Wrapper.create(fact['of']).fact(fact);
        return _;
    };
    var getPlaceFact = function (entity) {
        return first(where(entity.facts, function (fact) {
            return inheritsFrom(fact["!"], Traits.Place["!"]);
        }));
    };
    _.inside = function (other) {
        var myPlaceFactType = getPlaceFact(_entity);
        if (unull(myPlaceFactType)) return false;
        var otherPlaceFactType = getPlaceFact(other);
        if (unull(otherPlaceFactType)) return false;
        if (myPlaceFactType.__source__ == other) return true;
        if (!myPlaceFactType.inside) return false;
        if (myPlaceFactType.inside == other) return true;
        return wrap(myPlaceFactType.inside).inside(other);
    }
    _.entity = function (props) {
        _.fact(merge({ "!": "Entity" }, props));
        return _;
    }
    _.hasSubject = function (props) {
        var fact = _.fact(merge({ "!": Traits.HasSubject["!"] }, { __target__: unwrap(props.of) }, props))
        //if (fact.of) wrap(fact.of).fact(fact);
        return _;
    }
    _.theme = function (props) {
        var fact = _.fact(merge({ "!": Traits.HasTheme["!"] }, props))
        //if (fact.of) wrap(fact.of).fact(fact);
        return _;
    }
    _.sculpture = function (props) {
        _.fact(merge({ "!": Traits.Sculpture["!"] }, props));
        return _;
    }
    _.painting = function (props) {
        _.fact(merge({ "!": Traits.Painting["!"] }, props));
        return _;
    };
    _.drawing = function (props) {
        _.fact(merge({ "!": Traits.Drawing["!"] }, props));
        return _;
    }
    _.exhibitedBy = function (props) {
        var fact = _.fact(merge({ "!": Traits.Exhibited["!"] }, props, { __source__: unwrap(props.the), __target__: _entity }));
        //if (fact.the) Wrapper.create(fact.the).fact(fact);
        //if (fact.atPlace) Wrapper.create(fact.atPlace).fact(fact);
        return _;
    };
    _.wasCommissioned = function (props) {
        var fact = _.fact(merge({ "!": Traits.Commissioned["!"] }, props, { __source__: unwrap(props.by), __target__: _entity }));
        //if (fact.toCreate) Wrapper.create(fact.toCreate).fact(fact);
        //if (fact.atPlace) Wrapper.create(fact.atPlace).fact(fact);
        return _;
    }
    _.married = function (props) {
        var fact = _.fact(merge({ "!": Traits.Married["!"] }, props, { __target__: unwrap(props.to) }));
        //if (fact.to) wrap(fact.to).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.was = function (type) {
        return where(_entity.facts, function (fact) {
            return fact.__source__ == _entity
                && fact["!"].toLowerCase() == type.toLowerCase()
        });
    }
    _.where = function (factP) {
        return where(_entity.facts, factP);
    };
    _.getNames = function () {
        var names = where(_entity.facts, function (fact) {
            var F = getTraitTypeName(fact);
            return inheritsFrom(F, Traits.IsCalled["!"])
        });
        return select(names, function (called) {
            var result = called.name;
            if (called["!"]["!"] == Traits.Title["!"]) {
                result = "'" + called.name + "'";
            }
            if (!called.properNoun) {
                result = "the " + result;
            }
            var placeFact = first(where(_entity.facts, function (fact) {
                return inheritsFrom(fact["!"]["!"], Traits.Place["!"]) && fact["!"]["!"] != Traits.City["!"];
            }));
            if (placeFact) {
                if (placeFact.inside) {
                    result += ", " + first(wrap(placeFact.inside).was("IsCalled")).name + ",";
                }
            }
            return result;
        });
    }
    _.getName = function () {
        return first(_.getNames());
    }
    _.getArtworks = function () {
        var created = _.was(Traits.Created["!"]);
        var artworks = created.length == 0 ? "nothing" : select(created, function (work) { return wrap(work.__target__).getName() }).join(", ");
        var response = "{artist} created: {artworks}.".fmt({
            artist: _.getNames(),
            artworks: artworks
        });
        return response;
    }
    _.getPhotos = function () {
        var photo = first(Q({ "!": Traits.IsPhotograph["!"], of: _entity }));
        if (!photo) return [];
        var scans = Q({ "!": Traits.Scanned["!"], __target__: { "&": photo.__source__["&"] } });
        var photos = flatten(select(scans, function (scan) {
            return Q({ "!": Traits.IsImage["!"], __source__: { "&": scan.__source__["&"] } });
        }));
        return photos;
    }
    _.allegory = function (props) {
        var fact = _.fact(merge({ "!": Traits.IsAllegory["!"] }, props, { __target__: unwrap(props.of) }));
        //if (fact.of) wrap(fact.of).fact(fact);
        return _;
    }
    _.getImages = function () {
        var scans = Q({ "!": Traits.Scanned["!"], from: _entity });
        var images = flatten(select(scans, function (scan) {
            return Q({ "!": Traits.IsImage["!"], __source__: { "&": scan.__source__["&"] } });
        }).concat(_.getPhotos()));
        return images;
    }
    _.getImagesOfArtworks = function () {
        var images = flatten(select(_.was(Traits.Created["!"]), function (artwork) { return wrap(artwork.__target__).getImages() }));
        model.images(images);
        return images;
    }
    _.created = function (props) {
        var fact = _.fact(merge({ "!": Traits.Created["!"] }, props, { __target__: unwrap(props.the) }));
        if (fact.the) wrap(fact.the).fact(fact);
        if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.taught = function (props) {
        var fact = _.fact(merge({ "!": Traits.Taught["!"] }, props, { __target__: unwrap(props.the) }));
        //if (fact.the) wrap(fact.the).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.isPart = function (props) {
        var fact = _.fact(merge({ "!": Traits.Part["!"] }, props, { __target__: _entity, __source__: unwrap(props.of) }));
        //if (fact.of) wrap(fact.of).fact(fact);
        return _;
    };
    _.hasPart = function (props) {
        var fact = _.fact(merge({ "!": Traits.Part["!"] }, props, { __target__: unwrap(props.of), __source__: _entity }));
        //if (fact.of) wrap(fact.of).fact(fact);
        return _;
    };
    _.painted = function (props) {
        var fact = _.fact(merge({ "!": Traits.Painted["!"] }, props, { __target__: unwrap(props.the) }));
        //if (fact.the) wrap(fact.the).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    };
    _.drew = function (props) {
        var fact = _.fact(merge({ "!": Traits.Drew["!"] }, props, { __target__: unwrap(props.the) }));
        //if (fact.the) wrap(fact.the).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    };
    _.wasDrawn = function (props) {
        var fact = _.fact(merge({ "!": Traits.Drew["!"] }, props, { __source__: unwrap(props.by), __target__: _entity }));
        //if (fact.the) wrap(fact.the).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    };
    _.joined = function (props) {
        var fact = _.fact(merge({ "!": Traits.Joined["!"] }, props, { __target__: unwrap(props.the) }));
        //if (fact.the) wrap(fact.the).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.wasCreated = function (props) {
        var fact = _.fact(merge({ "!": Traits.Created["!"] }, props, { __source__: unwrap(props.by), __target__: _entity }));
        //if (fact.by) wrap(fact.by).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.wasSculpted = function (props) {
        var fact = _.fact(merge({ "!": Traits.Sculpted["!"] }, props, { __source__: unwrap(props.by), __target__: _entity }));
        //if (fact.by) wrap(fact.by).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.wasPainted = function (props) {
        var fact = _.fact(merge({ "!": Traits.Painted["!"] }, props, { __source__: unwrap(props.by), __target__: _entity }));
        //if (fact.by) wrap(fact.by).fact(fact);
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.isFatherTo = function (_person) {
        return count(where(wrap(_person).was(Traits.Born["!"]), function (birth) {
            return birth.toFather == _entity
        })) == 1;
    }
    _.lived = function (props) {
        var fact = _.fact(merge({ "!": Traits.LivedAt["!"] }, props));
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.worked = function (props) {
        var fact = _.fact(merge({ "!": Traits.WorkedAt["!"] }, props));
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.died = function (props) {
        var fact = _.fact(merge({ "!": Traits.Died["!"] }, props));
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    }
    _.represents = function (props) {
        var fact = _.fact(merge({ "!": Traits.DoesRepresent["!"] }, props));
        //if (fact.character) wrap(fact.character).fact(fact);
        //if (fact.playing) wrap(fact.playing).fact(fact);
        return _;
    }
    _.acquiredBy = function (props) {
        var fact = _.fact(merge({ "!": Traits.Acquired["!"] }, props, { __target__: _entity, __source__: unwrap(props.by) }));
        return _;
    };
    _.acquired = function (props) {
        var fact = _.fact(merge({ "!": Traits.Acquired["!"] }, props, { __target__: unwrap(props.the), __source__: _entity }));
        //if (fact.the) wrap(fact.the).fact(fact);
        return _;
    }
    _.marriage = function (props) {
        return first(_.was(Traits.Married["!"]))
    }
    _.scanned = function (props) {
        var fact = _.fact(merge({ "!": Traits.Scanned["!"] }, props, { __target__: unwrap(props.from) }));
        //if (fact.by) wrap(fact.by).fact(fact);
        //if (fact.from) wrap(fact.from).fact(fact);
        return _;
    }
    _.image = function (props) {
        var fact = _.fact(merge({ "!": Traits.IsImage["!"] }, props));
        return _;
    };
    _.hasImage = function (props) {
        var image = wrap().image(props.of).unwrap();
        var fact = _.fact(merge({ "!": Traits.IsImage["!"] }, props, { of: image, __source__: image, __target__: _entity }));
        return _;
    };
    _.building = function (props) {
        var fact = _.fact(merge({ "!": Traits.Building["!"] }, props));
        //if (fact.atPlace) wrap(fact.atPlace).fact(fact);
        return _;
    };
    _.photograph = function (props) {
        _.fact(merge({ "!": Traits.IsPhotograph["!"] }, props));
        return _;
    }
    _.source = function () {
        return wrap(_entity.__source__);
    }
    _.unwrap = function () {
        return _entity;
    }
    initialise();
    return _;
}
function unwrap(value) {
    return value instanceof Wrapper ? value.unwrap() : value;
}
Indexers.push(
    function (fact) {
        // onDate
        Index.onDate = Index.onDate || [];
        if (fact.onDate) Index.onDate.push(fact)
    }
);
var render = function (facts) {
    if (window.removeMarkers) {
        removeMarkers();
    }
    var results = facts["!"] ? render(facts.facts) : select(facts, function (fact) {
        var T = getTraitTypeName(fact);
        var displayMarker = function (place) {
            var coords = first(where(place.facts, function (f) { return inheritsFrom(getTraitTypeName(f), "Place") }));
            if (coords && coords.lat && coords.long) {
                if (window.addMarker) {
                    addMarker(coords.lat, coords.long, scrubHtml(text));
                }
            }
        };
        var text = Traits[T].hasOwnProperty('toString')
            ? Traits[T].toString(fact)
            : inheritsFrom(T, Traits.Action["!"])
                ? actionToString(fact)
                : propertyToString(fact);
        each(where(fact, function (member) { return getTraitTypeName(member) == "Entity" }),
            function (entity) { displayMarker(entity) });
        return text;
    });
    if (model && model.results) {
        model.results(results);
    }
    return results;

}
function scrubHtml(value) {
    if (!value) return "";
    var regex = /(<([^>]+)>)/ig;
    return value.replace(regex, "");
}
var ko, $;

// Set up the traits drop downs.
if (model)
    model.traits(Utils.zip(Traits, [], function (a, trait) { a.push(trait['!']) }));

Array.prototype.log = function (msg) {
    var list = this;
    var time = new Date(), hh = time.getHours(), mm = time.getMinutes(), ss = time.getSeconds();
    var header = "======{msg}========================================================================".fmt({
        msg: " {hh}:{mm}:{ss} ".fmt({ hh: hh, mm: mm, ss: ss })
    });
    console.log(header);
    each(list, function (item, __, index) {
        console.log(index, item);
    });
    var total = Utils.count(list);
    console.log("{total} {rows}.".fmt({ total: total, rows: total == 1 ? "row" : "rows" }));
    for (var footer = [], i = 0, len = header.length; i < len; footer.push("="), i++);
    console.log(footer.join(""));
};
markers = [];
function addMarker(lat, long, title) {
    if (!window.google) {
        return;
    }
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, long),
        title: title || (lat + ", " + long)
    });
    marker.setMap(map);
    markers.push(marker);
}
function removeMarkers() {
    Utils.each(markers, function (marker) { marker.setMap(null); });
    markers = [];
}
var map;
function initialize() {
    if (!window.google) {
        console.log("Could not find the 'google' module.");
        return;
    }
    var mapOptions = {
        zoom: 6,
        center: new google.maps.LatLng(41.9, 12.483333),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
    // addMarker(43.783333, 11.25);
}

if (window.google) {
    google.maps.event.addDomListener(window, 'load', initialize);
}