﻿var app = window.app || {};
(function () {
    (function (API) {
        var utils = app.Utils,
            log = utils.log;
        API.ajax = function (operation) {
            log({
                origin: "API.ajax",
                state: "AJAX-PRE",
                operation: operation
            });
            // var basePath = "http://localhost:49571";
            var basePath = "";
            // var basePath = "http://codex.localhost";
            $.ajax({
                data: JSON.stringify(operation.client.data),
                url: basePath + operation.url,
                type: operation.type || "POST",
                dataType: operation.dataType || "json",
                contentType: operation.contentType || 'application/json; charset=utf-8',
                success: function (response) {
                    log({
                        origin: "API.ajax",
                        state: "AJAX-SUCCESS",
                        operation: operation,
                        response: response
                    });
                    if (false == response.Success) {
                        if (operation.client.error) {
                            log({
                                origin: "API.ajax",
                                state: "AJAX-ERROR",
                                operation: operation,
                                response: response
                            });
                            operation.client.error.call(operation.context);
                            if (operation.client.after) {
                                operation.client.after.call(operation.context);
                            }
                        }
                    }
                    if (operation.client.success) {
                        operation.client.success.call(operation.context, response);
                    }
                    if (operation.client.after) {
                        operation.client.after.call(operation.context);
                    }
                },
                error: function (xhr, textStatus, error) {
                    log({
                        origin: "API.ajax",
                        state: "AJAX-ERROR",
                        operation: operation,
                        xhr: xhr,
                        textStatus: textStatus,
                        error: error
                    });
                    if (operation.client.error) {
                        operation.client.error.call(operation.context);
                    }
                    if (operation.client.after) {
                        operation.client.after.call(operation.context);
                    }
                }
            });
        };
        API.findAllEntities = function (client, context) {
            var operation = {
                url: "/api/findAllEntities",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.represents = function (client, context) {
            var operation = {
                url: "/api/represents",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findCategoryHierarchy = function (client, context) {
            var operation = {
                url: "/api/findCategoryHierarchy",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findAgentByName = function (client, context) {
            var operation = {
                url: "/api/findAgentByName",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findServedTest = function (client, context) {
            var operation = {
                url: "/api/findServedTest",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.createCategory = function (client, context) {
            var operation = {
                url: "/api/createCategory",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.allTempNodes = function (client, context) {
            var operation = {
                url: "/api/allTempNodes",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findAllPlaceFacts = function (client, context) {
            var operation = {
                url: "/api/findAllPlaceFacts",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.queryFacts = function (client, context) {
            var operation = {
                url: "/api/queryFacts",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.createPlace = function (client, context) {
            var operation = {
                url: "/api/createPlace",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.createEvent = function (client, context) {
            var operation = {
                url: "/api/createEvent",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findAllAgentAgentHyperNodes = function (client, context) {
            var operation = {
                url: "/api/findAllAgentAgentHyperNodes",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findParentCategories = function (client, context) {
            var operation = {
                url: "/api/findParentCategories",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.personIs = function (client, context) {
            var operation = {
                url: "/api/personIs",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findAllRelationships = function (client, context) {
            var operation = {
                url: "/api/findAllRelationships",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findAllAttributes = function (client, context) {
            var operation = {
                url: "/api/findAllAttributes",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findAllProperties = function (client, context) {
            var operation = {
                url: "/api/findAllProperties",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findAllSubcategories = function (client, context) {
            var operation = {
                url: "/api/findAllSubcategories",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findPlaces = function (client, context) {
            var operation = {
                url: "/api/findPlaces",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findEvents = function (client, context) {
            var operation = {
                url: "/api/findEvents",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findLivedAtHyperNode = function (client, context) {
            var operation = {
                url: "/api/findLivedAtHyperNode",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findVisitedHyperNode = function (client, context) {
            var operation = {
                url: "/api/findVisitedHyperNode",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findBornHyperNode = function (client, context) {
            var operation = {
                url: "/api/findBornHyperNode",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.findDiedHyperNode = function (client, context) {
            var operation = {
                url: "/api/findDiedHyperNode",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.saveAgent = function (client, context) {
            var operation = {
                url: "/api/saveAgent",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.allPlaces = function (client, context) {
            var operation = {
                url: "/api/allPlaces",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.allPeople = function (client, context) {
            var operation = {
                url: "/api/allPeople",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.allArtworks = function (client, context) {
            var operation = {
                url: "/api/allArtworks",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.allCategories = function (client, context) {
            var operation = {
                url: "/api/allCategories",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.createVisitedHyperNode = function (client, context) {
            var operation = {
                url: "/api/createVisitedHyperNode",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.createLivedAtHyperNode = function (client, context) {
            var operation = {
                url: "/api/createLivedAtHyperNode",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.createBornHyperNode = function (client, context) {
            var operation = {
                url: "/api/createBornHyperNode",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
        API.createDiedHyperNode = function (client, context) {
            var operation = {
                url: "/api/createDiedHyperNode",
                client: client,
                context: context
            };
            API.ajax(operation);
        };
    })(app.API || (app.API = {}));
})(app);