﻿
var App = window.App || {};
(function (App) {

    var utils = Utils,
        any = utils.any,
        __extends = utils.__extends,
        last = utils.last,
        merge = utils.merge,
        log = utils.log,
        copyTo = utils.copyTo,
        zip = utils.zip,
        each = utils.each,
        first = utils.first,
        broadcast = utils.broadcast,
        receive = utils.receive,
        where = utils.where,
        select = utils.select;

    (function (Editors) {
        App.API = App.API || {};
        App.API.EndPointBase = "http://localhost:7474";
        App.API.EndPoints = {
            Node: "/db/data/node",
            NodeIndex: "/db/data/index/node",
            RelationshipIndex: "/db/data/index/relationship",
            ExtensionsInfo: "/db/data/ext",
            RelationshipTypes: "/db/data/relationship/types",
            Batch: "/db/data/batch",
            Cypher: "/db/data/cypher",
            Transaction: "/db/data/transaction",
        };
        var exec = function (fn) {
            if (typeof fn == "function") {
                fn.apply(null, Array.prototype.slice.call(arguments, 1));
            }
        };
        App.API.ajax = function (operation) {
            var url = App.API.EndPointBase + operation.method;
            var data = JSON.stringify(operation.data);
            var type = operation.type || "POST";
            var contentType = operation.contentType || "application/json; charset=UTF-8";
            $.ajax({
                url: url,
                type: type,
                data: data,
                contentType: contentType,
                success: function (result) {
                    var payload = {
                        ajaxResult: "SUCCESS",
                        operation: operation,
                        result: result
                    };
                    if (result.errors) {
                        payload.operationResult = "ERROR";
                        log(payload);
                        exec(operation.error, payload);
                        return;
                    }
                    payload.operationResult = "SUCCESS";
                    log(payload);
                    exec(operation.success, result);
                },
                error: function (xhr, textStatus, error) {
                    var payload = {
                        ajaxResult: "ERROR",
                        operation: operation,
                        ajaxError: {
                            xhr: xhr, textStatus: textStatus, error: error
                        }
                    };
                    log(payload);
                    exec(operation.error, payload);
                }
            });
        };
        App.API.cypher = function (data) {
            var operation = {
                method: App.API.EndPoints.Cypher,
                data: {
                    query: data.query,
                    params: data.params
                },
                success: function (response) {
                    if (data.success) {
                        data.success(response)
                    }
                    else {
                        log(response)
                    }
                },
                error: function (response) {
                    if (data.error) {
                        data.error(response);
                    } else {
                        log(response.ajaxError.xhr.responseJSON);
                    }
                }
            };
            App.API.ajax(operation);
        };
        App.API.createNode = function (data) {
            var request = {
                query: data.query,
                params: {
                    props: data.props
                },
                success: data.success,
                error: data.error
            };
            App.API.cypher(request);
        };
        /**
        Clearly this function is just for development purposes to clear bad data ...
        */
        App.API.deleteDatabase = function () {
            var query = [
                "match (n) optional",
                "match (n)-[r]-()",
                "delete n, r"
            ];
            App.API.cypher({ query: query.join(" ") });
        };
        App.API.createBirthFact = function (operation) {
            var fact = operation.fact;
            // Assuming source, mother, father, and place exist.
            var query = [];
            var start = [];
            var query = [
                "START child=node({ child }), father=node({ father }), mother=node({ mother }), place=node({ place }) ",
                "CREATE (birth:Fact { type: 'birth', date: { date } }), ",
                "(child)-[:SUBJECT_OF]->(birth), ",
                "(father)-[:FATHER]->(birth), ",
                "(mother)-[:MOTHER]->(birth), ",
                "(place)-[:LOCATION_OF]->(birth) ",
                "(birth)-[:LOCATED_AT]->(place) ",
                "RETURN birth, id(birth)"
            ];
            App.API.cypher({
                query: query.join(''),
                params: {
                    props: {
                        child: fact.target.nodeId(),
                        father: fact.toFather.nodeId,
                        mother: fact.toMother.nodeId,
                        place: fact.atPlace.nodeId,
                        date: toNeo4jDate(fact.onDate)
                    }
                },
                success: operation.success,
                error: operation.error
            });
        };
        function toNeo4jDate(date) {
            var result = [];
            result.push((date.era ? date.era : "AD") + "-");
            result.push((date.year ? date.year : "yyyy") + "-");
            result.push((date.month ? date.month : "mm") + "-");
            result.push((date.day ? date.day : "dd") + "-");
            result.push((date.prox ? date.prox : "=="));
            return result.join("");
        }
        App.API.addFatherToBirthFact = function (params) {
            var query = [

            ];
            var createParams = [];
            var startParams = ["birth=node({ birth })"];
            if (params.father) {
                startParams.push("father=node({ father })");
                createParams.push("(father)-[:FATHER]->(birth)");
            }
            if (params.mother) {
                startParams.push("mother=node({ mother })");
                createParams.push("(mother)-[:MOTHER]->(birth)");
            }
            if (params.place) {
                startParams.push("place=node({ place })");
                createParams.push("(place)<-[:LOCATION_OF]->(birth)");
            }
            query.push("START " + startParams.join(", "));
            query.push("CREATE " + createParams.join(", "));
            query.push("RETURN birth, id(birth)");
            App.API.cypher({
                query: query.join(""),
                params: {
                    props: {} // etc
                }
            });
        };
        App.API.updateNode = function (id, props, complete) {
            App.API.cypher({
                query: "{... get node == id ...} SET n = { props } RETURN n".fmt({ id: id }),
                params: {
                    props: props
                },
                success: complete,
                error: complete
            });
        };
        App.API.createAgent = function (props, success, error) {
            App.API.createNode({
                query: "CREATE (n:Agent { props } ) RETURN id(n)",
                props: props,
                success: function (response) {
                    var nodeId = response.data[0][0];
                    if (success) {
                        success(nodeId);
                    } else {
                        log({ text: "Agent node creation SUCCEEDED.", nodeId: nodeId });
                    }
                },
                error: function (payload) {
                    if (error) {
                        error(payload);
                    } else {
                        log({ text: "Agent node creation FAILED.", payload: payload });
                    }
                }
            });
        };
        App.API.createPlace = function (props, success, error) {
            App.API.createNode({
                query: "CREATE (n:Place { props } ) RETURN id(n)",
                props: props,
                success: function (response) {
                    var nodeId = response.data[0][0];
                    if (success) {
                        success(nodeId);
                    } else {
                        log({ text: "Place node creation SUCCEEDED.", nodeId: nodeId });
                    }
                },
                error: function (payload) {
                    if (error) {
                        error(payload);
                    } else {
                        log({ text: "Place node creation FAILED.", payload: payload });
                    }
                }
            });
        };
        App.API.findAgentNodes = function (params, context) {
            var props = params.props;
            var query = [
                "MATCH (x:Agent)",
                "WHERE x.name =~ {name}",
                props.sex ? "AND x.sex = {sex}" : "",
                "RETURN id(x), x",
                "ORDER BY x.name LIMIT 10"
            ];
            App.API.cypher({
                query: query.join(" "),
                params: {
                    name: "(?i)" + props.name + ".*",
                    sex: props.sex
                },
                success: function (response) {
                    var people = select(response.data, function (tuple) {
                        return merge({ nodeId: tuple[0] }, tuple[1].data);  // { id: tuple[0], person: tuple[1].data };
                    });
                    if (params.success) {
                        params.success.call(context, people);
                    }
                },
                error: function (payload) {
                    if (params.error) {
                        params.error.call(context, payload);
                    }
                }
            });
        };
        App.API.findPlaceNodes = function (params, context) {
            var props = params.props;
            var query = [
                "MATCH (x:Place)",
                "WHERE x.name =~ {name}",
                "RETURN id(x), x",
                "ORDER BY x.name LIMIT 10"
            ];
            App.API.cypher({
                query: query.join(" "),
                params: {
                    name: "(?i)" + props.name + ".*"
                },
                success: function (response) {
                    var entities = select(response.data, function (tuple) {
                        return merge({ nodeId: tuple[0] }, tuple[1].data);  // { id: tuple[0], person: tuple[1].data };
                    });
                    if (params.success) {
                        params.success.call(context, entities);
                    }
                },
                error: function (payload) {
                    if (params.error) {
                        params.error.call(context, payload);
                    }
                }
            });
        };
        App.API.getProperty = function (id, key, complete) {
            $.ajax({
                type: "GET",
                url: "{server}/db/data/node/{id}/properties/{key}".fmt({
                    server: App.API.EndPointBase, id: id, key: key
                }),
                success: function (data) {
                    if (complete) {
                        complete(data)
                    } else {
                        log(data);
                    }
                },
                error: function () {
                    if (complete) {
                        complete(null);
                    } else {
                        log(null);
                    }
                }
            });
        };
        App.API.resultToJSON = function (result) {
            return select(result.data, function (row) {
                return zip(row, {}, function (temp, value, __, index) {
                    temp[result.columns[index]] = value;
                });
            });
        };
        App.Components = App.Components || {};
        var PubSub = (function () {
            function PubSub() {
                this.events = {};
            }
            PubSub.prototype.publish = function (eventName) {
                if (!this.events[eventName]) {
                    return;
                }
                var eventArgs = Array.prototype.slice.call(arguments, 1);
                each(this.events[eventName], function (subscriber) {
                    try {
                        subscriber.handler.apply(subscriber.context, eventArgs);
                    } catch (ex) {
                        log({
                            origin: "PubSub.publish",
                            message: "Error on invocation of subscriber to '{event}'.".fmt({ event: eventName }),
                            eventArgs: eventArgs,
                            exception: ex
                        });
                    }
                });
            };
            PubSub.prototype.subscribe = function (eventName, handler, context) {
                if (!this.events[eventName]) {
                    this.events[eventName] = [];
                }
                this.events[eventName].push({ handler: handler, context: context || arguments.callee.caller });
            };
            return PubSub;
        })();
        App.Components.PubSub = PubSub;
        App.Resources = App.Resources || {};
        var Languages = App.Resources.Languages = [
            { text: "", value: "" },
            { text: "English", value: "en" },
            { text: "French", value: "fr" },
            { text: "Italian", value: "it" },
            { text: "German", value: "de" },
            { text: "Spanish", value: "es" },
            { text: "English (Anglo Saxon)", value: "en-GB-SX" },
            { text: "French (Merovingian)", value: "fr-FR-MV" },
            { text: "Italian (Florentine)", value: "it-IT-FI" },
            { text: "Italian (Venetian)", value: "it-IT-VE" }
        ];
        App.Resources.Skills = select(utils.zip(where(Traits, function (trait) { return trait[">"] == Traits.Skill; }), [], function (a, v) { a.push(v) }),
                    function (skill) { return { text: skill["!"], value: skill }; });
        App.Selectors = App.Selectors || {};
        var AgentSelector = (function () {
            function AgentSelector(model) {
                this.model = {};
                this.model.nodeId = ko.observable();
                this.model.name = ko.observable();
                this.model.properNoun = ko.observable();
                this.model.sex = ko.observable();
                this.name = ko.observable();
                this.people = ko.observableArray([]);
                this.pubsub = new PubSub();
                this.errors = ko.observableArray();
                if (model) {
                    this.bind(model);
                }
            }
            AgentSelector.prototype.searchClicked = function () {
                if (!this.name()) {
                    this.people([]);
                    return;
                }
                App.API.findAgentNodes({
                    props: { name: this.name(), sex: this.model.sex() },
                    success: function (people) {
                        this.people(people);
                    }
                }, this);
            };
            AgentSelector.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                this.model.nodeId(model.nodeId);
                this.model.name(model.name);
                this.model.sex(model.sex);
                this.model.properNoun(model.properNoun);
                this.name(model.name);
            };
            AgentSelector.prototype.unbind = function () {
                return {
                    nodeId: this.model.nodeId(),
                    name: this.model.name(),
                    sex: this.model.sex(),
                    properNoun: this.model.properNoun()
                };
            };
            AgentSelector.prototype.clearErrors = function () {
                this.errors([]);
            };
            AgentSelector.prototype.isProperNoun = function (name) {
                return !name.toLowerCase().indexOf("the ") == 0;
            };
            AgentSelector.prototype.createAgentClicked = function () {
                this.clearErrors();
                var _this = this;
                var name = this.name();
                this.bind({
                    name: name,
                    properNoun: this.isProperNoun(name)
                });
                App.API.createAgent(this.unbind(), function (nodeId) {
                    _this.model.nodeId(nodeId);
                    broadcast("OnEntityStubCreated", _this.unbind());
                }, function () {
                    _this.errors.push("Sorry, I was unable to create {name} at this time.".fmt({ name: name }));
                });
            };
            AgentSelector.prototype.personSelected = function (person) {
                this.bind(person);
                this.name(person.name);
                this.people([]);
                this.pubsub.publish("SelectionComplete", this.unbind());
            };
            return AgentSelector;
        })();
        App.Selectors.AgentSelector = AgentSelector;
        var PlaceSelector = (function () {
            function PlaceSelector(model) {
                this.model = {};
                this.model.nodeId = ko.observable();
                this.model.name = ko.observable();
                this.name = ko.observable();
                this.entities = ko.observableArray([]);
                this.pubsub = new PubSub();
                this.errors = ko.observableArray();
                if (model) {
                    this.bind(model);
                }
            }
            PlaceSelector.prototype.searchClicked = function () {
                if (!this.name()) {
                    this.entities([]);
                    return;
                }
                App.API.findPlaceNodes({
                    props: { name: this.name() },
                    success: function (entities) {
                        this.entities(entities);
                    }
                }, this);
            };
            PlaceSelector.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                this.model.nodeId(model.nodeId);
                this.model.name(model.name);
                this.name(model.name);
            };
            PlaceSelector.prototype.unbind = function () {
                return {
                    nodeId: this.model.nodeId(),
                    name: this.model.name()
                };
            };
            PlaceSelector.prototype.clearErrors = function () {
                this.errors([]);
            };
            PlaceSelector.prototype.createPlaceClicked = function () {
                this.clearErrors();
                var _this = this;
                var name = this.name();
                this.bind({
                    name: name
                });
                App.API.createPlace(this.unbind(), function (nodeId) {
                    _this.model.nodeId(nodeId);
                    broadcast("OnEntityStubCreated", _this.unbind());
                }, function () {
                    _this.errors.push("Sorry, I was unable to create {name} at this time.".fmt({ name: name }));
                });
            };
            PlaceSelector.prototype.placeSelected = function (entity) {
                this.bind(entity);
                this.name(entity.name);
                this.entities([]);
                this.pubsub.publish("SelectionComplete", this.unbind());
            };
            return PlaceSelector;
        })();
        App.Selectors.PlaceSelector = PlaceSelector;
        var DateSelector = (function () {
            function DateSelector(model) {
                this.proximity = ko.observable();
                this.day = ko.observable();
                this.month = ko.observable();
                this.year = ko.observable();
                this.months = [{ value: "", text: "" },
                    { value: 1, text: "(01) January" }, { value: 2, text: "(02) February" }, { value: 3, text: "(03) March" },
                    { value: 4, text: "(04) April" }, { value: 5, text: "(05) May" }, { value: 6, text: "(06) June" }, { value: 7, text: "(07) July" },
                    { value: 8, text: "(08) August" }, { value: 9, text: "(09) September" }, { value: 10, text: "(10) October" },
                    { value: 11, text: "(11) November" }, { value: 12, text: "(12) December" }];
                this.proximities = [{ text: "", value: "" }, { value: "c.", text: "around" }, { value: "<", text: "before" },
                    { value: ">", text: "after" }, { value: ">=", text: "on or after" }, { value: "<=", text: "on or before" }];
                if (model) {
                    this.bind(model);
                }
            }
            DateSelector.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (typeof model.prox != 'undefined') this.proximity(model.prox);
                if (typeof model.day != 'undefined') this.day(model.day);
                if (typeof model.month != 'undefined') this.month(model.month);
                if (typeof model.year != 'undefined') this.year(model.year);
            };
            DateSelector.prototype.unbind = function () {
                return {
                    prox: this.proximity(),
                    day: this.day(),
                    month: this.month(),
                    year: this.year()
                };
            };
            DateSelector.prototype.hasDate = function () {
                var model = this.unbind();
                return model.prox || model.day || model.month || model.year;
            }
            return DateSelector;
        })();
        App.Selectors.DateSelector = DateSelector;
        var DateRangeSelector = (function () {
            function DateRangeSelector(model) {
                this.modes = ko.observableArray(["single", "range"]);
                this.mode = ko.observable("single"); // "single", "range"
                this.onDate = new DateSelector();
                this.fromDate = new DateSelector();
                this.toDate = new DateSelector();
                if (model) {
                    this.bind(model);
                }
            }
            DateRangeSelector.prototype.bind = function (model) {
                if (model.onDate) {
                    this.onDate.bind(model.onDate);
                    this.mode("single");
                } else {
                    this.fromDate.bind(model.fromDate);
                    this.toDate.bind(model.toDate);
                    this.mode("range");
                }
            };
            DateRangeSelector.prototype.unbind = function () {
                var model = this.mode() == "single" ?
                     {
                         onDate: this.onDate.unbind()
                     } : {
                         fromDate: this.fromDate.unbind(),
                         toDate: this.toDate.unbind()
                     };
                return model;
            };
            return DateRangeSelector;
        })();
        App.Selectors.DateRangeSelector = DateRangeSelector;
        var BaseEditor = (function () {
            function BaseEditor(model) {
                this.history = [];
                this.pubsub = new PubSub();
            }
            BaseEditor.prototype.undo = function () {
                if (this.history.length === 0) {
                    return;
                }
                this.bind(this.history.pop());
            };
            BaseEditor.prototype.updateHistory = function (model) {
                this.history.push(this.unbind());
            };
            BaseEditor.prototype.getDateIfExists = function (dateSelector/*: DateSelector */) {
                return dateSelector.hasDate() ? dateSelector.unbind() : undefined
            }
            BaseEditor.prototype.save = function () {
                this.pubsub.publish("OnSave", this);
            };
            BaseEditor.prototype.remove = function () {
                this.pubsub.publish("OnRemove", this);
            };
            BaseEditor.prototype.close = function () {
                this.pubsub.publish("OnClose", this);
            };
            return BaseEditor;
        })();
        Editors.BaseEditor = BaseEditor;
        var IsCalled = (function (_super) {
            __extends(IsCalled, _super);
            function IsCalled(model) {
                _super.call(this, model);
                if (!model) {
                    log("Expected a value for @model.");
                    return;
                }
                this.templateName = "called-editor";
                this.typeName = "IsCalled";
                this.model = {};
                this.model["!"] = Traits.IsCalled["!"];
                this.model["&"] = null;
                this.model.__source__ = null;
                this.model.name = ko.observable();
                this.model.language = ko.observable();
                this.model.properNoun = ko.observable(true);
                this.resources = {};
                this.resources.languages = ko.observableArray(Languages);
                if (model) {
                    this.bind(model);
                }
            }
            IsCalled.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                this.updateHistory();
                this.model.__source__ = model.__source__;
                this.model["&"] = model["&"];
                this.model.name(model.name);
                this.model.language(model.lang);
                this.model.properNoun(model.properNoun);
            };
            IsCalled.prototype.unbind = function () {
                var model = {
                    __source__: this.model.__source__,
                    "!": this.model["!"],
                    "&": this.model["&"],
                    name: this.model.name(),
                    lang: this.model.language(),
                    properNoun: this.model.properNoun()
                };
                return model;
            };
            return IsCalled;
        })(Editors.BaseEditor);
        Editors.IsCalled = IsCalled;
        var Born = (function (_super) {
            __extends(Born, _super);
            function Born(model) {
                var _this = this;
                _super.call(this, model);
                if (!model) {
                    log("Expected a value for @model.");
                    return;
                }
                this.templateName = "born-editor";
                this.typeName = "Born";
                this.model = {};
                this.model.nodeId = ko.observable();
                this.model.dateRange = new DateRangeSelector();
                this.model.target = ko.observable();
                this.model.toFather = new AgentSelector({ sex: "male" });
                this.model.toMother = new AgentSelector({ sex: "female" });
                this.model.atPlace = new PlaceSelector();
                if (model) {
                    this.bind(model);
                }
                this.cardTitle = ko.computed(function () {
                    var name = !_this.model || !_this.model.__source__ ? "<ANONYMOUS>" : wrap(_this.model.__source__).getName();
                    return "{name} {action}".fmt({ name: name, action: _this.typeName.toLowerCase() });
                });
            }
            Born.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                this.updateHistory();
                if (typeof model.target != "undefined") this.model.target(model.target);
                if (typeof model.source != "undefined") this.model.target(model.source);
                if (typeof model.nodeId != "undefined") this.model.nodeId(model.nodeId);
                if (typeof model.dateRange != "undefined") this.model.dateRange.bind({ onDate: model.onDate, fromDate: model.fromDate, toDate: model.toDate });
                if (typeof model.toFather != "undefined") this.model.toFather.bind(model.toFather);
                if (typeof model.toMother != "undefined") this.model.toMother.bind(model.toMother);
                if (typeof model.atPlace != "undefined") this.model.atPlace.bind(model.atPlace);
            };
            Born.prototype.unbind = function () {
                /*
                NOTE: Be sure to also unbind the contents of the observables, so that the nodeId properties are values and
                not themselves observables.
                */
                var model = {
                    target: this.model.target(),
                    nodeId: this.model.nodeId(),
                    toFather: this.model.toFather.unbind(),
                    toMother: this.model.toMother.unbind(),
                    atPlace: this.model.atPlace.unbind()
                };
                return merge(model, this.model.dateRange.unbind());
            };
            Born.prototype.saveClicked = function () {
                this.save();
            };
            Born.prototype.save = function () {
                var model = this.unbind();
                App.API.createBirthFact({
                    fact: model
                });
            };
            return Born;
        })(Editors.BaseEditor);
        Editors.Born = Born;
        var BaseProperty = (function () {
            function BaseProperty(T, model) {
                var _this = this;
                this.T = T;
                this.templateName = "property-editor";
                this.typeName = this.T["!"];
                this.pubsub = new PubSub();
                this.model = {};
                this.model["!"] = this.T["!"];
                this.model["&"] = null;
                this.model.__source__ = null;
                if (model) {
                    this.bind(model);
                }
                this.cardTitle = ko.computed(function () {
                    var name = !_this.model || !_this.model.__source__ ? "<ANONYMOUS>" : wrap(_this.model.__source__).getName();
                    return "{name} is a {something}".fmt({ name: name, something: _this.typeName.toLowerCase() });
                });
            }
            BaseProperty.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                this.model["&"] = model["&"];
                this.model.__source__ = model.__source__;
            }
            BaseProperty.prototype.unbind = function () {
                return {
                    "!": this.model["!"],
                    "&": this.model["&"],
                    __source__: this.model.__source__
                };
            };
            BaseProperty.prototype.save = function () {
                this.pubsub.publish("OnSave", this);
            };
            BaseProperty.prototype.close = function () {
                this.pubsub.publish("OnClose", this);
            };
            BaseProperty.prototype.remove = function () {
                this.pubsub.publish("OnRemove", this);
            };
            return BaseProperty;
        })();
        Editors.BaseProperty = BaseProperty;
        var Painter = (function (_super) {
            __extends(Painter, _super);
            function Painter(model) {
                _super.apply(this, [Traits.Painter, model]);
            }
            return Painter;
        })(BaseProperty);
        Editors.Painter = Painter;
        var Architect = (function (_super) {
            __extends(Architect, _super);
            function Architect(model) {
                _super.apply(this, [Traits.Architect, model]);
            }
            return Architect;
        })(BaseProperty);
        Editors.Architect = Architect;
        var Writer = (function (_super) {
            __extends(Writer, _super);
            function Writer(model) {
                _super.apply(this, [Traits.Writer, model]);
            }
            return Writer;
        })(BaseProperty);
        Editors.Writer = Writer;
        var Poet = (function (_super) {
            __extends(Poet, _super);
            function Poet(model) {
                _super.apply(this, [Traits.Poet, model]);
            }
            return Poet;
        })(BaseProperty);
        Editors.Poet = Poet;
        var Novelist = (function (_super) {
            __extends(Novelist, _super);
            function Novelist(model) {
                _super.apply(this, [Traits.Novelist, model]);
            }
            return Novelist;
        })(BaseProperty);
        Editors.Novelist = Novelist;
        var Essayist = (function (_super) {
            __extends(Essayist, _super);
            function Essayist(model) {
                _super.apply(this, [Traits.Essayist, model]);
            }
            return Essayist;
        })(BaseProperty);
        Editors.Essayist = Essayist;
        var Inventor = (function (_super) {
            __extends(Inventor, _super);
            function Inventor(model) {
                _super.apply(this, [Traits.Inventor, model]);
            }
            return Inventor;
        })(BaseProperty);
        Editors.Inventor = Inventor;
        var Goldsmith = (function (_super) {
            __extends(Goldsmith, _super);
            function Goldsmith(model) {
                _super.apply(this, [Traits.Goldsmith, model]);
            }
            return Goldsmith;
        })(BaseProperty);
        Editors.Goldsmith = Goldsmith;
        var Sculptor = (function (_super) {
            __extends(Sculptor, _super);
            function Sculptor(model) {
                _super.apply(this, [Traits.Sculptor, model]);
            }
            return Sculptor;
        })(BaseProperty);
        Editors.Sculptor = Sculptor;
        var Action = (function (_super) {
            __extends(Action, _super);
            function Action(T, component/*: Action */) {
                var _this = this;
                component = component || {};
                _super.call(this, component.model);
                if (!component.model) {
                    log("Expected a value for @model.");
                    return;
                }
                this.T = T;
                this.templateName = this.templateName || "action-editor";
                this.typeName = this.T["!"];
                this.model = this.model || {};
                this.model.type = ko.observable("Fact");
                this.model.source = ko.observable();
                this.model.target = ko.observable();
                this.model.type = ko.observable();
                this.model.nodeId = ko.observable();
                this.model.dateRange = new DateRangeSelector();
                this.model.atPlace = new PlaceSelector();
                if (component) {
                    this.bindComponent(component);
                }
                this.cardTitle = ko.computed(function () {
                    var name = !this.model || !this.model.source() ? "<ANONYMOUS>" : this.model.source().name;
                    return "{name} {action}".fmt({ name: name, action: this.typeName.toLowerCase() });
                }, this);
            }
            Action.prototype.bindComponent = function (component) {
                if (component.model) {
                    this.bind(component.model);
                }
            };
            Action.prototype.bind = function (model/*: Action */) {
                if (!model) {
                    return;
                }
                this.updateHistory();
                if (typeof model.source != "undefined") this.model.source(model.source);
                if (typeof model.target != "undefined") this.model.target(model.target);
                if (typeof model.nodeId != "undefined") this.model.nodeId(model.nodeId);
                if (typeof model.dateRange != "undefined") this.model.dateRange.bind({ onDate: model.onDate, fromDate: model.fromDate, toDate: model.toDate });
                if (typeof model.atPlace != "undefined") this.model.atPlace.bind(model.atPlace);
            };
            Action.prototype.unbind = function () /*: IDied */ {
                var model = {
                    type: this.model.type(),
                    nodeId: this.model.nodeId(),
                    source: this.model.source(),
                    target: this.model.target(),
                    atPlace: this.model.atPlace.unbind()
                };
                return merge(model, this.model.dateRange.unbind());
            };
            return Action;
        })(Editors.BaseEditor);
        Editors.Action = Action;
        var Died = (function (_super) {
            __extends(Died, _super);
            function Died(component) {
                _super.apply(this, [Traits.Died, component]);
            }
            return Died;
        })(Editors.Action);
        Editors.Died = Died;
        var Buried = (function (_super) {
            __extends(Buried, _super);
            function Buried(component) {
                _super.apply(this, [Traits.Buried, component]);
            }
            return Buried;
        })(Editors.Action);
        Editors.Buried = Buried;
        var Visited = (function (_super) {
            __extends(Visited, _super);
            function Visited(component) {
                _super.apply(this, [Traits.Visited, component]);
            }
            return Visited;
        })(Editors.Action);
        Editors.Visited = Visited;
        var LivedAt = (function (_super) {
            __extends(LivedAt, _super);
            function LivedAt(component) {
                _super.apply(this, [Traits.LivedAt, component]);
            }
            return LivedAt;
        })(Editors.Action);
        Editors.LivedAt = LivedAt;
        var WorkedAt = (function (_super) {
            __extends(WorkedAt, _super);
            function WorkedAt(component) {
                this.templateName = "worked-at-editor";
                this.model = this.model || {};
                this.model["for"] = new AgentSelector();
                _super.apply(this, [Traits.WorkedAt, component]);
                if (component) {
                    this.bindComponent(component);
                }
            }
            WorkedAt.prototype.bindComponent = function (component) {
                if (component.model) {
                    this.bind(component.model);
                }
            };
            WorkedAt.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                _super.prototype.bind.call(this, model);
                this.model["for"].bind(model["for"]);
            };
            WorkedAt.prototype.unbind = function () {
                var baseModel = _super.prototype.unbind.call(this);
                var merged = merge(baseModel, {
                    "for": this.model["for"].unbind()
                });
                return merged;
            };
            return WorkedAt;
        })(Editors.Action);
        Editors.WorkedAt = WorkedAt;
        var Taught = (function (_super) {
            __extends(Taught, _super);
            function Taught(component) {
                this.templateName = "taught-editor";
                this.model = this.model || {};
                this.model.type = "Fact";
                this.model.skill = ko.observable();
                this.model.to = new AgentSelector();
                this.resources = this.resources || {};
                this.resources.skills = ko.observableArray(App.Resources.Skills);
                _super.apply(this, [Traits.Taught, component]);
                if (component) {
                    this.bindComponent(component);
                }
            }
            Taught.prototype.bindComponent = function (component) {
                if (component.model) {
                    this.bind(component.model);
                }
            };
            Taught.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                _super.prototype.bind.call(this, model);
                this.model.to.bind(model.target);
                this.model.target(model.target);
                this.model.skill(model.skill);
            };
            Taught.prototype.unbind = function () {
                var baseModel = _super.prototype.unbind.call(this);
                var merged = merge(baseModel, {
                    target: this.model.to.unbind(),
                    skill: this.model.skill()
                });
                return merged;
            };
            return Taught;
        })(Editors.Action);
        Editors.Taught = Taught;
        var WasTaught = (function (_super) {
            __extends(WasTaught, _super);
            function WasTaught(model) {
                this.templateName = "was-taught-editor";
                this.title = "Was Taught";
                this.model = this.model || {};
                this.model.skill = ko.observable();
                this.model.by = new AgentSelector();
                if (model) {
                    // As this is the reverse vector of 'Taught' we need to switch __source__ and __target__
                    // as the EntityEditor always passes in a __source__ value.
                    var source = model.__source__;
                    var target = model.__target__;
                    model.__source__ = target;
                    model.__target__ = source;
                }
                this.resources = this.resources || {};
                this.resources.skills = ko.observableArray(App.Resources.Skills);
                _super.apply(this, [Traits.Taught, model]);
            }
            WasTaught.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                _super.prototype.bind.call(this, model);
                this.model.by.bind(model.__target__);
                this.model.skill(model.skill);
            };
            WasTaught.prototype.unbind = function () {
                var baseModel = _super.prototype.unbind.call(this);
                var merged = merge(baseModel, {
                    __source__: this.model.by.unbind(),
                    __target__: this.model.__target__,
                    skill: this.model.skill()
                });
                return merged;
            };
            return WasTaught;
        })(Editors.Action);
        Editors.WasTaught = WasTaught;
        App.Controllers = App.Controllers || {};
        var AgentVocabulary = [
            "IsCalled", "Born", "Died", "Visited", "LivedAt", "WorkedAt", "Buried",
            "Painter", "Sculptor", "Architect", "Novelist", "Inventor", "Goldsmith", "Poet", "Essayist",
            "Taught", "WasTaught"  // , "Studied"
        ];
        var ArtworkVocabulary = [
            "IsCalled", "WasCreated", "WasExhibited", "Painting", "Sculpture", "Architecture",
            "HasTheme", "HasSubject", "Represents", "Novel", "Essay", "Poem"
        ];
        var BaseEditor = (function () {
            function BaseEditor(vocabulary, model) {
                var _this = this;
                this.editors = ko.observableArray([]);
                this.entity = null;
                this.stack = [];
                this.stubs = ko.observableArray([]);
                this.vocabulary = ko.observableArray(vocabulary);
                this.verbToAdd = ko.observable();
                this.activeVocabulary = ko.observableArray();
                this.verbToEdit = ko.observable();
                this.activeEditor = ko.observable();
                this.activeEditorTemplateName = ko.observable();
                this.activeEditorTemplateLoading = ko.observable();
                this.output = ko.observableArray([]);
                if (model) {
                    this.bind(model);
                } else {
                    this.entity = new Entity();
                }
                this.setupEventHandlers();
            }
            BaseEditor.prototype.setupEventHandlers = function () {
                this.verbToEdit.subscribe(function (fact) {
                    var item = first(where(this.entity.facts, function (F) { return F.nodeId == fact.nodeId }));
                    var editor = new Editors[fact.type](item);
                    this.activeEditorTemplateLoading(true);
                    this.activeEditor(editor);
                    this.activeEditorTemplateName(editor.templateName);
                    this.activeEditorTemplateLoading(false);
                }, this);
            };
            BaseEditor.prototype.addNewVerb = function () {
                this.setActiveEditor(this.verbToAdd());
            };
            BaseEditor.prototype.removeFact = function (fact) {
                this.entity.facts = where(this.entity.facts, function (F) { return F.nodeId == fact.nodeId });
                this.updateActiveVocabulary();
            };
            BaseEditor.prototype.clearActiveEditor = function () {
                this.activeEditorTemplateLoading(true);
                this.activeEditorTemplateName("");
                this.activeEditor(null);
                this.activeEditorTemplateLoading(false);
            };
            BaseEditor.prototype.saveFact = function (fact) {
                if (!fact.source) fact.source = this.entity;
                var existing = first(where(this.entity.facts, function (F) { return F.nodeId == fact.nodeId; }));
                if (!existing) {
                    this.entity.facts.push(fact);
                    // window.Facts.push(fact);
                } else {
                    copyTo(existing, fact);
                }
                this.updateActiveVocabulary();
                this.clearActiveEditor();
                this.print();
            };
            BaseEditor.prototype.updateActiveVocabulary = function () {
                this.activeVocabulary(
                    select(this.entity.facts, function (fact) {
                        var type = getTraitTypeName(fact);
                        return { text: type, value: { id: fact.nodeId, type: type } };
                    })
                );
            };
            BaseEditor.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                this.entity = model;
                this.updateActiveVocabulary();
            };
            BaseEditor.prototype.unbind = function () {
                return {
                    type: this.entity.type,
                    nodeId: this.entity.nodeId,
                    facts: this.entity.facts
                };
            };
            BaseEditor.prototype.getEditorForFact = function (fact) {
                var factTypeName = getTraitTypeName(fact);
                var editor = new Editors[factTypeName](fact);
                this.attachEventHandlersTo(editor);
                return editor;
            };
            BaseEditor.prototype.getEditor = function (editorName) {
                var editor = new Editors[editorName]({ source: this.entity });
                this.attachEventHandlersTo(editor);
                return editor;
            };
            BaseEditor.prototype.attachEventHandlersTo = function (editor) {
                editor.pubsub.subscribe("OnSave", function (editor) {
                    var fact = editor.toFact();
                    this.saveFact(fact);
                }, this);
                editor.pubsub.subscribe("OnRemove", function (editor) {
                    var fact = editor.toFact();
                    this.removeFact(fact);
                }, this);
                editor.pubsub.subscribe("OnClose", function (editor) {
                    if (this.activeEditor() == editor) {
                        this.clearActiveEditor();
                    };
                }, this);
            };
            BaseEditor.prototype.print = function () {
                // var html = render(this.entity);
                // this.output(html);
            };
            BaseEditor.prototype.setActiveEditor = function (editorName) {
                this.activeEditorTemplateLoading(true);
                var editor = this.getEditor(editorName);
                this.activeEditor(editor);
                this.activeEditorTemplateName(editor.templateName || editor.typeName.toLowerCase() + "-editor");
                this.activeEditorTemplateLoading(false);
            };
            BaseEditor.prototype.getStubs = function () {
                return this.stubs;
            };
            return BaseEditor;
        })();
        App.Controllers.BaseEditor = BaseEditor;
        var AgentEditor = (function (_super) {
            __extends(AgentEditor, _super);
            function AgentEditor(model) {
                _super.apply(this, [AgentVocabulary, model]);
                var _this = this;
                this.entity = {
                    nodeId: ko.observable(),
                    name: ko.observable()
                };
                App.API.createAgent({ name: "test" }, function (response) {
                    _this.entity.nodeId(response);
                    _this.entity.name("test");
                });
            }
            return AgentEditor;
        })(BaseEditor);
        App.Controllers.AgentEditor = AgentEditor;
        var ArtworkEditor = (function (_super) {
            __extends(ArtworkEditor, _super);
            function ArtworkEditor(model) {
                _super.apply(this, [ArtworkVocabulary, model]);
            }
            return ArtworkEditor;
        })(BaseEditor);
        App.Controllers.ArtworkEditor = ArtworkEditor;
        var PlaceEditor = (function (_super) {
            __extends(PlaceEditor, _super);
            function PlaceEditor(model) {
                _super.apply(this, [PlaceVocabulary, model]);
            }
            return PlaceEditor;
        })(BaseEditor);
        App.Controllers.PlaceEditor = PlaceEditor;
        var MasterEditor = (function () {
            function MasterEditor() {
                this.currentEntityReady = ko.observable(false);
                this.currentEntityEditor = ko.observable();
                this.workspace = ko.observableArray([]);
                this.setupEventHandlers();
            }
            MasterEditor.prototype.createAgentClicked = function () {
                this.create(AgentEditor);
            };
            MasterEditor.prototype.createArtworkClicked = function () {
                this.create(ArtworkEditor);
            };
            MasterEditor.prototype.createPlaceClicked = function () {
                this.create(PlaceEditor);
            };
            MasterEditor.prototype.create = function (T) /* where T : IEditor */ {
                this.currentEntityReady(false);
                var editor = new T();
                this.currentEntityEditor(editor);
                this.workspace.push(editor.entity);
                this.currentEntityReady(true);
            };
            MasterEditor.prototype.setCurrentEntity = function (entity) {
                this.currentEntityReady(false);
                var editor = this.getEditorFor(entity);
                this.currentEntityEditor(editor);
                this.currentEntityReady(true);
            };
            /**
            Factory method for getting the matching editor for an entity ...
            The main categories are: Agent, Artwork, Place
            */
            MasterEditor.prototype.getEditorFor = function (entity) {
                var wrapped = wrap(entity);
                return new AgentEditor(entity); // temp
            };
            MasterEditor.prototype.setupEventHandlers = function () {
                receive("OnEntityStubCreated", function (entity) {
                    this.workspace.push(entity);
                }, this);
            };
            MasterEditor.prototype.workspaceEntitySelected = function (entity) {
                this.setCurrentEntity(entity);
            };
            return MasterEditor;
        })();
        App.Controllers.MasterEditor = MasterEditor;
    }(App.Editors || (App.Editors = {})));
}(App));