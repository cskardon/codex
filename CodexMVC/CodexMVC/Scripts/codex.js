﻿function refreshPage() {
    $.get(window.location.href, null, function (html) { $("html").html($(html).html()); }, "html");
}

function toggleTimeline(element) {
    var $el = $(element);
    var $filteringBy = $("#filtering-by");
    var filtering = $el.data("filtering") == "true";
    var entityClass = $el.data("entity");
    var acts = $(".act");
    var containers = acts.parents('.shadow-z-1');
    // clear old state
    $("a[data-entity]").css('background-color', "#fff");
    $("a[data-entity]").data("filtering", "false");
    $filteringBy.text("");
    // and then draw the new one
    if (false == filtering) {
        acts.hide();
        containers.hide();
        var actsToShow = acts.filter("." + entityClass);
        var containersToShow = actsToShow.parents('.shadow-z-1');
        containersToShow.show();
        actsToShow.show();
        $el.css('background-color', "#ccc");
        $filteringBy.text($el.data("name"));
    } else {
        acts.show();
        containers.show();
        $el.css('background-color', "#fff");
    }
    $el.data("filtering", (!filtering).toString());
}

function addBookmark(element) {
    var bookmark = $(element);
    var guid = bookmark.data("guid");
    var type = bookmark.data("type");
    var bookmarked = bookmark.data("bookmarked");
    var icon = bookmark.find("span");
    if (!bookmarked) {
        bookmark.data("bookmarked", true);
        $.get("/API/AddBookmark", {
            guid: guid,
            type: type
        }, function (response) {
            icon.css("color", "red");
        });
    } else {
        bookmark.data("bookmarked", false);
        $.get("/API/RemoveBookmark", {
            guid: guid,
            type: type
        }, function (response) {
            icon.css("color", "#009688");
        });
    }
    return false;
}

function addSeenIt(element) {
    var bookmark = $(element);
    var guid = bookmark.data("guid");
    var type = bookmark.data("type");
    var bookmarked = bookmark.data("seen");
    var icon = bookmark.find("span");
    if (!bookmarked) {
        bookmark.data("seen", true);
        $.get("/API/AddSeenIt", {
            artefactGuid: guid
        }, function (response) {
            icon.css("color", "red");
        });
    } else {
        bookmark.data("seen", false);
        $.get("/API/RemoveSeenIt", {
            artefactGuid: guid
        }, function (response) {
            icon.css("color", "#009688");
        });
    }
    return false;
}