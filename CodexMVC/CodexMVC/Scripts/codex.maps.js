﻿var codex = window.codex || {};
(function (codex) {

	(function (maps, google, app, MarkerClusterer) {

		/**
		type aliases
		*/
		var InfoWindow = google.maps.InfoWindow;
		var Marker = google.maps.Marker;
		var LatLng = google.maps.LatLng;
		var LatLngBounds = google.maps.LatLngBounds;
		var HeatmapLayer = google.maps.visualization.HeatmapLayer;
		var GoogleMapsEvent = google.maps.event;

		/**
		function aliases
		*/
		var select = app.Utils.select;
		var find = app.Utils.find;
		var each = app.Utils.each;

		var Dataset = {
			Current: null,
			Map: null,
			Timeline: {

			},
			Artworks: {

			}
		};

		function buildMarkerList(list, lat, lng, title, content) {
			var item = find(list, function (item) {
				return item.lat == lat && item.lng == lng;
			});
			if (item) {
				item.infoWindows.push({ title: title, content: content });
			} else {
				var temp = {
					lat: lat, lng: lng, infoWindows: [{ title: title, content: content }]
				};
				list.push(temp);
			}
		}

		function generateMarkersAndHeatMap(list, map, heatmapData) {
		    var markers = null;
		    var cluster = null;
            if (list) {
			    var markerList = select(list, function (item, __, index) {
				    var content = select(item.infoWindows, function (iw) {
					    return "<p>" + iw.content + "</p>";
				    });
				    var infowindow = new InfoWindow({
					    content: $("<div id=\"content\" style='min-height: 150px; width: 600px;'><div id='bodyContent' style='line-height: 14px;'><p><b>{title}</b></p><div style='-webkit-column-count: 1;'>{content}</div></div></div>".fmt({
						    content: content.join(""), title: item.infoWindows[0].title || "test"
					    }))[0]
				    });
				    var marker = new Marker({
					    position: new LatLng(item.lat, item.lng),
					    title: ""
				    });

				    GoogleMapsEvent.addListener(marker, 'click', function () {
					    infowindow.open(map, marker);
				    });

				    return {
					    marker: marker,
					    infoWindow: infowindow
				    }
			    });

			    markers = select(markerList, function (item) { return item.marker });
			    cluster = new MarkerClusterer(map, markers);

			    //var bounds = new LatLngBounds();
			    //each(markers, function (marker) {
			    //	bounds.extend(marker.getPosition());
			    //});
                    //map.fitBounds(bounds);

            }

			var heatmapTransformed = select(heatmapData, function (item) {
				return {
					location: new LatLng(item.Latitude, item.Longitude),
					weight: item.Weight
				};
			});
			var heatmap = new HeatmapLayer({
				data: heatmapTransformed
			});

			heatmap.set('radius', 30);

			return {
				Markers: markers,
				Heatmap: heatmap,
				Cluster: cluster
			};
		}

		function toggle() {
			var set = Dataset[Dataset.Current];
			if (Dataset.State == "Markers") {
				hideMarkers(set);
				showHeatMap(set);
				Dataset.State = "Heatmap";
			} else if (Dataset.State == "Heatmap") {
				hideHeatMap(set);
				showMarkers(set);
				Dataset.State = "Markers";
			}
		}

		function hideMarkers(set) {
			each(set.Markers, function (m) {
				m.setMap(null);
			});
			if (set.Cluster) set.Cluster.clearMarkers();
		}

		function fitBounds(set) {
			var bounds = new LatLngBounds();
			each(set.Markers, function (marker) {
				bounds.extend(marker.getPosition());
			});
			Dataset.Map.fitBounds(bounds);
		}

		function showMarkers(set) {
			each(set.Markers, function (m) {
				m.setMap(Dataset.Map);
			});
			if (set.Cluster) set.Cluster.addMarkers(set.Markers);
		}

		function hideHeatMap(set) {
			if (set.Heatmap) set.Heatmap.setMap(null);
		}

		function showHeatMap(set) {
			if (set.Heatmap) set.Heatmap.setMap(Dataset.Map);
		}

		function hideTimelineMap() {
			Dataset.Current = "Artworks";
			Dataset.State = "Markers";
			hideMarkers(Dataset.Timeline);
			hideHeatMap(Dataset.Timeline);

			showMarkers(Dataset.Artworks);
			hideHeatMap(Dataset.Artworks);
			fitBounds(Dataset.Artworks);
		}

		function hideArtworksMap() {
			Dataset.Current = "Timeline";
			Dataset.State = "Markers";
			hideMarkers(Dataset.Artworks);
			hideHeatMap(Dataset.Artworks);

			showMarkers(Dataset.Timeline);
			hideHeatMap(Dataset.Timeline);
			fitBounds(Dataset.Timeline);
		}

		function toggleMapExpansion(link) {
			var expando = $("#map-expando");
			var $map = $("#map");
			var height = parseInt($map.css("height"));
			var isContracted = height == 300;
			if (isContracted) {
				expando.removeClass("glyphicon-resize-full");
				expando.addClass("glyphicon-resize-small");
			}
			else {
				expando.removeClass("glyphicon-resize-small");
				expando.addClass("glyphicon-resize-full");
			}
			$map.css("height", isContracted ? "600px" : "300px");
			GoogleMapsEvent.trigger(Dataset.Map, "resize");
		}

		function initialiseMap(markerData, heatmapData, setName) {

			var markers = [];

			if (markerData) {
			    each(markerData, function (item) {
			        buildMarkerList(markers, item.Latitude, item.Longitude, item.ContainerName, item.Content);
			    });
			}
			
			var result = generateMarkersAndHeatMap(markers, Dataset.Map, heatmapData);
			Dataset[setName] = result;
			
		}

		/**
		export
		*/
		maps.Dataset = Dataset;
		maps.initialiseMap = initialiseMap;
		maps.toggle = toggle;
		maps.toggleMapExpansion = toggleMapExpansion;
		maps.generateMarkersAndHeatMap = generateMarkersAndHeatMap;
		maps.buildMarkerList = buildMarkerList;
		maps.hideTimelineMap = hideTimelineMap;
		maps.hideArtworksMap = hideArtworksMap;
		maps.hideMarkers = hideMarkers;
		maps.showMarkers = showMarkers;
		maps.hideHeatMap = hideHeatMap;
		maps.showHeatMap = showHeatMap;
		maps.fitBounds = fitBounds;

	})(codex.maps || (codex.maps = {}), google, app, MarkerClusterer);

})(codex);