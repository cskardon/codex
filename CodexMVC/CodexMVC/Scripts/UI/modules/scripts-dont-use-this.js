(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';

        ProjectFunctions.DocumentReady();
        ProjectFunctions.WindowLoad();
        ProjectFunctions.AfterResize();

	});
	
})(jQuery, this);

var ProjectFunctions = (function() {
    return {

        DocumentReady: function() {

            var mainNav = $(".main-nav").first(),
                search = $(".search").first();

            // Initialise mobile navigation
            MobileNavigation.Init({
                elements: [mainNav, search],
                navClass: "mobile-navigation",
                onComplete: function() {
                    MobileNavSetup.Init(this.navClass);
                    console.log("complete");
                }
            });

            // Fast click for mobile
            FastClick.attach(document.body);

            // Replace SVGs with PNGs
            Utils.ReplaceSvgWithPng($(".main-logo__img"));
        },

        WindowLoad: function() {
            $(window).load(function() {
                ProjectFunctions.StickyFooter($(".wrapper").first(), $(".main-footer").first());
            });
        },

        AfterResize: function() {
            $(window).afterResize(function() {

                ProjectFunctions.StickyFooter($(".wrapper").first(), $(".main-footer").first());

            }, true, 200); // bool == runs on load or not, int (miliseconds) == how soon it runs after the resize event has finished
        },

        OrientationChange: function() {
            if (Modernizr.touch) {
                window.addEventListener("orientationchange", function () {
                    setTimeout( function() {

                        // Do stuff after orientation change
                            
                    }, 200);

                });
            }
        },

        // Functions

        StickyFooter: function(wrapper, footer) {
            var wrapper = wrapper,
                footer = footer;
            wrapper.css({"padding-bottom": footer.outerHeight() + "px"});
        }

    }
})();

var MobileNavSetup = (function() {
    return {
        Init: function(navClass) {
            var body = $("body").first(),
                mainLogoWrapper = $(".wrapper--main-logo").first(),
                mobileMenuButton = $("<button class=\"mobile-navigation-button\">Show menu</button>"),
                mobileNavigation = $("." + navClass);

            mainLogoWrapper.prepend(mobileMenuButton);

            mobileMenuButton.click(function(e) {
                mobileNavigation.toggleClass("is-active");
                body.toggleClass("nav-active");
                e.preventDefault();
            });

            $(document).click(function(e) {
                var target = $(e.target),
                    inMobileNav = target.closest("." + navClass).length,
                    notTarget = !target.is(".mobile-navigation-button");

                if (!inMobileNav && notTarget) {
                    if (mobileNavigation.hasClass("is-active")) {
                        mobileNavigation.removeClass("is-active");
                        body.removeClass("nav-active");
                    }
                }
            });

            MediaQueries.WasIs({
                wasMaxWidth: "767px",
                isMinWidth: "768px",
                ifTrueDo: function() {
                    mobileNavigation.removeClass("is-active").addClass("is-inactive");
                    console.log("test");
                    body.removeClass("nav-active");
                }
            });

        }
    }
})();