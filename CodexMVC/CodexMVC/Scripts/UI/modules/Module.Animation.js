// Animation and sub-optimal canvas code by Laurent Baumann
// lbaumann.com

// http://mattpatenaude.com/

var requestFrame = null;

window.onload = function() {
    if (window.requestAnimationFrame) {
        requestFrame = window.requestAnimationFrame;
    } else if (window.webkitRequestAnimationFrame) {
        requestFrame = window.webkitRequestAnimationFrame;
    } else if (window.mozRequestAnimationFrame) {
        requestFrame = window.mozRequestAnimationFrame;
    }
    
	windowWidth = window.innerWidth;
	windowHeight = window.innerHeight;

	canvas = $("background");
	canvas.setAttribute('width', windowWidth);
	canvas.setAttribute('height', windowHeight);

	canvasHeight = windowHeight;
	canvasWidth = windowWidth;

	context = canvas.getContext("2d");

	numberOfRows = numberOfColumns = 6;
	numberOfPoints = numberOfRows * numberOfColumns;
	pointNumber = 0;
	hue = 202;
	// hue = 0;
	
	verticalMargin = canvasHeight / (numberOfRows - 1);
	horizontalMargin = canvasWidth / (numberOfColumns - 1);
	Grid = [];

	focalLength = 700;
	maximumWriggle = 15;

	for(var i = 0; i < numberOfRows; i++) {
		Row = [];
		
		for(var k = 0; k < numberOfColumns; k++) {
			var point = {};
			pointNumber += 1.9;
            
			point.amplitude = randomRange(1, 1.7);
			point.timer = pointNumber / numberOfPoints;
			point.angle = pointNumber;

			point.offsetX = randomRange(-20, 20);
			point.offsetY = randomRange(-20, 20);

			point.x = (horizontalMargin * i - canvasWidth / 2 + point.offsetX) * 1.1;
			point.y = (verticalMargin * k - canvasHeight / 2 + point.offsetY) * 1.1;
			point.z = 0;

			point.goalZ = - maximumWriggle;
			point.startZ = 0;

			point.differenceZ = point.startZ - point.goalZ;

			Row.push(point);
		}
		
		Grid.push(Row);
	}

    requestFrame(updatePoints);
}

var lastFrameTimestamp = null;

function updatePoints(timestamp) {
	if (lastFrameTimestamp === null) lastFrameTimestamp = timestamp;
	for(var i = 0; i < numberOfRows; i++) {
		for(var k = 0; k < numberOfColumns; k++) {
			Grid[i][k].angle -= 0.03;
			Grid[i][k].z = (Math.cos(Grid[i][k].angle) * maximumWriggle - maximumWriggle / 2) * Grid[i][k].amplitude;
		}
	}
    
	redraw();
	
    lastFrameTimestamp = timestamp;
    requestFrame(updatePoints);
}

function easeInOutCubic(t) {
	return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1;
}

function randomRange(min, max) {
	return ((Math.random()*(max-min)) + min); 
}

function calculate3D() {
	for(var i = 0; i < numberOfRows; i++) {
		for(var k = 0; k < numberOfColumns; k++) {
			scale = focalLength / (Grid[i][k].z + focalLength);
			Grid[i][k].dX = Grid[i][k].x * scale + canvasWidth / 2;
			Grid[i][k].dY = Grid[i][k].y * scale + canvasHeight / 2;
		}
	}
}

function redraw() {
	context.save();
	context.clearRect(0, 0, canvasWidth, canvasHeight);
	
	calculate3D();

	for(var i = 0; i < numberOfRows - 1; i++) {
		for(var k = 0; k < numberOfColumns - 1; k++) {
            // Fill I
            ///////////////////////////////////////////////////////////

			averageZTop = (Grid[i][k].z + Grid[i + 1][k].z) / 2;
			averageZBottom = Grid[i + 1][k + 1].z;
			slant = (averageZTop - averageZBottom) / 2 + 20;

			context.beginPath();

			if(window.mobilecheck()) {
				context.fillStyle = "hsl(" + (hue + slant) + ", 5%, " + slant + "%)";
				
			} else {
				gradientStartX = Grid[i][k].dX + (Grid[i + 1][k + 1].dX - Grid[i][k].dX) / 2; 
				gradientStartY = Grid[i][k].dY + (Grid[i + 1][k + 1].dY - Grid[i][k].dY) / 2;
				gradientStartZ = Grid[i][k].z + (Grid[i + 1][k + 1].z - Grid[i][k].z) / 2;
				gradientEndX = Grid[i + 1][k].dX;
				gradientEndY = Grid[i + 1][k].dY;
				gradientEndZ = Grid[i + 1][k].z;

				var gradient = context.createLinearGradient(gradientStartX, gradientStartY, gradientEndX, gradientEndY);
				gradient.addColorStop(0, "hsl(" + (hue + slant) + ", 5%, " + (slant - gradientStartZ / 2) + "%)");
				gradient.addColorStop(1, "hsl(" + (hue + slant) + ", 5%, " + (slant - gradientEndZ / 2) + "%)");

				context.fillStyle = gradient;
			}

			context.moveTo(Grid[i][k].dX, Grid[i][k].dY);
			context.lineTo(Grid[i + 1][k].dX, Grid[i + 1][k].dY);
			context.lineTo(Grid[i + 1][k + 1].dX, Grid[i + 1][k + 1].dY);
			context.fill();
            
            // Fill II
            ///////////////////////////////////////////////////////////
			averageZTop = Grid[i][k].z;
			averageZBottom = (Grid[i][k + 1].z + Grid[i + 1][k + 1].z) / 2;
			slant = (averageZTop - averageZBottom) / 2 + 20;

			context.beginPath();
			
			if(window.mobilecheck()) {
				context.fillStyle = "hsl(" + (hue + slant) + ", 5%, " + slant + "%)";
			} else {
				gradientStartX = Grid[i][k + 1].dX; 
				gradientStartY = Grid[i][k + 1].dY;
				gradientStartZ = Grid[i][k + 1].z;
				gradientEndX = Grid[i + 1][k + 1].dX - (Grid[i + 1][k + 1].dX - Grid[i][k].dX) / 2;
				gradientEndY = Grid[i + 1][k + 1].dY - (Grid[i + 1][k + 1].dY - Grid[i][k].dY) / 2;
				gradientEndZ = Grid[i + 1][k + 1].z - (Grid[i + 1][k + 1].z - Grid[i][k].z) / 2;
				
				var gradient = context.createLinearGradient(gradientStartX, gradientStartY, gradientEndX, gradientEndY);
				gradient.addColorStop(0, "hsl(" + (hue + slant) + ", 5%, " + (slant - gradientStartZ / 2) + "%)");
				gradient.addColorStop(1, "hsl(" + (hue + slant) + ", 5%, " + (slant - gradientEndZ / 2) + "%)");

				context.fillStyle = gradient;
			}
			
			context.fillStyle = gradient;
			context.moveTo(Grid[i][k].dX, Grid[i][k].dY);
			context.lineTo(Grid[i][k + 1].dX, Grid[i][k + 1].dY);
			context.lineTo(Grid[i + 1][k + 1].dX, Grid[i + 1][k + 1].dY);
			context.fill();
		}
	}
	
	context.restore();
}

window.addEventListener('resize', function() {
	scaleRatio = window.devicePixelRatio;
	windowWidth = window.innerWidth;
	windowHeight = window.innerHeight;
	canvas.setAttribute('width', windowWidth);
	canvas.setAttribute('height', windowHeight);

	canvasHeight = windowHeight;
	canvasWidth = windowWidth;

	verticalMargin = canvasHeight / (numberOfRows - 1);
	horizontalMargin = canvasWidth / (numberOfColumns - 1);

	for(var i = 0; i < numberOfRows; i++) {
		for(var k = 0; k < numberOfColumns; k++) {
			Grid[i][k].x = (horizontalMargin * i - canvasWidth / 2 + Grid[i][k].offsetX) * 1.1;
			Grid[i][k].y = (verticalMargin * k - canvasHeight / 2 + Grid[i][k].offsetY) * 1.1;
		}
	}
}, false);

function $(el) {
	return document.getElementById(el);
}