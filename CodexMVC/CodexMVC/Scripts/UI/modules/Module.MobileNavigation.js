//		Use like this:
//
//		MobileNavigation.Init({
//		    elements: ".main-nav",
//		    navClass: "mobile-navigation"
//		});
//
//		See default settings below. Only required argument is "elements".

var MobileNavigation = (function() {
	return {
		Init: function(options) {
			var settings = this.SetSettings(options);
			this.BuildMobileNavigation(settings);
		},

		SetSettings: function(options) {

			// Default settings
			var settings = {
				elements: null,		// Which elements we're going to use in our mobile nav - can be a string, array or jQuery object
				navType: "div",		// What our containing element should be (<div> by default)
				navClass: null,		// Optionally give our containing element a class
				prependTo: "body",	// Prepends to body by default, set to whatever
				appendTo: null,		// Choose to append to somewhere else instead
				onComplete: null	// Optionally do stuff when everything else is done (function)
			};

			// Change settings if set
			if (options !== null && typeof(options) === "object") {
				if (options.hasOwnProperty("elements"))
					settings.elements = options.elements;
				if (options.hasOwnProperty("navType"))
					settings.navType = optios.navType;
				if (options.hasOwnProperty("prependTo"))
					settings.prependTo = options.prependTo;
				if (options.hasOwnProperty("appendTo"))
					settings.appendTo = options.appendTo;
				if (options.hasOwnProperty("navClass"))
					settings.navClass = options.navClass;
				if (options.hasOwnProperty("onComplete")) {
					if (typeof(options.onComplete) == "function")
						settings.onComplete = options.onComplete;
				}
			}

			return settings;
		},

		BuildMobileNavigation: function(options) {
			var settings = options;

			// Only run if elements have been entered
			if (settings.elements !== null) {

				var elements = settings.elements,
					navClass = settings.navClass,
					navType = $("<" + settings.navType + (navClass !== null ? " class=\""+ navClass +"\"" : "") + ">"),
					elementsIsAnArray = Object.prototype.toString.call(elements) === "[object Array]",
					elementsIsAString = typeof(elements) === "string",
					elementsIsJqueryObject = elements instanceof jQuery,
					allElements;

				if (elementsIsAString || elementsIsAnArray || elementsIsJqueryObject) {
					// Elements parameter is a string
					if (elementsIsAString) {
						allElements = elements.split(",");

						// Clone each element and append to new mobile nav element
						for (var i = 0; i < allElements.length; i++) {
							var element = $(allElements[i]),
								clone = $(element).first().clone();
							clone.appendTo(navType);
						}
					}
					// Elements parameter is an array
					else if (elementsIsAnArray) {
						allElements = elements;

						// Clone each element and append to new mobile nav element
						for (var i = 0; i < allElements.length; i++) {
							var element = allElements[i],
								clone;

							// If element we're cloning is a string, clone its jQuery element
							if (typeof(element) === "string") {
								clone = $(element).first().clone();
							}

							// Otherwise we may have a jQuery element, so clone it
							else if (typeof(element) !== null && typeof(element) === "object") {
								if (element instanceof jQuery) {
									clone = element.clone();
								}
							}
							clone.appendTo(navType);
						}
					}
					// Elements parameter is a jQuery object
					else if (elementsIsJqueryObject) {
						allElements = elements;

						allElements.each(function() {
							var clone = $(this).clone();
							clone.appendTo(navType);
						});
					}
				}
				else {
					console.log("elements parameter must be a string, array or jQuery object.");
				}

				// Prepend (default) if appendTo isn't set
				if (settings.appendTo === null) {
					var prependTo = $(settings.prependTo);
					if (prependTo.length)
						navType.prependTo(prependTo);
					else
						console.log(settings.prependTo + " doesn't exist.");
				}

				// Otherwise appendTo
				else {
					var appendTo = $(settings.appendTo);
					if (appendTo.length)
						navType.appendTo(appendTo);
					else
						console.log(settings.appendTo + " doesn't exist.");
				}

				// Run onComplete function if set
				if (settings.onComplete !== null) {
					var onComplete = (function() {
						settings.onComplete();
					})();
					onComplete;
				}

			}

			else {
				console.log("Required \"elements\" parameter hasn't been set.");
			}
		}
	}
})();