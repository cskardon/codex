
(function ($, root, undefined) {
    
    $(function () {
        
        'use strict';

        Document.Init();

    });
    
})(jQuery, this);

var Document = (function() {
    return {

        Init: function() {
            this.Ready();
            this.Load();
            this.Resize();
            this.OrientationChange();
        },

        Ready: function() {
            ProjectInit;
            ProjectUtils;
            ProjectPlugins;

            var ProjectInit = (function() {

                // Mobile navigation
                MobileNavigation.Init({
                    elements: [$(".main-nav__list"), $(".secondary-nav__list"), $(".tertiary-nav__list")],
                    navClass: "mobile-navigation",
                    onComplete: function() {
                        MobileNav.Init(this.navClass);
                    }
                });

                // Desktop navigation
                DesktopNav.Init();

                // Project functions
                ProjectFunctions.Init();

                NavTabs.Init();

            })();

            var ProjectUtils = (function() {
                Utils.ReplaceSvgWithPng($(".main-logo__img"));
                Utils.MaintainAspectRatio("iframe");
                $("img[data-src]").unveil();
            })();

            var ProjectPlugins = (function() {
                // ScrollToFixed
                $(".main-header__wrapper").scrollToFixed();
            })();
        },

        Load: function() {
            $(window).load(function() {
                //ProjectFunctions.GallerySmall(".tab-pane.active .tab-pane.active");
            });
        },

        Resize: function() {
            $(window).smartresize(function() {
            }).resize();
        },

        OrientationChange: function() {
            if (Modernizr.touch) {
                window.addEventListener("orientationchange", function () {
                    setTimeout( function() {

                        // Do stuff after orientation change
                            
                    }, 200);

                });
            }
        }

    }
})();

var ProjectFunctions = (function() {
    return {

        Init: function() {
            this.Isotope();
            this.TimelineFilters();
            //this.AlphabetisedNav();
        },

        BodyInk: function() {
            var body = $("body").first();
            $(document).click(function(e) {
                DesktopNav.Ink(body, e);
            });
        },

        Isotope: function() {
            $(".portal-blocks").isotope({
                itemSelector: ".portal-block",
                layoutMode: "fitRows"
            });
        },

        GallerySmall: function(scope) {
            var gallerySmall = scope === undefined ? $(".gallery--small") : $(".gallery--small", scope);

            gallerySmall.each(function() {

                var galleryImage = $(this).find(".gallery__image"),
                    shortest = 0;

                galleryImage.each(function() {
                    var _this = $(this),
                        height = _this.height(),
                        src = _this.attr("src");

                    if (height < shortest) {
                        shortest = height;
                    }

                    // Was using this, seemed too slow
                    // function imageIsNotBroken(src){
                    //     $.ajax(src, {
                    //         success: function() {
                    //             return true;
                    //         },
                    //         error: function() {
                    //             return false;
                    //         }
                    //     })
                    // }
                });

                galleryImage.height(shortest);
            });
        },

        TimelineFilters: function() {

            var filterList = $(".filter__list"),
                showMoreButton = function(filterType) {
                    return "<button class=\"show-more-filters button\">Show more "+ filterType +"</button>";
                },
                showLessButton = function(filterType) {
                    return "<button class=\"show-less-filters button visually-hidden\">Show less "+ filterType +"</button>";
                };

            filterList.each(function() {
                var filterListItem = $(this).find(".filter__list-item"),
                    filterTitle = $(this).data("filter-title");
                if (filterListItem.length > 10) {
                    filterListItem.each(function() {
                        var index = $(this).index(),
                            toHide = index > 9;
                        if (toHide) {
                            $(this).addClass("visually-hidden");
                        }
                    });
                    $(this).append("<li class=\"filter__list-item\">" + showMoreButton(filterTitle) + showLessButton(filterTitle) + "</li>");
                }
            });

            $(document).on("click", ".show-more-filters, .show-less-filters", function(e) {
                var button = $(e.target),
                    thisFilterList = button.closest(".filter__list"),
                    listItem = thisFilterList.find(".filter__list-item"),
                    initiallyHiddenListItems = thisFilterList.find(".filter__list-item:gt(9)"),
                    lastIndex = listItem.length - 1,
                    firstHiddenItemIndex = thisFilterList.find(".visually-hidden").first().index();
                if (button.is(".show-more-filters")) {
                    if (firstHiddenItemIndex + 10 > lastIndex) {
                        listItem.slice(firstHiddenItemIndex, lastIndex).removeClass("visually-hidden");
                        $(this).siblings().first().removeClass("visually-hidden");
                        $(this).addClass("visually-hidden");
                    } else {
                        listItem.slice(firstHiddenItemIndex, (firstHiddenItemIndex + 10)).removeClass("visually-hidden");
                    }
                } else {
                    initiallyHiddenListItems.addClass("visually-hidden");
                    $(this).siblings().first().removeClass("visually-hidden");
                    $(this).parent().removeClass("visually-hidden");
                    $(this).addClass("visually-hidden");
                }
                e.preventDefault();
            });

        },

        AlphabetisedNav: function() {
            var mainHeaderWrapper = $(".main-header__wrapper").first(),
                nav = $(".tertiary-nav__list--alphabetised"),
                navLink = nav.find(".tertiary-nav__link");
            navLink.click(function(e) {
                var href = $(this).attr("href"),
                    target = $(href);
                if (target.length) {
                    var offset = target.offset().top - mainHeaderWrapper.height() - 10;
                    $("html, body").animate({
                        scrollTop: offset
                    }, 1000, function() {
                        history.pushState(null, null, href);
                    });
                }
                e.preventDefault();
            });
        }

    }
})();

var MobileNav = (function() {
    return {
        Init: function(navClass) {
            var body = $("body").first(),
                mainHeaderCellLogo = $(".main-header__cell--logo").first(),
                mobileMenuButton = $("<button class=\"mobile-navigation-button\">Show menu</button>"),
                mobileNavigation = $("." + navClass);

            // Set nav to "inactive" initially
            mobileNavigation.addClass("is-inactive");

            // Add menu button
            mainHeaderCellLogo.prepend(mobileMenuButton);

            // Menu button click
            mobileMenuButton.click(function(e) {
                mobileNavigation.toggleClass("is-active is-inactive");
                body.toggleClass("nav-active");
                e.preventDefault();
            });

            // Document click (close nav if not within)
            $(document).click(function(e) {
                var target = $(e.target),
                    inMobileNav = target.closest("." + navClass).length,
                    notTarget = !target.is(".mobile-navigation-button");

                if (!inMobileNav && notTarget) {
                    if (mobileNavigation.hasClass("is-active")) {
                        mobileNavigation.toggleClass("is-active").toggleClass("is-inactive");
                        body.toggleClass("nav-active");
                    }
                }
            });

            // Close nav if window's resized to desktop-nav size
            MediaQueries.WasIs({
                wasMaxWidth: "767px",
                isMinWidth: "768px",
                ifTrueDo: function() {
                    mobileNavigation.removeClass("is-active").addClass("is-inactive");
                    body.removeClass("nav-active");
                }
            });

        }
    }
})();

var DesktopNav = (function() {
    return {

        Init: function() {
            this.Setup();
            this.Search();
        },

        Setup: function() {

            var mainNav = $(".main-header__cell--main-nav").first(),
                mainNavLink = mainNav.find(".main-nav__link");

            mainNavLink.click(function(e) {
                // No ink because of dropdown
                //DesktopNav.Ink($(this), e);
                if ($(this).is(".main-nav__link--dropdown-toggle")) {
                    var mainNavDropdown = $(this).next(".main-nav__dropdown"),
                        isHidden = mainNavDropdown.hasClass("visually-hidden");

                    if (isHidden) {
                        Utils.SlideDown(mainNavDropdown, 200);
                    }
                    else {
                        Utils.SlideUp(mainNavDropdown, 200);
                    }

                    e.preventDefault();
                }
            });

        },

        Search: function() {
            var mainSearch = $(".main-search").first(),
                searchField = mainSearch.find(".main-search__field").first(),
                searchButton = mainSearch.find(".main-search__button").first();

            searchButton.click(function(e) {
                DesktopNav.Ink($(this), e);
                e.preventDefault();
            });
        },

        Ink: function(_this, e) {
            var parent, ink, d, x, y;

            parent = !_this.is(".main-search__button") ? _this.parent() : _this;

            //create .ink element if it doesn't exist
            if(!parent.find(".ink").length)
                parent.prepend("<span class='ink'></span>");
                
            ink = parent.find(".ink");

            // in case of quick double clicks stop the previous animation
            ink.removeClass("animate");
            
            // set size of .ink
            if(!ink.height() && !ink.width())
            {
                //use parent's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
                d = Math.max(parent.outerWidth(), parent.outerHeight());
                ink.css({height: d, width: d});
            }
            
            // get click coordinates
            // logic = click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center;
            x = e.pageX - parent.offset().left - ink.width()/2;
            y = e.pageY - parent.offset().top - ink.height()/2;
            
            //set the position and add class .animate
            ink.css({top: y+'px', left: x+'px'}).addClass("animate");
        }

    }
})();

var NavTabs = (function() {
    var mainHeaderWrapper = $(".main-header__wrapper").first(),
        viewType = $(".view-type");
    return {
        Init: function() {
            this.Click();
            this.Ready();
        },

        Click: function() {
            var link = $(".secondary-nav__link, .tertiary-nav__link");
            link.click(function(e) {
                var _this = $(this),
                    href = _this.attr("href"),
                    target = $(href);
                NavTabs.ShowHideViewTypes(target, href);
                // Trigger scroll event to show lazy loaded images
                $(window).scroll();
                e.preventDefault();
            });
        },

        Ready: function() {
            var hash = window.location.hash;
            if (hash) {
                var target = $(hash);
                $(window).load(function() {
                    NavTabs.ShowHideViewTypes(target, hash);
                });
            } else {
                var firstViewType = viewType.first(),
                    firstViewTypeChild = firstViewType.find(".view-type").first();
                firstViewType.removeClass("visually-hidden");
                if (firstViewTypeChild.length) firstViewTypeChild.removeClass("visually-hidden");
            }
        },

        ShowHideViewTypes: function(target, href) {
            if (target.length) {
                var href = href,
                    parentViewType = target.parents(".view-type").first(),
                    childViewType = target.find(".view-type");

                // Hide all view types
                viewType.each(function() {
                    $(this).addClass("visually-hidden");
                });

                // If target has parent view type, show it
                if (parentViewType.length) {
                   parentViewType.removeClass("visually-hidden"); 
                } 

                // Show target
                target.removeClass("visually-hidden");

                if (childViewType.length) childViewType.first().removeClass("visually-hidden");

                history.pushState(null, null, href);

                // var offset = target.offset().top - mainHeaderWrapper.height() - 10,
                //     scrollSpeed = offset - $(window).scrollTop();
                
                // // Scroll to target and change history
                // $("html, body").animate({
                //     scrollTop: offset
                // }, scrollSpeed, function() {
                //     history.pushState(null, null, href);
                // });
            }
        }

    }
})();