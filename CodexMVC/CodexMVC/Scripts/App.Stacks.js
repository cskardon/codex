﻿var app = window.app || {};
(function (app) {
    var utils = app.Utils,
        defined = utils.defined,
        select = utils.select,
        receive = utils.receive,
        each = utils.each,
        broadcast = utils.broadcast;
    var Common = app.Common,
        Cards = app.Cards,
        Nodes = app.Nodes;
    (function (Stacks) {
        Stacks.CardFactory = function (client, context) {
            var fact = client.data.fact;
            if (fact.Name == "CreatedBy") {
                app.API.findCreatedByHyperNode({
                    data: {
                        guid: fact.Guid
                    },
                    success: function (response) {
                        var hyperNode = response.Data;
                        var card = new Cards.CreatedByCard({
                            model: {
                                hyperNode: hyperNode
                            },
                            subjectOfSelector: {
                                model: {
                                    name: hyperNode.Agent.Name
                                }
                            },
                            createdBySelector: {
                                model: {
                                    name: hyperNode.Creator.Name
                                }
                            },
                            atPlaceSelector: {
                                model: {
                                    name: hyperNode.Place.Name
                                }
                            }
                        });
                        if (client.success) client.success.call(context, card);
                    },
                    error: function (a, b, c) {
                        if (client.error) client.error.call(context, a, b, c);
                    }
                }, this);
                return;
            }
            if (fact.Name == "OwnedBy") {
                app.API.findOwnedByHyperNode({
                    data: {
                        guid: fact.Guid
                    },
                    success: function (response) {
                        var hyperNode = response.Data;
                        var card = new Cards.OwnedByCard({
                            model: {
                                hyperNode: hyperNode
                            },
                            subjectOfSelector: {
                                model: {
                                    name: response.Data.Agent.Name
                                }
                            },
                            personOrOrganisationSelector: {
                                model: {
                                    name: response.Data.AgentOrOrganisation.Name
                                }
                            }
                        });
                        if (client.success) client.success.call(context, card);
                    },
                    error: function (a, b, c) {
                        if (client.error) client.error.call(context, a, b, c);
                    }
                }, this);
                return;
            }
            if (fact.Name == "Represents") {
                app.API.findRepresentsHyperNode({
                    data: {
                        guid: fact.Guid
                    },
                    success: function (response) {
                        var hyperNode = response.Data;
                        var card = new Cards.RepresentsCard({
                            model: {
                                hyperNode: hyperNode
                            },
                            subjectOfSelector: {
                                model: {
                                    name: response.Data.Agent.Name
                                }
                            },
                            somethingSelector: {
                                model: {
                                    name: response.Data.Something.Name
                                }
                            }
                        });
                        if (client.success) client.success.call(context, card);
                    },
                    error: function (a, b, c) {
                        if (client.error) client.error.call(context, a, b, c);
                    }
                }, this);
                return;
            }
            if (fact.Name == "KnownAs") {
                app.API.findKnownAsHyperNode({
                    data: {
                        guid: fact.Guid
                    },
                    success: function (response) {
                        var hyperNode = response.Data;
                        var card = new Cards.KnownAsCard({
                            model: {
                                hyperNode: hyperNode
                            }
                        });
                        if (client.success) client.success.call(context, card);
                    },
                    error: function (a, b, c) {
                        if (client.error) client.error.call(context, a, b, c);
                    }
                }, this);
                return;
            }
        };
        var CategoryStack = (function () {
            function CategoryStack(component) {
                this.templateName = "category-stack-template";
                this.API = app.API;
                this.model = {};
                this.model.self = new Nodes.Category();
                this.parentCategories = ko.observableArray([]);
                this.cards = ko.observableArray();
                this.currentCard = ko.observable();
                this.currentCardTemplateName = ko.observable();
                this.currentCardIsReady = ko.observable(false);
                if (component) {
                    this.bindComponent(component);
                }
                this.setupData();
            }
            CategoryStack.prototype.categoryClicked = function (category) {
                broadcast("ManageCategory", category);
            };
            CategoryStack.prototype.setupData = function () {
                var model = this.model.self.unbind();
                this.API.findParentCategories({
                    data: {
                        categoryGuid: model.Guid
                    },
                    success: function (response) {
                        this.parentCategories(response.Data);
                    }
                }, this);
            };
            CategoryStack.prototype.createCategoryCardClicked = function () {
                this.createCategoryCard();
            };
            CategoryStack.prototype.createCategoryCard = function () {
                var card = new Cards.CreateCategoryCard();
                this.switchCard(card);
            };
            CategoryStack.prototype.switchCard = function (card) {
                this.currentCardIsReady(false);
                this.currentCard(card);
                this.currentCardTemplateName(card.templateName);
                this.currentCardIsReady(true);
            };
            CategoryStack.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            CategoryStack.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.self)) this.model.self.bind(model.self);
            };
            return CategoryStack;
        })();
        Stacks.CategoryStack = CategoryStack;
        var AgentStack = (function () {
            function AgentStack(component) {
                this.templateName = "person-stack-template";
                this.API = app.API;
                this.model = {};
                this.model.factGrouping = ko.observableArray([]);
                this.model.attributes = ko.observableArray([]);
                this.model.relationships = ko.observableArray([]);
                this.model.properties = ko.observableArray([]);
                this.model.selectedAttribute = ko.observable();
                this.model.categories = ko.observableArray([]);
                this.model.facts = ko.observableArray([]);
                this.model.self = new Nodes.Agent();
                this.model.cards = ko.observableArray([]);
                this.isCardTemplateReady = ko.observable(false);
                this.currentCard = ko.observable();
                this.currentCardTemplate = ko.observable();
                this.setupEventHandlers();
                if (component) {
                    this.bindComponent(component);
                }
                this.setupData();
            }
            AgentStack.prototype.gotoGalleryClicked = function () {
                var url = "/editor/gallery?guid=" + this.model.self.Guid;
                window.location.href = url;
            };
            //ancestorCategoriesClicked
            AgentStack.prototype.ancestorCategoriesClicked = function () {
                var guid = this.model.self.Guid;
                this.API.findCategoryHierarchy({
                    data: {
                        entityGuid: guid
                    },
                    success: function (response) {
                        console.log(response);
                    }
                });
            };
            AgentStack.prototype.attributeEntityClicked = function (attribute) {
                broadcast("ManageAgent", attribute.Entity);
            };
            AgentStack.prototype.relationshipEntityClicked = function (relationship) {
                broadcast("ManageAgent", relationship.Entity);
            };
            AgentStack.prototype.factEntityClicked = function (fact) {
                var label = fact.EntityLabel;
                broadcast("Manage" + label, fact.Entity);
            };
            AgentStack.prototype.factGroupingSelected = function (factGroup) {
                var fact = factGroup[0].Fact;
                this.factSelected(fact);
            };
            AgentStack.prototype.findAllAttributes = function () {
                this.API.findAllAttributes({
                    data: {
                        agentGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        this.model.attributes(response.Data);
                    }
                }, this);
            };
            AgentStack.prototype.findAllProperties = function () {
                this.API.findAllProperties({
                    data: {
                        agentGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        this.model.properties(response.Data);
                    }
                }, this);
            };
            AgentStack.prototype.findAllRelationships = function () {
                this.API.findAllRelationships({
                    data: {
                        agentGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        each(response.Data, function (item) {
                            var label = Common.Labels[item.Relationship];
                            if (!label) {
                                return;
                            }
                            item.Relationship = item.Outgoing ? label.forward : label.back;
                        });
                        this.model.relationships(response.Data);
                    }
                }, this);
            };
            AgentStack.prototype.findAllSubcategories = function () {
                this.API.findAllSubcategories({
                    data: {
                        ancestorName: "Agent"
                    },
                    success: function (response) {
                        this.bindCategories(response.Data);
                    }
                }, this);
            };
            AgentStack.prototype.bindCategories = function (categories) {
                this.model.categories(select(categories, function (item) {
                    return {
                        text: item.Child.Name + ' [' + item.Parent.Name + ']',
                        value: item.Child.Guid
                    };
                }));
            };
            AgentStack.prototype.findAllHypernodes = function () {
                this.API.findAllAgentAgentHyperNodes({
                    data: {
                        agentGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        each(response.Data, function (group) {
                            each(group, function (item) {
                                var label = Common.Labels[item.Relationship];
                                if (!label) {
                                    return;
                                }
                                item.Relationship = label.forward;
                            });
                        });
                        this.model.factGrouping(response.Data);
                    }
                }, this);
            };
            AgentStack.prototype.setupData = function () {
                this.findAllSubcategories();
                this.findAllAttributes();
                this.findAllRelationships();
                this.findAllProperties();
                this.findAllHypernodes();
            };
            AgentStack.prototype.addAttribute = function () {
                var guid = this.model.selectedAttribute();
                this.API.personIs({
                    data: {
                        agentGuid: this.model.self.Guid,
                        categoryGuid: guid
                    },
                    success: function (response) {
                        this.model.selectedAttribute(null);
                        this.model.attributes(response.Data);
                    }
                }, this);
            };
            AgentStack.prototype.addProperty = function () {
                var propertyType = this.model.selectedPropertyType();
                var propertValue = this.model.propertyValue();
                this.API.personIs({
                    data: {
                        agentGuid: this.model.self.Guid,
                        propertyType: propertyType,
                        value: propertyValue
                    },
                    success: function (response) {
                        this.model.selectedPropertyType(null);
                        this.model.properties(response.Data);
                    }
                }, this);
            };
            AgentStack.prototype.propertySelected = function (property) {
                this.API.findProperty({
                    data: {
                        guid: property.Guid
                    },
                    success: function (response) {
                        var propertyNode = response.Data;
                        var card = new Cards.PropertyCard({
                            model: {
                                Agent: propertyNode.Agent,
                                Property: propertyNode.Property
                            }
                        });
                    }
                }, this);
            };
            AgentStack.prototype.factSelected = function (fact/*: IFact */) {
                if (fact.Name == "Born" || fact.Name == "Was Born" || fact.Name == "Birth") {
                    this.API.findBornHyperNode({
                        data: {
                            guid: fact.Guid
                        },
                        success: function (response) {
                            var hyperNode = response.Data;
                            var card = new Cards.BornCard({
                                model: {
                                    hyperNode: hyperNode
                                },
                                subjectOfSelector: {
                                    model: {
                                        name: hyperNode.Agent.Name
                                    }
                                },
                                atPlaceSelector: {
                                    model: {
                                        name: hyperNode.Place.Name
                                    }
                                }
                            });
                            this.switchCard(card);
                        }
                    }, this);
                    return;
                }
                if (fact.Name == "Died") {
                    this.API.findDiedHyperNode({
                        data: {
                            guid: fact.Guid
                        },
                        success: function (response) {
                            var hyperNode = response.Data;
                            var card = new Cards.DiedCard({
                                model: {
                                    hyperNode: hyperNode
                                },
                                subjectOfSelector: {
                                    model: {
                                        name: response.Data.Agent.Name
                                    }
                                },
                                atPlaceSelector: {
                                    model: {
                                        name: response.Data.Place.Name
                                    }
                                }
                            });
                            this.switchCard(card);
                        }
                    }, this);
                    return;
                }
                if (fact.Name == "Visited") {
                    this.API.findVisitedHyperNode({
                        data: {
                            guid: fact.Guid
                        },
                        success: function (response) {
                            var hyperNode = response.Data;
                            var card = new Cards.VisitedCard({
                                model: {
                                    hyperNode: hyperNode
                                },
                                subjectOfSelector: {
                                    model: {
                                        name: response.Data.Agent.Name
                                    }
                                },
                                atPlaceSelector: {
                                    model: {
                                        name: response.Data.Place.Name
                                    }
                                }
                            });
                            this.switchCard(card);
                        }
                    }, this);
                    return;
                }
                if (fact.Name == "LivedAt") {
                    this.API.findLivedAtHyperNode({
                        data: {
                            guid: fact.Guid
                        },
                        success: function (response) {
                            var hyperNode = response.Data;
                            var card = new Cards.LivedAtCard({
                                model: {
                                    hyperNode: hyperNode
                                },
                                subjectOfSelector: {
                                    model: {
                                        name: response.Data.Agent.Name
                                    }
                                },
                                atPlaceSelector: {
                                    model: {
                                        name: response.Data.Place.Name
                                    }
                                }
                            });
                            this.switchCard(card);
                        }
                    }, this);
                    return;
                }
            };
            AgentStack.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            AgentStack.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.self)) this.model.self = model.self;
            };
            AgentStack.prototype.setupEventHandlers = function () {
                receive("CloseCard", function (card) {
                    this.clearCard();
                }, this);
                receive("RefreshRelationships", function () {
                    this.findAllRelationships();
                }, this);
                receive("RefreshFacts", function () {
                    this.findAllHypernodes();
                }, this);
                receive("RefreshAttributes", function () {
                    this.findAllAttributes();
                }, this);
            };
            AgentStack.prototype.clearCard = function () {
                this.isCardTemplateReady(false);
            };
            AgentStack.prototype.newRelatedToCardClicked = function () {
                this.newRelatedToCard();
            };
            AgentStack.prototype.newBornCardClicked = function () {
                this.newBornCard();
            };
            AgentStack.prototype.newDiedCardClicked = function () {
                this.newDiedCard();
            };
            AgentStack.prototype.newVisitedCardClicked = function () {
                this.newVisitedCard();
            };
            AgentStack.prototype.newKnownAsCardClicked = function () {
                this.newKnownAsCard();
            };
            AgentStack.prototype.newCreatedCardClicked = function () {
                this.newCreatedCard();
            };
            AgentStack.prototype.newLivedAtCardClicked = function () {
                this.newLivedAtCard();
            };
            AgentStack.prototype.saveAgentClicked = function () {
                this.saveAgent();
            };
            AgentStack.prototype.saveAgent = function () {
                var self = this.model.self.unbind();
                this.API.saveAgent({
                    data: self,
                    success: function (response) {
                        this.model.self.bind({
                            guid: response.Data.Guid
                        });
                    }
                }, this);
            };
            AgentStack.prototype.newRelatedToCard = function () {
                var card = new Cards.RelatedToCard({
                    model: {
                        hyperNode: {
                            Agent: this.model.self
                        }
                    }
                });
                this.switchCard(card);
            };
            AgentStack.prototype.newBornCard = function () {
                var card = new Cards.BornCard({
                    model: {
                        hyperNode: {
                            Agent: this.model.self
                        }
                    }
                });
                this.switchCard(card);
            };
            AgentStack.prototype.newDiedCard = function () {
                var card = new Cards.DiedCard({
                    model: {
                        hyperNode: {
                            Agent: this.model.self
                        }
                    }
                });
                this.switchCard(card);
            };
            AgentStack.prototype.newVisitedCard = function () {
                var card = new Cards.VisitedCard({
                    model: {
                        hyperNode: {
                            Agent: this.model.self
                        }
                    }
                });
                this.switchCard(card);
            };
            AgentStack.prototype.newLivedAtCard = function () {
                var card = new Cards.LivedAtCard({
                    model: {
                        hyperNode: {
                            Agent: this.model.self
                        }
                    }
                });
                this.switchCard(card);
            };
            AgentStack.prototype.newKnownAsCard = function () {
                var card = new Cards.KnownAsCard({
                    model: {
                        knownAs: {
                            subjectOf: {
                                start: this.model.self
                            }
                        }
                    }
                });
                this.switchCard(card);
            };
            AgentStack.prototype.switchCard = function (card) {
                this.isCardTemplateReady(false);
                this.currentCard(card);
                this.currentCardTemplate(card.templateName);
                this.isCardTemplateReady(true);
            };
            AgentStack.prototype.unbind = function () {
                return {
                    cards: this.model.cards()
                };
            };
            return AgentStack;
        })();
        Stacks.AgentStack = AgentStack;
        var PlaceStack = (function () {
            function PlaceStack(component) {
                this.templateName = "place-stack-template";
                this.API = app.API;
                this.model = {};
                this.model.factGrouping = ko.observableArray([]);
                this.model.attributes = ko.observableArray([]);
                this.model.selectedAttribute = ko.observable();
                this.model.categories = ko.observableArray([]);
                this.model.self = new Nodes.Place();
                this.model.cards = ko.observableArray([]);
                this.isCardTemplateReady = ko.observable(false);
                this.currentCard = ko.observable();
                this.currentCardTemplate = ko.observable();
                this.setupEventHandlers();
                if (component) {
                    this.bindComponent(component);
                }
                this.setupData();
            }
            PlaceStack.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            PlaceStack.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.self)) this.model.self = model.self;
            };
            PlaceStack.prototype.setupEventHandlers = function () {
                receive("CloseCard", function (card) {
                    this.clearCard();
                }, this);
            };
            PlaceStack.prototype.factGroupingSelected = function (factGroup) {
                var fact = factGroup[0].Fact;
                this.factSelected(fact);
            };
            PlaceStack.prototype.findAllFacts = function () {
                this.API.findAllPlaceFacts({
                    data: {
                        placeGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        each(response.Data, function (group) {
                            each(group, function (item) {
                                var label = Common.Labels[item.Relationship];
                                if (!label) {
                                    return;
                                }
                                item.Relationship = label.forward;
                            });
                        });
                        this.model.factGrouping(response.Data);
                    }
                }, this);
            };
            PlaceStack.prototype.setupData = function () {
                this.findAllSubcategories();
                // this.findAllAttributes();
                this.findAllFacts();
            };
            PlaceStack.prototype.findAllSubcategories = function () {
                this.API.findAllSubcategories({
                    data: {
                        ancestorName: "Place"
                    },
                    success: function (response) {
                        this.bindCategories(response.Data);
                    }
                }, this);
            };
            PlaceStack.prototype.bindCategories = function (categories) {
                this.model.categories(select(categories, function (item) {
                    return {
                        text: item.Child.Name + ' [' + item.Parent.Name + ']',
                        value: item.Child.Guid
                    };
                }));
            };
            PlaceStack.prototype.newDetailsCardClicked = function () {

            };
            PlaceStack.prototype.addAttribute = function () {
                var guid = this.model.selectedAttribute();
                this.API.placeIs({
                    data: {
                        placeGuid: this.model.self.Guid,
                        categoryGuid: guid
                    },
                    success: function (response) {
                        this.model.selectedAttribute(null);
                        this.model.attributes(response.Data);
                    }
                }, this);
            };
            return PlaceStack;
        })();
        Stacks.PlaceStack = PlaceStack;
        var ArtworkStack = (function () {
            function ArtworkStack(component) {
                this.templateName = "person-stack-template";
                this.API = app.API;
                this.model = {};
                this.model.factGrouping = ko.observableArray([]);
                this.model.attributes = ko.observableArray([]);
                this.model.relationships = ko.observableArray([]);
                this.model.properties = ko.observableArray([]);
                this.model.selectedAttribute = ko.observable();
                this.model.categories = ko.observableArray([]);
                this.model.facts = ko.observableArray([]);
                this.model.self = new Nodes.Agent();
                this.model.cards = ko.observableArray([]);
                this.isCardTemplateReady = ko.observable(false);
                this.currentCard = ko.observable();
                this.currentCardTemplate = ko.observable();
                this.setupEventHandlers();
                if (component) {
                    this.bindComponent(component);
                }
                this.setupData();
            }
            ArtworkStack.prototype.factGroupingSelected = function (factGroup) {
                var fact = factGroup[0].Fact;
                this.factSelected(fact);
            };
            ArtworkStack.prototype.findAllAttributes = function () {
                this.API.findAllAttributes({
                    data: {
                        agentGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        this.model.attributes(response.Data);
                    }
                }, this);
            };
            ArtworkStack.prototype.findAllProperties = function () {
                this.API.findAllProperties({
                    data: {
                        agentGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        this.model.properties(response.Data);
                    }
                }, this);
            };
            ArtworkStack.prototype.findAllRelationships = function () {
                this.API.findAllRelationships({
                    data: {
                        agentGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        each(response.Data, function (item) {
                            var label = Common.Labels[item.Relationship];
                            if (!label) {
                                return;
                            }
                            item.Relationship = item.Outgoing ? label.forward : label.back;
                        });
                        this.model.relationships(response.Data);
                    }
                }, this);
            };
            ArtworkStack.prototype.findAllSubcategories = function () {
                this.API.findAllSubcategories({
                    data: {
                        ancestorName: "Agent"
                    },
                    success: function (response) {
                        this.bindCategories(response.Data);
                    }
                }, this);
            };
            ArtworkStack.prototype.bindCategories = function (categories) {
                this.model.categories(select(categories, function (item) {
                    return {
                        text: item.Child.Name + ' [' + item.Parent.Name + ']',
                        value: item.Child.Guid
                    };
                }));
            };
            ArtworkStack.prototype.findAllHypernodes = function () {
                this.API.findAllAgentAgentHyperNodes({
                    data: {
                        agentGuid: this.model.self.Guid
                    },
                    success: function (response) {
                        each(response.Data, function (group) {
                            each(group, function (item) {
                                var label = Common.Labels[item.Relationship];
                                if (!label) {
                                    return;
                                }
                                item.Relationship = label.forward;
                            });
                        });
                        this.model.factGrouping(response.Data);
                    }
                }, this);
            };
            ArtworkStack.prototype.setupData = function () {
                this.findAllSubcategories();
                this.findAllAttributes();
                this.findAllRelationships();
                this.findAllProperties();
                this.findAllHypernodes();
            };
            ArtworkStack.prototype.addAttribute = function () {
                var guid = this.model.selectedAttribute();
                this.API.personIs({
                    data: {
                        agentGuid: this.model.self.Guid,
                        categoryGuid: guid
                    },
                    success: function (response) {
                        this.model.selectedAttribute(null);
                        this.model.attributes(response.Data);
                    }
                }, this);
            };
            ArtworkStack.prototype.addProperty = function () {
                var propertyType = this.model.selectedPropertyType();
                var propertValue = this.model.propertyValue();
                this.API.personIs({
                    data: {
                        agentGuid: this.model.self.Guid,
                        propertyType: propertyType,
                        value: propertyValue
                    },
                    success: function (response) {
                        this.model.selectedPropertyType(null);
                        this.model.properties(response.Data);
                    }
                }, this);
            };
            ArtworkStack.prototype.propertySelected = function (property) {
                this.API.findProperty({
                    data: {
                        guid: property.Guid
                    },
                    success: function (response) {
                        var propertyNode = response.Data;
                        var card = new Cards.PropertyCard({
                            model: {
                                Agent: propertyNode.Agent,
                                Property: propertyNode.Property
                            }
                        });
                    }
                }, this);
            };
            ArtworkStack.prototype.factSelected = function (fact/*: IFact */) {
                this.cardFactory({
                    data: {
                        fact: fact
                    },
                    success: function (card) {
                        if (card) {
                            this.switchCard(card);
                        } else {
                            // inform user card could not be found for fact ?  
                        }
                    },
                    error: function (message) {
                        // inform user of an error ?
                    }
                }, this);
            };
            ArtworkStack.prototype.bindComponent = function (component) {
                if (!component) {
                    return;
                }
                if (component.model) {
                    this.bind(component.model);
                }
            };
            ArtworkStack.prototype.bind = function (model) {
                if (!model) {
                    return;
                }
                if (defined(model.self)) this.model.self = model.self;
            };
            ArtworkStack.prototype.setupEventHandlers = function () {
                receive("CloseCard", function (card) {
                    this.clearCard();
                }, this);
            };
            ArtworkStack.prototype.clearCard = function () {
                this.currentCard(null);
                this.currentCardTemplate(null);
                this.isCardTemplateReady(false);
            };
            ArtworkStack.prototype.newOwnedByCardClicked = function () {
                this.newOwnedByCard();
            };
            ArtworkStack.prototype.newRepresentsCardClicked = function () {
                this.newRepresentsCard();
            };
            ArtworkStack.prototype.newKnownAsCardClicked = function () {
                this.newKnownAsCard();
            };
            ArtworkStack.prototype.newCreatedByCardClicked = function () {
                this.newCreatedByCard();
            };
            ArtworkStack.prototype.saveArtworkClicked = function () {
                this.createAgent();
            };
            ArtworkStack.prototype.createArtwork = function () {
                var self = this.model.self.unbind();
                this.API.createArtwork({
                    data: self,
                    success: function (response) {
                        this.model.self.bind({
                            guid: response.Data.Guid
                        });
                    }
                }, this);
            };
            ArtworkStack.prototype.newRepresentsCard = function () {
                var card = new Cards.RepresentsCard({
                    model: {
                        Source: this.model.self

                    }
                });
                this.switchCard(card);
            };
            ArtworkStack.prototype.newOwnedByCard = function () {
                var card = new Cards.OwnedByCard({
                    model: {
                        hyperNode: {
                            Agent: this.model.self
                        }
                    }
                });
                this.switchCard(card);
            };
            ArtworkStack.prototype.newCreatedByCard = function () {
                var card = new Cards.CreatedByCard({
                    model: {
                        hyperNode: {
                            Agent: this.model.self
                        }
                    }
                });
                this.switchCard(card);
            };
            ArtworkStack.prototype.newKnownAsCard = function () {
                var card = new Cards.KnownAsCard({
                    model: {
                        knownAs: {
                            subjectOf: {
                                start: this.model.self
                            }
                        }
                    }
                });
                this.switchCard(card);
            };
            ArtworkStack.prototype.switchCard = function (card) {
                this.isCardTemplateReady(false);
                this.currentCard(card);
                this.currentCardTemplate(card.templateName);
                this.isCardTemplateReady(true);
            };
            ArtworkStack.prototype.unbind = function () {
                return {
                    cards: this.model.cards()
                };
            };
            return ArtworkStack;
        })();
        Stacks.ArtworkStack = ArtworkStack;
    })(app.Stacks || (app.Stacks = {}));
})(app);