﻿using CodexMVC.Helpers;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditQualifiedBy
    {
        public Category Category { get; set; }

        public string QualifierGuid { get; set; }

        public string RelationshipGuid { get; set; }

        public List<SelectListItem> Qualifiers { get; set; }

        public EditQualifiedBy()
        {

        }

        public EditQualifiedBy(bool loadResources)
        {
            Qualifiers = RazorCache.CategoryService.AllWithAncestors();
        }
    }
}