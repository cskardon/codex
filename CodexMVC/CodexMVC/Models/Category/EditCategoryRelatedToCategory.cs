﻿using CodexMVC.Helpers;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddAestheticConventionModel
    {
        public string ParentGuid { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<SelectListItem> AestheticConventions { get; set; }

        public AddAestheticConventionModel()
        {
            var qualities = DependencyResolver.Current.GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.AestheticConvention);
            AestheticConventions = qualities
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();
            AestheticConventions.Insert(0, new SelectListItem { });
        }
    }
    public class AddInterestModel : BaseAddCategoryModel
    {
        public AddInterestModel()
        {
            var interests = RazorHelper.SelectListOfDescendants(Label.Categories.Interest);
            Categories = interests;
            Categories.Insert(0, new SelectListItem { });
        }
    }
    public class AddArtistTypeModel : BaseAddCategoryModel
    {
        public AddArtistTypeModel()
        {
            var artistTypes = RazorHelper.SelectListOfDescendants(Label.Categories.Artist);
            Categories = artistTypes;
            Categories.Insert(0, new SelectListItem { });
        }
    }
    public class AddStyleModel : BaseAddCategoryModel
    {
        public AddStyleModel()
        {
            var styles = RazorHelper.SelectListOfDescendants(Label.Categories.ArtMovement);
            Categories = styles;
            Categories.Insert(0, new SelectListItem { });
        }
    }
    public class AddSubstanceModel : BaseAddCategoryModel
    {
        public AddSubstanceModel()
        {
            var substances = DependencyResolver.Current.GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.Substance);
            Categories = substances
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();
            Categories.Insert(0, new SelectListItem { });
        }
    }
    public class BaseAddCategoryModel
    {
        public string Action { get; set; }

        public string Title { get; set; }

        public string ParentGuid { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<SelectListItem> Categories { get; set; }
    }
    public class AddImplementModel : BaseAddCategoryModel
    {
        public AddImplementModel()
        {
            var substances = DependencyResolver.Current.GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.Implement);
            Categories = substances
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();
            Categories.Insert(0, new SelectListItem { });
        }
    }
    public class AddGenreModel
    {
        public string ParentGuid { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<SelectListItem> Genres { get; set; }

        public AddGenreModel()
        {
            var genres = DependencyResolver.Current.GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.ArtisticGenre);
            Genres = genres
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();
            Genres.Insert(0, new SelectListItem { });
        }
    }
    public class AddSubjectModel
    {
        public string ParentGuid { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<SelectListItem> Subjects { get; set; }

        public AddSubjectModel()
        {
            var subjects = DependencyResolver.Current.GetService<IAgentService>().AllSubcategoriesOf(Label.Categories.ArtisticSubject);
            Subjects = subjects
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();
            Subjects.Insert(0, new SelectListItem { });
        }
    }
    public class AddMotifModel
    {
        public string ParentGuid { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<SelectListItem> Motifs { get; set; }

        public AddMotifModel()
        {
            var motifs = DependencyResolver.Current.GetService<IAgentService>().AllSubcategoriesOf("Motif");
            Motifs = motifs
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();
            Motifs.Insert(0, new SelectListItem { });
        }
    }
    public class EditCategoryRelatedToCategory
    {
        public string CategoryGuid { get; set; }
        public string Name { get; set; }
        public string OtherCategoryGuid { get; set; }
        public string RelationshipGuid { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public EditCategoryRelatedToCategory()
        {
            Categories = RazorCache.CategoryService.AllWithAncestors();
        }
    }
}