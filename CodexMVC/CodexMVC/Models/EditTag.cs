﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class EditTag
    {
        public string Guid { get; set; }

        [Required]
        public string Name { get; set; }

        public bool IsTheme { get; set; }

        public string Description { get; set; }

    }
}