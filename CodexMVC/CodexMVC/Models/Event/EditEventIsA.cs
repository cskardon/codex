﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditEventIsA
    {
        public Event Event { get; set; }
        public string CategoryGuid { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public EditEventIsA()
        {
            var categoryService = DependencyResolver.Current.GetService<ICategoryService>();
            Categories = categoryService.All()
                .OrderBy(x => x.Name)
                .Select(x => new System.Web.Mvc.SelectListItem { Value = x.Guid, Text = x.Name }).ToList();
        }
    }
}