﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddAssociatedWith
    {
        public Event Event { get; set; }

        public string AgentGuid { get; set; }

        public List<SelectListItem> Agents { get; set; }

        public AddAssociatedWith()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var agents = agentService.AllPeople();
            Agents = agents.Select(x => new System.Web.Mvc.SelectListItem { Text = x.FullName, Value = x.Guid }).ToList();
        }
    }
}