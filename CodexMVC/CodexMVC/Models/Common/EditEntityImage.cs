﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class EditImage : IValidatableObject, IImageUploadModel
    {
        public Image ImageNode { get; set; }

        public string Tags { get; set; }

        public string ImageGuid { get; set; }

        public bool Primary { get; set; }

        public string Name { get; set; }

        public string Uri { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Image { get; set; }

        public string Description { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public int? FileSizeBytes { get; set; }

        public string Source { get; set; }

        //public string SourceUrl { get; set; }

        public bool PublicDomain { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Image != null && false == string.IsNullOrEmpty(ImageUrl))
            {
                yield return new ValidationResult("You cannot upload an image and specify the URL at the same time.", new string[] { "Image" });
            }
        }

        public string ImageUrl
        {
            get;
            set;
        }
    }

    public class EditEntityImage : IValidatableObject
    {
        public Image ImageNode { get; set; }

        public string EntityGuid { get; set; }

        public string ImageGuid { get; set; }

        public bool Primary { get; set; }

        public string Name { get; set; }

        public string Uri { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Image { get; set; }

        public string Description { get; set; }

        public int? Width { get; set; }

        public int? Height { get; set; }

        public int? FileSizeBytes { get; set; }

        public string Source { get; set; }

        public string SourceUrl { get; set; }

        public bool PublicDomain { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (ImageGuid == null)
            {
                if (Image == null)
                {
                    yield return new ValidationResult("Please provide an image.", new string[] { "Image" });
                }
            }
        }
    }
}