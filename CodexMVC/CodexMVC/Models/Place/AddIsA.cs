﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddIsA
    {
        public string Name { get; set; }
        public string PlaceGuid { get; set; }
        public string CategoryGuid { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public AddIsA()
        {
            var personService = DependencyResolver.Current.GetService<IAgentService>();
            Categories = personService.AllCategories().Select(x => new SelectListItem { Value = x.Guid, Text = x.Name }).ToList();
        }
    }
}