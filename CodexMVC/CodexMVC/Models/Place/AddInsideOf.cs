﻿using CodexMVC.Helpers;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddInsideOf
    {
        public string Name { get; set; }

        [Required]
        public string PlaceGuid { get; set; }

        [Required]
        public string ContainerGuid { get; set; }

        public List<SelectListItem> Places { get; set; }
        public AddInsideOf()
        {
            Places = RazorCache.PlaceService.AllWithContainers();
            Places.Insert(0, new SelectListItem { });
        }
    }
}