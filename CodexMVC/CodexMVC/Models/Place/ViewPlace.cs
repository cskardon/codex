﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class ViewPlace
    {
        public IEnumerable<TourDetails> Tours { get; set; }
        public IEnumerable<ExhibitionDetails> Exhibitions { get; set; }
        public IEnumerable<NewAct> Timeline { get; set; }
        public Place Place { get; set; }
        public Category PlaceType { get; set; }
        public IEnumerable<Image> Images { get; set; }
        public IEnumerable<PlaceCategoryTuple> Containers { get; set; }
        public List<PlaceAttributeHyperNode> Attributes { get; set; }
        public List<PlaceAttributeHyperNode> ReverseAttributes { get; set; }
        public IEnumerable<ContextHyperNode> Contexts { get; set; }
        public IEnumerable<AgentWithAgentTypes> Artists { get; set; }
        public IEnumerable<CollectionWithArtefacts> Collections { get; set; }
    }
    public class ViewPlaceBySubject
    {
        public Place Place { get; set; }
        public IEnumerable<SubjectWithArefacts> Subjects { get; set; }
    }
    public class ViewPlaceByMovement
    {
        public Place Place { get; set; }
        public IEnumerable<SubjectWithArefacts> Movements { get; set; }
    }
    public class ViewPlaceByArtefactType
    {
        public Place Place { get; set; }
        public IEnumerable<SubjectWithArefacts> ArtefactTypes { get; set; }
    }
}