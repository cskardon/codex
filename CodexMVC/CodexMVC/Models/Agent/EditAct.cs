﻿using CodexMVC.Helpers;
using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ViewTimeList
    {
        public IEnumerable<YearActs> Years { get; set; }
    }
    public enum TimelineMode
    {
        ByDateRange,
        ByTag,
        ByLatest,
        ByTagCategory,
        ByAgent,
        BySimilarities,
        ByAgentType,
        ByPlace,
        ByArtefact,
        ByAct,
        ByAgents
    }
    public class ViewTimeline
    {
        public List<IEntity> Selectors { get; set; }
        public Category AgentType { get; set; }
        public ViewArtefactPublic Artefact { get; set; }

        public Place Place { get; set; }
        public NewAct Act { get; set; }
        public Agent Agent { get; set; }
        public Category TagCategory { get; set; }
        public Tag TagEntity { get; set; }
        public TimelineMode Mode { get; set; }
        public string Tag { get; set; }
        public int Year { get; set; }
        public int? EndYear { get; set; }
        public IEnumerable<NewAct> Timeline { get; set; }
        public IEnumerable<IGrouping<string, MarkerJson>> Markers { get; set; }
        public IEnumerable<HeatMapJson> Heatmap { get; set; }
        public PersonalDetails PersonalDetails { get; set; }
        public IEnumerable<ArtistWithRepresentativeArtwork> Connections { get; set; }
        public ViewTimeline()
        {
            Mode = TimelineMode.ByDateRange;
        }
    }
    public class EditDocument
    {
        public Agent Agent { get; set; }

        public Document Document { get; set; }

        public DocumentHyperNode HyperNode { get; set; }

        public Proximity ProximityModel { get; set; }
        public Proximity ProximityEndModel { get; set; }

        public string AgentGuid { get; set; }
        public string PlaceGuid { get; set; }
        public string CategoryGuid { get; set; }
        public string ArtefactGuid { get; set; }

        /**
         * Resources
         * */
        [ReadOnly(true)]
        public List<SelectListItem> Places { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Agents { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Artefacts { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Categories { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Milennia { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Months { get; set; }

        public EditDocument()
        {
            var places = PlaceServiceCache.AllWithContainers();
            Places = places
                .OrderBy(x => x.Place.Name)
                .Select(x => new SelectListItem
                {
                    Text = x.ToDisplay(),
                    Value = x.Place.Guid
                })
                .ToList();

            var categories = CategoryServiceCache.AllWithAncestors();
            Categories = categories
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();

            var artefacts = ArtefactServiceCache.AllMadeBy();
            Artefacts = artefacts
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                .ToList();

            var agents = AgentServiceCache.AllWithAttributes();
            Agents = agents
                .OrderBy(x => x.Agent.FullName)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Agent.Guid })
                .ToList();


            var blank = new SelectListItem { };
            Agents.Insert(0, blank);
            Artefacts.Insert(0, blank);
            Categories.Insert(0, blank);
            Places.Insert(0, blank);

            Milennia = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "AD", Value = "AD"},
                new SelectListItem { Text = "BC", Value = "BC"}
            };

            Months = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text="01 January", Value = "1" },
                new SelectListItem { Text="02 February", Value = "2" },
                new SelectListItem { Text="03 March", Value = "3" },
                new SelectListItem { Text="04 April", Value = "4" },
                new SelectListItem { Text="05 May", Value = "5" },
                new SelectListItem { Text="06 June", Value = "6" },
                new SelectListItem { Text="07 July", Value = "7" },
                new SelectListItem { Text="08 August", Value = "8" },
                new SelectListItem { Text="09 September", Value = "9" },
                new SelectListItem { Text="10 October", Value = "10" },
                new SelectListItem { Text="11 November", Value = "11" },
                new SelectListItem { Text="12 December", Value = "12" },
                new SelectListItem { Text="   Spring", Value = "13" },
                new SelectListItem { Text="   Summer", Value = "14" },
                new SelectListItem { Text="   Autumn", Value = "15" },
                new SelectListItem { Text="   Winter", Value = "16" }
            };
        }
    }
    public class EditActModel : Act
    {
        [AllowHtml]
        public new string Description { get; set; }
    }

    public class EditAct : IValidatableObject
    {
        public EditActModel Act { get; set; }

        public Proximity ProximityModel { get; set; }
        public Proximity ProximityEndModel { get; set; }

        public string AgentGuid { get; set; }
        public string PlaceGuid { get; set; }
        public string CategoryGuid { get; set; }
        public string ArtefactGuid { get; set; }
        public string DocumentGuid { get; set; }
        public string SourceGuid { get; set; }
        public string StatedByGuid { get; set; }
        public string StatedByRelationGuid { get; set; }
        public string HasSourceGuid { get; set; }
        public string Attachments { get; set; }
        public string Tags { get; set; }

        public GenericRelationNode<Act, Act> SubsetOf { get; set; }

        public NewAct NewAct { get; set; }

        /**
         * Resources
         * */
        [ReadOnly(true)]
        public List<SelectListItem> Places { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Agents { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Artefacts { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Sources { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Videos { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Categories { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Containers { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Proximities { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Milennia { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Months { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> Documents { get; set; }

        public EditAct()
        {
            var places = PlaceServiceCache.AllWithContainers();
            Places = places
                .OrderBy(x => x.Place.Name)
                .Select(x => new SelectListItem
                {
                    Text = x.ToDisplay(),
                    Value = x.Place.Guid
                })
                .ToList();

            var categories = CategoryServiceCache.AllWithAncestors();
            Categories = categories
                .OrderBy(x => x.Child.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Child.Guid })
                .ToList();

            var artefacts = ArtefactServiceCache.AllMadeBy();
            Artefacts = artefacts
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                .ToList();

            var sources = ArtefactServiceCache.AllSources();
            Sources = sources
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                .ToList();

            var commonService = DependencyResolver.Current.GetService<ICommonService>();
            var videos = commonService.GetVideos();
            Videos = videos
                .OrderBy(x => x.Name)
                .Select(x => new SelectListItem { Text = x.Name, Value = x.Guid })
                .ToList();

            var agents = AgentServiceCache.AllWithAttributes();
            Agents = agents
                .OrderBy(x => x.Agent.FullName)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Agent.Guid })
                .ToList();

            var documents = ActServiceCache.AllDocumentHyperNodes();
            Documents = documents
                .Select(x => new SelectListItem { Text = x.SelectListItemText, Value = x.Document.Guid })
                .ToList();

            var actService = DependencyResolver.Current.GetService<IActService>();
            var containers = actService.FindAllContainers();
            Containers = containers
                .Select(x => new SelectListItem { Text = x.Name, Value = x.Guid })
                .ToList();

            var blank = new SelectListItem { };
            Agents.Insert(0, blank);
            Containers.Insert(0, blank);
            Artefacts.Insert(0, blank);
            Sources.Insert(0, blank);
            Categories.Insert(0, blank);
            Places.Insert(0, blank);
            Documents.Insert(0, blank);
            Videos.Insert(0, blank);

            Proximities = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "exactly", Value = "==" },
                new SelectListItem { Text = "around", Value = "c." },
                new SelectListItem { Text = "before", Value = "<" },
                new SelectListItem { Text = "after", Value = ">" },
                new SelectListItem { Text = "on or before", Value = ">=" },
                new SelectListItem { Text = "on or after", Value = "<=" },
            };

            Milennia = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text = "AD", Value = "AD"},
                new SelectListItem { Text = "BC", Value = "BC"}
            };

            Months = new List<SelectListItem>
            {
                blank,
                new SelectListItem { Text="01 January", Value = "1" },
                new SelectListItem { Text="02 February", Value = "2" },
                new SelectListItem { Text="03 March", Value = "3" },
                new SelectListItem { Text="04 April", Value = "4" },
                new SelectListItem { Text="05 May", Value = "5" },
                new SelectListItem { Text="06 June", Value = "6" },
                new SelectListItem { Text="07 July", Value = "7" },
                new SelectListItem { Text="08 August", Value = "8" },
                new SelectListItem { Text="09 September", Value = "9" },
                new SelectListItem { Text="10 October", Value = "10" },
                new SelectListItem { Text="11 November", Value = "11" },
                new SelectListItem { Text="12 December", Value = "12" },
                new SelectListItem { Text="------------", Value = "" },
                new SelectListItem { Text="   Spring", Value = "13" },
                new SelectListItem { Text="   Summer", Value = "14" },
                new SelectListItem { Text="   Autumn", Value = "15" },
                new SelectListItem { Text="   Winter", Value = "16" },
                new SelectListItem { Text="------------", Value = "" },
                new SelectListItem { Text="   Early", Value = "17" },
                new SelectListItem { Text="   Late", Value = "18" }

            };
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Act == null || false == Act.Year.HasValue)
            {
                yield return new ValidationResult("A date is required.", new string[] { "Act.Year" });
            }
            if (Act != null && Act.Container && Act.Name == null)
            {
                yield return new ValidationResult("A title is mandatory if this act is a container for other acts.", new string[] { "Act.Name " });
            }
        }
    }
}