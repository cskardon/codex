﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class ViewWorkedOn
    {
        public Agent Agent { get; set; }
        public Act Act { get; set; }
        public Place Place { get; set; }
        public string AgentOfGuid { get; set; }
        public string AtPlaceGuid { get; set; }
    }
}