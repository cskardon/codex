﻿using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditWorkedOnAnArtefact
    {
        public string ActGuid { get; set; }
        public Agent Agent { get; set; }
        public Artefact Artefact { get; set; }
        public string AnArtefactGuid { get; set; }
        public List<SelectListItem> Artefacts { get; set; }
        public EditWorkedOnAnArtefact()
        {
            var artefactService = DependencyResolver.Current.GetService<IArtefactService>();
            var artefacts = artefactService.All();
            Artefacts = artefacts
                .OrderBy(x => x.Name)
                .Select(x => new SelectListItem
                {
                    Text = x.Name,
                    Value = x.Guid
                })
                .ToList();
        }
    }
}