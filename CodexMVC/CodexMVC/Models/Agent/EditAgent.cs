﻿using Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class EditAgent : IValidatableObject
    {
        public string AgentGuid { get; set; }

        public string Article { get; set; }

        public string Name { get; set; }

        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        public Sex Sex { get; set; }

        public string Description { get; set; }

        public decimal? Rating { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Name == null)
            {
                if (FirstName == null || LastName == null)
                {
                    yield return new ValidationResult("Please enter either a full name (Name) or a first name and last name.", new[] { "Names" });
                }
            }
        }
    }
}