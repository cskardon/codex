﻿using CodexMVC.Helpers;
using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ByAgentTypeRepresentedModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string AgentTypeGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> AgentTypes { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByAgentTypeRepresentedModel()
        {
            var blank = new SelectListItem { };

            var agentTypes = DependencyResolver.Current.GetService<IAgentService>()
               .AgentTypesRepresentedWithArtefacts()
               .OrderBy(x => x.Name);
            AgentTypes = agentTypes.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            AgentTypes.Insert(0, blank);

            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }

    public class ByCategoryRepresentedModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string CategoryGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByCategoryRepresentedModel()
        {
            var blank = new SelectListItem { };

            var categories = DependencyResolver.Current.GetService<IAgentService>()
               .ObjectsRepresentedWithArtefacts()
               .OrderBy(x => x.Name);
            Categories = categories.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Categories.Insert(0, blank);

            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }

    public class ByAgentRepresentedModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string AgentGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Agents { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByAgentRepresentedModel()
        {
            var blank = new SelectListItem { };

            var agents = DependencyResolver.Current.GetService<IAgentService>()
               .AgentsRepresentedWithArtefacts()
               .OrderBy(x => x.FullName);
            Agents = agents.Select(x => new SelectListItem { Text = x.FullName2, Value = x.Guid }).ToList();
            Agents.Insert(0, blank);

            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }

    public class BySubjectModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string SubjectGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Subjects { get; set; }
        public List<SelectListItem> Places { get; set; }

        public BySubjectModel()
        {
            var blank = new SelectListItem { };

            var subjects = DependencyResolver.Current.GetService<IAgentService>()
               .SubjectsWithArtefacts()
               .OrderBy(x => x.Name);
            Subjects = subjects.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Subjects.Insert(0, blank);

            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }

    public class ByAestheticConventionModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string ConventionGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Conventions { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByAestheticConventionModel()
        {
            var blank = new SelectListItem { };

            var qualities = DependencyResolver.Current.GetService<IAgentService>()
               .AestheticConventionsWithArtefacts()
               .OrderBy(x => x.Name);
            Conventions = qualities.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Conventions.Insert(0, blank);



            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }

    public class ByMotifModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string MotifGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Motifs { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByMotifModel()
        {
            var blank = new SelectListItem { };

            var genres = DependencyResolver.Current.GetService<IAgentService>()
               .MotifsWithArtefacts()
               .OrderBy(x => x.Name);
            Motifs = genres.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Motifs.Insert(0, blank);

            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }
    public class UniversalSearchModel
    {
        [Required]
        public string Search { get; set; }

        public UniversalSearch Results { get; set; }
    }
    public class ByAgeModel
    {
        public int? Start { get; set; }
        public int? End { get; set; }

        public IEnumerable<ArtworksByAge> Result { get; set; }
    }
    public class ByExteriorModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByExteriorModel()
        {
            var blank = new SelectListItem { };

            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }

    public class ByGenreModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string GenreGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Genres { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByGenreModel()
        {
            var blank = new SelectListItem { };

            var genres = DependencyResolver.Current.GetService<IAgentService>()
                .GenresWithArtefacts()
                .OrderBy(x => x.Name);
            Genres = genres.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Genres.Insert(0, blank);


            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }

    public class ByMovementModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string MovementGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Movements { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByMovementModel()
        {
            var blank = new SelectListItem { };

            var movements = DependencyResolver.Current.GetService<IAgentService>()
                .MovementsWithArtefacts()
                .OrderBy(x => x.Name);
            Movements = movements.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Movements.Insert(0, blank);


            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }

    public class ByAgentModel
    {
        public string ArtworkTypeGuid { get; set; }
        public string ArtistGuid { get; set; }
        public string PlaceGuid { get; set; }

        public List<ArtefactsMadeByResult> Artefacts { get; set; }

        public List<SelectListItem> ArtworkTypes { get; set; }
        public List<SelectListItem> Artists { get; set; }
        public List<SelectListItem> Places { get; set; }

        public ByAgentModel()
        {
            var blank = new SelectListItem { };

            //var artists = DependencyResolver.Current.GetService<IAgentService>().GetArtistsWithTaxonomy(Label.Categories.Artist)
            //    .OrderBy(x => x.Agent.FullName);
            var artists = DependencyResolver.Current.GetService<IAgentService>()
                .ArtistsWithArtefacts()
                .OrderBy(x => x.FullName);
            Artists = artists.Select(x => new SelectListItem { Text = x.FullName2, Value = x.Guid }).ToList();
            Artists.Insert(0, blank);

            var places = DependencyResolver.Current.GetService<IAgentService>()
                .CitiesRegionsCountries()
                .OrderBy(x => x.Name);
            Places = places.Select(x => new SelectListItem { Text = x.Name, Value = x.Guid }).ToList();
            Places.Insert(0, blank);

            // ArtworkTypes = RazorCache.CategoryService.AllSubcategoriesOfArtwork();
            // ArtworkTypes.Insert(0, blank);
            ArtworkTypes = new List<SelectListItem>
            {
                new SelectListItem { Text = "All artworks", Value = "b0bab228-4fb7-49c4-805c-fa5414adcfd7" },
                new SelectListItem { Text = "Sculptures", Value = "70d88877-a7e3-49c9-a11b-1312c7e6d746" },
                new SelectListItem { Text = "Paintings", Value = "d5eccfb1-0893-409d-a4eb-70b5749bfd2a" },
                new SelectListItem { Text = "Drawings", Value = "209b0f53-56cc-4b2e-969b-37182ebcbbf2" },
                new SelectListItem { Text = "Prints", Value = "8541f437-31ef-4eee-8d04-dc4549433d90" },
                new SelectListItem { Text = "Architecture", Value = "11b1b458-1637-4b36-9f26-4e76121fe9ae" }
            };
        }
    }
    public class ViewOrderedList
    {
        public string DisciplineGuid { get; set; }

        public List<SelectListItem> Disciplines { get; set; }

        public IEnumerable<ArtistWithTaxonomy> Results { get; set; }

        public ViewOrderedList()
        {
            var blank = new SelectListItem { };
            Disciplines = RazorCache.CategoryService.Disciplines();
            Disciplines.Insert(0, blank);
        }
    }
    public class ViewSearchResults
    {
        public IEnumerable<ArtefactsMadeByResult> Artefacts { get; set; }
    }
    public class ViewTag
    {
        public Tag Tag { get; set; }
        public IEnumerable<GenericRelationNode<Tag, IEntity>> Outgoing { get; set; }
        public IEnumerable<GenericRelationNode<IEntity, Tag>> Incoming { get; set; }
    }
    public class ViewAgent
    {
        public List<HeatMapJson> TimelineHeatMap { get; set; }
        public List<IGrouping<string, MarkerJson>> TimelineMarkers { get; set; }
        public List<HeatMapJson> ArtworkHeatMap { get; set; }
        public List<IGrouping<string, ArtworkMarkerJson>> ArtworkMarkers { get; set; }
        public bool Bookmarked { get; set; }
        public IEnumerable<DocumentHyperNode> ReferredDocs { get; set; }
        public IEnumerable<DocumentHyperNode> Documents { get; set; }
        public IEnumerable<NewAct> NewActs { get; set; }
        public List<Website> Websites { get; set; }
        public List<Image> Images { get; set; }
        public Agent Agent { get; set; }
        public AgentWithAttribute AgentWithAttribute { get; set; }
        public List<AgentAttributeHyperNode> Relationships { get; set; }
        public List<AgentAttributeHyperNode> RelationshipsIncoming { get; set; }

        public IEnumerable<GenericRelationHyperNode<Agent, IEntity>> OutgoingRelations { get; set; }
        public IEnumerable<GenericRelationHyperNode<IEntity, Agent>> IncomingRelations { get; set; }
        public IEnumerable<GenericRelationNode<Agent, IEntity>> BasicOutgoingRelations { get; set; }
        public IEnumerable<GenericRelationNode<IEntity, Agent>> BasicIncomingRelations { get; set; }

        [ReadOnly(true)]
        public List<SelectListItem> FactTypes { get; set; }
    }
    public class Timeline
    {
        [ReadOnly(true)]
        public IEntity Agent { get; set; }

        public List<NewAct> NewActs { get; set; }
    }
    public class Timelines
    {
        public Dictionary<string, List<Timeline>> Time { get; set; }
    }
    public class ViewTimelineDates
    {
        public string Year { get; set; }
        public string YearEnd { get; set; }

        public List<NewAct> Timeline { get; set; }
    }
    public enum ArtefactListMode
    {
        ByArtwork,
        ByMedium
    }
    public enum CollectionListMode
    {
        ByCity,
        ByType
    }
    public enum AgentListMode
    {
        Everyone,
        ByArtist,
        ByCitizenship,
        ByRepresentativeOf,
        ByAttribute,
        ByDateOfBirth,
        ByDateOfDeath,
        ShowChart
    }
    public class ViewTourList
    {
        public IEnumerable<TourDetails> Tours { get; set; }
    }
    public class ViewExhibitionList
    {
        public IEnumerable<ExhibitionDetails> Exhibitions { get; set; }
    }
    public class ViewCollectionList
    {
        public CollectionListMode Mode { get; set; }
        public IEnumerable<CollectionDetails> Collections { get; set; }
    }
    public class ViewArtefactList
    {
        public ArtefactListMode Mode { get; set; }
        public IEnumerable<ArtefactMadeByAgent> Artefacts { get; set; }
    }
    public class ViewCharacterList
    {
        public IEnumerable<CharacterWithArtefacts> Characters { get; set; }
    }
    public class ViewMotifList
    {
        public IEnumerable<MotifWithArtefacts> Motifs { get; set; }
    }
    public class ViewSubjectList
    {
        public IEnumerable<SubjectWithArtefacts> Subjects { get; set; }
    }
    public class ViewAgentList
    {
        public AgentListMode Mode { get; set; }
        public IEnumerable<ArtistsWhoHaveArtworks> Artists { get; set; }
    }
    public class ViewAgentGraph
    {
        public Agent Agent { get; set; }
    }
    public class ViewIntersectCategoryTimeline
    {
        public string AgentGuid { get; set; }
        public string CategoryGuid { get; set; }
        public List<SelectListItem> Agents { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public List<NewAct> Timeline { get; set; }
        public ViewIntersectCategoryTimeline()
        {
            var blank = new SelectListItem { };
            Agents = RazorCache.AgentService.AllWithAttributes();
            Agents.Insert(0, blank);

            Categories = RazorCache.CategoryService.AllWithAncestors();
            Categories.Insert(0, blank);
        }
    }
    public class ViewCompareTimeline
    {
        public string Agent1 { get; set; }
        public string Place1 { get; set; }
        public string Artefact1 { get; set; }
        public string Category1 { get; set; }

        public string Agent2 { get; set; }
        public string Place2 { get; set; }
        public string Artefact2 { get; set; }
        public string Category2 { get; set; }

        public List<SelectListItem> Agents { get; set; }
        public List<SelectListItem> Categories { get; set; }
        public List<SelectListItem> Places { get; set; }
        public List<SelectListItem> Artefacts { get; set; }

        public Timelines Timelines { get; set; }
        public ViewCompareTimeline()
        {
            var blank = new SelectListItem { };
            Agents = RazorCache.AgentService.AllWithAttributes();
            Agents.Insert(0, blank);

            Categories = RazorCache.CategoryService.AllWithAncestors();
            Categories.Insert(0, blank);

            Places = RazorCache.PlaceService.AllWithContainers();
            Places.Insert(0, blank);

            Artefacts = RazorCache.ArtefactService.AllMadeBy();
            Artefacts.Insert(0, blank);
        }
    }
    public class TimelineJS
    {
        public IEntity[] Visitors { get; set; }
        public string Name { get; set; }
        public string SubTitle { get; set; }
        public IEnumerable<NewAct> Timeline { get; set; }
    }
    public class ViewAgentVerticalTimeline
    {
        public Agent Agent { get; set; }
        public IEnumerable<NewAct> Timeline { get; set; }
    }
    public class ViewCharacterPublic
    {
        public PersonalDetails Character { get; set; }
        public IEnumerable<ContainerWithArtefacts> ContainerWithArtefacts { get; set; }
    }
    public class ViewMotifPublic
    {
        public Category Motif { get; set; }
        public IEnumerable<ContainerWithArtefacts> ContainerWithArtefacts { get; set; }
    }
    public class ViewStylePublic
    {
        public Category Style { get; set; }
        public IEnumerable<ContainerWithArtefacts> ContainerWithArtefacts { get; set; }
    }
    public class ViewSubjectPublic
    {
        public Category Subject { get; set; }
        public IEnumerable<ContainerWithArtefacts> ContainerWithArtefacts { get; set; }
    }
    public class HeatMapJson
    {
        public decimal Weight { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
    }
    public class ArtworkMarkerJson : MarkerJson
    {
        public IEnumerable<decimal> Ratings { get; set; }
    }
    public class MarkerJson
    {
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Content { get; set; }
        public string ContainerName { get; set; }
    }
    public class ActJson
    {
        public Act Act { get; set; }
        public EntityJsonModel CreatedBy { get; set; }
        public EntityJsonModel Source { get; set; }
        public IEnumerable<EntityJsonModel> RefersTo { get; set; }
        public IEnumerable<EntityJsonModel> AssociatedWith { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
    }
    
    public class EntityJsonModel
    {
        public string Type { get; set; }
        public string Guid { get; set; }
        public string Name { get; set; }
        public string DescriptiveName { get; set; }
    }
    public class ViewAgentPublic
    {
        public UserAgentDetails UserAgentDetails { get; set; }
        public IEnumerable<HeatMapJson> TimelineHeatMap { get; set; }
        public IEnumerable<IGrouping<string, MarkerJson>> TimelineMarkers { get; set; }
        public IEnumerable<HeatMapJson> ArtworkHeatMap { get; set; }
        public List<IGrouping<string, ArtworkMarkerJson>> ArtworkMarkers { get; set; }

        public string AgentGuid { get; set; }
        public string PlaceGuid { get; set; }
        public IEnumerable<TourDetails> Tours { get; set; }
        public IEnumerable<ExhibitionDetails> Exhibitions { get; set; }
        public IEnumerable<ArtistWithRepresentativeArtwork> OutgoingConnexions { get; set; }
        public IEnumerable<NewAct> NewActs { get; set; }
        public IEnumerable<RelatedArtist> RelatedArtists { get; set; }
        public AgentWithAttribute Agent { get; set; }
        public GenericRelationHyperNode<Agent, Place> BornAt { get; set; }
        public GenericRelationHyperNode<Agent, Place> DiedAt { get; set; }
        public BaseActHyperNode Died { get; set; }
        public List<AgentAttributeHyperNode> Relationships { get; set; }
        public List<AgentAttributeHyperNode> RelationshipsIncoming { get; set; }
        public IEnumerable<ArtefactsMadeByResult> Artefacts { get; set; }
        public IEnumerable<ContainerWithArtefacts> ContainerWithArtefacts { get; set; }
        public PersonalDetails Details { get; set; }
    }
    //public class ViewPlacePublic
    //{
    //    public PlaceResult Place { get; set; }
    //    public List<IActHyperNode> Acts { get; set; }
    //    public IEnumerable<Category> Attributes { get; set; }
    //}
}