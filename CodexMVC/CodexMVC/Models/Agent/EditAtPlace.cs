﻿using CodexMVC.Helpers;
using Data;
using Data.Entities;
using Services;
using Services.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ViewCompareActionModel
    {
        [Required(ErrorMessage = "Please select an action")]
        public string ActionGuid1 { get; set; }
        [Required(ErrorMessage = "Please select an action")]
        public string ActionGuid2 { get; set; }

        public List<SelectListItem> Actions { get; set; }

        public Category Action1 { get; set; }
        public Category Action2 { get; set; }

        public Timelines Timelines { get; set; }

        public IEnumerable<IGrouping<string, MarkerJson>> Markers1 { get; set; }
        public IEnumerable<IGrouping<string, MarkerJson>> Markers2 { get; set; }
        public IEnumerable<HeatMapJson> Heatmap1 { get; set; }
        public IEnumerable<HeatMapJson> Heatmap2 { get; set; }

        public ViewCompareActionModel()
        {
            var BLANK = new SelectListItem { };

            Actions = RazorHelper.SelectListOfDescendants(Label.Categories.Verb);
            Actions.Insert(0, BLANK);
        }
    }
    public class ViewCompareModel
    {
        [Required(ErrorMessage = "Please select an artist")]
        public string ArtistGuid1 { get; set; }
        [Required(ErrorMessage = "Please select an artist")]
        public string ArtistGuid2 { get; set; }

        public List<SelectListItem> Artists { get; set; }

        public Timelines Timelines { get; set; }

        public Agent Agent1 { get; set; }
        public Agent Agent2 { get; set; }

        public IEnumerable<IGrouping<string, MarkerJson>> Markers1 { get; set; }
        public IEnumerable<IGrouping<string, MarkerJson>> Markers2 { get; set; }
        public IEnumerable<HeatMapJson> Heatmap1 { get; set; }
        public IEnumerable<HeatMapJson> Heatmap2 { get; set; }

        public ViewCompareModel()
        {
            var BLANK = new SelectListItem { };

            Artists = RazorHelper.SelectListOfAgentsByType(Label.Categories.Artist);
            Artists.Insert(0, BLANK);
        }
    }
    public class EditAtPlace : EditAgentRelation
    {
        public Place Place { get; set; }
        public string AtPlaceGuid { get; set; }
        public List<SelectListItem> Places { get; set; }
        public EditAtPlace()
        {
            var placeService = DependencyResolver.Current.GetService<IPlaceService>();
            var places = placeService.AllWithContainers();
            Places = places
                .OrderBy(x => x.Place.Name)
                .Select(x => new SelectListItem
                {
                    Text = x.Place.Name + " (" + string.Join(" > ", x.Containers.Select(x2 => x2.Name).ToArray()) + ")",
                    Value = x.Place.Guid
                })
                .ToList();
        }
    }
}