﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class ViewHistory
    {
        public List<History> Items { get; set; }
    }
    public class History
    {
        public string EntityType { get; set; }
        public string EntityGuid { get; set; }
        public DateTimeOffset Date { get; set; }
        public string Event { get; set; }
    }
}