﻿using Data;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class EditAlias
    {
        public Agent Agent { get; set; }
        public string Alias { get; set; }
    }
    public class EditArtefactAlias
    {
        public Artefact Artefact { get; set; }
        public string Alias { get; set; }
    }
}