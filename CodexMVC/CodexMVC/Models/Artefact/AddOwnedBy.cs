﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddOwnedBy
    {
        public Artefact Artefact { get; set; }

        /// <summary>
        /// Substance
        /// </summary>
        public string OwnerGuid { get; set; }

        public List<SelectListItem> Agents { get; set; }

        public AddOwnedBy()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var agents = agentService.AllPeople();
            Agents = agents.Select(x => new System.Web.Mvc.SelectListItem { Text = x.FullName, Value = x.Guid })
                           .ToList();
        }
    }
}