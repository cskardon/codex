﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddArtefactApplies
    {
        public Artefact Artefact { get; set; }

        /// <summary>
        /// Technique
        /// </summary>
        public string CategoryGuid { get; set; }

        public List<SelectListItem> Techniques { get; set; }

        public AddArtefactApplies()
        {
            var categoryService = DependencyResolver.Current.GetService<ICategoryService>();
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            Techniques = categoryService.FindChildren(Label.Categories.Technique)
                .Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid })
                .ToList();
        }
    }
}