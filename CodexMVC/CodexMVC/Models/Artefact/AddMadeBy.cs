﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddMadeBy
    {
        public Artefact Artefact { get; set; }

        public string AgentGuid { get; set; }

        public List<SelectListItem> Agents { get; set; }

        public AddMadeBy()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var agents = agentService.AllWithAttributes()
                .OrderBy(x => x.Agent.FullName)
                .ToList();
            Agents = agents.Select(x => new System.Web.Mvc.SelectListItem { Text = x.ToDisplay(), Value = x.Agent.Guid }).ToList();
        }
    }
}