﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class ViewTour
    {
        public CollectionDetail Detail { get; set; }
        public IEnumerable<Category> IsA { get; set; }
        public Artefact Artefact { get; set; }

        public IEnumerable<GenericRelationHyperNode<Artefact, IEntity>> OutgoingRelations { get; set; }
        public IEnumerable<GenericRelationHyperNode<IEntity, Artefact>> IncomingRelations { get; set; }

        public IEnumerable<GenericRelationNode<Artefact, IEntity>> BasicOutgoingRelations { get; set; }
        public IEnumerable<GenericRelationNode<IEntity, Artefact>> BasicIncomingRelations { get; set; }

        public List<Image> Images { get; set; }
    }

    public class ViewCollection
    {
        public CollectionDetail Detail { get; set; }
        public IEnumerable<Category> IsA { get; set; }
        public Artefact Artefact { get; set; }
        public IEnumerable<NewAct> NewActs { get; set; }
        public IEnumerable<GenericRelationHyperNode<Artefact, IEntity>> OutgoingRelations { get; set; }
        public IEnumerable<GenericRelationHyperNode<IEntity, Artefact>> IncomingRelations { get; set; }

        public IEnumerable<GenericRelationNode<Artefact, IEntity>> BasicOutgoingRelations { get; set; }
        public IEnumerable<GenericRelationNode<IEntity, Artefact>> BasicIncomingRelations { get; set; }

        public List<Image> Images { get; set; }
    }

    public class ViewExhibition
    {
        public CollectionDetail Detail { get; set; }
        public IEnumerable<Category> IsA { get; set; }
        public Artefact Artefact { get; set; }

        public IEnumerable<GenericRelationHyperNode<Artefact, IEntity>> OutgoingRelations { get; set; }
        public IEnumerable<GenericRelationHyperNode<IEntity, Artefact>> IncomingRelations { get; set; }

        public IEnumerable<GenericRelationNode<Artefact, IEntity>> BasicOutgoingRelations { get; set; }
        public IEnumerable<GenericRelationNode<IEntity, Artefact>> BasicIncomingRelations { get; set; }

        public List<Image> Images { get; set; }
    }

    public class ViewArtefact
    {
        public CollectionDetail Detail { get; set; }
        public IEnumerable<NewAct> NewActs { get; set; }
        public IEnumerable<Category> IsA { get; set; }
        public IEnumerable<Category> MadeFrom { get; set; }
        public IEnumerable<ContextHyperNode> Contexts { get; set; }
        public Artefact Artefact { get; set; }

        public IEnumerable<GenericRelationHyperNode<Artefact, IEntity>> OutgoingRelations { get; set; }
        public IEnumerable<GenericRelationHyperNode<IEntity, Artefact>> IncomingRelations { get; set; }

        public IEnumerable<GenericRelationNode<Artefact, IEntity>> BasicOutgoingRelations { get; set; }
        public IEnumerable<GenericRelationNode<IEntity, Artefact>> BasicIncomingRelations { get; set; }

        public List<Image> Images { get; set; }
        public List<Text> Texts { get; set; }
    }
}