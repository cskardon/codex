﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditHasMotif
    {
        public Artefact Artefact { get; set; }

        public string MotifGuid { get; set; }

        public string HasMotifGuid { get; set; }

        public List<SelectListItem> Motifs { get; set; }

        public EditHasMotif()
        {
            var categoryService = DependencyResolver.Current.GetService<ICategoryService>();
            var motifs = categoryService.FindChildrenWithAncestors("Motif");
            Motifs = motifs.Select(x => new SelectListItem
            {
                Text = x.Child.Name + " (" + string.Join(" > ", x.Ancestors.Select(x2 => x2.Name).ToArray()) + ")",
                Value = x.Child.Guid
            }).ToList();
        }
    }
}