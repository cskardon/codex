﻿using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class EditWrittenBy
    {
        public Text Text { get; set; }
        public Artefact Artefact { get; set; }
        public string WrittenByGuid { get; set; }
        public string AuthorGuid { get; set; }
        public List<SelectListItem> Agents { get; set; }
        public EditWrittenBy()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var agents = agentService.AllPeople().OrderBy(x => x.FullName);
            Agents = agents.Select(x => new SelectListItem { Text = x.FullName, Value = x.Guid }).ToList();
        }
    }
    
}