﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddHasSubject
    {
        public Artefact Artefact { get; set; }

        public string SubjectGuid { get; set; }

        public List<SelectListItem> Subjects { get; set; }

        public AddHasSubject()
        {
            var categoryService = DependencyResolver.Current.GetService<ICategoryService>();
            var subjects = categoryService.FindChildren("Subject");
            Subjects = subjects.Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid })
                .OrderBy(x => x.Text)
                           .ToList();
        }
    }
}