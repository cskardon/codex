﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace CodexMVC.Models
{
    public class AddArtefactRepresentsPlace
    {
        [ReadOnly(true)]
        public string Name { get; set; }
        public string ArtGuid { get; set; }
        public string PlaceGuid { get; set; }
    }
}