﻿using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class AddMadeWith
    {
        public Artefact Artefact { get; set; }

        /// <summary>
        /// Implement
        /// </summary>
        public string ImplementGuid { get; set; }

        public List<SelectListItem> Implements { get; set; }

        public AddMadeWith()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var categories = agentService.AllSubcategoriesOf(Label.Categories.Implement);
            Implements = categories.Select(x => new System.Web.Mvc.SelectListItem { Text = x.Child.Name + " (is a " + string.Join(" ", x.Ancestors.Select(x2 => x2.Name.ToLower()).ToArray()) + ")", Value = x.Child.Guid })
                        .ToList();
        }
    }
}