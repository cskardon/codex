﻿using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ViewAllAgentsWithSharedAttributes
    {
        public List<Services.AgentsWithSameAttributes> Results = new List<AgentsWithSameAttributes>();

        [Required]
        public string AgentGuid { get; set; }

        public List<SelectListItem> Agents { get; set; }
        public ViewAllAgentsWithSharedAttributes()
        {
            var service = DependencyResolver.Current.GetService<IAgentService>();
            Agents = service.AllPeople().Select(x => new System.Web.Mvc.SelectListItem { Text = x.FullName, Value = x.Guid }).ToList();
        }
    }
}