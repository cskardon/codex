﻿using CodexMVC.Helpers;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ViewAllArtefactsWithSharedAttributes
    {
        public List<ArtefactsWithSameAttributes> Results = new List<Services.ArtefactsWithSameAttributes>();

        [Required]
        public string ArtefactGuid { get; set; }

        public int? Separation { get; set; }

        public List<object> Attributes { get; set; }

        public List<ArtefactImageTuple> Similar { get; set; }

        public ArtefactImageTuple ArtefactImage { get; set; }

        public IOrderedEnumerable<IGrouping<string, ArtefactsWithSameAttributes>> AttributeGrouping { get; set; }

        public IOrderedEnumerable<IGrouping<string, ArtefactsWithSameAttributes>> UsagesGrouping { get; set; }

        public IOrderedEnumerable<IGrouping<string, ArtefactsWithSameAttributes>> MotifsGrouping { get; set; }

        public IOrderedEnumerable<IGrouping<string, ArtefactsWithSameAttributes>> RepresentsGrouping { get; set; }

        public IOrderedEnumerable<IGrouping<string, ArtefactsWithSameAttributes>> RepresentsAgentsGrouping { get; set; }

        public IOrderedEnumerable<IGrouping<string, ArtefactsWithSameAttributes>> RepresentsPlacesGrouping { get; set; }

        public IOrderedEnumerable<IGrouping<string, ArtefactsRelatedToSimilarArtefacts>> RelationsToSimilarArtefacts { get; set; }

        public List<SelectListItem> Artefacts { get; set; }
        public ViewAllArtefactsWithSharedAttributes()
        {
            var artefacts = ArtefactServiceCache.AllMadeBy();
            Artefacts = artefacts
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                .ToList();
        }
    }
}