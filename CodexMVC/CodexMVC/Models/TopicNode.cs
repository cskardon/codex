﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class TopicNode
    {
        /**
         * Model
         */
        [ReadOnly(true)]
        public string TopicGuid { get; set; }
        [Required]
        public string Name { get; set; }
        public string ParentGuid { get; set; }
        /**
         * Entities
         */
        [ReadOnly(true)]
        public Topic Parent { get; set; }
        [ReadOnly(true)]
        public List<SelectListItem> Parents { get; set; }
    }
}