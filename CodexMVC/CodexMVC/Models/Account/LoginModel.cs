﻿using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{

    public class ViewTourModel
    {
        public TourDetails Tour { get; set; }
    }

    public class ViewExhibitionModel
    {
        public ExhibitionDetails Exhibition { get; set; }
    }

    public class ViewArtefactPublicModel
    {
        public string TripAdvisorJson { get; set; }
        public ViewArtefactPublic Item { get; set; }
    }

    public static class ValidationMessage
    {
        public const string Required = "Please enter a value for {0}.";
    }
    public class JoinModel : IValidatableObject
    {
        [Required(ErrorMessage = ValidationMessage.Required)]
        public string Email { get; set; }

        [DisplayName("First Name")]
        [Required(ErrorMessage = ValidationMessage.Required)]
        public string FirstName { get; set; }

        public int? Age { get; set; }

        public string Profession { get; set; }

        [DisplayName("Last Name")]
        [Required(ErrorMessage = ValidationMessage.Required)]
        public string LastName { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        public string Password { get; set; }

        [Required(ErrorMessage = ValidationMessage.Required)]
        public string PasswordConfirmation { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Email != null)
            {
                var userService = DependencyResolver.Current.GetService<IUserService>();
                var user = userService.FindUser(Email);
                if (user != null)
                {
                    yield return new ValidationResult("There is another user with this email already registered", new string[] { "Email" });
                }
            }
            if (Password != null && PasswordConfirmation != null)
            {
                if (Password != PasswordConfirmation)
                {
                    yield return new ValidationResult("The passwords must be the same", new string[] { "PasswordConfirmation" });
                }
            }
        }
    }
    public class ResetPasswordFromEmailModel : IValidatableObject
    {
        public string UserName { get; set; }

        public string TokenGuid { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string NewPasswordConfirmation { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (NewPassword != null && NewPasswordConfirmation != null)
            {
                if (NewPassword != NewPasswordConfirmation)
                {
                    yield return new ValidationResult("The passwords must be the same", new string[] { "PasswordConfirmation" });
                }
            }
        }

    }
    public class ForgotPasswordModel
    {
        [Required]
        public string Email { get; set; }
    }
    public class ResetPasswordModel : IValidatableObject
    {
        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string NewPasswordConfirmation { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (NewPassword != null && NewPasswordConfirmation != null)
            {
                if (NewPassword != NewPasswordConfirmation)
                {
                    yield return new ValidationResult("The passwords must be the same", new string[] { "PasswordConfirmation" });
                }
            }
        }
    }
    public class AdminResetPasswordModel : ResetPasswordFromEmailModel
    {
        public string UserGuid { get; set; }
    }
    public class LoginModel
    {
        public string ReturnUrl { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public bool Remember { get; set; }
    }
}