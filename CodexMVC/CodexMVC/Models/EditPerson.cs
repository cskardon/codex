﻿using CodexMVC.Helpers;
using Data;
using Data.Entities;
using Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages.Html;

namespace CodexMVC.Models
{
    public class BasicRelationshipNode
    {
        public string AgentOf { get; set; }
        public string AgentName { get; set; }
        public string RelationshipGuid { get; set; }
        public string Other { get; set; }
        public List<System.Web.Mvc.SelectListItem> Others { get; set; }
        public BasicRelationshipNode()
        {
            Others = RazorCache.AgentService.AllWithAttributes();
        }
    }
    public class AddFriendNode : BasicRelationshipNode
    {
    }
    public class AddKnownsNode : BasicRelationshipNode
    {
    }
    public class EditRelatedTo
    {
        public Agent Agent { get; set; }
        public string RelationshipGuid { get; set; }
        public string RelativeGuid { get; set; }
        public List<System.Web.Mvc.SelectListItem> Categories { get; set; }
        public List<System.Web.Mvc.SelectListItem> Agents { get; set; }
        public EditRelatedTo()
        {
            Categories = RazorCache.CategoryService.AllSubcategoriesOfFamilialRelation();
            Agents = RazorCache.AgentService.AllWithAttributes();
        }
    }
    public class AddStudentOf
    {
        public string AgentOf { get; set; }
        public string AgentName { get; set; }
        public string RelationshipGuid { get; set; }
        public string Master { get; set; }
        public List<System.Web.Mvc.SelectListItem> Masters { get; set; }
    }
    public class OrganisationNode
    {
        [ReadOnly(true)]
        public string Organisation { get; set; }
        [Required]
        public string Name { get; set; }
    }





    public class AddAgentRecognisabilityByCategory
    {
        public Agent Agent { get; set; }

        public string CategoryGuid { get; set; }

        [Required]
        public int? Rank { get; set; }

        public List<System.Web.Mvc.SelectListItem> Categories { get; set; }

        public AddAgentRecognisabilityByCategory()
        {
            var categoryService = DependencyResolver.Current.GetService<ICategoryService>();
            var categories = categoryService.AllWithAncestors();
            var childWithAncestors = categories.Select(x => new System.Web.Mvc.SelectListItem { Text = x.Child.Name + " (is a " + string.Join(" ", x.Ancestors.Select(x2 => x2.Name.ToLower()).ToArray()) + ")", Value = x.Child.Guid })
                        .OrderBy(x => x.Text)
                        .ToList();
            Categories = childWithAncestors;
        }
    }
    //
    public class AddRecognisabilityByAgent
    {
        public Artefact Artefact { get; set; }

        public string AgentGuid { get; set; }

        [Required]
        public int? Rank { get; set; }

        public List<System.Web.Mvc.SelectListItem> Agents { get; set; }

        public AddRecognisabilityByAgent()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var agents = agentService.AllPeople();
            Agents = agents.Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid })
                        .OrderBy(x => x.Text)
                        .ToList();
        }
    }
    public class AddRecognisabilityByCategory
    {
        public Artefact Artefact { get; set; }

        public string CategoryGuid { get; set; }

        [Required]
        public int? Rank { get; set; }

        public List<System.Web.Mvc.SelectListItem> Categories { get; set; }

        public AddRecognisabilityByCategory()
        {
            var categoryService = DependencyResolver.Current.GetService<ICategoryService>();
            var categories = categoryService.AllWithAncestors();
            var childWithAncestors = categories.Select(x => new System.Web.Mvc.SelectListItem { Text = x.Child.Name + " (is a " + string.Join(" ", x.Ancestors.Select(x2 => x2.Name.ToLower()).ToArray()) + ")", Value = x.Child.Guid })
                        .OrderBy(x => x.Text)
                        .ToList();
            Categories = childWithAncestors;
        }
    }

    public class AgentAddLocatedAt
    {
        public Agent Agent { get; set; }

        public string PlaceGuid { get; set; }

        public List<System.Web.Mvc.SelectListItem> Places { get; set; }

        public AgentAddLocatedAt()
        {
            var placeService = DependencyResolver.Current.GetService<IPlaceService>();
            //var places = placeService.All();
            //Places = places.Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid })
            //               .ToList();
            var places = placeService.AllWithContainers();
            Places = places.Select(x => new System.Web.Mvc.SelectListItem
            {
                Text = x.Place.Name + " (" + string.Join(" > ", x.Containers.Select(x2 => x2.Name).ToArray()) + ")",
                Value = x.Place.Guid
            })
            .ToList();
        }
    }
    //



    //


    public class AddCommissionedBy
    {
        public Artefact Artefact { get; set; }

        public string AgentGuid { get; set; }

        public List<System.Web.Mvc.SelectListItem> Agents { get; set; }

        public AddCommissionedBy()
        {
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            var agents = AgentServiceCache.AllWithAttributes();
            Agents = agents.Select(x => new System.Web.Mvc.SelectListItem { Text = x.ToDisplay(), Value = x.Agent.Guid }).ToList();
        }
    }
    //


    //

    public class EditText
    {
        public Artefact Artefact { get; set; }

        /// <summary>
        /// Technique
        /// </summary>
        public string CategoryGuid { get; set; }

        public List<System.Web.Mvc.SelectListItem> Techniques { get; set; }

        public EditText()
        {
            var categoryService = DependencyResolver.Current.GetService<ICategoryService>();
            var agentService = DependencyResolver.Current.GetService<IAgentService>();
            Techniques = categoryService.FindChildren(Label.Categories.Technique)
                .Select(x => new System.Web.Mvc.SelectListItem { Text = x.Name, Value = x.Guid })
                .ToList();
        }
    }

    public class EditEntityWebsite
    {
        public string EntityGuid { get; set; }

        public string WebsiteGuid { get; set; }

        public bool? Primary { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter a uri")]
        public string Uri { get; set; }

        public string Description { get; set; }
    }

    public class AddArtAttribute
    {
        public string ArtGuid { get; set; }
        public string CategoryGuid { get; set; }
    }
    //public class AddDocumentToFact
    //{
    //    public string AgentGuid { get; set; }
    //    public string AgentName { get; set; }
    //    public string FactGuid { get; set; }
    //    public string Text { get; set; }
    //}
    //public class AddStickyNoteToFact
    //{
    //    public string AgentGuid { get; set; }
    //    public string AgentName { get; set; }
    //    public string FactGuid { get; set; }
    //    public string Text { get; set; }
    //}
    public class EditCategory
    {
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Adjective { get; set; }
        public string Description { get; set; }
        public bool Clade { get; set; }
    }
    public class AddArt
    {
        public string ArtGuid { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string CreatorGuid { get; set; }
        public string CategoryGuid { get; set; }
    }

    public class EditArt : AddArt
    {
        public Artefact Artefact { get; set; }
    }
    public class ViewCategory
    {
        public IEnumerable<NewAct> NewActs { get; set; }
        public Category Category { get; set; }
        public List<CategoryRelatedToOtherCategoryHyperNode> Attributes { get; set; }
        public List<CategoryRelatedToOtherCategoryHyperNode> AttributesIncoming { get; set; }
        public List<CategoryToArtefactHyperNode> CategoryToArtefactIncoming { get; set; }
    }


    public class EditPlaceModel
    {
        [Required]
        public string Name { get; set; }

        public string Adjective { get; set; }

        public string Article { get; set; }

        public string Description { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public int? TimeZoneOffset { get; set; }

        public string ImageUrl { get; set; }

        public string PlaceGuid { get; set; }
    }
    public class BaseArtefactModel
    {
        //public string ArtefactGuid { get; set; }

        [Required]
        public string Name { get; set; }

        public string CategoryGuid { get; set; }

        public string MadeByGuid { get; set; }

        public string Article { get; set; }

        public string Description { get; set; }

        public string Subtitle { get; set; }

        public List<System.Web.Mvc.SelectListItem> Categories { get; set; }

        public List<System.Web.Mvc.SelectListItem> Agents { get; set; }

        public List<System.Web.Mvc.SelectListItem> Artefacts { get; set; }

        public List<System.Web.Mvc.SelectListItem> Places { get; set; }

        public BaseArtefactModel()
        {
            var blank = new System.Web.Mvc.SelectListItem();

            Categories = RazorCache.CategoryService.AllWithAncestors();
            Categories.Insert(0, blank);

            Agents = RazorCache.AgentService.AllWithAttributes();
            Agents.Insert(0, blank);

            Artefacts = RazorCache.ArtefactService.AllMadeBy();
            Artefacts.Insert(0, blank);

            Places = RazorCache.PlaceService.AllWithContainers();
            Places.Insert(0, blank);
        }
    }
    public class AddMusicModel : BaseArtefactModel
    {
        public string CatalogueIdent { get; set; }
        public string CatalogueIdentPrefix { get; set; }
        public string ScoredFor1Guid { get; set; }
        public string ScoredFor2Guid { get; set; }
        public string ScoredFor3Guid { get; set; }
        public string ScoredFor4Guid { get; set; }
    }
    public class AddMusicalPerformanceModel : BaseArtefactModel
    {
        public string MusicGuid { get; set; }
        public string VenueOfGuid { get; set; }
        public string ConductorOfGuid { get; set; }
        /**
         * For the performers we will need the GenericRelationHyperNodeDouble to capture
         * both [a] the performer and [b] the instrument.
         */
    }
    public class BaseAgentModel
    {
        public string AgentGuid { get; set; }

        public string Name { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Article { get; set; }

        public string Description { get; set; }

        public string IsA1Guid { get; set; }

        public string IsA2Guid { get; set; }

        public string CitizenOfGuid { get; set; }

        

        public string RepresentativeOfGuid { get; set; }

        public string ImageUrl { get; set; }

        public string ImageName { get; set; }

        public Sex Sex { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Image { get; set; }

        public List<System.Web.Mvc.SelectListItem> Categories { get; set; }

        public List<System.Web.Mvc.SelectListItem> Agents { get; set; }

        public List<System.Web.Mvc.SelectListItem> Artists { get; set; }

        public List<System.Web.Mvc.SelectListItem> Artefacts { get; set; }

        public List<System.Web.Mvc.SelectListItem> Places { get; set; }

        public int? Rating { get; set; }

        public BaseAgentModel()
        {
            var BLANK = new System.Web.Mvc.SelectListItem();

            Artists = RazorHelper.SelectListOfAgentsByType(Label.Categories.Artist);
            Artists.Insert(0, BLANK);

            Categories = RazorCache.CategoryService.AllWithAncestors();
            Categories.Insert(0, BLANK);

            Agents = RazorCache.AgentService.AllWithAttributes();
            Agents.Insert(0, BLANK);

            Artefacts = RazorCache.ArtefactService.AllMadeBy();
            Artefacts.Insert(0, BLANK);

            Places = RazorCache.PlaceService.AllWithContainers();
            Places.Insert(0, BLANK);
        }
    }
    public class EditContextModel
    {
        public ContextHyperNode Node { get; set; }

        public List<System.Web.Mvc.SelectListItem> Categories { get; set; }

        public List<System.Web.Mvc.SelectListItem> Artefacts { get; set; }

        public List<System.Web.Mvc.SelectListItem> Places { get; set; }

        public EditContextModel()
        {
            var blank = new System.Web.Mvc.SelectListItem();

            Categories = RazorCache.CategoryService.AllWithAncestors();
            Categories.Insert(0, blank);

            Artefacts = RazorCache.ArtefactService.AllMadeBy();
            Artefacts.Insert(0, blank);

            Places = RazorCache.PlaceService.AllWithContainers();
            Places.Insert(0, blank);
        }
    }
    public class EditDetailModel
    {
        public Artefact Artefact { get; set; }
        public string DetailGuid { get; set; }
        public List<string> OpenDayStart { get; set; }
        public List<string> OpenDayEnd { get; set; }
        public string OpenDayOther { get; set; }
        public string Address { get; set; }
        public string Directions { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string AffiliationCode { get; set; }
        public string PurchaseTicketUrl { get; set; }
        public List<System.Web.Mvc.SelectListItem> Hours { get; set; }
        public EditDetailModel()
        {
            Hours = new List<System.Web.Mvc.SelectListItem>();
            for (var i = 0; i <= 23; i++)
            {
                var hour = new System.Web.Mvc.SelectListItem
                {
                    Value = i.ToString("00"),
                    Text = string.Format("{0}:00 {1}", i.ToString("00"), i < 12 ? "AM" : "PM")
                };
                Hours.Add(hour);
            }
            Hours.Insert(0, new System.Web.Mvc.SelectListItem { });
        }
    }
    public class AddAgentModel : BaseAgentModel
    {

    }
    public class AddArtistModel : BaseAgentModel
    {
        public string StudentOfGuid { get; set; }

        public string BornAtGuid { get; set; }

        public string BornAtMilennium { get; set; }

        public int? BornAtYear { get; set; }

        public int? BornAtMonth { get; set; }

        public int? BornAtDay { get; set; }

        public bool BornAtDecade { get; set; }

        public string DiedAtGuid { get; set; }

        public string DiedAtMilennium { get; set; }

        public int? DiedAtYear { get; set; }

        public int? DiedAtMonth { get; set; }

        public int? DiedAtDay { get; set; }

        public bool DiedAtDecade { get; set; }

        [ReadOnly(true)]
        public List<System.Web.Mvc.SelectListItem> Milennia { get; set; }

        [ReadOnly(true)]
        public List<System.Web.Mvc.SelectListItem> Months { get; set; }

        public List<System.Web.Mvc.SelectListItem> ArtistTypes { get; set; }

        public List<System.Web.Mvc.SelectListItem> Artists { get; set; }

        public List<System.Web.Mvc.SelectListItem> Styles { get; set; }

        public AddArtistModel()
        {
            var BLANK = new System.Web.Mvc.SelectListItem { };

            Styles = RazorHelper.SelectListOfDescendants(Label.Categories.ArtMovement);
            Styles.Insert(0, BLANK);

            ArtistTypes = RazorHelper.SelectListOfDescendants(Label.Categories.Artist);
            ArtistTypes.Insert(0, BLANK);

            Artists = RazorHelper.SelectListOfAgentsByType(Label.Categories.Artist);
            Artists.Insert(0, BLANK);

            Milennia = new List<System.Web.Mvc.SelectListItem>
            {
                BLANK,
                new System.Web.Mvc.SelectListItem { Text = "AD", Value = "AD"},
                new System.Web.Mvc.SelectListItem { Text = "BC", Value = "BC"}
            };

            Months = new List<System.Web.Mvc.SelectListItem>
            {
                BLANK,
                new System.Web.Mvc.SelectListItem { Text="01 January", Value = "1" },
                new System.Web.Mvc.SelectListItem { Text="02 February", Value = "2" },
                new System.Web.Mvc.SelectListItem { Text="03 March", Value = "3" },
                new System.Web.Mvc.SelectListItem { Text="04 April", Value = "4" },
                new System.Web.Mvc.SelectListItem { Text="05 May", Value = "5" },
                new System.Web.Mvc.SelectListItem { Text="06 June", Value = "6" },
                new System.Web.Mvc.SelectListItem { Text="07 July", Value = "7" },
                new System.Web.Mvc.SelectListItem { Text="08 August", Value = "8" },
                new System.Web.Mvc.SelectListItem { Text="09 September", Value = "9" },
                new System.Web.Mvc.SelectListItem { Text="10 October", Value = "10" },
                new System.Web.Mvc.SelectListItem { Text="11 November", Value = "11" },
                new System.Web.Mvc.SelectListItem { Text="12 December", Value = "12" },
                new System.Web.Mvc.SelectListItem { Text="------------", Value = "" },
                new System.Web.Mvc.SelectListItem { Text="   Spring", Value = "13" },
                new System.Web.Mvc.SelectListItem { Text="   Summer", Value = "14" },
                new System.Web.Mvc.SelectListItem { Text="   Autumn", Value = "15" },
                new System.Web.Mvc.SelectListItem { Text="   Winter", Value = "16" },
                new System.Web.Mvc.SelectListItem { Text="------------", Value = "" },
                new System.Web.Mvc.SelectListItem { Text="   Early", Value = "17" },
                new System.Web.Mvc.SelectListItem { Text="   Late", Value = "18" }

            };
        }
    }
    public class AddTourModel : BaseArtefactModel
    {
        public string Url { get; set; }
        public int? Rating { get; set; }

        public string LocatedAtGuid { get; set; }

        public string Artist1Guid { get; set; }
        public string Artist2Guid { get; set; }
        public string Artist3Guid { get; set; }

        public string Artwork1Guid { get; set; }
        public string Artwork2Guid { get; set; }
        public string Artwork3Guid { get; set; }

        public string StopsIn1Guid { get; set; }
        public string StopsIn2Guid { get; set; }
        public string StopsIn3Guid { get; set; }

        /// <summary>
        /// Renaissance, Baroque
        /// </summary>
        public string StyleGuid { get; set; }
        public string MediumGuid { get; set; }
        /// <summary>
        /// Portraits, Landscapes, Sketches
        /// </summary>
        public string GenreGuid { get; set; }

        public List<System.Web.Mvc.SelectListItem> Artists { get; set; }
        public List<System.Web.Mvc.SelectListItem> Artworks { get; set; }
        public List<System.Web.Mvc.SelectListItem> Collections { get; set; }
        public List<System.Web.Mvc.SelectListItem> Styles { get; set; }
        public List<System.Web.Mvc.SelectListItem> Genres { get; set; }

        public AddTourModel()
        {
            var empty = new System.Web.Mvc.SelectListItem { };

            Genres = RazorHelper.SelectListOfDescendants(Label.Categories.ArtisticGenre);
            Genres.Insert(0, empty);

            Styles = RazorHelper.SelectListOfDescendants(Label.Categories.ArtMovement);
            Styles.Insert(0, empty);

            Artists = RazorHelper.SelectListOfAgentsByType(Label.Categories.Artist);
            Artists.Insert(0, empty);

            var artefacts = ArtefactServiceCache.AllMadeBy();
            Artworks = artefacts
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new System.Web.Mvc.SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                .ToList();
            Artworks.Insert(0, empty);

            Collections = RazorHelper.SelectListOfArtefactsByType(Label.Categories.Collection);
            Collections.Insert(0, empty);
        }
    }

    public class AddExhibitionModel : BaseArtefactModel
    {
        public string Url { get; set; }
        public int DayStart { get; set; }
        public int MonthStart { get; set; }
        public int YearStart { get; set; }
        public int? DayEnd { get; set; }
        public int? MonthEnd { get; set; }
        public int? YearEnd { get; set; }

        public int? Rating { get; set; }

        /// <summary>
        /// National Gallery of Art, The Louvre, QAG
        /// </summary>
        public string LocatedInGuid { get; set; }

        public string Artist1Guid { get; set; }
        public string Artist2Guid { get; set; }
        public string Artist3Guid { get; set; }

        public string Artwork1Guid { get; set; }
        public string Artwork2Guid { get; set; }
        public string Artwork3Guid { get; set; }

        /// <summary>
        /// Renaissance, Baroque
        /// </summary>
        public string StyleGuid { get; set; }
        public string MediumGuid { get; set; }
        /// <summary>
        /// Portraits, Landscapes, Sketches
        /// </summary>
        public string GenreGuid { get; set; }

        public List<System.Web.Mvc.SelectListItem> Artists { get; set; }
        public List<System.Web.Mvc.SelectListItem> Artworks { get; set; }
        public List<System.Web.Mvc.SelectListItem> Collections { get; set; }
        public List<System.Web.Mvc.SelectListItem> Styles { get; set; }
        public List<System.Web.Mvc.SelectListItem> Genres { get; set; }

        public List<System.Web.Mvc.SelectListItem> Days { get; set; }
        public List<System.Web.Mvc.SelectListItem> Months { get; set; }
        public List<System.Web.Mvc.SelectListItem> Years { get; set; }

        public AddExhibitionModel()
        {
            var empty = new System.Web.Mvc.SelectListItem { };

            Genres = RazorHelper.SelectListOfDescendants(Label.Categories.ArtisticGenre);
            Genres.Insert(0, empty);

            Styles = RazorHelper.SelectListOfDescendants(Label.Categories.ArtMovement);
            Styles.Insert(0, empty);

            Artists = RazorHelper.SelectListOfAgentsByType(Label.Categories.Artist);
            Artists.Insert(0, empty);

            var artefacts = ArtefactServiceCache.AllMadeBy();
            Artworks = artefacts
                .OrderBy(x => x.Artefact.Name)
                .Select(x => new System.Web.Mvc.SelectListItem { Text = x.ToDisplay(), Value = x.Artefact.Guid })
                .ToList();
            Artworks.Insert(0, empty);

            Collections = RazorHelper.SelectListOfArtefactsByType(Label.Categories.Collection);
            Collections.Insert(0, empty);

            Days = Enumerable.Range(1, 31).Select(x => new System.Web.Mvc.SelectListItem { Value = x.ToString(), Text = x.ToString("00") }).ToList();
            Days.Insert(0, empty);

            Months = Enumerable.Range(1, 12).Select(x => new System.Web.Mvc.SelectListItem { Value = x.ToString(), Text = x.ToString("00") }).ToList();
            Months.Insert(0, empty);

            var year = DateTime.Now.Year - 5;
            Years = Enumerable.Range(0, 9).Select(x => new System.Web.Mvc.SelectListItem { Value = (x + year).ToString(), Text = (x + year).ToString() }).ToList();
            Years.Insert(0, empty);
        }
    }

    public class AddCollectionModel : BaseArtefactModel, IImageUploadModel
    {
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? Rating { get; set; }
        public int? Ordinal { get; set; }
        public int?  TripAdvisorLocationId { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Image { get; set; }

        public string ImageUrl { get; set; }

        public string Source { get; set; }

        public bool PublicDomain { get; set; }

        public string LocatedInGuid { get; set; }
        public string LocatedAtGuid { get; set; }
        public List<string> OpenDayStart { get; set; }
        public List<string> OpenDayEnd { get; set; }
        public string OpenDayOther { get; set; }
        public string Address { get; set; }
        public string Directions { get; set; }
        public string Website { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string AffiliationCode { get; set; }
        public string PurchaseTicketUrl { get; set; }

        public List<System.Web.Mvc.SelectListItem> Hours { get; set; }

        public List<System.Web.Mvc.SelectListItem> Collections { get; set; }

        public List<System.Web.Mvc.SelectListItem> CollectionTypes { get; set; }


        public AddCollectionModel()
        {
            CollectionTypes = RazorHelper.SelectListOfDescendants(Label.Categories.Collection);
            CollectionTypes.Insert(0, new System.Web.Mvc.SelectListItem { });

            Collections = RazorHelper.SelectListOfArtefactsByType(Label.Categories.Collection);
            Collections.Insert(0, new System.Web.Mvc.SelectListItem { });

            Hours = new List<System.Web.Mvc.SelectListItem>();
            for (var i = 8; i <= 20; i++)
            {
                var hour = new System.Web.Mvc.SelectListItem
                {
                    Value = i.ToString(),
                    Text = string.Format("{0}:00 {1}", i.ToString("00"), i < 12 ? "AM" : "PM")
                };
                Hours.Add(hour);
            }
            Hours.Insert(0, new System.Web.Mvc.SelectListItem { });
        }
    }
    public interface IImageUploadModel
    {
        string Name { get; set; }

        [DataType(DataType.Upload)]
        HttpPostedFileBase Image { get; set; }

        bool PublicDomain { get; set; }

        string ImageUrl { get; set; }

        string Source { get; set; }
    }
    public class AddArtworkModel : BaseArtefactModel, IImageUploadModel, IValidatableObject
    {
        public bool Refresh { get; set; }

        public bool Clone { get; set; }

        public Range Range { get; set; }

        public string YearMadeStart { get; set; }

        public string YearMadeEnd { get; set; }

        public int? Ordinal { get; set; }

        public string ImageUrl { get; set; }

        public string Source { get; set; }

        [DataType(DataType.Upload)]
        public HttpPostedFileBase Image { get; set; }

        public bool PublicDomain { get; set; }

        public decimal? Width { get; set; }

        public decimal? Height { get; set; }

        public decimal? Depth { get; set; }

        public string MadeFromFirstGuid { get; set; }

        public string MadeFromSecondGuid { get; set; }

        public string LocatedInGuid { get; set; }

        public string SubjectGuid { get; set; }

        public string GenreGuid { get; set; }

        public string Represents1Guid { get; set; }

        public string Represents2Guid { get; set; }

        //public string Represents3Guid { get; set; }

        public int? Rating { get; set; }

        public List<System.Web.Mvc.SelectListItem> Artists { get; set; }

        public List<System.Web.Mvc.SelectListItem> ArtworkTypes { get; set; }

        public List<System.Web.Mvc.SelectListItem> Substances { get; set; }

        public List<System.Web.Mvc.SelectListItem> Subjects { get; set; }

        public List<System.Web.Mvc.SelectListItem> Genres { get; set; }

        public List<System.Web.Mvc.SelectListItem> Collections { get; set; }

        public AddArtworkModel()
        {
            Artists = RazorHelper.SelectListOfAgentsByType(Label.Categories.Artist);
            Artists.Insert(0, new System.Web.Mvc.SelectListItem { });

            ArtworkTypes = RazorHelper.SelectListOfDescendants(Label.Categories.Artwork);
            ArtworkTypes.Insert(0, new System.Web.Mvc.SelectListItem { });

            Substances = RazorHelper.SelectListOfDescendants(Label.Categories.Substance);
            Substances.Insert(0, new System.Web.Mvc.SelectListItem { });

            Subjects = RazorHelper.SelectListOfDescendants(Label.Categories.ArtisticSubject);
            Subjects.Insert(0, new System.Web.Mvc.SelectListItem { });

            Genres = RazorHelper.SelectListOfDescendants(Label.Categories.ArtisticGenre);
            Genres.Insert(0, new System.Web.Mvc.SelectListItem { });

            Collections = RazorHelper.SelectListOfArtefactsByType(Label.Categories.Collection);
            Collections.Insert(0, new System.Web.Mvc.SelectListItem { });
        }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Image != null && false == string.IsNullOrEmpty(ImageUrl))
            {
                yield return new ValidationResult("You cannot upload an image and specify the URL at the same time.", new string[] { "Image" });
            }
        }
    }
    public class AddBookModel : BaseArtefactModel
    {
        public bool Refresh { get; set; }

        public bool Clone { get; set; }

        public Range Range { get; set; }

        public string YearMadeStart { get; set; }

        public string YearMadeEnd { get; set; }

        public string AuthorOfGuid { get; set; }

        public string PublishedByGuid { get; set; }

        public string TranslatedByGuid { get; set; }

        public int? Rating { get; set; }

        public List<System.Web.Mvc.SelectListItem> Writers { get; set; }

        public List<System.Web.Mvc.SelectListItem> Publishers { get; set; }

        public List<System.Web.Mvc.SelectListItem> PublicationTypes { get; set; }

        public AddBookModel()
        {
            Writers = RazorHelper.SelectListOfAgentsByType(Label.Categories.Writer);
            Writers.Insert(0, new System.Web.Mvc.SelectListItem { });

            Publishers = RazorHelper.SelectListOfAgentsByType(Label.Categories.Publisher);
            Publishers.Insert(0, new System.Web.Mvc.SelectListItem { });

            PublicationTypes = RazorHelper.SelectListOfDescendants(Label.Categories.Publication);
            PublicationTypes.Insert(0, new System.Web.Mvc.SelectListItem { });
        }
    }
    public class EditArtefactModel
    {
        public string ArtefactGuid { get; set; }

        public int? TripAdvisorLocationId { get; set; }

        public int? Ordinal { get; set; }

        [Required]
        public string Name { get; set; }

        public string CatalogueIdentPrefix { get; set; }

        public string Article { get; set; }

        public string Description { get; set; }

        public string Subtitle { get; set; }

        public decimal? Width { get; set; }

        public decimal? Height { get; set; }

        public decimal? Depth { get; set; }

        public string CatalogueIdent { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? Rating { get; set; }
    }
    public class PlaceNode
    {
        [ReadOnly(true)]
        public string Place { get; set; }
        [Required]
        public string Name { get; set; }
        public string Inside { get; set; }
        public List<System.Web.Mvc.SelectListItem> ContainingPlaces { get; set; }
    }

    public class CategoryNode
    {
        [ReadOnly(true)]
        public string Category { get; set; }
        [Required]
        public string Name { get; set; }
        public string Parent { get; set; }
        public List<System.Web.Mvc.SelectListItem> InheritFrom { get; set; }
    }

    public class IsANode
    {
        public string AgentOf { get; set; }
        [ReadOnly(true)]
        public string AgentName { get; set; }
        public string Category { get; set; }
        [ReadOnly(true)]
        public List<Category> Categories { get; set; }

        public List<System.Web.Mvc.SelectListItem> ChildWithAncestors { get; set; }
    }
    public class AssociateFact
    {
        [Required]
        public string FactGuid { get; set; }
        [Required]
        public string TopicGuid { get; set; }
        [ReadOnly(true)]
        public List<System.Web.Mvc.SelectListItem> Topics { get; set; }
    }
    public class DiedNode
    {
        public string AgentOf { get; set; }
        public ActNode Fact { get; set; }
        public string AtPlace { get; set; }
        [ReadOnly(true)]
        public Data.Agent Agent { get; set; }
        public List<Place> Places { get; set; }
        public DiedNode()
        {
            Fact = new ActNode();
        }
    }
    public class ServedNode
    {
        public string AgentGuid { get; set; }
        public string AgentName { get; set; }
        public string LeaderGuid { get; set; }
        public string PlaceGuid { get; set; }
        public ActNode Fact { get; set; }
        public ServedNode()
        {
            Fact = new ActNode();
        }
    }
    public class MetNode
    {
        public string AgentGuid { get; set; }
        public string AgentName { get; set; }
        public string PlaceGuid { get; set; }
        public string OtherGuid { get; set; }
        public ActNode Fact { get; set; }
        public MetNode()
        {
            Fact = new ActNode();
        }
    }
    public class LivedAtNode
    {
        public string AgentGuid { get; set; }
        public string AgentName { get; set; }
        public string PlaceGuid { get; set; }
        public ActNode Fact { get; set; }
        public LivedAtNode()
        {
            Fact = new ActNode();
        }
    }

    public class TravelledNode
    {
        public string Name { get; set; }
        public string AgentGuid { get; set; }
        public ActNode Fact { get; set; }
        public string FromGuid { get; set; }
        public string ToGuid { get; set; }

        public TravelledNode()
        {
            Fact = new ActNode();
        }
    }
    public class CreatedNode
    {
        public string AgentName { get; set; }
        public string AgentGuid { get; set; }
        public ActNode Fact { get; set; }
        public string ArtworkGuid { get; set; }
        public string ImageUrl { get; set; }
        public string PlaceGuid { get; set; }
        public CreatedNode()
        {
            Fact = new ActNode();
        }
    }
    public class CalledNode
    {
        public string Agent { get; set; }
        public ActNode Fact { get; set; }
        public string Name { get; set; }
        public string CultureCode { get; set; }
    }


}