﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodexMVC.Models
{
    public class ActNode
    {
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int? Hour { get; set; }
        public int? Minute { get; set; }
        public string Milennium { get; set; }
        public string Proximity { get; set; }
        public int? YearEnd { get; set; }
        public int? MonthEnd { get; set; }
        public int? DayEnd { get; set; }
        public int? HourEnd { get; set; }
        public int? MinuteEnd { get; set; }
        public string MilenniumEnd { get; set; }
        public string ProximityEnd { get; set; }
        public List<SelectListItem> Proximities { get; set; }
        public List<SelectListItem> Milennia { get; set; }
        public ActNode()
        {
            Proximities = new List<SelectListItem>
            {
                new SelectListItem { Text = "exactly", Value = "==" },
                new SelectListItem { Text = "around", Value = "c." },
                new SelectListItem { Text = "before", Value = "<" },
                new SelectListItem { Text = "after", Value = ">" },
                new SelectListItem { Text = "on or before", Value = ">=" },
                new SelectListItem { Text = "on or after", Value = "<=" },
            };
        }
    }
}