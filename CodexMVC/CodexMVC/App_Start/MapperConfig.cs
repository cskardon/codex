﻿using AutoMapper;
using CodexMVC.Areas.Admin.Models;
using CodexMVC.Models;
using Data.Entities;

namespace CodexMVC.App_Start
{
    public static class MapperConfig
    {
        public static void RegisterMappers()
        {
            Mapper.CreateMap<EditActModel, Act>().ReverseMap();
            Mapper.CreateMap<DataPointHyperNodeModel, DataPointHyperNode>().ReverseMap();
            // 
        }
    }
}