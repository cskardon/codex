﻿using System.Web.Mvc;
using System.Web.Routing;

namespace CodexMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{guid}",
                defaults: new { controller = "Home", action = "Index", guid = UrlParameter.Optional },
                namespaces: new[] { "CodexMVC.Controllers" }
            );
        }
    }
}