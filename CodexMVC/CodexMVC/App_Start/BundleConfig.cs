﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace CodexMVC.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css/bootstrap").Include(
              "~/Content/bootstrap-3.3.2-dist/css/bootstrap.css",
              "~/Content/bootstrap-3.3.2-dist/css/material.min.css",
              "~/Content/bootstrap-3.3.2-dist/css/material-fullpalette.min.css",
              "~/Content/bootstrap-3.3.2-dist/css/ripples.min.css"
            ));
            bundles.Add(new StyleBundle("~/css").Include(
                "~/Content/slabtext.css",
                "~/Content/justifiedGallery.min.css",
                "~/Content/codex.css",
                "~/Content/star-rating.min.css"
            ));
            bundles.Add(new ScriptBundle("~/js").Include(
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Scripts/knockout-3.3.0.js",
                "~/Scripts/to-markdown.js",
                "~/Scripts/markdown.js",
                "~/Scripts/bootstrap-markdown.js",
                "~/Content/bootstrap-3.3.2-dist/js/ripples.min.js",
                "~/Content/bootstrap-3.3.2-dist/js/material.min.js",
                "~/Scripts/jquery.slabtext.min.js",
                "~/Scripts/jquery.justifiedGallery.min.js",
                "~/Scripts/star-rating.min.js"
            ));
            bundles.Add(new ScriptBundle("~/appJs").Include(
                "~/Scripts/App.Utils.js",
                "~/Scripts/App.API.js",
                "~/Scripts/App.Common.js",
                "~/Scripts/codex.js"
            ));
            BundleTable.EnableOptimizations = true;
        }
    }
}