﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public static class FactExtensions
    {
        public static string ToShortYear(this IDateRange fact)
        {
            var start = ShortYear(fact.Year, fact.Proximity, fact.Milennium);
            return start == "?" ? string.Empty : start;
        }
        public static string ToShortYearRange(this IDateRange fact)
        {
            var start = ShortYear(fact.Year, fact.Proximity, fact.Milennium);
            var end = ShortYear(fact.YearEnd, fact.ProximityEnd, fact.MilenniumEnd);
            return start + "-" + end;
        }
        public static string ToDisplay(this IDateRange fact)
        {
            var start = Start(fact);
            var end = End(fact);
            if (end == null)
            {
                return start;
            }
            return ("from " + start + " to " + end).Replace("in", "");
        }
        private static string GetMonth(int value)
        {
            if (value - 1 >= Months.Count())
            {
                return "???";
            }
            return Months[value - 1];
        }
        public static class Proximity
        {
            public const string Circa = "c.";
            public const string Exactly = "==";
            public const string Before = "<";
            public const string After = ">";
            public const string OnOrBefore = ">=";
            public const string OnOrAfter = "<=";
            public const string None = null;
        }
        public static string ShortYear(int? year, string proximity, string milennium)
        {
            if (false == year.HasValue)
            {
                return "?";
            }
            var sb = new List<string>();
            var prox = "";
            switch (proximity)
            {
                case Proximity.Circa: proximity = "c."; break;
                case Proximity.OnOrBefore:
                case Proximity.Before: proximity = "before"; break;
                case Proximity.OnOrAfter:
                case Proximity.After: proximity = "after"; break;
                case Proximity.Exactly:
                case Proximity.None:
                default: proximity = null; break;
            }
            sb.Add(prox);
            sb.Add(year.Value.ToString());
            if (milennium == "BC")
            {
                sb.Add("BC");
            }
            return string.Join(" ", sb.ToArray());
        }
        public static string Start(IDateRange dateRange)
        {
            var sb = new List<string>();
            if (dateRange.Day.HasValue)
            {
                sb.Add(dateRange.Day.Value + St(dateRange.Day.Value));
            }
            if (dateRange.Month.HasValue)
            {
                if (dateRange.Day.HasValue)
                {
                    sb.Add("of");
                }
                var month = GetMonth(dateRange.Month.Value);
                if (dateRange.Year.HasValue)
                {
                    month += ",";
                }
                sb.Add(month);
            }
            if (dateRange.Year.HasValue)
            {
                sb.Add(dateRange.Year.Value.ToString());
            }
            if (dateRange.Milennium == "BC")
            {
                sb.Add(dateRange.Milennium);
            }
            var notnull = sb.Where(x => false == string.IsNullOrEmpty(x));
            var result = string.Join(" ", notnull.ToArray());
            var prox = FromProximity(dateRange.Proximity);
            if (dateRange.Year.HasValue && dateRange.Day.HasValue && dateRange.Month.HasValue && prox == "in")
            {
                prox = "on the";
            }
            return notnull.Any() ? " " + prox + " " + result : "";
        }
        public static string End(IDateRange fact)
        {
            var sb = new List<string>();
            if (fact.DayEnd.HasValue)
            {
                sb.Add(fact.DayEnd.Value + St(fact.DayEnd.Value));
            }
            if (fact.MonthEnd.HasValue)
            {
                if (fact.DayEnd.HasValue)
                {
                    sb.Add("of");
                }
                var month = GetMonth(fact.Month.Value);
                if (fact.YearEnd.HasValue)
                {
                    month += ",";
                }
                sb.Add(month);
            }
            if (fact.YearEnd.HasValue)
            {
                sb.Add(fact.YearEnd.Value.ToString());
            }
            if (fact.MilenniumEnd == "BC")
            {
                sb.Add(fact.MilenniumEnd);
            }
            var notnull = sb.Where(x => false == string.IsNullOrEmpty(x));
            var result = string.Join(" ", notnull.ToArray());
            var prox = FromProximity(fact.ProximityEnd);
            if (fact.YearEnd.HasValue && fact.DayEnd.HasValue && fact.MonthEnd.HasValue && prox == "in")
            {
                prox = "on the";
            }
            return notnull.Any() ? " " + prox + " " + result : null;
        }
        private static string[] Months = new string[] {
            "January","February","March","April","May","June","July","August","September","October","November","December"
        };
        public static string St(int number)
        {
            var text = number.ToString();
            var last = text.Substring(text.Length - 1, 1);
            if (text.Length >= 2)
            {
                var secondLast = text.Substring(text.Length - 2, 1);
                if (secondLast == "1")
                {
                    return "th";
                }
            }
            switch (last)
            {
                case "1": return "st";
                case "2": return "nd";
                case "3": return "rd";
                default: return "th";
            }
        }
        public static string FromProximity(string proximity)
        {
            switch (proximity)
            {
                case "==": return "in";
                case "<": return "sometime before";
                case ">": return "sometime after";
                case "c.": return "around";
                case "<=": return "on or before";
                case ">=": return "on or after";
                default: return "in";
            }
        }
    }
}
