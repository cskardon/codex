﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public static class Label
    {
        public static string CapitolOf = "CAPITAL_OF";
        public static string SourceOf = "SOURCE_OF";
        public static string OwnedBy = "OWNED_BY";
        public static string LocatedAt = "LOCATED_AT";
        public static string SubjectOf = "SUBJECT_OF";
        public static string ToStudent = "TO_STUDENT";
        public static string AnArtwork = "AN_ARTWORK";
        public static string ApprenticeTo = "APPRENTICE_TO";
        public static string ProducedBy = "PRODUCED_BY";
        // public static string Uses = "USES";
        public static string BelongsTo = "BELONGS_TO";
        public static string ForPatron = "FOR_PATRON";
        public static string WithAgent = "WITH_PERSON";
        public static string MemberOf = "MEMBER_OF";
        public static string ChildOf = "CHILD_OF";
        public static string IsA = "IS_A";
        public static string InFactGroup = "IN_FACT_GROUP";
        public static string IsStuckTo = "IS_STUCK_TO";
        public static string MadeStickyNote = "MADE_STICKY_NOTE";
        public static string Role = "ROLE";
        public static string Models = "MODELS";
        public static string AtPlace = "AT_PLACE";
        public static string FromPlace = "FROM_PLACE";
        public static string ToPlace = "TO_PLACE";
        public static string AgentOf = "AGENT_OF";
        public static string ParticipatedIn = "PARTICIPATED_IN";
        public static string Serves = "SERVES";
        public static string InCapacity = "IN_CAPACITY";
        public static string Knows = "KNOWS";
        public static string Translated = "TRANSLATED";
        public static string KindOf = "KIND_OF";
        public static string ParentOf = "PARENT_OF";
        public static string RivalOf = "RIVAL_OF";
        public static string FriendOF = "FRIEND_OF";
        public static string PartOf = "PART_OF";
        public static string Practised = "PRACTISED";
        public static string AffiliatedWith = "AFFILIATED_WITH";
        public static string HasNationality = "HAS_NATIONALITY";
        public static string GenreOf = "GENRE_OF";
        public static string HasDatabase = "HAS_DATABASE";
        public static string InsideOf = "INSIDE_OF";
        public static string Represents = "REPRESENTS";
        public static string HasAlias = "HAS_ALIAS";
        public static string HasTopic = "HAS_TOPIC";
        public static string AntonymOf = "ANTONYM_OF";
        public static string SynonymOf = "SYNONYM_OF";
        public static string KnownAs = "KNOWN_AS";
        public static string MadeOf = "MADE_OF";
        public static string ImageOf = "IMAGE_OF";
        // entities
        public static string Alias = "Alias";
        public static string Place = "Place";
        public static string Agent = "Agent";
        public static class Relationship
        {
            public const string AuditFrom = "AUDIT_FROM";
            public const string AuditTo = "AUDIT_TO";
            public const string CitizenOf = "CITIZEN_OF";
            public const string Represents = "REPRESENTS";
            public const string RepresentativeOf = "REPRESENTATIVE_OF";
            public const string RepresentedBy = "REPRESENTED_BY";
            public const string InsideOf = "INSIDE_OF";
            public const string IsA = "IS_A";
            public const string ExhibitedIn = "EXHIBITED_IN";
            public const string ExhibitingIn = "EXHIBITING_IN";
            public const string Covers = "COVERS";
            public const string WithAgent = "WITH_AGENT";
            public const string AnArtwork = "AN_ARTWORK";
            public const string TeacherOf = "TEACHER_OF";
            public const string AdversaryOf = "ADVERSARY_OF";
            public const string BenefactorOf = "BENEFACTOR_OF";
            public const string MadeFrom = "MADE_FROM";
            public const string HasSubject = "HAS_SUBJECT";
            public const string HasTag = "HAS_TAG";
            public const string HasValue = "HAS_VALUE";
            public const string ValueSource = "VALUE_SOURCE";
            public const string StatedBy = "STATED_BY";
            public const string StatedIn = "STATED_IN";
            public const string HasSource = "HAS_SOURCE";
            public const string TourFeatures = "TOUR_FEATURES";
            public const string ManagerOf = "MANAGER_OF";
            public const string InRole = "IN_ROLE";
            public const string InfluenceOn = "INFLUENCE_ON";
            public const string InterestedIn = "INTERESTED_IN";
            public const string CollaboratedWith = "COLLABORATED_WITH";
            public const string RelatedAs = "RELATED_AS";
            public const string AtPlace = "AT_PLACE";
            public const string PertainsToAgent = "PERTAINS_TO_AGENT";
            public const string PertainsToArtefact = "PERTAINS_TO_ARTEFACT";
            public const string PertainsToPlace = "PERTAINS_TO_PLACE";
            public const string BornAt = "BORN_AT";
            public const string DiedAt = "DIED_AT";
            public const string StudentOf = "STUDENT_OF";
            public const string StudentAt = "STUDENT_AT";
            public const string TeacherAt = "TEACHER_AT";
            public const string ToStudent = "TO_STUDENT";
            public const string SubjectOf = "SUBJECT_OF";
            public const string FromPlace = "FROM_PLACE";
            public const string ToPlace = "TO_PLACE";
            public const string ByTransport = "BY_TRANSPORT";
            public const string SymbolOf = "SYMBOL_OF";
            public const string SymbolOfPlace = "SYMBOL_OF_PLACE";
            public const string About = "ABOUT";
            public const string StopsIn = "STOPS_IN";
            public const string StopsAt = "STOPS_AT";
            public const string AgentOf = "AGENT_OF";
            public const string Follows = "FOLLOWS";
            public const string Rates = "RATES";
            public const string WrittenBy = "WRITTEN_BY";
            public const string ProducedBy = "PRODUCED_BY";
            public const string SimilarTo = "SIMILAR_TO";
            public const string AspectOf = "ASPECT_OF";
            public const string SpouseOf = "SPOUSE_OF";
            public const string PartnerOf = "PARTNER_OF";
            public const string WorkedFor = "WORKED_FOR";
            public const string PartOf = "PART_OF";
            public const string SubsetOf = "SUBSET_OF";
            public const string StudyFor = "STUDY_FOR";
            public const string CopyOf = "COPY_OF";
            public const string OwnedBy = "OWNED_BY";
            public const string LocatedAt = "LOCATED_AT";
            public const string LocatedIn = "LOCATED_IN";
            public const string TakesPlaceIn = "TAKES_PLACE_IN";
            public const string OntologicalStateOf = "ONTOLOGICAL_STATE_OF";
            public const string Bookmarked = "BOOKMARKED";
            public const string CommissionedBy = "COMMISSIONED_BY";
            public const string WorkedOn = "WORKED_ON";
            public const string PublishedBy = "PUBLISHED_BY";
            public const string PublishedOf = "PUBLISHED_OF";
            public const string MadeWith = "MADE_WITH";
            public const string ScoredFor = "SCORED_FOR";
            public const string PerformedBy = "PERFORMED_BY";
            public const string PerformedIn = "PERFORMED_IN";
            public const string TranscriptionOf = "TRANSCRIPTION_OF";
            public const string TranslatedBy = "TRANSLATED_BY";
            public const string AssociatedWith = "ASSOCIATED_WITH";
            public const string MadeBy = "MADE_BY";
            public const string AttributedTo = "ATTRIBUTED_TO";
            public const string DesignedBy = "DESIGNED_BY";
            public const string AppliesTo = "APPLIES_TO";
            public const string BuriedIn = "BURIED_IN";
            public const string ManagedBy = "MANAGED_BY";
            public const string HasMotif = "HAS_MOTIF";
            public const string HasGenre = "HAS_GENRE";
            public const string HasPhysicalPropertyOf = "HAS_PHYSICAL_PROPERTY_OF";
            public const string KindOf = "KIND_OF";
            public const string OppositeOf = "OPPOSITE_OF";
            public const string Knows = "KNOWS";
            public const string FriendOf = "FRIEND_OF";
            public const string ChildOf = "CHILD_OF";
            public const string HasContext = "HAS_CONTEXT";
            public const string HasDetail = "HAS_DETAIL";
            public const string ContextFor = "CONTEXT_FOR";
            public const string ContextAt = "CONTEXT_AT";
            public const string RefersTo = "REFERS_TO";
            public const string SharedWith = "SHARED_WITH";
            public const string HasDay = "HAS_DAY";
            public const string HasItinerary = "HAS_ITINERARY";
            public const string Saw = "SAW";
            public const string Uses = "USES";
            public const string HasRelation = "HAS_RELATION";
            public const string QualifiedBy = "QUALIFIED_BY";
            public const string RelatedTo = "RELATED_TO";
            public const string Recognisability = "RECOGNISABILITY";
            public const string HasRole = "HAS_ROLE";
            public const string HasPermission = "HAS_PERMISSION";
            public const string AssertedBy = "ASSERTED_BY";
            public const string CitedIn = "CITED_IN";
            public const string Mentions = "MENTIONS";
            public const string MemberOf = "MEMBER_OF";
            public const string HasImage = "HAS_IMAGE";
            public const string HasAttachment = "HAS_ATTACHMENT";
            public const string HasVideo = "HAS_VIDEO";
            public const string HasWebsite = "HAS_WEBSITE";
            public static string ToPresentTense(string label, bool reverse = false)
            {
                switch (label)
                {
                    case IsA: return "becomes a";
                    case Knows: return "becomes acquainted with";
                    case OwnedBy: return reverse ? "buys" : "is purchased by";
                    case BornAt: return reverse ? "is the birth place of" : "is born in";
                    case DiedAt: return reverse ? "is the death place of" : "dies in";
                    case CommissionedBy: return reverse ? "commissions" : "is commissioned by";
                    case MadeBy: return reverse ? "works on" : "is created by";
                    case FriendOf: return "becomes friends with";
                    case AdversaryOf: return "becomes an enemy of";
                    case BenefactorOf: return reverse ? "is patronised by" : "becomes the patron of";
                    case StudentAt: return reverse ? "is the school of" : "studied at";
                    case StudentOf: return reverse ? "becomes the teacher of" : "becomes a student of";
                    case PartnerOf: return "becomes the romantic partner of";
                    case WorkedFor: return reverse ? "employs" : "works for";
                    case TeacherOf: return reverse ? "becomes a student of" : "becomes the teacher of";
                    case SpouseOf: return "marries";
                    case CollaboratedWith: return "collaborated with";
                    default: return label;
                }
            }
            public static string ToEnglish(string label, Agent agent)
            {
                switch (label)
                {
                    case Knows: return "knew";
                    case RelatedTo: return "was related to";
                    case AdversaryOf: return "was an enemy of";
                    case AssociatedWith: return "was associated with";
                    case PartOf: return "belonged to";
                    case FriendOf: return "was a friend of";
                    case BenefactorOf: return "was a patron of";
                    case StudentOf: return "was a student of";
                    case PartnerOf: return "was a romantic partner of";
                    case InfluenceOn: return "influenced";
                    case ManagedBy: return "was managed by";
                    case ManagerOf: return "was the manager of";
                    case WorkedFor: return "worked for";
                    case MemberOf: return "was a member of";
                    case StudentAt: return "studied at";
                    case TeacherOf: return "taught";
                    case ChildOf:
                        {
                            if (agent.Sex.HasValue)
                            {
                                return agent.Sex == Sex.Male ? "was the son of" : "was the daughter of";
                            }
                            return "was a child of";
                        }
                    case SpouseOf:
                        {
                            if (agent.Sex.HasValue)
                            {
                                return agent.Sex == Sex.Male ? "was the husband of" : "was the wife of";
                            }
                            return "was the spouse of";
                        }

                    default: return label;
                }
            }
            public static string ToEnglish(string label, bool reverse = false)
            {
                switch (label)
                {
                    case IsA: return "is a";
                    case Knows: return "knows";
                    case OwnedBy: return reverse ? "owns" : "is owned by";
                    case RepresentativeOf: return "is a representative of";
                    case BornAt: return reverse ? "is the birth place of" : "was born in";
                    case DiedAt: return reverse ? "is the death place of" : "died in";
                    case RelatedAs: return "related as";
                    case RelatedTo: return "is related to";
                    case CommissionedBy: return reverse ? "commissioned" : "was commissioned by";
                    case LocatedAt: return reverse ? "is the location of" : "is in";
                    case LocatedIn: return reverse ? "is the location of" : "is in";
                    case CitizenOf: return reverse ? "is the homeland of" : "is a citizen of";
                    case AdversaryOf: return "adversary of";
                    case MadeBy: return reverse ? "made" : "made by";
                    case Represents: return reverse ? "is represented in" : "represents";
                    case Recognisability: return reverse ? "is famous for" : "is famous for";
                    case AssociatedWith: return "is associated with";
                    case PartOf: return "is part of";
                    case FriendOf: return "friend of";
                    case BenefactorOf: return reverse ? "is patronised by" : "is a patron of";
                    case StudentOf: return reverse ? "is a teacher of" : "is a student of";
                    case PartnerOf: return "is a romantic partner of";
                    case InfluenceOn: return reverse ? "was influenced by" : "influenced";
                    case WorkedFor: return reverse ? "employed" : "worked for";
                    case TeacherOf: return reverse ? "is a student of" : "is a teacher of";
                    case ChildOf: return reverse ? "is the parent of" : "is a child of";
                    case SpouseOf: return "is the spouse of";
                    case SymbolOf:
                    case SymbolOfPlace: return "symbolises";
                    default: return label;
                }
            }
        }
        public static class Entity
        {
            public static string Text = "Text";
            public static string Alias = "Alias";
            public static string Event = "Event";
            public static string Category = "Category";
            public static string Place = "Place";
            public static string Audit = "Audit";
            public static string ErrorLog = "ErrorLog";
            public static string Agent = "Agent";
            public static string Context = "Context";
            public static string Artefact = "Artefact";
            public static string Day = "Day";
            public static string Detail = "Detail";
            public static string EventDetail = "EventDetail";
            public static string Act = "Act";
            public static string Video = "Video";
            public static string Tag = "Tag";
            public static string TagValue = "TagValue";
            public static string Document = "Document";
            public static string Relation = "Relation";
            public static string Recognisability = "RECOGNISABILITY";
            public static string FactGroup = "FactGroup";
            public static string StickyNote = "StickyNote";
            public static string Topic = "Topic";
            public static string User = "User";
            public static string Itinerary = "Itinerary";
            public static string Role = "Role";
            public static string Permission = "Permission";
            public static string Organisation = "Organisation";
            public static string Movement = "Movement";
            public static string Style = "Style";
            public static string Genre = "Genre";
            public static string Database = "Database";
            public static string DataSet = "DataSet";
            public static string DataPoint = "DataPoint";
            public static string Image = "Image";
            public static string Website = "Website";

            //public static string Document = "Document";
        }
        public static class Act
        {
            public static string WorkedOn = "WorkedOn";
            public static string Served = "Served";
            public static string LivedAt = "LivedAt";
        }
        public static class Roles
        {
            public static string God = "God";
            public static string ContentEditor = "ContentEditor";
            public static string PublicUser = "PublicUser";
        }
        public static class Categories
        {
            public static string ArtistMotif = "Artist Motif";
            public static string Motif = "Motif";
            public static string Born = "Born";
            public static string Died = "Died";
            public static string Exhibition = "Exhibition";
            public static string Tour = "Tour";
            public static string Landmark = "Landmark";
            public static string Interest = "Interest";
            public static string UniversalInterest = "UniversalInterest";
            public static string FamilialRelation = "Familial Relation";
            public static string OntologicalState = "Ontological State";
            public static string Painting = "Painting";
            public static string PhysicalProperty = "Physical Property";
            public static string MusicalInstrument = "Musical Instrument";
            public static string Implement = "Implement";
            public static string Sculpture = "Sculpture";
            public static string AestheticConvention = "Aesthetic Convention";
            public static string Architecture = "Architecture";
            public static string Reference = "Reference";
            public static string Artist = "Artist";
            public static string Writer = "Writer";
            public static string Publisher = "Publisher";
            public static string Publication = "Publication";
            public static string ArtMovement = "Art Movement";
            public static string ArtisticGenre = "Artistic Genre";
            public static string ArtisticSubject = "Artistic Subject";
            public static string Collection = "Collection";
            public static string Verb = "Verb";
            public static string Created = "Created";
            public static string Taught = "Taught";
            public static string TravelledTo = "TravelledTo";
            public static string Technique = "Technique";
            public static string Creation = "Creation";
            public static string Artwork = "Artwork";
            public static string Substance = "Substance";
            //public static string Implement = "Implement";
            public static string WorkedFor = "WorkedFor";
            public static string Served = "Served";
            public static string Met = "Met";
            public static string Creating = "Creating";
        }
    }
}
