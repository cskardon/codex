﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class DocumentHyperNode
    {
        public Agent MadeBy { get; set; }
        public Document Document { get; set; }
        public IEnumerable<IEntity> Entities { get; set; }
        public IEnumerable<Category> MadeByTypes { get; set; }
        public Place Citizenship { get; set; }
        public int? Birth { get; set; }
        public int? Death { get; set; }
        public string BirthDeath
        {
            get
            {
                if (false == Birth.HasValue && false == Death.HasValue)
                {
                    return null;
                }
                var birth = Birth.HasValue ? Birth.Value.ToString() : "?";
                var death = Death.HasValue ? Death.Value.ToString() : "?";
                return string.Format("({0}-{1})", birth, death);
            }
        }
        public string MadeByDesc
        {
            get
            {
                var results = new List<string>();
                var citizenship = Citizenship != null ? Citizenship.Adjective + " " : "";
                if (MadeByTypes.Any())
                {
                    results.AddRange(MadeByTypes.Select(x => x.Name.ToLower()));
                }
                else
                {
                    results.Add("person");
                }
                return citizenship + string.Join(", ", results);
            }
        }
        public string SelectListItemText
        {
            get
            {
                var result = MadeBy.FullName2 + ":  \"" + (Document.Name ?? "Untitled") + "\"";
                if (Document.Year.HasValue)
                {
                    result += " (" + Document.Year.Value.ToString() + ")";
                }
                return result;
            }
        }
        public string ToHtml()
        {
            if (Document == null || Document.Description == null)
            {
                return null;
            }
            var text = Document.Description;
            var result = text;
            foreach (var entity in Entities.Where(x => x != null))
            {
                var name = entity is Agent ? ((Agent)entity).FullName2 : entity is Artefact ? ((Artefact)entity).ToDisplay() : entity.Name;
                var type = entity.GetType().Name;
                var guid = entity.Guid;
                var partPattern = @"\[" + type + @"\|" + guid + @"\]";
                var fullPattern = @"\[" + type + @"\|" + guid + @"(\|.*?)\]";
                if (Regex.IsMatch(result, partPattern))
                {
                    var link = "<a href=\"/" + type + "/ViewDetail?guid=" + guid + "\">" + name + "</a>";
                    result = Regex.Replace(result, partPattern, link);
                }
                else
                {
                    var matches = Regex.Matches(result, fullPattern);
                    if (matches.Count > 0)
                    {
                        var element = matches[0].Value;
                        var lastIndex = element.LastIndexOf("|");
                        if (lastIndex > 0)
                        {
                            var customName = element.Substring(lastIndex + 1).Replace("]", "");
                            var link = "<a href=\"/" + type + "/ViewDetail?guid=" + guid + "\" title=\"" + name + "\">" + customName + "</a>";
                            result = Regex.Replace(result, fullPattern, link);

                        }
                    }
                }

            }
            return result;
        }
    }
    public class Document : Entity, IEntity, IDateRange
    {
        public string Milennium { get; set; }
        public string Proximity { get; set; }
        public int? Year { get; set; }
        public bool Decade { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int? Hour { get; set; }
        public int? Minute { get; set; }
        public string MilenniumEnd { get; set; }
        public string ProximityEnd { get; set; }
        public int? YearEnd { get; set; }
        public bool DecadeEnd { get; set; }
        public int? MonthEnd { get; set; }
        public int? DayEnd { get; set; }
        public int? HourEnd { get; set; }
        public int? MinuteEnd { get; set; }
        public Document()
        {
            Label = "Document";
        }

    }
}
