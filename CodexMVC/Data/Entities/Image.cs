﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Image : Resource
    {
        private readonly string FileUploadPath = ConfigurationManager.AppSettings["FileUploadPath"];
        public int? Height { get; set; }
        public int? Width { get; set; }
        public int? FileSizeBytes { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public string VirtualPath()
        {
            //if (false == string.IsNullOrEmpty(Uri))
            //{
            //    return Uri;
            //}
            return FileUploadPath + "/" + Guid + "." + Extension;
        }
        public Image()
        {
            Label = "Image";
        }
    }
}
