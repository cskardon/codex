﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public interface IGeo
    {
        decimal? Latitude { get; set; }
        decimal? Longitude { get; set; }
        string Name { get; set; }
    }

    public class Place : Entity, IGeo
    {
        public string Adjective { get; set; }
        public decimal? Latitude { get; set; }
        public decimal? Longitude { get; set; }
        public int? TimeZoneOffset { get; set; }
        public Place()
        {
            Label = "Place";
        }
        public string ToDisplay()
        {
            if (false == string.IsNullOrEmpty(Article))
            {
                return Article + " " + Name;
            }
            return Name;
        }
    }
}
