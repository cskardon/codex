﻿using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public interface IShortDateRange
    {
        int? Year { get; set; }
        int? Month { get; set; }
        int? Day { get; set; }
        int? YearEnd { get; set; }
        int? MonthEnd { get; set; }
        int? DayEnd { get; set; }
    }

    public interface IDateRange : IShortDateRange
    {
        string Milennium { get; set; }
        string Proximity { get; set; }
        int? Year { get; set; }
        bool Decade { get; set; }
        int? Month { get; set; }
        int? Day { get; set; }
        int? Hour { get; set; }
        int? Minute { get; set; }
        string MilenniumEnd { get; set; }
        string ProximityEnd { get; set; }
        int? YearEnd { get; set; }
        bool DecadeEnd { get; set; }
        int? MonthEnd { get; set; }
        int? DayEnd { get; set; }
        int? HourEnd { get; set; }
        int? MinuteEnd { get; set; }
    }
    public class Topic : Entity
    {

    }
}
