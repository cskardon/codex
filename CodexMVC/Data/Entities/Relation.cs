﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Relation : Entity, IEntity, IDateRange
    {
        /// <summary>
        /// between, continuous
        /// </summary>
        public string Range { get; set; }
        public string Milennium { get; set; }
        public string Proximity { get; set; }
        public int? Year { get; set; }
        public bool Decade { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int? Hour { get; set; }
        public int? Minute { get; set; }
        public string MilenniumEnd { get; set; }
        public string ProximityEnd { get; set; }
        public int? YearEnd { get; set; }
        public bool DecadeEnd { get; set; }
        public int? MonthEnd { get; set; }
        public int? DayEnd { get; set; }
        public int? HourEnd { get; set; }
        public int? MinuteEnd { get; set; }
        public bool Primary { get; set; }
        public bool? IsCurrent { get; set; }
        public Relation()
        {
            Label = "Relation";
        }
    }
}
