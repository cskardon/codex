﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class User : Entity
    {
        public DateTimeOffset? PasswordResetStartWindow { get; set; }

        /// <summary>
        /// Guid
        /// </summary>
        public string PasswordResetToken { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Username { get; set; }

        /// <summary>
        /// Hashed and salted
        /// </summary>
        public string Password { get; set; }

        public string Salt { get; set; }

        public int? Age { get; set; }

        public string Profession { get; set; }

        public User()
        {
            Label = "User";
        }
    }
}
