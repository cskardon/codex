﻿using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Audit
    {
        public string Guid { get; set; }
        public string IPAddress { get; set; }
        public DateTimeOffset Date { get; set; }
        public AuditAction Action { get; set; }
        public string Description { get; set; }
    }
    public interface IOrdinal
    {
        int? Ordinal { get; set; }
    }
    public interface IIdentifiable
    {
        string Guid { get; set; }
    }
    public interface ICreated
    {
        DateTimeOffset? Created { get; set; }
    }
    public interface IBaseEntity : IIdentifiable, ICreated
    {
        string Name { get; set; }
    }
    public interface IEntity : IBaseEntity
    {
        DateTimeOffset? Created { get; set; }

        /// <summary>
        /// Optional override of the default, which just suffixes 'Name' with an 's'.
        /// </summary>
        string Plural { get; set; }

        /// <summary>
        /// Optional override of the default, which is the personal pronoun - e.g., if adding 'The Uffizi'
        /// the Name would be 'Uffizi' and the Article would be 'the'.
        /// </summary>
        string Article { get; set; }

        /// <summary>
        /// Optional description of the entity, of any length.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// Optional summary of entity - e.g., Leonardo da Vinci's subtitle could be 'Italian Renaissance artist and polymath'.
        /// </summary>
        string Subtitle { get; set; }

        string Label { get; set; }

        decimal? Rating { get; set; }

        string UrlFriendlyName { get; set; }

        bool? QuickAdded { get; set; }
    }    
    public class Entity : IEntity
    {
        public static string Type = "Entity";
        public string Guid { get; set; }
        public string Name { get; set; }
        public string Plural { get; set; }
        public string Article { get; set; }
        public string Description { get; set; }
        public string Subtitle { get; set; }
        public string Label { get; set; }
        public decimal? Rating { get; set; }
        public string[] Aliases { get; set; }
        public DateTimeOffset? Created { get; set; }
        public string UrlFriendlyName { get; set; }
        public bool? QuickAdded { get; set; }
        public bool IsNew()
        {
            if (false == Created.HasValue)
            {
                return false;
            }
            var now = DateTimeOffset.Now;
            return (now - Created.Value).TotalDays <= 1;
        }
        public string PluralName
        {
            get
            {
                if (string.IsNullOrEmpty(Plural))
                {
                    if (string.IsNullOrEmpty(Name))
                    {
                        return null;
                    }
                    var last = Name.Substring(Name.Length - 1, 1);
                    if (last == "o")
                    {
                        return Name;
                    }
                    if (last == "s" || last == "h")
                    {
                        return Name + "es";
                    }
                    return Name + "s";
                }
                return Plural;
            }
        }
    }
}
