﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class DataPoint : IIdentifiable, ICreated
    {
        public string Guid { get; set; }

        public string Value { get; set; }

        public string Description { get; set; }

        public DateTimeOffset? Created { get; set; }

        /// <summary>
        /// Description of where the fact was stated. If 'StatedInGuid' is supplied the 'Citation' will be a reference within
        /// the book, but if there is no 'StatedInGuid' it is just a free-form text reference to the source.
        /// </summary>
        public string Citation { get; set; }

        /// <summary>
        /// AD; BC; default is AD
        /// </summary>
        public string Milennium { get; set; }

        /// <summary>
        /// The minimal time reference.
        /// </summary>
        public int Year { get; set; }

        public int? Month { get; set; }

        public int? Day { get; set; }

        public int? Hour { get; set; }

        public int? Minute { get; set; }

        public DateTime GetDateTime()
        {
            var date = new DateTime(Year, Month ?? 1, Day ?? 1, Hour ?? 0, Minute ?? 0, 0);
            return date;
        }
    }
    public class DataSet : IIdentifiable
    {
        public string Guid { get; set; }

        public string Name { get; set; }

        public string UnitOfMeasure { get; set; }

        public string UnitOfMeasurePlural { get; set; }

        public string UnitOfMeasureType { get; set; }

        public string Description { get; set; }

        public DateTimeOffset? Created { get; set; }
    }
    public class DataSetHyperNode
    {
        public DataSet DataSet { get; set; }

        public string SubsetOfGuid { get; set; }
        public DataSet Parent { get; set; }

        public string AboutGuid { get; set; }
        public Tag About { get; set; }

        public IEnumerable<DataPoint> DataPoints { get; set; }

        public string SourceGuid { get; set; }
        public Artefact Source { get; set; }
    }
    public class DataPointSet
    {
        public DataSetPlace DataSetPlace { get; set; }
        public DataSet DataSet { get; set; }
    }

    public class DataSetPlaceHyperNode
    {
        public DataSet DataSet { get; set; }

        public string SubsetOfGuid { get; set; }
        public DataSet Parent { get; set; }

        public string AboutGuid { get; set; }
        public Tag About { get; set; }

        public IEnumerable<DataSetPlace> DataSetPlaces { get; set; }

        public string SourceGuid { get; set; }
        public Artefact Source { get; set; }
    }
    public class DataSetsContainer
    {
        public DataSetPlaceHyperNode Root { get; set; }
        public List<DataSetPlaceHyperNode> DataSets { get; set; }
    }
    public class DataSetPlace
    {
        public DataPoint DataPoint { get; set; }

        public Place At { get; set; }

        public Act Act { get; set; }

        public IEnumerable<DataSet> DataSets { get; set; }
    }
    public class DataPointHyperNode
    {
        public bool IsAdding()
        {
            return DataPoint.Guid == null;
        }

        public DataPoint DataPoint { get; set; }

        //public string PartOfGuid { get; set; }
        //public DataSet DataSet { get; set; }

        //public IEnumerable<DataSet> RelatedDataSets { get; set; }

        public IEnumerable<DataSet> DataSets { get; set; }

        public string AtGuid { get; set; }
        public Place At { get; set; }

        public string HasSourceGuid { get; set; }
        public Act Act { get; set; }
    }
}
