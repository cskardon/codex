﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public enum Importance
    {
        Big,
        Medium,
        Small
    }
    public enum Significance
    {
        Historic,
        Personal,
        Minutiea
    }
    public enum HistoricalSource
    {
        Primary,
        Supposition
    }
    
    public class Act : Entity, IEntity, IDateRange
    {
        public string Source { get; set; }
        public Importance Importance { get; set; }
        public Significance Significance { get; set; }
        public HistoricalSource HistoricalSource { get; set; }
        public bool Experienceable { get; set; }
        public string Milennium { get; set; }
        public string Proximity { get; set; }
        public int? Year { get; set; }
        public bool Decade { get; set; }
        public int? Month { get; set; }
        public int? Day { get; set; }
        public int? Hour { get; set; }
        public int? Minute { get; set; }
        public string MilenniumEnd { get; set; }
        public string ProximityEnd { get; set; }
        public int? YearEnd { get; set; }
        public bool DecadeEnd { get; set; }
        public int? MonthEnd { get; set; }
        public int? DayEnd { get; set; }
        public int? HourEnd { get; set; }
        public int? MinuteEnd { get; set; }
        public bool Container { get; set; }
        public Act()
        {
            Label = "Act";
            Significance = Entities.Significance.Personal;
            Importance = Entities.Importance.Medium;
        }
        public string TimelineDate()
        {
            var result = new List<string>();
            result.Add(Year.Value.ToString());
            if (false == Month.HasValue)
            {
                return string.Join("/", result.ToArray());
            }
            result.Add(Month.Value.ToString());
            if (false == Day.HasValue)
            {
                return string.Join("/", result.ToArray());
            }
            result.Add(Day.Value.ToString());
            return string.Join("/", result.ToArray());
        }
    }
}
