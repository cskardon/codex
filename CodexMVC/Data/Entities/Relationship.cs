﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class Relationship
    {
        public string Type { get; set; }
        public string Guid { get; set; }
        public bool IsActive { get; set; }
        public int Rank { get; set; }
    }
}
