﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public enum AuditAction
    {
        Create,
        Modify,
        Delete,
        LogIn,
        LogOut,
        Other
    }
    public enum Sex
    {
        Male,
        Female,
        None,
        Other
    }
    public class Agent : Entity, IEntity
    {
        public Sex? Sex { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? BirthYear { get; set; }
        public int? DeathYear { get; set; }
        public string FullName
        {
            get
            {
                if (Name != null)
                {
                    return Name;
                }
                return LastName + ", " + FirstName;
            }
        }
        public string FullName2
        {
            get
            {
                if (Name != null)
                {
                    return Name;
                }
                return FirstName + " " + LastName;
            }
        }
        public Agent()
        {
            Label = Data.Entities.Label.Agent;
        }
    }
}
