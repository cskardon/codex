﻿using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    // test
    public class RefersToPayload
    {
        public string Guid { get; set; }
        public bool IsActive { get; set; }
    }
    // test
    public class RefersTo : Relationship<RefersToPayload>, IRelationshipAllowingSourceNode<Artefact>, IRelationshipAllowingTargetNode<Act>
    {
        public RefersTo() : base(-1, null) { }
        public RefersTo(NodeReference targetNode, RefersToPayload data) : base(targetNode, data) { }

        public override string RelationshipTypeKey
        {
            get { return Label.Relationship.RefersTo; }
        }
    }
}
